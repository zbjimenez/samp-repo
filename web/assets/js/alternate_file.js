var fileHandlerControl = {
	init: function() {
		$( ".btn-add-item" ).on( "click", function() {
			fileHandlerControl.addEmptyRow();
		});
		
		$( ".table-collection" ).on( "click", "a.delete-btn", function() {
			if ( confirm( "Delete record?" ) ) {
				fileHandlerControl.deleteRow( $( this ) );
			}
			return false;
		});
		
		$( ".table-collection tbody > tr .file-input" ).pixelFileInput({ placeholder: 'No file selected...' });
	},
	
	addEmptyRow: function() {
		var current = $( ".table-collection tr:last" ).data( "key" );
		var rowCount = !isNaN( current ) ? current+1 : 0;
    	var $newRow = $( $.trim( itemPrototype.replace( /__name__/g, rowCount ) ) );
    	$( ".table-collection" ).append( $newRow );
    	$newRow.attr( "data-key", rowCount );
    	$newRow.find( ".file-input" ).pixelFileInput({ placeholder: 'No file selected...' });
	},
	
	deleteRow: function( $btn ) {
		$btn.closest( "tr" ).remove();
	}
}

$( document ).ready( function() {
	fileHandlerControl.init();
});