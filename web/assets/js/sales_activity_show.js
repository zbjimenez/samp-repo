var sasControl = {
	picPreviewRequest : null,
	
	init: function() {
		$( ".file-table" ).on( "click", "a.preview", function() {
			$( ".pic-preview-modal #picPreviewName" ).html( $( this ).closest( "tr" ).find( "span.file-name" ).html() );
			sasControl.getPicPreview( $( this ) );
		});
		
		$( ".pic-preview-modal" ).on( "hidden.bs.modal", function() {
			$( ".pic-preview-modal #picPreviewName" ).html( "" );
			$( ".pic-preview-modal .modal-body" ).html( "" );
		});
	},
	
	getPicPreview: function( $btn ) {
		if ( sasControl.picPreviewRequest ) {
			sasControl.picPreviewRequest.abort();
			sasControl.picPreviewRequest = null;
		}
		
		$modalContent = $( ".pic-preview-modal .modal-body" );
		$modalContent.append( "<div id='loader'></div>" );
		$unavailableText = "<h3 id='preview-unavailable' class='text-muted'>No Preview Available.</h3>";
		
		var cl = new CanvasLoader( "loader" );
		cl.setDiameter(96);
		cl.setDensity(94);
		cl.setFPS(60);
		cl.show();
		 
		if ( $btn.data( "field" ) ) {
			sasControl.picPreviewRequest = $.getJSON( app.base_uri + '/admin/sales-activity/' + $btn.data( "field" ) + '/preview', function( data ) {
				cl.hide();
				if ( data !== undefined && !$.isEmptyObject( data ) ) {
					$modalContent.append( data );
				} else {
					$modalContent.append( $unavailableText );
				}
			}).done( function() {
				sasControl.picPreviewRequest = null;
			}).fail(function() {
				cl.hide();
				$modalContent.append( $unavailableText );
				sasControl.picPreviewRequest = null;
			});
		}
	},
}

$( document ).ready( function() {
	sasControl.init();
});