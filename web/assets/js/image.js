var imageControl = {
	init: function() {
		$( ".image-input" ).pixelFileInput({ placeholder: "No file selected..." });
		
		$( ".image-thumbnail" ).on( "click", ".delete-image-btn", function() {
			$( ".image-thumbnail" ).fadeOut( "slow", function() {
				$( ".image-input-container" ).removeClass( "hide" ).fadeIn( "slow" );
			});
			$( ".delete-image" ).val( 1 );
		});
	}	
};

$( document ).ready( function() {
	imageControl.init();
});