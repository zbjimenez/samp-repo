function SearchControl() {
	this.results = [];
	this.selectedItems = [];
	this.unique = false;
}

SearchControl.prototype.addItem = function( id ) {
	id = parseInt( id );
	if ( $.inArray( id, this.selectedItems ) == -1 ) {
		this.selectedItems.push( id );
		return true;
	}
	
	return false;
}

SearchControl.prototype.defaultFor = function( arg, val ) {
	return typeof arg !== 'undefined' ? arg : val;
}

SearchControl.prototype.removeItem = function( id ) {
	id = parseInt( id );
	index = $.inArray( id, this.selectedItems );
	if ( index != -1 ) {
		this.selectedItems.splice( index, 1 );
		return true;
	}
	
	return false;
}

SearchControl.prototype.init = function( $formElem, $resultElem, $unique ) {
	var that = this;
	var $searchForm = $formElem;
	var $searchResult = $resultElem;
	
	if ( !$searchForm ) {
		$searchForm = $( "#search-form" );
	}
	
	if ( !$searchResult ) {
		$searchResult = $searchForm.find( "#search-result" );
	}
	
	if ( this.defaultFor( $unique, false ) ) {
		this.unique = $unique;
	}
	
	$searchForm.ajaxForm({
		beforeSend: function() {
			$searchForm.find( "button[type=submit]" ).button( "loading" );
		},
		
		data: {'miscIds' : that.selectedItems},
		
		success: function( data ) {
			that.results = data.results;
			
			$searchForm.find( "button[type=submit]" ).button( "reset" );
			$searchResult.show();
			$searchResult.find( ".items tbody" ).empty();
			$searchResult.find( ".result" ).hide();
			$searchResult.find( ".no-result" ).hide();
			
			var $cell;
			if ( !$.isEmptyObject( data.results ) ) {
				$.each( data.results, function( key, value ) {
					var rowClass = "success";
					if ( $.inArray( parseInt( value.id ), that.selectedItems ) == -1 ) {
						rowClass = '';
					}
					
					$cell = $( "<td>" + value.name + "</td>" );
					$searchResult.find( ".items tbody" ).append( $( "<tr data-id='" + value.id + "' class='" + rowClass + "'>" ).append( $cell ) );
				});
				
				var result = " Result";
				$searchResult.find( ".result" ).html( data.length == 1 ? "1" + result : data.length + result + "s" ).show();
				$searchResult.find( ".no-result" ).hide();
			} else {
				$searchResult.find( ".no-result" ).removeClass( "hide" ).show();
			}
		}
	});
}

function AutocompleteControl() {
	this.name = null;
	this.label = null;
	this.path = null;
	this.unique = false;
	this.selectedItems = [];
}

AutocompleteControl.prototype.defaultFor = function( arg, val ) {
	return typeof arg !== 'undefined' ? arg : val;
}

AutocompleteControl.prototype.addItem = function( id ) {
	id = parseInt( id );
	if ( $.inArray( id, this.selectedItems ) == -1 ) {
		this.selectedItems.push( id );
		return true;
	}
	
	return false;
}

AutocompleteControl.prototype.removeItem = function( id ) {
	id = parseInt( id );
	index = $.inArray( id, this.selectedItems );
	if ( index != -1 ) {
		this.selectedItems.splice( index, 1 );
		return true;
	}
	
	return false;
}

AutocompleteControl.prototype.setOptions = function( optionsObj ) {
	this.label = optionsObj.label;
	
	if ( optionsObj.name ) {
		this.name = optionsObj.name;
	} else {
		this.name = optionsObj.label;
	}
	
	if ( this.defaultFor( optionsObj.full_path, false ) ) {
		this.path = optionsObj.path;
	} else {
		this.path = app.base_uri + "/admin/" + optionsObj.path + "/search?query=%QUERY";
	}
	
	if ( this.defaultFor( optionsObj.unique, false ) ) {
		this.unique = optionsObj.unique;
	}
}

AutocompleteControl.prototype.initializeTypeahead = function( $row, updaterCallback ) {
	var that = this;
	var bloodhound = new Bloodhound({
		datumTokenizer: function ( datum ) {
			return Bloodhound.tokenizers.whitespace( datum.id );
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: that.path,
			ajax: {
				type: "POST",
				data: { 
					"typeahead" : 1
				},
				beforeSend: function() {
					$row.find( ".typeahead" ).addClass( "autocomplete-loading" );
				},
				
				complete: function() {
					$row.find( ".typeahead" ).removeClass( "autocomplete-loading" );
				}
			},
			filter: function( data ) {
				if ( that.unique == true ) {
					return $.map( data.results, function( result ) {
						if ( $.inArray( result.id, that.selectedItems ) == -1 ) {
							return result;
						}
					});
				} else {
					return $.map( data.results, function( result ) {
						return result;
					});
				}
			}
		}
	});
	
	bloodhound.initialize();
	
	$row.find( ".typeahead" ).typeahead({
		hint: true,
		highlight: true,
		minLength: 3
	},
	{
		displayKey: "id",
		name: that.name,
		source: bloodhound.ttAdapter(),
		templates: {
			empty: [
				"<div class='empty-message'>",
				"<em>Unable to find any " + that.label + " that match the given query.</em>",
				"</div>"
			].join( "\n" ),
			suggestion: function( result ) {
				return "<p>" + result.label + "</p>";
			}
		}
	}).on( "typeahead:selected", function( $e, result ) {
		updaterCallback( $row, result );
	}).on( "typeahead:cursorchanged", function( $e, result ) {
		var elem = $e.target;
		$( elem ).val( result.label );
	});
}