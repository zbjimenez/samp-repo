var quotationControl = {
	init: function() {
		$('#checkbox-all').change(function() {
			$('.bulk-checkbox').prop('checked', $('#checkbox-all').is(":checked"));
		});

		$('.bulk-checkbox, #checkbox-all').change(function() {
			var numChecked = $(':checked.bulk-checkbox').length;
			if (numChecked > 0) {
				$('#bulk-indicator').text(numChecked + ' selected ');
				$('#bulk-actions').removeClass('hidden');
				$('#bulk-actions').fadeIn(500);
			} else {
				$('#bulk-indicator').text('');
				$('#bulk-actions').fadeOut(500);
			}
		});

		$('#bulk-action-approve').click(function() {
			quotationControl.changeStatusMultiple($('#bulk-action-approve').attr('data-status'));
		});

		if (sessionStorage.lastBulkActionResult) {
			quotationControl.handleBulkResults(JSON.parse(sessionStorage.lastBulkActionResult));
			sessionStorage.removeItem("lastBulkActionResult");
		}
	},

	changeStatusMultiple: function(status) {
		var ids = [];
		$(':checked.bulk-checkbox').each(function () {
			ids.push($(this).attr('data-quotation-id'));
		});

		$.post(app.base_uri + "/admin/quotation/change-status-multiple",
		{
			status: status,
			ids: ids,
		},
		function(data, status){
			if (status == 'success') {
				if (typeof(Storage) !== "undefined") {
					sessionStorage.lastBulkActionResult = JSON.stringify(data);
				}
				location.reload();
			} else {
				$('#jq-growl-error').click(function () {
					$.growl.error({ message: "An unknown error has occured...please try again." });
				});
			}
		});
	},

	handleBulkResults: function(results) {
		$('.quotation-tooltip').each(function () {
			for (ii in results) {
				if (results[ii].id == $(this).attr('data-quotation-id')) {
					if (results[ii].success == false) {
						$(this).removeClass('hidden');
						$(this).attr('title', results[ii].message);
					}
					break;
				}
			}
		});
		$('.quotation-tooltip').tooltip();
	}
}

$( document ).ready( function() {
	quotationControl.init();
});