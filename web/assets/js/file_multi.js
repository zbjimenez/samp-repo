var fileMultiControl = {
	init: function( $dropzoneElem, url, previewTemplate, type, id ) {
		if ( $dropzoneElem && url && previewTemplate && type && id ) {
			$dropzoneElem.dropzone({
				url: url,
				paramName: "file", // The name that will be used to transfer the file
				maxFilesize: 10, // MB
		 		//acceptedFiles: ".jpg,.jpeg,.gif,.png",

				addRemoveLinks : false,
				dictResponseError: "Can't upload file!",
				autoProcessQueue: true,
				thumbnailWidth: 138,
				thumbnailHeight: 120,

				previewTemplate: previewTemplate,

				resize: function(file) {
					var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
						srcRatio = file.width / file.height;
					if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
						info.trgHeight = this.options.thumbnailHeight;
						info.trgWidth = info.trgHeight * srcRatio;
						if (info.trgWidth > this.options.thumbnailWidth) {
							info.trgWidth = this.options.thumbnailWidth;
							info.trgHeight = info.trgWidth / srcRatio;
						}
					} else {
						info.trgHeight = file.height;
						info.trgWidth = file.width;
					}
					return info;
				},

				sending: function(file, xhr, formData) {
					formData.append("type", type);
					formData.append("id", id);
				},

				success: function(file, response) {
					if ( response['status'] == false ) {
						this.removeFile( file );
						$.growl.error({ message: response['error'], duration: 5000 });
					}
				}
			});
		}
	}
}