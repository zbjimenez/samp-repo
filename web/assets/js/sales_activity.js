var saControl = {
	init: function() {
		$( ".btn-add-item" ).on( "click", function() {
			saControl.addEmptyRow();
		});
		
		$( ".table-collection" ).on( "click", "a.delete-btn", function() {
			if ( confirm( "Delete record?" ) ) {
				saControl.deleteRow( $( this ) );
			}
			return false;
		});
		
		$( ".table-collection" ).on( "click", ".in-office", function() {
			saControl.toggleCustomer( $( this ) );
		});
	},
	
	addEmptyRow: function() {
		var itemPrototype = $( ".table-collection" ).attr( "data-prototype" );
		var current = $( ".table-collection tr:last" ).data( "key" );
		var rowCount = !isNaN( current ) ? current+1 : 0;
    	var $newRow = $( $.trim( itemPrototype.replace( /__name__/g, rowCount ) ) );
    	$( ".table-collection" ).append( $newRow );
    	$newRow.attr( "data-key", rowCount );
    	$newRow.find( ".select2" ).select2();
	},
	
	deleteRow: function( $btn ) {
		$btn.closest( "tr" ).remove();
	},
	
	toggleCustomer: function( $checkbox ) {
		$customer = $checkbox.closest( "tr" ).find( "select.customer" );
		$customer.attr( "disabled", true );
		
		if ( !$checkbox.is( ":checked" ) ) {
			$customer.attr( "disabled", false );
		}
	}
}

$( document ).ready( function() {
	saControl.init();
});