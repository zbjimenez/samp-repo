var dashboardControl = {
	clickable: true,
	selected: [],
	init: function () {
		var svgElement = document.getElementsByClassName("sucat")[0];

		
		//AW05 AT01

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "1px");
		rectElement.setAttribute("x", 550);
		rectElement.setAttribute("y", 190);
		rectElement.setAttribute("width", "200px");
		rectElement.setAttribute("height", "150px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		gElement.classList.add('sw01');

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 595);
		textElement.setAttribute("y", 280);
		textElement.setAttribute("font-size", 45);

		displayText = 'AA01'

		if ($.inArray('AW05' + displayText, dashboardControl.selected) !== -1) {
			rectElement.setAttribute("fill", "#64BED8");
		} else {
			rectElement.setAttribute("fill", "none");
		}

		var textNode = document.createTextNode(displayText);
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		svgElement.toDataURL("image/png", {
			callback: function (data) {
				var imagelink = document.getElementsByClassName("imagelink")[0];
				imagelink.setAttribute('href', data);
				imagelink.setAttribute('href', data);
				imagelink.classList.remove('hide');
			}
		});
	},
};

$(document).ready(function () {
	if (dashboardControl.clickable === true) {
		$("svg").on('click', 'g.sw01', function () {
			var warehouse = $(this).attr('class');
			var lane = '';

			if (warehouse.substr(0, 3) == 'awf') {
				lane = 'AA00';
			} else {
				lane = $(this).find("text")[0].textContent;
			}

			$('#inventory-modal').find('tbody').empty();
			$('#inventory-modal').find('span.warehouse').html('');
			$('#inventory-modal').find('span.warehouse-number').html('');
			$('#inventory-modal').find('span.lane').html('');
			$('#inventory-modal').find('span.wh-row').html('');

			$.ajax({
				type: "GET",
				url: app.base_uri + "/admin/inventory/find-stocks-by-location",
				data: { "warehouseNumber": warehouse, "lane": lane },
				success: function (results) {
					$('#inventory-modal').modal('show');
					$('#inventory-modal').find('span.warehouse').html('Sucat');
					$('#inventory-modal').find('span.warehouse-number').html(warehouse.toUpperCase());
					$('#inventory-modal').find('span.lane').html(lane.substr(0, 2).toUpperCase());
					$('#inventory-modal').find('span.wh-row').html(lane.substr(2, 2));

					$.each(results, function (key, result) {

						if (result.lotNumber == null) {
							var lotNumber = "";
						} else {
							var lotNumber = result.lotNumber;
						}
						if (result.palletId == null) {
							var palletId = "";
						} else {
							var palletId = result.palletId;
						}
						if (result.productionDate == null) {
							var productionDate = "";
						} else {
							var productionDate = result.productionDate;
						}
						if (result.factoryDate == null) {
							var factoryDate = "";
						} else {
							var factoryDate = result.factoryDate;
						}

						$('#inventory-modal').find('tbody').append(
							'<tr>' +
							'<td>' + result.highLevelNumber + '</td>' +
							'<td>' + result.productName + '</td>' +
							'<td>' + lotNumber + '</td>' +
							'<td>' + palletId + '</td>' +
							'<td>' + productionDate + '</td>' +
							'<td>' + factoryDate + '</td>' +
							'<td class="text-right">' + result.qtyOnHand + '</td>'
							+ '</tr>'
						);
					});
				}
			});
		});

		$("svg").on('mouseover', 'g.sw01', function () {
			$(this).css('cursor', 'pointer');
			$(this).prevAll('rect:first').css('fill', '#64BED8');
		});


		$("svg").on('mouseout', 'g', function () {
			$(this).prevAll('rect:first').css('fill', '#FFFFFF');
		});
	}
});