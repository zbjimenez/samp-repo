var dashboardControl = {
	clickable: true,
	selected: [],
	init: function() {
		var svgElement = document.getElementsByClassName("pasig")[0];

		var counter = 5;
		var displayText = '';
		var ct = 27;

		//PW07 AA
		while (counter < 675) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", 30);
			rectElement.setAttribute("y", counter);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pw07');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 43);
			textElement.setAttribute("y", counter + 18);
			textElement.setAttribute("font-size", 14);

			if (ct < 10) {
				displayText = 'AA0' + ct; 
			} else {
				displayText = 'AA' + ct;
			}

			if ( $.inArray('PW07' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct--;
		}

		//PW07 AT01

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "1px");
		rectElement.setAttribute("fill", "none");
		rectElement.setAttribute("x", 30);
		rectElement.setAttribute("y", 710);
		rectElement.setAttribute("width", "60px");
		rectElement.setAttribute("height", "40px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		gElement.classList.add('pw07');

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 43);
		textElement.setAttribute("y", 733);
		textElement.setAttribute("font-size", 14);

		displayText = 'AT01'; 

		if ( $.inArray('PW07' + displayText, dashboardControl.selected) !== -1 ) {
			rectElement.setAttribute("fill", "#64BED8");
		} else {
			rectElement.setAttribute("fill", "none");
		}

		var textNode = document.createTextNode(displayText);
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		var counter = 5;
		var displayText = '';
		var ct = 27;

		//PW07 AB
		while (counter < 675) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", 220);
			rectElement.setAttribute("y", counter);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pw07');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 233);
			textElement.setAttribute("y", counter + 18);
			textElement.setAttribute("font-size", 14);

			if (ct < 10) {
				displayText = 'AB0' + ct; 
			} else {
				displayText = 'AB' + ct;
			}

			if ( $.inArray('PW07' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct--;
		}

		//PW07 AT02

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "1px");
		rectElement.setAttribute("fill", "none");
		rectElement.setAttribute("x", 220);
		rectElement.setAttribute("y", 710);
		rectElement.setAttribute("width", "60px");
		rectElement.setAttribute("height", "40px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		gElement.classList.add('pw07');

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 233);
		textElement.setAttribute("y", 733);
		textElement.setAttribute("font-size", 14);

		displayText = 'AT02'; 

		if ( $.inArray('PW07' + displayText, dashboardControl.selected) !== -1 ) {
			rectElement.setAttribute("fill", "#64BED8");
		} else {
			rectElement.setAttribute("fill", "none");
		}

		var textNode = document.createTextNode(displayText);
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		var counter = 5;
		var displayText = '';
		var ct = 27;

		//PW04 AA
		while (counter < 675) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", 550);
			rectElement.setAttribute("y", counter);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pw04');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 563);
			textElement.setAttribute("y", counter + 18);
			textElement.setAttribute("font-size", 14);

			if (ct < 10) {
				displayText = 'AA0' + ct; 
			} else {
				displayText = 'AA' + ct;
			}

			if ( $.inArray('PW04' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct--;
		}

		//PW04 AT01

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "1px");
		rectElement.setAttribute("fill", "none");
		rectElement.setAttribute("x", 550);
		rectElement.setAttribute("y", 710);
		rectElement.setAttribute("width", "60px");
		rectElement.setAttribute("height", "40px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		gElement.classList.add('pw04');

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 563);
		textElement.setAttribute("y", 733);
		textElement.setAttribute("font-size", 14);

		displayText = 'AT01'; 

		if ( $.inArray('PW04' + displayText, dashboardControl.selected) !== -1 ) {
			rectElement.setAttribute("fill", "#64BED8");
		} else {
			rectElement.setAttribute("fill", "none");
		}

		var textNode = document.createTextNode(displayText);
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		//WH04 Cold Storage
		var yCounter = 5;
		var wh = 1;

		while (yCounter < 715) {

			if (wh == 1) {
				var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
				rectElement.setAttribute("stroke", "#000000");
				rectElement.setAttribute("stroke-width", "1px");
				rectElement.setAttribute("fill", "none");
				rectElement.setAttribute("x", 815);
				rectElement.setAttribute("y", yCounter + 65);
				rectElement.setAttribute("width", "60px");
				rectElement.setAttribute("height", "25px");
	
				svgElement.appendChild(rectElement);
	
				var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
				gElement.classList.add('pwc' + wh);
	
				var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
				textElement.setAttribute("fill", "black");
				textElement.setAttribute("x", 827);
				textElement.setAttribute("y", yCounter + 83);
				textElement.setAttribute("font-size", 14);
	
				var textNode = document.createTextNode('AD01');
				textElement.appendChild(textNode);
				gElement.appendChild(textElement);
				svgElement.appendChild(gElement);
	
				if ( $.inArray('PWC' + wh + 'AD01', dashboardControl.selected) !== -1 ) {
					rectElement.setAttribute("fill", "#64BED8");
				} else {
					rectElement.setAttribute("fill", "none");
				}
	
				var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
				rectElement.setAttribute("stroke", "#000000");
				rectElement.setAttribute("stroke-width", "1px");
				rectElement.setAttribute("fill", "none");
				rectElement.setAttribute("x", 815);
				rectElement.setAttribute("y", yCounter + 90);
				rectElement.setAttribute("width", "60px");
				rectElement.setAttribute("height", "25px");
	
				svgElement.appendChild(rectElement);
	
				var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
				gElement.classList.add('pwc' + wh);
	
				var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
				textElement.setAttribute("fill", "black");
				textElement.setAttribute("x", 827);
				textElement.setAttribute("y", yCounter + 108);
				textElement.setAttribute("font-size", 14);
	
				var textNode = document.createTextNode('AD02');
				textElement.appendChild(textNode);
				gElement.appendChild(textElement);
				svgElement.appendChild(gElement);
	
				if ( $.inArray('PWC' + wh + 'AD02', dashboardControl.selected) !== -1 ) {
					rectElement.setAttribute("fill", "#64BED8");
				} else {
					rectElement.setAttribute("fill", "none");
				}
			}

			if (wh > 1) {
				var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
				rectElement.setAttribute("stroke", "#000000");
				rectElement.setAttribute("stroke-width", "1px");
				rectElement.setAttribute("fill", "none");
				rectElement.setAttribute("x", 815);
				rectElement.setAttribute("y", yCounter + 5);
				rectElement.setAttribute("width", "60px");
				rectElement.setAttribute("height", "25px");
	
				svgElement.appendChild(rectElement);
	
				var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
				gElement.classList.add('pwc' + wh);
	
				var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
				textElement.setAttribute("fill", "black");
				textElement.setAttribute("x", 827);
				textElement.setAttribute("y", yCounter + 23);
				textElement.setAttribute("font-size", 14);
	
				var textNode = document.createTextNode('AA01');
				textElement.appendChild(textNode);
				gElement.appendChild(textElement);
				svgElement.appendChild(gElement);
	
				if ( $.inArray('PWC' + wh + 'AA01', dashboardControl.selected) !== -1 ) {
					rectElement.setAttribute("fill", "#64BED8");
				} else {
					rectElement.setAttribute("fill", "none");
				}

				var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
				rectElement.setAttribute("stroke", "#000000");
				rectElement.setAttribute("stroke-width", "1px");
				rectElement.setAttribute("fill", "none");
				rectElement.setAttribute("x", 875);
				rectElement.setAttribute("y", yCounter + 5);
				rectElement.setAttribute("width", "60px");
				rectElement.setAttribute("height", "25px");
	
				svgElement.appendChild(rectElement);
	
				var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
				gElement.classList.add('pwc' + wh);
	
				var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
				textElement.setAttribute("fill", "black");
				textElement.setAttribute("x", 888);
				textElement.setAttribute("y", yCounter + 23);
				textElement.setAttribute("font-size", 14);
	
				var textNode = document.createTextNode('AA02');
				textElement.appendChild(textNode);
				gElement.appendChild(textElement);
				svgElement.appendChild(gElement);
	
				if ( $.inArray('PWC' + wh + 'AA02', dashboardControl.selected) !== -1 ) {
					rectElement.setAttribute("fill", "#64BED8");
				} else {
					rectElement.setAttribute("fill", "none");
				}

				var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
				rectElement.setAttribute("stroke", "#000000");
				rectElement.setAttribute("stroke-width", "1px");
				rectElement.setAttribute("fill", "none");
				rectElement.setAttribute("x", 935);
				rectElement.setAttribute("y", yCounter + 5);
				rectElement.setAttribute("width", "60px");
				rectElement.setAttribute("height", "25px");
	
				svgElement.appendChild(rectElement);
	
				var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
				gElement.classList.add('pwc' + wh);
	
				var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
				textElement.setAttribute("fill", "black");
				textElement.setAttribute("x", 947);
				textElement.setAttribute("y", yCounter + 23);
				textElement.setAttribute("font-size", 14);
	
				var textNode = document.createTextNode('AA03');
				textElement.appendChild(textNode);
				gElement.appendChild(textElement);
				svgElement.appendChild(gElement);
	
				if ( $.inArray('PWC' + wh + 'AA03', dashboardControl.selected) !== -1 ) {
					rectElement.setAttribute("fill", "#64BED8");
				} else {
					rectElement.setAttribute("fill", "none");
				}
			}
			
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", 995);
			rectElement.setAttribute("y", yCounter + 5);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 1005);
			textElement.setAttribute("y", yCounter + 23);
			textElement.setAttribute("font-size", 14);

			if (wh > 1) {
				var aaRow1 = 'AA04';
			} else {
				var aaRow1 = 'AA01';
			}

			var textNode = document.createTextNode(aaRow1);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + aaRow1, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", 1055);
			rectElement.setAttribute("y", yCounter + 5);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 1065);
			textElement.setAttribute("y", yCounter + 23);
			textElement.setAttribute("font-size", 14);

			if (wh > 1) {
				var aaRow2 = 'AA05';
			} else {
				var aaRow2 = 'AA02';
			}

			var textNode = document.createTextNode(aaRow2);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + aaRow2, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", 1115);
			rectElement.setAttribute("y", yCounter + 5);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 1125);
			textElement.setAttribute("y", yCounter + 23);
			textElement.setAttribute("font-size", 14);

			if (wh > 1) {
				var aaRow3 = 'AA06';
			} else {
				var aaRow3 = 'AA01';
			}

			var textNode = document.createTextNode(aaRow3);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + aaRow3, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", 1115);
			rectElement.setAttribute("y", yCounter + 40);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 1125);
			textElement.setAttribute("y", yCounter + 58);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AB01');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + 'AB01', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", 1115);
			rectElement.setAttribute("y", yCounter + 65);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 1125);
			textElement.setAttribute("y", yCounter + 83);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AB02');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + 'AB02', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", 1115);
			rectElement.setAttribute("y", yCounter + 90);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 1125);
			textElement.setAttribute("y", yCounter + 108);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AB03');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + 'AB03', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", 1115);
			rectElement.setAttribute("y", yCounter + 135);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 1125);
			textElement.setAttribute("y", yCounter + 152);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AC06');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + 'AC06', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", 1055);
			rectElement.setAttribute("y", yCounter + 135);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 1065);
			textElement.setAttribute("y", yCounter + 152);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AC05');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + 'AC05', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", 995);
			rectElement.setAttribute("y", yCounter + 135);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 1005);
			textElement.setAttribute("y", yCounter + 152);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AC04');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + 'AC04', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", 935);
			rectElement.setAttribute("y", yCounter + 135);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 947);
			textElement.setAttribute("y", yCounter + 152);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AC03');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + 'AC03', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", 875);
			rectElement.setAttribute("y", yCounter + 135);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 888);
			textElement.setAttribute("y", yCounter + 152);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AC02');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + 'AC02', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", 815);
			rectElement.setAttribute("y", yCounter + 135);
			rectElement.setAttribute("width", "60px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 827);
			textElement.setAttribute("y", yCounter + 152);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AC01');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + 'AC01', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", 940);
			rectElement.setAttribute("y", yCounter + 65);
			rectElement.setAttribute("width", "120px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('pwc' + wh);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 985);
			textElement.setAttribute("y", yCounter + 82);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AT01');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('PWC' + wh + 'AT0' + 'wh', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "2px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", 800);
			rectElement.setAttribute("y", yCounter);
			rectElement.setAttribute("width", "380px");
			rectElement.setAttribute("height", "185px");
			rectElement.classList.add('coldstorage');

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", 895);
			textElement.setAttribute("y", yCounter + 82);
			textElement.setAttribute("font-size", 16);

			var textNode = document.createTextNode('C' + wh);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			yCounter += 185;
			wh++;
			
		}

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "2px");
		rectElement.setAttribute("x", 800);
		rectElement.setAttribute("y", yCounter);
		rectElement.setAttribute("width", "380px");
		rectElement.setAttribute("height", "100px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		gElement.classList.add('pwf5');

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 980);
		textElement.setAttribute("y", yCounter + 60);
		textElement.setAttribute("font-size", 30);

		var textNode = document.createTextNode('F5');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		if ( $.inArray('PWF5AA00', dashboardControl.selected) !== -1 ) {
			rectElement.setAttribute("fill", "#64BED8");
		} else {
			rectElement.setAttribute("fill", "none");
		}

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "2px");
		rectElement.setAttribute("fill", "none");
		rectElement.setAttribute("x", 800);
		rectElement.setAttribute("y", yCounter + 100);
		rectElement.setAttribute("width", "380px");
		rectElement.setAttribute("height", "50px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 978);
		textElement.setAttribute("y", yCounter + 130);
		textElement.setAttribute("font-size", 20);

		var textNode = document.createTextNode('Office');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 630);
		textElement.setAttribute("y", 800);
		textElement.setAttribute("font-size", 25);

		var textNode = document.createTextNode('PW04');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 150);
		textElement.setAttribute("y", 800);
		textElement.setAttribute("font-size", 25);

		var textNode = document.createTextNode('PW07');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		//lines

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 450);
		lineElement.setAttribute("x2", 450);
		lineElement.setAttribute("y1", 0);
		lineElement.setAttribute("y2", 900);

		svgElement.appendChild(lineElement);

		// misc boxes

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "2px");
		rectElement.setAttribute("fill", "none");
		rectElement.setAttribute("x", 450);
		rectElement.setAttribute("y", 750);
		rectElement.setAttribute("width", "100px");
		rectElement.setAttribute("height", "80px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 480);
		textElement.setAttribute("y", 785);
		textElement.setAttribute("font-size", 20);

		var textNode = document.createTextNode('CR');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		svgElement.toDataURL("image/png", {
			callback: function(data) {
				var imagelink = document.getElementsByClassName("imagelink")[0];
				imagelink.setAttribute('href', data);
				imagelink.setAttribute('href', data);
				imagelink.classList.remove('hide');
			}
		});
	},
};

$(document).ready(function() {
	if (dashboardControl.clickable === true) {
		$("svg").on('click', 'g.pw07, g.pw04, g.pwc1, g.pwc2, g.pwc3, g.pwc4, g.pwf5', function() {
			var warehouse = $(this).attr('class');
			var lane = '';

			if (warehouse.substr(0,3) == 'pwf') {
				lane = 'AA00';
			} else {
				lane = $(this).find("text")[0].textContent;
			}

			$('#inventory-modal').find('tbody').empty();
			$('#inventory-modal').find('span.warehouse').html('');
			$('#inventory-modal').find('span.warehouse-number').html('');
			$('#inventory-modal').find('span.lane').html('');
			$('#inventory-modal').find('span.wh-row').html('');

			$.ajax({
				type: "GET",
				url: app.base_uri + "/admin/inventory/find-stocks-by-location",
				data: { "warehouseNumber" : warehouse, "lane" : lane },
				success: function(results) {
					$('#inventory-modal').modal('show');
					$('#inventory-modal').find('span.warehouse').html('Pasig');
					$('#inventory-modal').find('span.warehouse-number').html(warehouse.toUpperCase());
					$('#inventory-modal').find('span.lane').html(lane.substr(0,2).toUpperCase());
					$('#inventory-modal').find('span.wh-row').html(lane.substr(2,2));

					$.each( results, function(key, result) {
						if (result.lotNumber == null) {
							var lotNumber = "";
						} else {
							var lotNumber = result.lotNumber;
						}
						if (result.palletId == null) {
							var palletId = "";
						} else {
							var palletId = result.palletId;
						}
						if (result.productionDate == null) {
							var productionDate = "";
						} else {
							var productionDate = result.productionDate;
						}
						if (result.factoryDate == null) {
							var factoryDate = "";
						} else {
							var factoryDate = result.factoryDate;
						}

						$('#inventory-modal').find('tbody').append(
							'<tr>' +
								'<td>' + result.highLevelNumber + '</td>' +
								'<td>' + result.productName + '</td>' +
								'<td>' + lotNumber + '</td>' +
								'<td>' + palletId + '</td>' +
								'<td>' + productionDate + '</td>' +
								'<td>' + factoryDate + '</td>' +
								'<td class="text-right">' + result.qtyOnHand + '</td>'
							+ '</tr>'
						);
					});
				}
			});
		});

		$("svg").on('mouseover', 'g.pw07, g.pw04, g.pwc1, g.pwc2, g.pwc3, g.pwc4, g.pwf5', function() {
			$(this).css('cursor', 'pointer');
			$(this).prevAll('rect:first').css('fill', '#64BED8');
		});


		$("svg").on('mouseout', 'g', function() {
			$(this).prevAll('rect:first:not(.coldstorage)').css('fill', '#FFFFFF');
		});
	}
});