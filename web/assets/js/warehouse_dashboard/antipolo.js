var dashboardControl = {
	clickable: true,
	selected: [],
	init: function() {
		var svgElement = document.getElementsByClassName("antipolo")[0];

		var counter = 0;
		var displayText = '';
		var ct = 23;

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 250);
		textElement.setAttribute("y", 85);
		textElement.setAttribute("font-size", 16);

		var textNode = document.createTextNode('AW05');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		//AW05 AB
		while (counter < 560) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 1);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "60px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw05');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 12);
			textElement.setAttribute("y", 12);
			textElement.setAttribute("font-size", 14);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AB0' + ct; 
			} else {
				displayText = 'AB' + ct;
			}

			if ( $.inArray('AW05' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}
			
			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct--;
		}

		counter = 0;
		displayText = '';
		ct = 21;

		//AW05 AA
		while (counter < 520) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 100);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "60px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw05');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 12);
			textElement.setAttribute("y", 113);
			textElement.setAttribute("font-size", 14);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AA0' + ct; 
			} else {
				displayText = 'AA' + ct;
			}

			if ( $.inArray('AW05' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct--;
		}

		//AW05 AT01

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "1px");
		rectElement.setAttribute("x", 550);
		rectElement.setAttribute("y", 100);
		rectElement.setAttribute("width", "25px");
		rectElement.setAttribute("height", "60px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		gElement.setAttribute("writing-mode", "tb-rl");
		gElement.classList.add('aw05');

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 562);
		textElement.setAttribute("y", 113);
		textElement.setAttribute("font-size", 14);
		textElement.setAttribute("glyph-orientation-vertical", 90);

		displayText = 'AT01'

		if ( $.inArray('AW05' + displayText, dashboardControl.selected) !== -1 ) {
			rectElement.setAttribute("fill", "#64BED8");
		} else {
			rectElement.setAttribute("fill", "none");
		}

		var textNode = document.createTextNode(displayText);
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		//AW06 AB
		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 250);
		textElement.setAttribute("y", 300);
		textElement.setAttribute("font-size", 16);

		var textNode = document.createTextNode('AW06');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		counter = 0;
		displayText = '';
		ct = 21;

		while (counter < 520) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 210);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "60px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw06');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 12);
			textElement.setAttribute("y", 220);
			textElement.setAttribute("font-size", 14);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AB0' + ct; 
			} else {
				displayText = 'AB' + ct;
			}

			if ( $.inArray('AW06' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct--;
		}

		//AW06 AT01

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "1px");
		rectElement.setAttribute("x", 550);
		rectElement.setAttribute("y", 210);
		rectElement.setAttribute("width", "25px");
		rectElement.setAttribute("height", "60px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		gElement.setAttribute("writing-mode", "tb-rl");
		gElement.classList.add('aw06');

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 562);
		textElement.setAttribute("y", 220);
		textElement.setAttribute("font-size", 14);
		textElement.setAttribute("glyph-orientation-vertical", 90);

		displayText = 'AT01'

		if ( $.inArray('AW06' + displayText, dashboardControl.selected) !== -1 ) {
			rectElement.setAttribute("fill", "#64BED8");
		} else {
			rectElement.setAttribute("fill", "none");
		}

		var textNode = document.createTextNode(displayText);
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		//AW06 AA
		counter = 0;
		displayText = '';
		ct = 21;

		while(counter < 125) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 320);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "60px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw06');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 12);
			textElement.setAttribute("y", 330);
			textElement.setAttribute("font-size", 14);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AA0' + ct; 
			} else {
				displayText = 'AA' + ct;
			}

			if ( $.inArray('AW06' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct--;
		}

		counter += 40;

		while(counter < 560) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 320);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "60px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw06');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 12);
			textElement.setAttribute("y", 330);
			textElement.setAttribute("font-size", 14);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AA0' + ct; 
			} else {
				displayText = 'AA' + ct;
			}

			if ( $.inArray('AW06' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct--;
		}

		//CQ04
		counter = 795;

		while(counter < 890) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 1);
			rectElement.setAttribute("width", "90px");
			rectElement.setAttribute("height", "85px");

			svgElement.appendChild(rectElement);

			counter += 90;
		}

		var xCounter = 795;

		var codes = ['c1', 'c4', 'c2', 'c3', 'c5', 'c6'];
		var codeCounter = 0;

		while (xCounter < 900) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", xCounter);
			rectElement.setAttribute("y", 10);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 2);
			textElement.setAttribute("y", 25);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AA01');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AA01', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", xCounter);
			rectElement.setAttribute("y", 35);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 2);
			textElement.setAttribute("y", 50);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AA02');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AA02', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", xCounter + 30);
			rectElement.setAttribute("y", 10);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 33);
			textElement.setAttribute("y", 25);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AB01');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AB01', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", xCounter + 30);
			rectElement.setAttribute("y", 35);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 33);
			textElement.setAttribute("y", 50);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AB02');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AB02', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", xCounter + 60);
			rectElement.setAttribute("y", 10);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 63);
			textElement.setAttribute("y", 25);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AC01');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AC01', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", xCounter + 60);
			rectElement.setAttribute("y", 35);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 63);
			textElement.setAttribute("y", 51);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AC02');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AC02', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", xCounter + 60);
			rectElement.setAttribute("y", 60);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 63);
			textElement.setAttribute("y", 76);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AC03');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AC03', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			xCounter += 90;

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter - 80);
			textElement.setAttribute("y", 78);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AW' + codes[codeCounter].toUpperCase());
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			codeCounter++;
		}

		// Floor Stocking

		counter = 975;
		floor = 1;

		while(counter < 1200) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 1);
			rectElement.setAttribute("width", "90px");
			rectElement.setAttribute("height", "85px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('awf' + floor);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 25);
			textElement.setAttribute("y", 45);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AWF' + floor);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AWF' + floor + 'AA00', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			counter += 90;
			floor++;
		}

		counter = 795;

		while(counter < 1160) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 105);
			rectElement.setAttribute("width", "90px");
			rectElement.setAttribute("height", "85px");

			svgElement.appendChild(rectElement);

			counter += 90;
		}

		var xCounter = 795;

		while (xCounter < 1100) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", xCounter);
			rectElement.setAttribute("y", 130);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 2);
			textElement.setAttribute("y", 145);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AA01');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AA01', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", xCounter);
			rectElement.setAttribute("y", 155);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 2);
			textElement.setAttribute("y", 172);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AA02');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AA02', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", xCounter + 30);
			rectElement.setAttribute("y", 130);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 33);
			textElement.setAttribute("y", 145);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AB01');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AB01', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", xCounter + 30);
			rectElement.setAttribute("y", 155);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 33);
			textElement.setAttribute("y", 172);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AB02');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AB02', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", xCounter + 60);
			rectElement.setAttribute("y", 105);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 63);
			textElement.setAttribute("y", 120);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AC01');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AC01', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", xCounter + 60);
			rectElement.setAttribute("y", 130);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 63);
			textElement.setAttribute("y", 145);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AC02');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AC02', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", xCounter + 60);
			rectElement.setAttribute("y", 155);
			rectElement.setAttribute("width", "30px");
			rectElement.setAttribute("height", "25px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.classList.add('aw' + codes[codeCounter]);

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter + 63);
			textElement.setAttribute("y", 172);
			textElement.setAttribute("font-size", 10);

			var textNode = document.createTextNode('AC03');
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			if ( $.inArray('AW' + codes[codeCounter].toUpperCase() + 'AC03', dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			xCounter += 90;

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", xCounter - 80);
			textElement.setAttribute("y", 122);
			textElement.setAttribute("font-size", 14);

			var textNode = document.createTextNode('AW' + codes[codeCounter].toUpperCase());
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			codeCounter++;
		}

		//AW03
		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 940);
		textElement.setAttribute("y", 290);
		textElement.setAttribute("font-size", 16);

		var textNode = document.createTextNode('AW03');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		counter = 720
		displayText = '';
		ct = 1;

		while(counter < 1225) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 210);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "60px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw03');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 10);
			textElement.setAttribute("y", 223);
			textElement.setAttribute("font-size", 14);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AA0' + ct; 
			} else {
				displayText = 'AA' + ct;
			}

			if ( $.inArray('AW03' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct++;
		}

		//AW03 AT01

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "1px");
		rectElement.setAttribute("x", 650);
		rectElement.setAttribute("y", 210);
		rectElement.setAttribute("width", "25px");
		rectElement.setAttribute("height", "60px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		gElement.setAttribute("writing-mode", "tb-rl");
		gElement.classList.add('aw03');

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 662);
		textElement.setAttribute("y", 223);
		textElement.setAttribute("font-size", 14);
		textElement.setAttribute("glyph-orientation-vertical", 90);

		displayText = 'AT01'

		if ( $.inArray('AW03' + displayText, dashboardControl.selected) !== -1 ) {
			rectElement.setAttribute("fill", "#64BED8");
		} else {
			rectElement.setAttribute("fill", "none");
		}

		var textNode = document.createTextNode(displayText);
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		counter = 720
		displayText = '';
		ct = 1;

		while(counter < 1245) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 300);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "40px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw03');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 10);
			textElement.setAttribute("y", 307);
			textElement.setAttribute("font-size", 11);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AB0' + ct; 
			} else {
				displayText = 'AB' + ct;
			}

			if ( $.inArray('AW03' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct++;
		}

		counter = 720
		displayText = '';
		ct = 1;

		while(counter < 1245) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 340);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "40px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw03');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 10);
			textElement.setAttribute("y", 345);
			textElement.setAttribute("font-size", 11);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AC0' + ct; 
			} else {
				displayText = 'AC' + ct;
			}

			if ( $.inArray('AW03' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct++;
		}

		//AW03 AT02

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "1px");
		rectElement.setAttribute("x", 650);
		rectElement.setAttribute("y", 310);
		rectElement.setAttribute("width", "25px");
		rectElement.setAttribute("height", "60px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		gElement.setAttribute("writing-mode", "tb-rl");
		gElement.classList.add('aw03');

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 662);
		textElement.setAttribute("y", 323);
		textElement.setAttribute("font-size", 14);
		textElement.setAttribute("glyph-orientation-vertical", 90);

		displayText = 'AT02'

		if ( $.inArray('AW03' + displayText, dashboardControl.selected) !== -1 ) {
			rectElement.setAttribute("fill", "#64BED8");
		} else {
			rectElement.setAttribute("fill", "none");
		}

		var textNode = document.createTextNode(displayText);
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);



		//AW02
		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 940);
		textElement.setAttribute("y", 508);
		textElement.setAttribute("font-size", 16);

		var textNode = document.createTextNode('AW02');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		counter = 720
		displayText = '';
		ct = 1;

		while (counter < 1245) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 405);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "40px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw02');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 10);
			textElement.setAttribute("y", 412);
			textElement.setAttribute("font-size", 11);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AA0' + ct; 
			} else {
				displayText = 'AA' + ct;
			}

			if ( $.inArray('AW02' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct++;
		}

		//AW02 AT01

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "1px");
		rectElement.setAttribute("x", 650);
		rectElement.setAttribute("y", 415);
		rectElement.setAttribute("width", "25px");
		rectElement.setAttribute("height", "70px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		gElement.setAttribute("writing-mode", "tb-rl");
		gElement.classList.add('aw02');

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 663);
		textElement.setAttribute("y", 430);
		textElement.setAttribute("font-size", 14);
		textElement.setAttribute("glyph-orientation-vertical", 90);

		displayText = 'AT01'

		if ( $.inArray('AW02' + displayText, dashboardControl.selected) !== -1 ) {
			rectElement.setAttribute("fill", "#64BED8");
		} else {
			rectElement.setAttribute("fill", "none");
		}

		var textNode = document.createTextNode(displayText);
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		counter = 720
		displayText = '';
		ct = 1;

		while (counter < 1245) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 445);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "40px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw02');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 10);
			textElement.setAttribute("y", 452);
			textElement.setAttribute("font-size", 11);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AB0' + ct; 
			} else {
				displayText = 'AB' + ct;
			}

			if ( $.inArray('AW02' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct++;
		}

		counter = 675
		displayText = '';
		ct = 1;

		while (counter < 1245) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 520);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "60px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw02');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 10);
			textElement.setAttribute("y", 530);
			textElement.setAttribute("font-size", 14);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AC0' + ct; 
			} else {
				displayText = 'AC' + ct;
			}

			if ( $.inArray('AW02' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct++;
		}

		//AW01
		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 940);
		textElement.setAttribute("y", 690);
		textElement.setAttribute("font-size", 16);

		var textNode = document.createTextNode('AW01');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		counter = 720
		displayText = '';
		ct = 1;

		while (counter < 1245) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 610);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "60px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw01');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 12);
			textElement.setAttribute("y", 625);
			textElement.setAttribute("font-size", 14);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AA0' + ct; 
			} else {
				displayText = 'AA' + ct;
			}

			if ( $.inArray('AW01' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct++;
		}

		counter = 720
		displayText = '';
		ct = 1;

		while (counter < 1245) {
			var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectElement.setAttribute("stroke", "#000000");
			rectElement.setAttribute("stroke-width", "1px");
			rectElement.setAttribute("fill", "none");
			rectElement.setAttribute("x", counter);
			rectElement.setAttribute("y", 700);
			rectElement.setAttribute("width", "25px");
			rectElement.setAttribute("height", "60px");

			svgElement.appendChild(rectElement);

			var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
			gElement.setAttribute("writing-mode", "tb-rl");
			gElement.classList.add('aw01');

			var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
			textElement.setAttribute("fill", "black");
			textElement.setAttribute("x", counter + 12);
			textElement.setAttribute("y", 715);
			textElement.setAttribute("font-size", 14);
			textElement.setAttribute("glyph-orientation-vertical", 90);

			if (ct < 10) {
				displayText = 'AB0' + ct; 
			} else {
				displayText = 'AB' + ct;
			}

			if ( $.inArray('AW01' + displayText, dashboardControl.selected) !== -1 ) {
				rectElement.setAttribute("fill", "#64BED8");
			} else {
				rectElement.setAttribute("fill", "none");
			}

			var textNode = document.createTextNode(displayText);
			textElement.appendChild(textNode);
			gElement.appendChild(textElement);
			svgElement.appendChild(gElement);

			counter += 25;
			ct++;
		}

		//AW01 AT01

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "1px");
		rectElement.setAttribute("x", 640);
		rectElement.setAttribute("y", 700);
		rectElement.setAttribute("width", "50px");
		rectElement.setAttribute("height", "80px");

		svgElement.appendChild(rectElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
		gElement.setAttribute("writing-mode", "tb-rl");
		gElement.classList.add('aw01');

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 660);
		textElement.setAttribute("y", 715);
		textElement.setAttribute("font-size", 14);
		textElement.setAttribute("glyph-orientation-vertical", 90);

		displayText = 'AT01'

		if ( $.inArray('AW01' + displayText, dashboardControl.selected) !== -1 ) {
			rectElement.setAttribute("fill", "#64BED8");
		} else {
			rectElement.setAttribute("fill", "none");
		}

		var textNode = document.createTextNode(displayText);
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		//Lines

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 0);
		lineElement.setAttribute("x2", 590);
		lineElement.setAttribute("y1", 180);
		lineElement.setAttribute("y2", 180);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 590);
		lineElement.setAttribute("x2", 590);
		lineElement.setAttribute("y1", 0);
		lineElement.setAttribute("y2", 400);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 165);
		lineElement.setAttribute("x2", 590);
		lineElement.setAttribute("y1", 400);
		lineElement.setAttribute("y2", 400);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 0);
		lineElement.setAttribute("x2", 130);
		lineElement.setAttribute("y1", 400);
		lineElement.setAttribute("y2", 400);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 165);
		lineElement.setAttribute("x2", 165);
		lineElement.setAttribute("y1", 400);
		lineElement.setAttribute("y2", 800);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 210);
		lineElement.setAttribute("x2", 210);
		lineElement.setAttribute("y1", 400);
		lineElement.setAttribute("y2", 560);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 0);
		lineElement.setAttribute("x2", 210);
		lineElement.setAttribute("y1", 560);
		lineElement.setAttribute("y2", 560);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 0);
		lineElement.setAttribute("x2", 530);
		lineElement.setAttribute("y1", 799);
		lineElement.setAttribute("y2", 799);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 210);
		lineElement.setAttribute("x2", 210);
		lineElement.setAttribute("y1", 690);
		lineElement.setAttribute("y2", 800);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 300);
		lineElement.setAttribute("x2", 300);
		lineElement.setAttribute("y1", 690);
		lineElement.setAttribute("y2", 800);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 330);
		lineElement.setAttribute("x2", 330);
		lineElement.setAttribute("y1", 690);
		lineElement.setAttribute("y2", 800);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 420);
		lineElement.setAttribute("x2", 420);
		lineElement.setAttribute("y1", 690);
		lineElement.setAttribute("y2", 800);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 505);
		lineElement.setAttribute("x2", 505);
		lineElement.setAttribute("y1", 690);
		lineElement.setAttribute("y2", 800);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 530);
		lineElement.setAttribute("x2", 530);
		lineElement.setAttribute("y1", 690);
		lineElement.setAttribute("y2", 800);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 330);
		lineElement.setAttribute("x2", 531);
		lineElement.setAttribute("y1", 690);
		lineElement.setAttribute("y2", 690);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 640);
		lineElement.setAttribute("x2", 640);
		lineElement.setAttribute("y1", 0);
		lineElement.setAttribute("y2", 800);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 640);
		lineElement.setAttribute("x2", 675);
		lineElement.setAttribute("y1", 190);
		lineElement.setAttribute("y2", 190);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 720);
		lineElement.setAttribute("x2", 1250);
		lineElement.setAttribute("y1", 190);
		lineElement.setAttribute("y2", 190);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 720);
		lineElement.setAttribute("x2", 1250);
		lineElement.setAttribute("y1", 395);
		lineElement.setAttribute("y2", 395);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 640);
		lineElement.setAttribute("x2", 675);
		lineElement.setAttribute("y1", 395);
		lineElement.setAttribute("y2", 395);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "2px");
		lineElement.setAttribute("x1", 640);
		lineElement.setAttribute("x2", 1250);
		lineElement.setAttribute("y1", 595);
		lineElement.setAttribute("y2", 595);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "1px");
		lineElement.setAttribute("x1", 530);
		lineElement.setAttribute("x2", 640);
		lineElement.setAttribute("y1", 740);
		lineElement.setAttribute("y2", 800);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "1px");
		lineElement.setAttribute("x1", 640);
		lineElement.setAttribute("x2", 1250);
		lineElement.setAttribute("y1", 780);
		lineElement.setAttribute("y2", 780);

		svgElement.appendChild(lineElement);

		var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
		lineElement.setAttribute("stroke", "#000000");
		lineElement.setAttribute("stroke-width", "1px");
		lineElement.setAttribute("x1", 640);
		lineElement.setAttribute("x2", 1250);
		lineElement.setAttribute("y1", 800);
		lineElement.setAttribute("y2", 800);

		svgElement.appendChild(lineElement);

		//Misc boxes

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "2px");
		rectElement.setAttribute("fill", "none");
		rectElement.setAttribute("x", 660);
		rectElement.setAttribute("y", 10);
		rectElement.setAttribute("width", "110px");
		rectElement.setAttribute("height", "50px");

		svgElement.appendChild(rectElement);

		var rectElement = document.createElementNS("http://www.w3.org/2000/svg", "rect");
		rectElement.setAttribute("stroke", "#000000");
		rectElement.setAttribute("stroke-width", "2px");
		rectElement.setAttribute("fill", "none");
		rectElement.setAttribute("x", 640);
		rectElement.setAttribute("y", 595);
		rectElement.setAttribute("width", "60px");
		rectElement.setAttribute("height", "60px");

		svgElement.appendChild(rectElement);

		//Misc texts

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 25);
		textElement.setAttribute("y", 480);
		textElement.setAttribute("font-size", 14);

		var textNode = document.createTextNode('Staging/Receiving');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 215);
		textElement.setAttribute("y", 740);
		textElement.setAttribute("font-size", 12);

		var textNode = document.createTextNode('Locker Room');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 306);
		textElement.setAttribute("y", 740);
		textElement.setAttribute("font-size", 12);

		var textNode = document.createTextNode('CR');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 343);
		textElement.setAttribute("y", 740);
		textElement.setAttribute("font-size", 12);

		var textNode = document.createTextNode('Main Office');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 430);
		textElement.setAttribute("y", 740);
		textElement.setAttribute("font-size", 12);

		var textNode = document.createTextNode('Conference');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 445);
		textElement.setAttribute("y", 755);
		textElement.setAttribute("font-size", 12);

		var textNode = document.createTextNode('Room');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 645);
		textElement.setAttribute("y", 630);
		textElement.setAttribute("font-size", 11);

		var textNode = document.createTextNode('Stock Rm.');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		var gElement = document.createElementNS("http://www.w3.org/2000/svg", "g");

		var textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
		textElement.setAttribute("fill", "black");
		textElement.setAttribute("x", 850);
		textElement.setAttribute("y", 795);
		textElement.setAttribute("font-size", 16);

		var textNode = document.createTextNode('RESTRICTED AREA');
		textElement.appendChild(textNode);
		gElement.appendChild(textElement);
		svgElement.appendChild(gElement);

		svgElement.toDataURL("image/png", {
			callback: function(data) {
				var imagelink = document.getElementsByClassName("imagelink")[0];
				imagelink.setAttribute('href', data);
				imagelink.setAttribute('href', data);
				imagelink.classList.remove('hide');
			}
		});
	},
};

$(document).ready(function() {
	if (dashboardControl.clickable === true) {
		$("svg").on('click', 'g.aw05, g.aw06, g.aw03, g.aw02, g.aw01, g.awc1, g.awc2, g.awc3, g.awc4, g.awc5, g.awc6, g.awf1, g.awf2, g.awf3', function() {
			var warehouse = $(this).attr('class');
			var lane = '';

			if (warehouse.substr(0,3) == 'awf') {
				lane = 'AA00';
			} else {
				lane = $(this).find("text")[0].textContent;
			}

			$('#inventory-modal').find('tbody').empty();
			$('#inventory-modal').find('span.warehouse').html('');
			$('#inventory-modal').find('span.warehouse-number').html('');
			$('#inventory-modal').find('span.lane').html('');
			$('#inventory-modal').find('span.wh-row').html('');

			$.ajax({
				type: "GET",
				url: app.base_uri + "/admin/inventory/find-stocks-by-location",
				data: { "warehouseNumber" : warehouse, "lane" : lane },
				success: function(results) {
					$('#inventory-modal').modal('show');
					$('#inventory-modal').find('span.warehouse').html('Antipolo');
					$('#inventory-modal').find('span.warehouse-number').html(warehouse.toUpperCase());
					$('#inventory-modal').find('span.lane').html(lane.substr(0,2).toUpperCase());
					$('#inventory-modal').find('span.wh-row').html(lane.substr(2,2));
					
					$.each( results, function(key, result) {

						if (result.lotNumber == null) {
							var lotNumber = "";
						} else {
							var lotNumber = result.lotNumber;
						}
						if (result.palletId == null) {
							var palletId = "";
						} else {
							var palletId = result.palletId;
						}
						if (result.productionDate == null) {
							var productionDate = "";
						} else {
							var productionDate = result.productionDate;
						}
						if (result.factoryDate == null) {
							var factoryDate = "";
						} else {
							var factoryDate = result.factoryDate;
						}

						$('#inventory-modal').find('tbody').append(
							'<tr>' +
								'<td>' + result.highLevelNumber + '</td>' +
								'<td>' + result.productName + '</td>' +
								'<td>' + lotNumber + '</td>' +
								'<td>' + palletId + '</td>' +
								'<td>' + productionDate + '</td>' +
								'<td>' + factoryDate + '</td>' +
								'<td class="text-right">' + result.qtyOnHand + '</td>'
							+ '</tr>'
						);
					});
				}
			});
		});

		$("svg").on('mouseover', 'g.aw05, g.aw06, g.aw03, g.aw02, g.aw01, g.awc1, g.awc2, g.awc3, g.awc4, g.awc5, g.awc6, g.awf1, g.awf2, g.awf3', function() {
			$(this).css('cursor', 'pointer');
			$(this).prevAll('rect:first').css('fill', '#64BED8');
		});


		$("svg").on('mouseout', 'g', function() {
			$(this).prevAll('rect:first').css('fill', '#FFFFFF');
		});
	}
});