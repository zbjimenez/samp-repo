var inventory = angular.module( "inventory" );

inventory.value( "Items", items );

inventory.config( function( $interpolateProvider ) {
    $interpolateProvider.startSymbol( "ANGLR_" ).endSymbol( "_ANGLR" );
});

var roCtrl = inventory.controller( "ShippingController", function( $scope, $http, Items, StorageLocationFactory) {
	$scope.form = {};
	$scope.form.items = Items;
});
