var inventory = angular.module( "inventory" );

inventory.value( "Items", items );

inventory.config( function( $interpolateProvider ) {
	$interpolateProvider.startSymbol( "ANGLR_" ).endSymbol( "_ANGLR" );
});

var opCtrl = inventory.controller( "OrderPickingController", function( $scope, $http, $timeout, Items ) {
	var	pickedPalletIds = {};
	$scope.form = {
		items: Items,
		indexes: { item: -1 },
	};

	$scope.init = function() {
		$timeout( function() {
			angular.forEach( $scope.form.items, function( value, key ) {
				angular.element( ".shippingitem-" + key ).select2( "val", value.shippingItem.id );
				angular.element( ".lotnumber-" + key ).select2( "val", value.lotNumber.label );
				angular.element( ".pallet-" + key ).select2( "val", value.palletId.label );
			});
		}, 500 );
	};

	$scope.addItem = function() {
		var index = parseFloat( angular.copy( $scope.form.indexes.item ) ) + 1;

		$scope.form.items[ index ] = {
			shippingItem: { id: 'Choose a product' },
			product: {},
			lotNumbers: {},
			lotNumber: '',
			palletIds: {},
			palletId: '',
			productionDate: '',
			storageLocation: {}
		};
	};

	$scope.removeItem = function( key ) {
		var prodId = items[key].product.id;
		var pallet = items[key].palletId;

		if (pallet != "") {
			var palId = pallet.label;
			delete pickedPalletIds[prodId][palId];
		}
		delete $scope.form.items[ key ];
	};

	$scope.onChangeProduct = function( item, key ) {
		item.lotNumbers = {};
		item.lotNumber = '';
		item.productionDate = '';
		item.storageLocation = {};
		angular.element('.lotnumber-' + key).find('.select2-chosen').empty();

		$http({
			method: "GET",
			url: app.base_uri + "/admin/delivery/find-product-by-item-id/" + item.shippingItem.id,
		}).success( function( response ) {
			item.product = response;

			$http({
				method: "GET",
				url: app.base_uri + "/admin/inventory/find-lot-numbers/" + item.product.id
			}).success( function( response ) {
				item.lotNumbers = response;

				if (pickedPalletIds[item.product.id] != undefined) {
					for (var palletId in pickedPalletIds[item.product.id]) {
						for (var i = 0; i < item.lotNumbers.length; i++) {
							if (item.lotNumbers[i].label === pickedPalletIds[item.product.id][palletId]) {
								item.lotNumbers.splice(i,1);
								break;
							}
						}
					}
				}
			});
		});
	};

	$scope.onChangeLotNumber = function( item, key ) {
		item.palletIds = {};
		item.palletId = '';
		item.productionDate = '';
		item.storageLocation = {};
		angular.element('.pallet-' + key).find('.select2-chosen').empty();

		$http({
			method: "GET",
			url: app.base_uri + "/admin/inventory/find-production-date/" + item.lotNumber.label,
		}).success( function( response ) {
			if (response.length > 0) {
				item.productionDate = response[0].productionDate;
			}
		});

		$http({
			method: "GET",
			url: app.base_uri + "/admin/inventory/find-pallet-ids/" + item.product.id + "/" + item.lotNumber.label,
		}).success( function( response ) {
			item.palletIds = response;

			if (pickedPalletIds[item.product.id] != undefined) {
				for (var palletId in pickedPalletIds[item.product.id]) {
					for (var i = 0; i < item.palletIds.length; i++) {
						if (item.palletIds[i].label === palletId) {
							item.palletIds.splice(i,1);
							break;
						}
					}
				}
			}
		});
	};

	$scope.onChangePalletId = function( item ) {
		item.storageLocation = {};
		
		$http({
			method: "GET",
			url: app.base_uri + "/admin/inventory/find-storage-location/" + item.product.id + "/" + item.lotNumber.label + "/" + item.palletId.label,
		}).success( function( response ) {
			if (response.length > 0) {
				item.storageLocation = response[0];
			}

			if (!(item.product.id in pickedPalletIds)) {
				pickedPalletIds[item.product.id] = {};
			}

			if (!(item.palletId.label in pickedPalletIds[item.product.id])) {
				pickedPalletIds[item.product.id][item.palletId.label] = item.lotNumber.label;
			}
		});
	};
});