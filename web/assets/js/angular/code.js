//branch angular js

var inventory = angular.module( "inventory" );

var codeCtrl = inventory.controller( "CodeController", function( $scope, $modal, $http ) {
	$scope.codes = codeCtrl.codes;
	$scope.form = {};
	
	$scope.add = function() {
		var modalInstance = $modal.open({
			templateUrl: app.base_uri + "/admin/product-code/" + codeCtrl.productId + "/new",
			controller: CodeModalInstanceCtrl,
			scope: $scope
	    });
	};
	
	$scope.edit = function( codeId, codeIndex ) {
		$scope.codeId = codeId;
		$scope.codeIndex = codeIndex;
		$scope.form = angular.copy( $scope.codes[ $scope.codeIndex ] );
		
		var modalInstance = $modal.open({
			templateUrl: app.base_uri + "/admin/product-code/" + codeId + "/edit" + "?a=" + Math.random(),
			controller: CodeModalInstanceCtrl,
			scope: $scope
	    });
	};
	
	$scope.remove = function( codeId, codeIndex ) {
		$http({
		    method: "POST",
		    url: app.base_uri + "/admin/product-code/" + codeId + "/delete",
		    headers: { 
		    	"Content-Type" : "application/x-www-form-urlencoded" 
		    }
		})
		.success( function( data ) {
			if ( data.success == true ) {
				$.growl.notice({ title: "<strong>Deleted</strong>", message: data.message, size: "large", duration: 5000 });
				$scope.codes.splice( codeIndex, 1 );
			} else {
				$.growl.error({ title: "<strong>Error</strong>", message: data.message, size: "large", duration: 5000 });
			}
		});
	};
});

var CodeModalInstanceCtrl = function( $scope, $modalInstance, $http ) {
	$scope.postData = function( url, action ) {
		$http({
		    method: "POST",
		    url: url,
		    data: $( "form" ).serialize(),
		    headers: { 
		    	"Content-Type" : "application/x-www-form-urlencoded" 
		    }
		})
		.success( function( data ) {
			$( "button[type=submit]" ).button( "reset" );
			
			if ( data.status == "success" ) {
				$.growl.notice({ title: "<strong>Saved</strong>", message: data.message, size: "large", duration: 5000 });
				
				var code = {
					id: data.entityId,
					customer: $scope.form.customer,
					customerName: data.customerName,
					code: $scope.form.code,
					alias: $scope.form.alias
				};
				
				if ( action == "create" ) {
					$scope.codes.push( code );
				} else {
					$scope.codes[ $scope.codeIndex ] = code;
				}

				$scope.close();
			} else {
				$scope.message = data.message;
				$scope.errors = data.errors;
			}
		});
	};
	
	$scope.create = function() {
		$( "button[type=submit]" ).button( "loading" );
		$scope.postData( app.base_uri + "/admin/product-code/create", "create" );
	};
	
	$scope.update = function() {
		$( "button[type=submit]" ).button( "loading" );
		$scope.postData( app.base_uri + "/admin/product-code/" + $scope.codeId, "update" );
	};

	$scope.close = function() {
		$scope.form.id = "";
		$scope.form.customer = "";
		$scope.form.code = "";
		$scope.form.alias = "";
		
		$modalInstance.dismiss( "close" );
	};
};
