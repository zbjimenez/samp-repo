var inventory = angular.module( "inventory" );

inventory.value( "Items", items );
inventory.value( "PoItems", poItems);
inventory.value( "PoNo", poNo );
inventory.value( "ReceivedPalletIds", receivedPalletIds)

inventory.config( function( $interpolateProvider ) {
	$interpolateProvider.startSymbol( "ANGLR_" ).endSymbol( "_ANGLR" );
});

var roCtrl = inventory.controller( "ReceiveOrderController", function( $scope, Items, PoItems, PoNo, ReceivedPalletIds ) {
	$scope.form = {};
	$scope.form.items = Items;
	$scope.poItems = PoItems;
	$scope.poNo = PoNo;
	$scope.receivedPalletIds = ReceivedPalletIds;

	$scope.forbiddenPalletIds = [];

	$scope.importItemFromScan = function(mode, type, quantity, barcode, lotNumber, palletId, productionDateFromScan) {
		var index = 0;
		var existing = false;
		var copiedItem = null;

		for (var key in $scope.form.items) {
			index++;
		}

		for (var key in $scope.poItems) {
			if ($scope.poItems[key].product.barcode == barcode) {
				$scope.form.items[ index + 1 ] = {
					poItem: $scope.poItems[key].id,
					isIncluded: true,
					quantity: quantity,
					maxQty: $scope.poItems[key].maxQty,
					totalReceived: $scope.poItems[key].totalReceived,
					product: $scope.poItems[key].product,
					lotNumber: lotNumber,
					mode: mode,
					type: type,
					barcode: barcode,
					palletId: palletId,
					productionDateFromScan: productionDateFromScan
				};
				return;
			}
		}
	};

	$scope.hasReceivedPalletId = function($palletId) {
		for (var key in $scope.receivedPalletIds) {
			if ($scope.receivedPalletIds[key] == $palletId) {
				return true;
			}
		}
		return false;
	}

	$scope.isDuplicatePalletId = function($palletId) {
		var counter = 0;
		for (var key in $scope.poItems) {
			if ($scope.poItems[key].palletId() == $palletId) {
				counter++;
				if (counter == 2) {
					return true;
				}
			}
		}
		return false;
	}

	$scope.isValidProductionDate = function($exp) {
		if ($exp.indexOf("/") == -1) {
			return false;
		}

		if ($exp.match(/[a-z]/i)) {
			return false;
		}

		var split = $exp.split("/");
		if (parseInt(split[0].trim()) > 13 || 
			parseInt(split[1].trim()) > 31 ||
			parseInt(split[2].trim()) < 1) {
			return false;
		}

		if (split[0].length != 2) {
			return false;
		}
		if (split[1].length != 2) {
			return false;
		}
		if (split[2].length != 4) {
			return false;
		}

		return true;
	}

	$scope.growlForbiddenPallets = function() {
		var growlDuration = 10000;
		var growlSize = "large";

		if ($scope.forbiddenPalletIds.length > 0) {
			var message = "The following Pallet ID's are already included in other active Receive Orders and weren't included:\n";
			for (var ii in $scope.forbiddenPalletIds) {
				if (ii > 0) {
					message = message + ", ";
				}
				message = message + $scope.forbiddenPalletIds[ii];
			}

			$.growl.warning(
				{
					title: "<strong>Duplicate Pallet ID's</strong>",
					message: message, 
					size: growlSize, 
					duration: growlDuration 
				}
			);
		}
	}

	$scope.isValidPalletId = function($fileContent) {
		$fileContent = $fileContent.replace(/(\r\n|\n|\r)/gm, "\n");

		var headerNamePoNo = "PO NO.";
		var headerNamePalletId = "PALLET ID";
		var lines = $fileContent.split("\n");
		var headers = lines[0].split(",");
		var palletIds = [];

		for (var ii = 1; ii < lines.length; ii++) {
			if (lines[ii].indexOf(",") == -1) {
				continue;
			}

			$details = lines[ii].split(",");

			for (kk = 0; kk < headers.length; kk++) { 
				if (headers[kk] == headerNamePalletId) {
					palId = $details[kk];
				} else if (headers[kk] == headerNamePoNo) {
					poNo = $details[kk];
					if (poNo != $scope.poNo) {
						break;
					} else {
						palletIds.push(palId);
					}
				}
			}
		}

		for (var ii = 1; ii < lines.length; ii++) {
			if (lines[ii].indexOf(",") == -1) {
				continue;
			}

			$details = lines[ii].split(",");

			for (kk = 0; kk < headers.length; kk++) { 
				if (headers[kk] == headerNamePalletId) {
					palId = $details[kk];
				} else if (headers[kk] == headerNamePoNo) {
					poNo = $details[kk];
					if (poNo != $scope.poNo) {
						break;
					} else {
						var ctr = 0;
						for (var key in palletIds) {
							if (palletIds[key] == palId) {
								ctr++;
							}
							if (ctr == 2) {
								$.growl.error({ title: "<strong>Error</strong>", message: 'Duplicate pallet ID of ' + palId, size: "large", duration: 5000 });
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	$scope.parseScannerText = function($fileContent) {
		$scope.forbiddenPalletIds = [];

		$scope.form.items = {};

		$fileContent = $fileContent.replace(/(\r\n|\n|\r)/gm, "\n");
		var headerNameMode = "MODE";
		var headerNameType = "TYPE";
		var headerNamePoNo = "PO NO.";
		var headerNameBarcode = "BARCODE";
		var headerNameQuantity = "QTY.";
		var headerNameLotNumber = "LOT NO.";
		var headerNamePalletId = "PALLET ID";
		var headerNameProductionDate = "PRODUCTION DATE";

		var lines = $fileContent.split("\n");

		if (lines[0].indexOf(headerNameMode) == -1 ||
			lines[0].indexOf(headerNameType) == -1 ||
			lines[0].indexOf(headerNamePoNo) == -1 ||
			lines[0].indexOf(headerNameBarcode) == -1 ||
			lines[0].indexOf(headerNameQuantity) == -1 ||
			lines[0].indexOf(headerNameLotNumber) == -1 ||
			lines[0].indexOf(headerNamePalletId) == -1 ||
			lines[0].indexOf(headerNameProductionDate) == -1) {
			$.growl.error({ title: "<strong>Error</strong>", message: 'Please upload a file with the correct header format.', size: "large", duration: 5000 });
			return;
		}

		var headers = lines[0].split(",");

		if ($scope.isValidPalletId($fileContent)) {
			return;
		}

		for (var ii = 1; ii < lines.length; ii++) {
			var mode = "";
			var type = "";
			var poNo = "";
			var barcode = "";
			var quantity = "";
			var lotNumber = "";
			var palletId = "";
			var productionDate = "";

			if (lines[ii].indexOf(",") == -1) {
				continue;
			}

			$contents = lines[ii].split(",");

			var err = 0;
					
			for (kk = 0; kk < headers.length; kk++) { 
				if (headers[kk] == headerNameMode) {
					mode = $contents[kk];
				} else if (headers[kk] == headerNameType) {
					type = $contents[kk];
				} else if (headers[kk] == headerNamePoNo) {
					poNo = $contents[kk];
					if (poNo != $scope.poNo) {
						break;
					} else  {
						palletId = tmpPalletId;
						date = tmpDate.split("/");
						if (date[0].length != 2) {
							err = 1;
						} else if (date[1].length != 2) {
							err = 1;
						} else if (date[2].length != 4) {
							err = 1;
						} else {
							productionDate = tmpDate;
						}
					}
				} else if (headers[kk] == headerNameBarcode) {
					barcode = $contents[kk];
				} else if (headers[kk] == headerNameQuantity) {
					quantity = $contents[kk];
				} else if (headers[kk] == headerNameLotNumber) {
					lotNumber = $contents[kk];
				} else if (headers[kk] == headerNamePalletId) {
					tmpPalletId = $contents[kk];
				} else if (headers[kk] == headerNameProductionDate) {
					tmpDate = $contents[kk];
				}
			}
			if (palletId != "") {
				if ($scope.hasReceivedPalletId(palletId)) {
					$scope.forbiddenPalletIds.push(palletId);
				} else if (poNo == $scope.poNo) {
					$scope.importItemFromScan(mode, type, quantity, barcode, lotNumber, palletId, productionDate);
				}
			}
		}

		if (err) {
			$.growl.error({ title: "<strong>Error</strong>", message: 'Incorrect date format, please use correct date format MM/DD/YYYY.', size: "large", duration: 5000 });
		}else if ( Object.keys($scope.form.items).length == Object.keys($scope.poItems).length ) {
			$.growl.notice({ title: "<strong>Success</strong>", message: 'File has successfully been imported.', size: "large", duration: 5000 });
		} else if ( Object.keys($scope.form.items).length == 0 ) {
			$.growl.error({ title: "<strong>Error</strong>", message: 'No item has been imported from the file. Please check the file and correct any discrepancies.', size: "large", duration: 5000 });
		}	 else {
			$.growl.warning({ title: "<strong>Warning</strong>", message: 'Not all the receive order items for Purchase Order # ' +  $scope.poNo +
			', have been imported from the file . Please check the file and correct any discrepancies.', size: "large", duration: 5000 });
		}

		$scope.growlForbiddenPallets();
	}
});