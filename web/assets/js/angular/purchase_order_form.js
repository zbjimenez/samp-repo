var inventory = angular.module( "inventory" );

inventory.value( "Items", items );
inventory.value( "Adjustments", adjustments );
inventory.value( "Supplier", supplier );
inventory.value( "Terms", terms );
inventory.value( "Currency", currency );
inventory.value( "Warehouse", warehouse );

inventory.config( function( $interpolateProvider ) {
	$interpolateProvider.startSymbol( "ANGLR_" ).endSymbol( "_ANGLR" );
});

var poCtrl = inventory.controller( "PurchaseOrderController", function( $scope, $http, Items, Adjustments, Supplier, Terms, ProductFactory, Currency, Warehouse ) {
	$scope.form = {
		supplier: Supplier,
		currency: Currency.id,
		warehouse: Warehouse,
		items: Items,
		adjustments: Adjustments,
		total: 0,
		adjustmentTotal: 0,
		subtotal: 0,
		terms: parseInt( Terms ),
		indexes: { item: -1, adjustment: -1 }
	};
	$scope.currency = Currency.currencyCode;
	
	$scope.onChangeSupplier = function() {
		$http({
			method: "GET",
			url: app.base_uri + "/admin/supplier/" + $scope.form.supplier + "/get"
		}).success( function( response ) {
			$scope.form.terms = response.terms;
		});
		
		if ( Object.keys( $scope.form.items ).length > 0 ) {
			$scope.form.items = {};
		}
	};
	
	$scope.onChangeCurrency = function() {
		$http({
			method: "GET",
			url: app.base_uri + "/admin/currency/" + $scope.form.currency + "/get"
		}).success( function( response ) {
			$scope.currency = response.currencyCode;
		});
	};

	$scope.addItem = function() {
		var index = parseFloat( angular.copy( $scope.form.indexes.item ) ) + 1;
		$scope.form.items[ index ] = {
			productId: null,
			productName: "",
			quantity: 0,
			contractNumber: null,
			unitCost: 0,
			hasProduct: false,
			noResults: false
		};
	};
	
	$scope.onSelectItemProduct = function( $product, $model, $label, index ) {
		var item = $scope.form.items[ index ];
		item.productId = $product.id;
		item.productName = $product.label;
		item.hasProduct = true;
	};

	$scope.getProducts = function( searchString, item ) {
		return ProductFactory.getProducts( searchString, $scope.form.supplier ).then( function( response ) {
			if ( response.data.products.length == 0 ) {
				item.noResults = true;
			}
			
			return response.data.products;
		});
	};
	
	$scope.removeItem = function( key ) {
		delete $scope.form.items[ key ];
		$scope.computeSubtotal();
	};
	
	$scope.computeSubtotal = function() {
		$scope.form.subtotal = 0;
		angular.forEach($scope.form.items, function( row ) {
			$scope.form.subtotal += $.roundDecimal( parseFloat( row.quantity * row.unitCost ), 2 );
		});
		
		$scope.form.subtotal = $scope.form.subtotal ? $scope.form.subtotal : 0;
	};
	
	$scope.computeAdjustmentTotal = function() {
		$scope.form.adjustmentTotal = 0;
		angular.forEach($scope.form.adjustments, function( row ) {
			$scope.form.adjustmentTotal += parseFloat( row.amount );
		});
		
		$scope.form.adjustmentTotal = $scope.form.adjustmentTotal ? $scope.form.adjustmentTotal : 0;
	};
	
	$scope.$watch( "[form.subtotal, form.adjustmentTotal]", function() {
		$scope.form.total = $scope.form.subtotal - $scope.form.adjustmentTotal;
	});
	
	$scope.addAdjustment = function() {
		var index = parseFloat( angular.copy( $scope.form.indexes.adjustment ) ) + 1;
		$scope.form.adjustments[ index ] = {
			description: null,
			amount: 0
		};
	};
	
	$scope.removeAdjustment = function( key ) {
		delete $scope.form.adjustments[ key ];
		$scope.computeAdjustmentTotal();
	};
	
	$scope.clearResults = function( item ) {
		item.noResults = false;
	}
	
	if ( !$scope.form.supplier ) {
		$scope.form.supplier = "Choose a supplier";
	}
	
	if ( !$scope.form.currency ) {
		$scope.form.currency = "Choose a currency";
	}
});