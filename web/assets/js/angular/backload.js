var inventory = angular.module( "inventory" );

inventory.value( "Items", items );

inventory.config( function( $interpolateProvider ) {
	$interpolateProvider.startSymbol( "ANGLR_" ).endSymbol( "_ANGLR" );
});

var backloadCtrl = inventory.controller( "BackloadController", function( $scope, $http, $timeout, Items ) {
	$scope.form = {
		items: Items,
		indexes: { item: -1 },
	};

	$scope.init = function() {
		$timeout( function() {
			angular.forEach( $scope.form.items, function( value, key ) {
				angular.element( ".lotnumber-" + key ).select2( "val", value.lotNumber.label );
			});
		}, 500 );
	};

	$scope.onChangeLotNumber = function( item ) {
		item.productionDate = '';

		$http({
			method: "GET",
			url: app.base_uri + "/admin/inventory/find-production-date/" + item.lotNumber.label,
		}).success( function( response ) {
			if (response.length > 0) {
				item.productionDate = response[0].productionDate;
			}
		});
	};
});