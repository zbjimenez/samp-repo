//customer angular js

var inventory = angular.module( "inventory" );

inventory.controller( "TableCollectionController", function( $scope, $compile ) {
	//action of add row button
	$scope.addRow = function() {
		var prototype = $( ".table-collection" ).attr( "data-prototype" );
		var current = $( ".table-collection tr:last" ).data( "key" );
		var rowCount = !isNaN( current ) ? current + 1 : 0;
    	var $newRow = $compile( $( $.trim( prototype.replace( /__name__/g, rowCount ) ) ) )( $scope );

    	$( ".table-collection" ).append( $newRow );
    	$newRow.attr( "data-key", rowCount );
	};
});

