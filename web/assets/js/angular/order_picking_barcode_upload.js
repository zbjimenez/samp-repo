var inventory = angular.module( "inventory" );

inventory.value( "OrderPickingId", orderPickingId );

var bcuCtrl = inventory.controller( "BarCodeUploadController", function( $scope, $http, $window, OrderPickingId ) {

	$scope.parseScannerText = function($fileContent) {
		$fileContent = $fileContent.replace(/(\r\n|\n|\r)/gm, "\n");
		var lines = $fileContent.split(/\r?\n/);
		var items = [];
		var ct = 0;
		var headerText = lines[0].split(",");

		angular.forEach( lines, function( value, key ) {
			if ( key > 0 ) {
				var rows = value.split(",");

				if (rows[0] != '' && rows[1] != '' && rows[2] != '' && rows[3] != '' && rows[4] != '' && rows[5] != '') {
					items[ct] = { 
						quantity: rows[0],
						barcode: rows[1],
						lotNumber: rows[2],
						productionDate: rows[3],
						palletId: rows[4],
						storageLocation: rows[5],
					};

					ct++;
				}
			}
		});

		$http({
		    method: "POST",
		    url: app.base_uri + "/admin/order-picking/" + OrderPickingId + "/check-barcode",
		    data: items,
		    headers: { 
		    	"Content-Type" : "application/x-www-form-urlencoded" 
		    }
		})
		.then( function( data ) {
			$window.location.reload();
		});
	};

	$scope.onClickUpload = function () {
		angular.element('#scannerFileUpload').trigger('click'); 
	};
});