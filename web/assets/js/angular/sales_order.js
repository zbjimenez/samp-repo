var inventory = angular.module( "inventory" );

inventory.value( "Items", items );
inventory.value( "Customer", customer );
inventory.value( "EWTLabel", ewtLabel );
inventory.value( "EWT", ewt );
inventory.value( "Terms", terms );
inventory.value( "Included", included );
inventory.value( "VatAmount", vatAmount );

inventory.config( function( $interpolateProvider ) {
	$interpolateProvider.startSymbol( "ANGLR_" ).endSymbol( "_ANGLR" );
});

var soCtrl = inventory.controller( "SalesOrderController", function( $scope, $timeout, $http, Items, Customer, EWTLabel, EWT, Terms, Included, QuotationFactory, VatAmount ) {
	$scope.form = {
		customer: { id: Customer, terms: parseInt( Terms ), ewt: parseFloat( EWT ), ewtLabel: EWTLabel },
		items: Items,
		subtotal: 0,
		total: 0,
		vatAmount: VatAmount,
		ewtAmount: 0,
		indexes: { item: -1 },
		included : Included
	};
	
	$scope.fetchCustomerData = function( customerId ) {
		$http({
			method: "GET",
			url: app.base_uri + "/admin/customer/" + customerId + "/get",
		}).success( function( data ) {
			if ( data.success == true ) {
				$scope.form.included.length = 0;
				$scope.form.customer.id = customerId;
				$scope.form.customer.ewt = parseFloat(data.ewt);
				$scope.form.customer.ewtLabel = data.ewtLabel;
				$scope.form.customer.terms = parseInt(data.terms);
			} else {
				$scope.form.customer.ewt = 0;
				$scope.form.customer.ewtLabel = 'None';
				$scope.form.customer.terms = 0;
			}
		});
		
		if ( Object.keys( $scope.form.items ).length > 0 ) {
			$scope.form.items = {};
		}
		
		$scope.computeSubtotal();
	};
	
	$scope.addItem = function() {
		var index = parseFloat( angular.copy( $scope.form.indexes.item ) ) + 1;
		$scope.form.items[ index ] = {
			quotation: {
				id: null,
				refNum: "",
				endDate: "",
				productName: "",
				quantity: 0,
				price: 0,
				isVatInclusive: false,
				noResults: false
			},
			quantity: 0,
			unitPrice: 0,
			hasQuotation: false
		};
	};
	
	$scope.onSelectItem = function( $item, $model, $label, index ) {
		var item = $scope.form.items[ index ];
		item.quotation = $item;
		item.quantity = $item.quantity;
		item.unitPrice = $item.price;
		item.hasQuotation = true;
		$scope.form.included.push( $item.id );
		
		$scope.computeSubtotal();
	};
	
	$scope.getQuotations = function( searchString, item ) {
		return QuotationFactory.getQuotations( searchString, $scope.form.customer.id, $scope.form.included ).then( function( response ) {
			if ( response.data.quotations.length == 0 ) {
				item.quotation.noResults = true;
			}
			
			return response.data.quotations;
		});
	};
	
	$scope.removeItem = function( key ) {
		var index = $scope.form.included.indexOf($scope.form.items[key].quotation.id);
		if (index > -1) {
			$scope.form.included.splice(index, 1);
		}
		
		delete $scope.form.items[ key ];
		$scope.computeSubtotal();
	};
	
	$scope.computeSubtotal = function() {
		$scope.form.subtotal = 0;

		$scope.form.total = 0;
		
		angular.forEach($scope.form.items, function( row ) {
			var lineTotal = parseFloat( $.roundDecimal( row.quantity * row.unitPrice ) );
			$scope.form.subtotal += lineTotal;
		});
		
		var total = parseFloat( $scope.form.subtotal ) + parseFloat( $scope.form.vatAmount );
		$scope.form.ewtAmount = $.roundDecimal( ( $scope.form.subtotal / 1.12 ) * $scope.form.customer.ewt );

		$scope.form.total = parseFloat( total ) - parseFloat( $scope.form.ewtAmount );
	};
	
	$scope.displayLineTotal = function( quantity, price ) {
		if ( quantity == undefined || isNaN( quantity ) ) {
			quantity = 0;
		}
		
		if ( price == undefined || isNaN( price ) ) {
			price = 0;
		}
		
		return $.roundDecimal( quantity * price );
	};
	
	$scope.clearResults = function( item ) {
		item.quotation.noResults = false;
	};
	
	$scope.computeSubtotal();
});


$( function() {
	$( "select.customer" ).change( function() {
		$( "#so-ctrl" ).scope().fetchCustomerData( $( this ).val() );
	});
});