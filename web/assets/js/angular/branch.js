//branch angular js

var inventory = angular.module( "inventory" );

var branchCtrl = inventory.controller( "BranchController", function( $scope, $modal, $http, $filter ) {
	$scope.branches = branchCtrl.branches;
	$scope.form = {};
	
	window.onbeforeunload = function() {
		var count = 0;
		angular.forEach($scope.branches, function( branch, key ) {
			if ( branch.isMain == true ) {
				count++;
			}
		});
		
		if ( count == 0 ) {
			return "Not setting a main branch for this customer will impact some of its transactions.";
		}
	};	
	
	$scope.add = function() {
		var modalInstance = $modal.open({
			templateUrl: app.base_uri + "/admin/branch/" + branchCtrl.customerId + "/new",
			controller: ModalInstanceCtrl,
			scope: $scope
	    });
	};
	
	$scope.edit = function( branchId, branchIndex ) {
		$scope.branchId = branchId;
		$scope.branchIndex = branchIndex;
		$scope.form = angular.copy( $scope.branches[ $scope.branchIndex ] );
		
		var modalInstance = $modal.open({
			templateUrl: app.base_uri + "/admin/branch/" + branchId + "/edit",
			controller: ModalInstanceCtrl,
			scope: $scope
	    });
	};
	
	$scope.remove = function( branchId, branchIndex ) {
		$http({
		    method: "POST",
		    url: app.base_uri + "/admin/branch/" + branchId + "/delete",
		    headers: { 
		    	"Content-Type" : "application/x-www-form-urlencoded" 
		    }
		})
		.success( function( data ) {
			if ( data.success == true ) {
				$.growl.notice({ title: "<strong>Deleted</strong>", message: data.message, size: "large", duration: 8000 });
				$scope.branches.splice( branchIndex, 1 );
			} else {
				$.growl.error({ title: "<strong>Error</strong>", message: data.message, size: "large", duration: 8000 });
			}
		});
	};
});

var ModalInstanceCtrl = function( $scope, $modalInstance, $http ) {
	$scope.postData = function( url, action ) {
		$http({
		    method: "POST",
		    url: url,
		    data: $( "form" ).serialize(),
		    headers: { 
		    	"Content-Type" : "application/x-www-form-urlencoded" 
		    }
		})
		.success( function( data ) {
			$( "button[type=submit]" ).button( "reset" );
			
			if ( data.status == "success" ) {
				$.growl.notice({ title: "<strong>Saved</strong>", message: data.message, size: "large", duration: 5000 });
				
				var branch = {
					id: data.entityId,
					isMain: $scope.form.isMain,
					isMainText: $scope.form.isMain == 1 ? "Yes" : "No",
					name: $scope.form.name,
					address: $scope.form.address,
					area: $scope.form.area,
					contactDetails: $scope.form.contactDetails
				};
				
				if ( action == "create" ) {
					$scope.branches.push(branch);
				} else {
					$scope.branches[ $scope.branchIndex ] = branch;
				}

				$scope.close();
			} else {
				$scope.message = data.message;
				$scope.errors = data.errors;
			}
		});
	};
	
	$scope.create = function() {
		$( "button[type=submit]" ).button( "loading" );
		$scope.postData( app.base_uri + "/admin/branch/create", "create" );
	};
	
	$scope.update = function() {
		$( "button[type=submit]" ).button( "loading" );
		$scope.postData( app.base_uri + "/admin/branch/" + $scope.branchId, "update" );
	};

	$scope.close = function() {
		$scope.form.id = "";
		$scope.form.isMain = false;
		$scope.form.isMainText = "";
		$scope.form.name = "";
		$scope.form.address = "";
		$scope.form.area = "";
		$scope.form.contactDetails = "";
		
		$modalInstance.dismiss( "close" );
	};
};
