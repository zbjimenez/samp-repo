//Adding or deducing inventory in product show page angular js

var inventory = angular.module( "inventory" );

var invCtrl = inventory.controller( "InventoryAdjustmentController", function( $scope, $modal, $http ) {
	$scope.designatedWarehouseNumber = invCtrl.designatedWarehouseNumber;
	$scope.totalAllocated = invCtrl.totalAllocated;
	$scope.totalOnHand = invCtrl.totalOnHand;
	$scope.form = {};
	$scope.allocations = invCtrl.allocations;
	$scope.stagings = invCtrl.stagings;
	$scope.inventories = invCtrl.inventories;
	$scope.isAuthorizedForInventory = true;
	$scope.hasMismatch = invCtrl.hasMismatch;

	$scope.transferOriginInventory = {};
	$scope.transferStorageLocationId = 0;
	$scope.transferQuantity = 0;

	$scope.detectWarehouseMismatch = function() {
		if ($scope.designatedWarehouseNumber != ''){
			$scope.hasMismatch = false;
			for (ii in $scope.inventories) {
				if ($scope.inventories[ii].storageLocation.warehouseNumber.toUpperCase() == $scope.designatedWarehouseNumber.toUpperCase()) {
					$scope.inventories[ii].isMismatched = false;
				} else {
					$scope.inventories[ii].isMismatched = true;
					$scope.hasMismatch = true;
				}
			}
		}

		if ($scope.hasMismatch == true) {
			$("#mismatch-alert").show();
		} else {
			$("#mismatch-alert").hide();
		}
	};

	$scope.getHasMisMatch = function() {
		return $scope.hasMismatch;
	}

	$scope.add = function() {
		$scope.form.quantity = 0;
		$scope.isExisting = false;
		$scope.isTracked = invCtrl.isTracked;

		$scope.form.lotNumber = "";
		$scope.form.palletId = "";
		$scope.form.productionDateForm = "";
		$scope.form.factoryDateForm = "";
		
		var modalInstance = $modal.open({
			templateUrl: app.base_uri + "/admin/product/inventory/" + invCtrl.productId + "/adjust",
			controller: ModalInstanceCtrl,
			scope: $scope
		});
	};
	
	$scope.adjust = function(inventory) {
		$scope.form = {
			quantity: 0,
			storageLocation: inventory.storageLocation.id + "",
			qtyOnHand: inventory.qtyOnHand,
			qtyOnHandPackage: inventory.qtyOnHandPackage,
			lotNumber: inventory.lotNumber,
			palletId: inventory.palletId,
			productionDate: inventory.productionDate,
			factoryDate: inventory.factoryDate,
			productionDateForm: inventory.productionDateForm,
			factoryDateForm: inventory.factoryDateForm
		};
		$scope.isExisting = true;
		$scope.isTracked = invCtrl.isTracked;
		$scope.storageLocation = inventory.storageLocation;
		
		var modalInstance = $modal.open({
			templateUrl: app.base_uri + "/admin/product/inventory/" + invCtrl.productId + "/adjust",
			controller: ModalInstanceCtrl,
			scope: $scope
		});
	};

	$scope.launchTransferModal = function(inventory) {
		$scope.transferOriginInventory = inventory;
		$scope.transferStorageLocationId = 0;
		$scope.transferQuantity = inventory.qtyOnHand;

		$('#modal-transfer-storage-location').modal('show');

		$("#select-transfer-storage-location").select2({
			ajax: {
				url: app.base_uri + "/admin/storage-location/search-from-select",
				method: 'GET',
				dataType: 'json',
				delay: 250,
				data: function (term) {
					return {
						searchString: term,
						excludedId: $scope.transferOriginInventory.storageLocation.id
					};
				},
				processResults: function (data) {
					var results = [];
					$.each(data.items, function(index, item){
						results.push({
							id: item.id,
							text: item.fullLocation
						});
					});
				  	return {
						results: results
					};
				},
				cache: true
			}
		});
	};

	$scope.hideTransferModal = function() {
		$('#modal-transfer-storage-location').modal('hide');
	}

	$scope.remove = function(inventoryId, inventoryIndex) {
		$http({
			method: "POST",
			url: app.base_uri + "/admin/inventory/" + inventoryId + "/delete",
			scope: $scope,
			headers: { 
				"Content-Type" : "application/x-www-form-urlencoded" 
			}
		})
		.success( function( data ) {
			if ( data.success == true ) {
				$scope.totalOnHand.kg = data.totalOnHand.kg;
				$scope.totalOnHand.packageCount = data.totalOnHand.packageCount;
				if (data.hasGenerated == true) {
					$.growl.notice({ title: "<strong>Purchase Order Auto-Generation</strong>", message: "A new Purchase Order for " + invCtrl.productName + " has automatically been generated.", size: "large", duration: 5000 });
				}

				$.growl.notice({ title: "<strong>Deleted</strong>", message: data.message, size: "large", duration: 5000 });
				$scope.inventories.splice( inventoryIndex, 1 );
				$scope.detectWarehouseMismatch();
			} else {
				$.growl.error({ title: "<strong>Error</strong>", message: data.message, size: "large", duration: 5000 });
			}
		});
	};

	$scope.transferStorageLocation = function() {
		$http({
			method: "POST",
			url:  app.base_uri + "/admin/inventory/transfer-storage-location/" + $scope.transferOriginInventory.id + "/" + $scope.transferStorageLocationId + "/" + $scope.transferQuantity,
			headers: { 
				"Content-Type" : "application/json; charset=utf-8" 
			}
		})
		.success( function( data ) {
			$( "button[type=submit]" ).button( "reset" );
			if ( data.success == true ) {
				$scope.hideTransferModal();
				var affectedInventories = data.affectedInventories;

				var originFound = false;
				var targetFound = false;

				for (ii in $scope.inventories) {
					if (affectedInventories.origin != null && $scope.inventories[ii].id == affectedInventories.origin.id) {
						$scope.inventories[ ii ] = affectedInventories.origin;
						originFound = true;
					}

					if (affectedInventories.target != null && $scope.inventories[ii].id == affectedInventories.target.id) {
						$scope.inventories[ ii ] = affectedInventories.target;
						targetFound = true;
					}
				}
				
				if (affectedInventories.origin != null && originFound == false) {
					$scope.inventories.push(affectedInventories.origin);
				}

				if (affectedInventories.target != null && targetFound == false) {
					$scope.inventories.push(affectedInventories.target);
				}

				$scope.detectWarehouseMismatch();

				if (affectedInventories.origin == null) {
					for (ii in $scope.inventories) {
						if ($scope.inventories[ii].id == $scope.transferOriginInventory.id) {
							$scope.inventories.splice( ii, 1 );
						}
					}
				}

				$.growl.notice({ title: "<strong>Inventory Transfer</strong>", message: data.message, size: "large", duration: 5000 });
			} else {
				$.growl.error({ title: "<strong>Inventory Transfer</strong>", message: data.message, size: "large", duration: 5000 });
			}
		});
	};
});

var ModalInstanceCtrl = function( $scope, $modalInstance, $http ) {
	$scope.postData = function() {
		$http({
			method: "POST",
			url:  app.base_uri + "/admin/product/inventory/" + invCtrl.productId,
			data: $( "form" ).serialize(),
			headers: { 
				"Content-Type" : "application/x-www-form-urlencoded" 
			}
		})
		.success( function( data ) {
			$( "button[type=submit]" ).button( "reset" );
			
			if ( data.status == "success" ) {
				$scope.totalOnHand.kg = data.totalOnHand.kg;
				$scope.totalOnHand.packageCount = data.totalOnHand.packageCount;
				var changedInventory = data.changedInventory;
				var mismatch = false;
				if ($scope.designatedWarehouseNumber != '') {
					mismatch = (changedInventory.storageLocation.warehouseNumber.toUpperCase() != $scope.designatedWarehouseNumber.toUpperCase());
				}
				changedInventory.mismatch = mismatch;
				var found = false;

				for (ii in $scope.inventories) {
					if ($scope.inventories[ii].id == changedInventory.id) {
						$scope.inventories[ ii ] = changedInventory;
						found = true;
					}
				}
				
				if (found == false) {
					$scope.inventories.push(changedInventory);
				}
				$scope.detectWarehouseMismatch();

				$.growl.notice({ title: "<strong>Updated</strong>", message: data.message, size: "large", duration: 5000 });
				if (data.hasGenerated == true) {
					$.growl.notice({ title: "<strong>Purchase Order Auto-Generation</strong>", message: "A new Purchase Order for " + invCtrl.productName + " has automatically been generated.", size: "large", duration: 5000 });
				}

				$scope.close();
			} else {
				$scope.message = data.message;
				$scope.errors = data.errors;
			}
		});
	};
	
	$scope.update = function() {
		$( "button[type=submit]" ).button( "loading" );
		$scope.postData();
	};

	$scope.close = function() {
		$modalInstance.dismiss( "close" );
	};
};

$( function() {
	$( "#inventories" ).scope().detectWarehouseMismatch();
});