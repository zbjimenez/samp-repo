var inventory = angular.module( "inventory" );

inventory.value( "Items", items );
inventory.value( "RoNo", RoNo );
inventory.value( "RoItems", roItems );
inventory.value( "BackloadNo", backloadNo );
inventory.value( "BackloadItems", backloadItems );
inventory.value( "Warehouse", Warehouse );
inventory.value( "PossibleStorageLocations", possibleStorageLocations );
inventory.value( "Type", type );

inventory.config( function( $interpolateProvider ) {
    $interpolateProvider.startSymbol( "ANGLR_" ).endSymbol( "_ANGLR" );
});

var pwCtrl = inventory.controller( "PutAwayController", function( $scope, $http, Items, Type, RoNo, RoItems, BackloadNo, BackloadItems, Warehouse, PossibleStorageLocations ) {
	$scope.type = Type;
	$scope.form = {};
	$scope.form.items = Items;
	$scope.warehouse = Warehouse;
	$scope.possibleStorageLocations = PossibleStorageLocations;

	if ($scope.type == "RO") {
		$scope.roItems = RoItems;
		$scope.roNo = RoNo;
	} else {
		$scope.backloadItems = BackloadItems;
		$scope.backloadNo = BackloadNo;
	}

	$scope.importItemFromScan = function(storageLocationFromScan, palletId) {
		var index = 0;

		for (var key in $scope.form.items) {
			index++;
		}
		
		if ($scope.type == "RO") {
			for (var key in $scope.roItems) {
				if ($scope.roItems[key].palletId == palletId) {
					$scope.form.items[ index + 1 ] = {
						isIncluded: true,
						storageLocationFromScan: storageLocationFromScan,
						palletId: palletId,
						roItem: $scope.roItems[key],
					};
					break;
				}
			}
		} else {
			for (var key in $scope.backloadItems) {
				if ($scope.backloadItems[key].palletId == palletId) {
					$scope.form.items[ index + 1 ] = {
						isIncluded: true,
						storageLocationFromScan: storageLocationFromScan,
						palletId: palletId,
						backloadItem: $scope.backloadItems[key],
					};
					break;
				}
			}
		}
	};

	$scope.isValidStorageLocation = function($exp) {
		for (var key in $scope.possibleStorageLocations) {
			if ($scope.possibleStorageLocations[key].toUpperCase() === $exp.toUpperCase()) {
				return true;
			}
		}
		return false;
	}

	$scope.isValidPalletId = function($fileContent) {
		if ($scope.type == "RO") {
			var headerNamePalletId = "PALLET ID";
			var headerNameRoNo = "RO NO.";
			var lines = $fileContent.split(/\r?\n/);
			var headers = lines[0].split(",");
			var palletIds = [];

			for (var ii = 1; ii < lines.length; ii++) {
				if (lines[ii].indexOf(",") == -1) {
					continue;
				}

				$details = lines[ii].split(",");

				for (kk = 0; kk < headers.length; kk++) {
					if (headers[kk] == headerNamePalletId) {
						palId = $details[kk];
					} else if (headers[kk] == headerNameRoNo) {
						roNo = $details[kk];
						if (roNo != $scope.roNo) {
							break;
						} else {
							palletIds.push(palId);
						}
					}
				}
			}
			
			for (var ii = 1; ii < lines.length; ii++) {
				if (lines[ii].indexOf(",") == -1) {
					continue;
				}

				$details = lines[ii].split(",");

				for (kk = 0; kk < headers.length; kk++) {
					if (headers[kk] == headerNamePalletId) {
						palId = $details[kk];
					} else if (headers[kk] == headerNameRoNo) {
						roNo = $details[kk];
						if (roNo != $scope.roNo) {
							break;
						} else {
							var ctr = 0;
							for (var key in palletIds) {
								if (palletIds[key] == palId) {
									ctr++;
								}
								if (ctr == 2) {
									$.growl.error({ title: "<strong>Error</strong>", message: 'Duplicate pallet ID of ' + palId, size: "large", duration: 5000 });
									return true;
								}
							}
						}
					}
				}
			}
			return false;
		} else {
			var headerNamePalletId = "PALLET ID";
			var headerNameBlNo = "BL NO.";
			var lines = $fileContent.split(/\r?\n/);
			var headers = lines[0].split(",");
			var palletIds = [];

			for (var ii = 1; ii < lines.length; ii++) {
				if (lines[ii].indexOf(",") == -1) {
					continue;
				}

				$details = lines[ii].split(",");


				for (kk = 0; kk < headers.length; kk++) {
					if (headers[kk] == headerNamePalletId) {
						palId = $details[kk];
					} else if (headers[kk] == headerNameBlNo) {
						backloadNo = $details[kk];
						if (backloadNo != $scope.backloadNo) {
							break;
						} else {
							palletIds.push(palId);
						}
					}
				}
			}
			
			for (var ii = 1; ii < lines.length; ii++) {
				if (lines[ii].indexOf(",") == -1) {
					continue;
				}

				$details = lines[ii].split(",");

				for (kk = 0; kk < headers.length; kk++) {
					if (headers[kk] == headerNamePalletId) {
						palId = $details[kk];
					} else if (headers[kk] == headerNameBlNo) {
						backloadNo = $details[kk];
						if (backloadNo != $scope.backloadNo) {
							break;
						} else {
							var ctr = 0;
							for (var key in palletIds) {
								if (palletIds[key] == palId) {
									ctr++;
								}
								if (ctr == 2) {
									$.growl.error({ title: "<strong>Error</strong>", message: 'Duplicate pallet ID of ' + palId, size: "large", duration: 5000 });
									return true;
								}
							}
						}
					}
				}
			}
			return false;
		}
		
	}

	$scope.parseScannerText = function($fileContent) {
		$scope.form.items = {};

		if ($scope.type == "RO") {
			var headerNameRoNo = "RO NO.";
			var headerNameStorageLocation = "STORAGE LOCATION";
			var headerNamePalletId = "PALLET ID";

			var lines = $fileContent.split(/\r?\n/);

			if (lines[0].indexOf(headerNameRoNo) == -1 ||
				lines[0].indexOf(headerNameStorageLocation) == -1 ||
				lines[0].indexOf(headerNamePalletId) == -1) {
				$.growl.error({ title: "<strong>Error</strong>", message: 'Please upload a file with the correct header format.', size: "large", duration: 5000 });
				return;
			}

			var headers = lines[0].split(",");

			if ($scope.isValidPalletId($fileContent)) {
				return;
			}

			for (var ii = 1; ii < lines.length; ii++) { 
				var roNo = "";
				var storageLocationFromScan = "";
				var palletId = "";

				if (lines[ii].indexOf(",") == -1) {
					continue;
				}

				$contents = lines[ii].split(",");

				for (kk = 0; kk < headers.length; kk++) { 
					if (headers[kk] == headerNameRoNo) {
						roNo = $contents[kk];
						if (roNo != $scope.roNo) {
							break;
						}
					} else if (headers[kk] == headerNameStorageLocation) {
						storageLocationFromScan = $contents[kk];
					} else if (headers[kk] == headerNamePalletId) {
						palletId = $contents[kk];
					}
				}

				if (roNo == $scope.roNo) {
					$scope.importItemFromScan(storageLocationFromScan, palletId);
				}
			}

			if ( Object.keys($scope.form.items).length == Object.keys($scope.roItems).length ) {
				$.growl.notice({ title: "<strong>Success</strong>", message: 'File has successfully been imported.', size: "large", duration: 5000 });
			} else if ( Object.keys($scope.form.items).length == 0 ) {
				$.growl.error({ title: "<strong>Error</strong>", message: 'No item has been imported from the file. Please check the file and correct any discrepancies.', size: "large", duration: 5000 });
			} else {
				$.growl.warning({ title: "<strong>Warning</strong>", message: 'Not all the put away items for Receive Order # ' +  $scope.roNo +
				', have been imported from the file . Please check the file and correct any discrepancies.', size: "large", duration: 5000 });
			}
		} else {
			var headerNameBlNo = "BL NO.";
			var headerNameStorageLocation = "STORAGE LOCATION";
			var headerNamePalletId = "PALLET ID";

			var lines = $fileContent.split("\n");

			if (lines[0].indexOf(headerNameBlNo) == -1 ||
				lines[0].indexOf(headerNameStorageLocation) == -1 ||
				lines[0].indexOf(headerNamePalletId) == -1) {
				$.growl.error({ title: "<strong>Error</strong>", message: 'Please upload a file with the correct header format.', size: "large", duration: 5000 });
				return;
			}


			var headers = lines[0].split(",");

			if ($scope.isValidPalletId($fileContent)) {
				return;
			}

			for (var ii = 1; ii < lines.length; ii++) {
				var backloadNo = "";
				var storageLocationFromScan = "";
				var palletId = "";

				if (lines[ii].indexOf(",") == -1) {
					continue;
				}

				$contents = lines[ii].split(",");

				for (kk = 0; kk < headers.length; kk++) { 
					if (headers[kk] == headerNameBlNo) {
						backloadNo = $contents[kk];
						if (backloadNo != $scope.backloadNo) {
							break;
						}
					} else if (headers[kk] == headerNameStorageLocation) {
						storageLocationFromScan = $contents[kk];
					} else if (headers[kk] == headerNamePalletId) {
						palletId = $contents[kk];
					}
				}

				if (backloadNo == $scope.backloadNo) {
					$scope.importItemFromScan(storageLocationFromScan, palletId);
				}
			}

			if ( Object.keys($scope.form.items).length == Object.keys($scope.backloadItems).length ) {
				$.growl.notice({ title: "<strong>Success</strong>", message: 'File has successfully been imported.', size: "large", duration: 5000 });
			} else if ( Object.keys($scope.form.items).length == 0 ) {
				$.growl.error({ title: "<strong>Error</strong>", message: 'No item has been imported from the file. Please check the file and correct any discrepancies.', size: "large", duration: 5000 });
			} else {
				$.growl.warning({ title: "<strong>Warning</strong>", message: 'Not all the put away items for Backload # ' +  $scope.backloadNo +
				', have been imported from the file . Please check the file and correct any discrepancies.', size: "large", duration: 5000 });
			}
		}
	}
});