//PO shipping angular js

var inventory = angular.module( "inventory" );

var shippingCtrl = inventory.controller( "PurchaseOrderShippingController", function( $scope, $modal, $http ) {
	$scope.shippingInformation = shippingCtrl.shippingInformation;
	$scope.form = {};
	
	$scope.edit = function() {
		$scope.form = angular.copy( $scope.shippingInformation );
		
		var modalInstance = $modal.open({
			templateUrl: app.base_uri + "/admin/purchase-order/shipping-information/" + shippingCtrl.poId + "/edit",
			controller: ModalInstanceCtrl,
			scope: $scope
	    });
	};
});

var ModalInstanceCtrl = function( $scope, $modalInstance, $http ) {
	$scope.postData = function() {
		$http({
		    method: "POST",
		    url:  app.base_uri + "/admin/purchase-order/shipping-information/" + shippingCtrl.poId,
		    data: $( "form" ).serialize(),
		    headers: { 
		    	"Content-Type" : "application/x-www-form-urlencoded" 
		    }
		})
		.success( function( data ) {
			$( "button[type=submit]" ).button( "reset" );
			
			if ( data.status == "success" ) {
				$scope.shippingInformation.cnPf = $scope.form.cnPf;
				$scope.shippingInformation.invoiceNumber = $scope.form.invoiceNumber;
				$scope.shippingInformation.containerNumber = $scope.form.containerNumber;
				$scope.shippingInformation.sealNumber = $scope.form.sealNumber;
				$scope.shippingInformation.shipmentAdvice = $scope.form.shipmentAdvice;
				
				$.growl.notice({ title: "<strong>Updated</strong>", message: data.message, size: "large", duration: 5000 });
				$scope.close();
			} else {
				$scope.message = data.message;
				$scope.errors = data.errors;
			}
		});
	};
	
	$scope.update = function() {
		$( "button[type=submit]" ).button( "loading" );
		$scope.postData();
	};

	$scope.close = function() {
		$modalInstance.dismiss( "close" );
	};
};
