//product angular js

var inventory = angular.module( "inventory" );

inventory.config( ['$httpProvider', function( $httpProvider ) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
}]);

var packagingCtrl = inventory.controller( "PackagingController", function( $scope, $modal ) {
	$scope.form = {};
	
	$scope.add = function() {
		var modalInstance = $modal.open({
			templateUrl: app.base_uri + "/admin/packaging/new",
			controller: ModalInstanceCtrl,
			scope: $scope
	    });
	};
});

var ModalInstanceCtrl = function( $scope, $modalInstance, $http ) {
	$scope.postData = function() {
		$http({
		    method: "POST",
		    url: app.base_uri + "/admin/packaging/",
		    data: $( "form[id=packaging-modal]" ).serialize(),
		    headers: { 
		    	"Content-Type" : "application/x-www-form-urlencoded"
		    }
		})
		.success( function( data ) {
			$( "button[id=ci_inventorybundle_packaging_save]" ).button( "reset" );

			if ( data.status == "success" ) {
				$( "select[id=ci_inventorybundle_product_packaging]" ).append($("<option></option>").val(data.entityId).html(data.name));
				$.growl.notice({ title: "<strong>Saved</strong>", message: data.message, size: "large", duration: 5000 });
				$scope.close();
			} else {
				$scope.message = data.message;
				$scope.errors = data.errors;
			}
		});
	};

	$scope.create = function() {
		$( "button[id=ci_inventorybundle_packaging_save]" ).button( "loading" );
		$scope.postData();
	};
	
	$scope.close = function() {
		$modalInstance.dismiss( "close" );
	};
};