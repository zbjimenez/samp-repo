var inventory = angular.module( "inventory" );

inventory.value( "Items", items );
inventory.value( "Included", included );
inventory.value( "PanelItems", panelItems );

inventory.config( function( $interpolateProvider ) {
	$interpolateProvider.startSymbol( "ANGLR_" ).endSymbol( "_ANGLR" );
});

var srCtrl = inventory.controller( "SalesReturnController", function( $scope, $filter, $http, Items, Included, PanelItems ) {
	$scope.form = {
		items: Items,
		included: Included,
		panelItems: PanelItems,
		indexes: { item: -1 },
		total: 0,
	};
	
	$scope.getFilteredPanelItems = function() {
		return $filter( "filter" )( $scope.form.panelItems, $scope.filterPanelItems );
	};
	
	$scope.addItem = function( key ) {
		var fpanelItems = $scope.getFilteredPanelItems();
		var ind = $scope.form.included.indexOf( fpanelItems[key].id );
		if ( ind == -1 ) {
			var index = parseFloat( angular.copy( $scope.form.indexes.item ) ) + 1;
			$scope.form.items[ index ] = {
				sItem: fpanelItems[key],
				quantity: fpanelItems[key].balance
			};
			
			$scope.form.included.push( fpanelItems[key].id );
			$scope.computeTotal();
		}
	};
	
	$scope.removeItem = function( key ) {
		var index = $scope.form.included.indexOf( $scope.form.items[key].sItem.id );
		if ( index > -1 ) {
			$scope.form.included.splice( index, 1 );
		}
		
		delete $scope.form.items[ key ];
		$scope.computeTotal();
	};
	
	$scope.computeTotal = function() {
		$scope.form.total = 0;
		angular.forEach($scope.form.items, function( item ) {
			var price = item.sItem.unitPrice;
			var quantity = item.quantity;
			
			if ( quantity == undefined || isNaN( quantity ) ) {
				quantity = 0;
			}
			
			if ( price == undefined || isNaN( price ) ) {
				price = 0;
			}
			
            $scope.form.total += parseFloat( $.roundDecimal( price * quantity ) );
        });
	};
	
	$scope.displayLineTotal = function( quantity, price ) {
		if ( undefined == quantity || isNaN( quantity ) ) {
			quantity = 0;
		}
		
		if ( undefined == price || isNaN( price ) ) {
			price = 0;
		}
		
		return $.roundDecimal( quantity * price );
	};
	
	$scope.isSelected = function( id ) {
		var index = $scope.form.included.indexOf( id );
		if ( index > -1 ) {
			return true;
		}
		
		return false;
	};
	
	$scope.filterPanelItems = function( row ) {
		return (
			angular.lowercase( row.product ).indexOf( angular.lowercase( $scope.itemSearchText ) || "" ) !== -1
		);
	}
	
	$scope.computeTotal();
});