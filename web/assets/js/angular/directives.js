//main directives module

var directives = angular.module( "directives", ["ui.bootstrap.tooltip"] );

//popover for delete button in tables
var deletePopover = directives.directive( "deletePopover", function( $compile ) {
	return {
		restrict: "AE",
		scope: {
			entityId: "=",
			index: "="
		},
		controller: "@",
		name: "controllerName",
		link: function( scope, element, attrs ) { 
			
			var popoverContent = $compile( '<p>This will be deleted permanently. You cannot undo this action. </p>' +
	            		'<div class="text-center">' +
	            			'<a class="btn btn-danger btn-outline" ng-click="remove(entityId, index)">' +
	            				'<span class="fa fa-trash"></span> Delete Permanently' +
	            			'</a>' + 
	        		'</div>' )( scope );
			
	        $( element ).popover({ 
	            content: popoverContent,
	            title: "Confirmation",
	            trigger: "click", 
	            placement: "right",
	            html: true
	        });
		}
	}
});

//directive for select2, just add select2 as attribute
var select2 = directives.directive( "select2", function() {
	return {
		restrict: "AE",
		link: function( scope, element, attrs ) {
			$( element ).select2();
		}
	}
});

/*
 * directive for datepicker, just add datepicker => true as attribute
 * 
 * to edit options for the datepicker, just add attributes separated by underscores to the datepicker 
 * with the prefix specified in the prefix variable here
 * 
 * date variables should be unix timestamp
 */
var datepicker = directives.directive( "datepicker", function() {
	return {
		restrict: "AE",
		link: function( scope, element, attrs ) {
			var options = {
				autoclose: true,
				todayHighlight: true
			}
			
			var prefix = "dpoption-";
			
			for( attr in attrs ) {
				var orig = attrs.$attr[ attr ];
				if ( orig != undefined && orig.slice( 0,  prefix.length ) == prefix ) {
					var option = orig.substr( prefix.length );
					var value = attrs[ attr ];
					var segments = option.split( "_" );
					option = '';
					
					for( index in segments ) {
						if ( index != 0 ) {
							option += segments[ index ].charAt(0).toUpperCase() + segments[ index ].substr( 1 );
						} else {
							option += segments[ index ];
						}
					}
					
					if ( option == "startDate" || option == "endDate" ) {
						value = new Date( value * 1000 );
					}
					
					options[ option ] = value;
				}
			}
			
			if ( options.endDate == undefined ) {
				var today = new Date();
				options.endDate = new Date( today.getFullYear() + 10, today.getMonth(), today.getDate() );
			}
			
			$( element ).datepicker( options );
		}
	}
});

//directive of the delete button in table collections
directives.directive( "deleteButton", function() {
	return {
		restrict: "E",
		template: '<a class="btn btn-default btn-sm"><span class="fa fa-trash-o"></span></a>',
		link: function( scope, element ) {
			element.bind( "click", function() {
				element.parent().parent().parent().remove();
			});
		}
	}
});

//file upload
directives.directive('onReadFile', function ($parse) {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, element, attrs) {
            var fn = $parse(attrs.onReadFile);
            
			element.on('change', function(onChangeEvent) {
				var reader = new FileReader();
                
				reader.onload = function(onLoadEvent) {
					scope.$apply(function() {
						fn(scope, {$fileContent:onLoadEvent.target.result});
					});
				};

				reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
			});
		}
	}
});

//custom ttpo directive settings
var ttpoDirectiveSettings = function directiveSettings( tooltipOrPopover ) {
	
	if ( typeof tooltipOrPopover === "undefined" ) {
		tooltipOrPopover = 'tooltip';
	}
	
	var directiveName = tooltipOrPopover;
	
	// events to handle show & hide of the tooltip or popover
	var showEvent = 'show-' + directiveName;
	var hideEvent = 'hide-' + directiveName;
	
	// set up custom triggers
	var directiveConfig = ['$tooltipProvider', function ( $tooltipProvider ) {
		var trigger = {};
		trigger[showEvent] = hideEvent;
		$tooltipProvider.setTriggers( trigger );
	}];
	
	var directiveFactory = function() {
		return ['$timeout', function( $timeout ) {
			var d = {
				name: directiveName,
				restrict: 'A',
				link: function( scope, element, attr ) {
					if ( angular.isUndefined( attr[directiveName + 'Toggle'] ) ) {
						return;
					}
					
					// set the trigger to the custom show trigger
					attr[directiveName + 'Trigger'] = showEvent;
					
					// redraw the popover when responsive UI moves its source
					var redrawPromise;
					$( window ).on( 'resize', function() {
						if ( redrawPromise ) $timeout.cancel( redrawPromise );
						redrawPromise = $timeout( function() {
							if ( !scope['tt_isOpen'] ) {
								return;
							}
							element.triggerHandler( hideEvent );
							element.triggerHandler( showEvent );
							
						}, 100 );
					});
					
					scope.$watch( attr[directiveName + 'Toggle'], function( value ) {
						if ( value ) {
							// tooltip provider will call scope.$apply, so need to get out of this digest cycle first
							$timeout( function() {
								element.triggerHandler( showEvent );
							});
						} else if ( !value ) {
							$timeout( function() {
								element.triggerHandler( hideEvent );
							});
						}
					});
				}
			};
			return d;
		}];
	};
	
	var directive = directiveFactory();
	
	var directiveSettings = {
		directiveName: directiveName,
		directive: directive,
		directiveConfig: directiveConfig
	};
	
	return directiveSettings;
}

// used to enable the -toggle attribute
var tooltipToggle = ttpoDirectiveSettings();
var popoverToggle = ttpoDirectiveSettings( "popover" );
directives.directive( tooltipToggle.directiveName, tooltipToggle.directive ).config( tooltipToggle.directiveConfig );
directives.directive( popoverToggle.directiveName, popoverToggle.directive ).config( popoverToggle.directiveConfig );