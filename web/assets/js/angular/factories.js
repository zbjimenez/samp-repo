var factories = angular.module( "factories", [] );

factories.factory( "ProductFactory", function( $http ) {
	var product = {};
	
	product.getProducts = function( searchString, supplierId ) {
		return $http({
		    method: "POST",
		    url: app.base_uri + "/admin/product/search",
		    data: {
		    	searchString: searchString,
		    	supplierId: supplierId !== undefined ? supplierId : 0
		    }
		});
	};
	
	product.getCustomerProducts = function( searchString, customerId ) {
		return $http({
		    method: "POST",
		    url: app.base_uri + "/admin/product/customer/search",
		    data: {
		    	searchString: searchString,
		    	customerId: customerId !== undefined ? customerId : 0
		    }
		});
	};
	
	return product;
});

factories.factory( "QuotationFactory", function( $http ) {
	var quotation = {};
	
	quotation.getQuotations = function( searchString, customerId, included ) {
		return $http({
		    method: "POST",
		    url: app.base_uri + "/admin/quotation/search",
		    data: {
		    	searchString: searchString,
		    	customerId: customerId !== undefined ? customerId : 0,
		    	included: included !== undefined ? included : []
		    }
		});
	};
	
	return quotation;
});

factories.factory( "StorageLocationFactory", function( $http ) {
	var storagelocation = {};
	
	storagelocation.getStorageLocations = function( searchString, warehouseId ) {
		return $http({
			method: "POST",
			url: app.base_uri + "/admin/storagelocation/search",
			data: {
				searchString: searchString,
				warehouseId: warehouseId !== undefined ? warehouseId : 0,
			}
		});
	};

	return storagelocation;
});
