var shippingControl = {
	toggleCheck: false,
	init: function() {
		$('.bulk-checkbox').change(function() {
			var numChecked = $('.bulk-checkbox:checked').length;
			if (numChecked > 0) {
				$('#bulk-indicator').text(numChecked + ' selected ');
				$('#bulk-actions').removeClass('hidden');
				$('#bulk-actions').fadeIn(500);
			} else {
				$('#bulk-indicator').text('');
				$('#bulk-actions').fadeOut(500);
			}
		});

		$('#bulk-action-for-delivery, #bulk-action-deliver, #bulk-action-fulfill').click(function() {
			shippingControl.changeStatusMultiple($(this).data('status'));
		});

		$('.check-all').click(function() {
			this.toggleCheck = !this.toggleCheck;
			$('.bulk-checkbox').prop('checked', this.toggleCheck);

			if (this.toggleCheck) {
				var numChecked = $('.bulk-checkbox:checked').length;
				$('#bulk-indicator').text(numChecked + ' selected ');
				$('#bulk-actions').removeClass('hidden');
				$('#bulk-actions').fadeIn(500);
			} else {
				$('#bulk-indicator').text('');
				$('#bulk-actions').fadeOut(500);
			}
		});

		if (sessionStorage.lastBulkActionResult) {
			shippingControl.handleBulkResults(JSON.parse(sessionStorage.lastBulkActionResult));
			sessionStorage.removeItem('lastBulkActionResult');
		}
	},

	changeStatusMultiple: function(status) {
		var ids = [];

		$('.bulk-checkbox:checked').each(function () {
			ids.push($(this).data('shipping-id'));
		});

		$.post(app.base_uri + "/admin/delivery/change-status-multiple",
		{
			status: status,
			ids: ids,
		},
		function(data, status){
			if (status == 'success') {
				if (typeof(Storage) !== "undefined") {
					sessionStorage.lastBulkActionResult = JSON.stringify(data);
				}
				
				location.reload();
			} else {
				$.growl.error({ title: "<strong>Error</strong>", message: 'An error has occured. Please try again.', size: "large", duration: 5000 });
			}
		});
	},

	handleBulkResults: function(results) {
		$('.shipping-tooltip').each(function() {
			for (key in results) {
				if (results[key].id == $(this).data('shipping-id')) {
					if (results[key].success == false) {
						$(this).removeClass('hidden');
						$(this).attr('title', results[key].message);
					}

					break;
				}
			}
		});

		$('.shipping-tooltip').tooltip();
	}
};

$( document ).ready( function() {
	shippingControl.init();
});
