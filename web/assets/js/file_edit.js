$( document ).ready( function() {
	$( '.change-image-btn' ).click( function() {
		if ( confirm( "Are you sure you want to delete the file?" ) ) {
			$( this ).parents( '.image-container' ).remove();
			return false;
		}
	});
	
	$( '.btn-delete-all' ).click( function() {
		if ( confirm( "Are you sure you want to delete all existing files?" ) ) {
			$( 'div.current-images' ).empty();
			return false;
		}
	});
});