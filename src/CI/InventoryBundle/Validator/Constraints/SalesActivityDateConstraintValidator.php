<?php

namespace CI\InventoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SalesActivityDateConstraintValidator extends ConstraintValidator
{
	private $em;
	
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}
	
	public function validate($value, Constraint $constraint)
	{
		$today = new \DateTime("now");
		
		if ($value > $today) {
			$this->context->addViolation($constraint->dateExceedMessage);
		}
		
		if (isset($value)) {
			$salesActivity = $this->em->getRepository('CIInventoryBundle:SalesActivity')->validateDate($constraint->agentId, $value);
			
			if (!empty($salesActivity)) {
				$salesActivity = $salesActivity[0];
				if ($constraint->currentId != $salesActivity['id']) {
					$this->context->addViolation($constraint->dateExistMessage);
				}
			}
		}
	}
}