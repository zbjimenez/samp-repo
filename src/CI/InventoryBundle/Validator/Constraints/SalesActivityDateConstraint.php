<?php

namespace CI\InventoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class SalesActivityDateConstraint extends Constraint
{
	public $agentId;
	
	public $currentId;
	
	public $dateExceedMessage = "Date cannot exceed today's date.";
	
	public $dateExistMessage = "Sales activity on this date already exists.";
	
	public function validatedBy()
	{
		return 'salesactivity_date_validator';
	}
}