<?php

namespace CI\InventoryBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use CI\InventoryBundle\Entity\File;
use CI\InventoryBundle\DependencyInjection\FileInterface;
use CI\CoreBundle\Helper\FileUtils;
use Oneup\UploaderBundle\Uploader\Response\EmptyResponse;

class FileUploadListener
{
	private $em;
	private $config;
	
	public function __construct(EntityManager $em, $config)
	{
		$this->em = $em;
		$this->config = $config;
	}

	public function onUpload(PostPersistEvent $event)
	{
		$response = $event->getResponse();
		
		try {
			$em = $this->em;
			$request = $event->getRequest();
			$config = $this->config[$request->get("type")];
			$allowedFileExt = $config['allowedFileExt'];
			
			$fileName = $request->files->get("file")->getClientOriginalName();
			$ext = FileUtils::getExtension($fileName);
			if (count($allowedFileExt) != 0 && !in_array(strtolower($ext), $allowedFileExt)) {
				throw new \Exception($ext . " is not an allowed extension.");
			}
			
			$wrapperClass = $config['forms']['childClass'];
			$wrapper = new $wrapperClass();
			$entity = $em->getRepository($config['entityClass'])->find($request->get("id"));
			$entity->{$config['addChildMethod']}($wrapper);
			$em->persist($entity);
			$em->persist($wrapper);
			$em->flush();
			
			$file = new File();
			$file->setFileName($request->files->get("file")->getClientOriginalName());
			$file->setHashName(uniqid("", true));
			$file->setRelPath(FileUtils::getRealClass($config['entityClass']) . "/" . $request->get("id"));
			$file->setClass($wrapperClass);
			$file->setClassID($wrapper->getId());
			$event->getFile()->move($file->getUploadRootDir(), $file->getHashName() . '.' . $file->getExtension());
			$em->persist($file);
			$em->flush($file);
			$response['status'] = true;
		} catch (\Exception $e) {
			$response['status'] = false;
			$response['error'] = $e->getMessage();
		}
	}
}