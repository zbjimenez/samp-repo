<?php
namespace CI\InventoryBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use CI\InventoryBundle\Entity\Inventory;
use CI\InventoryBundle\Entity\InventoryLog;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;

class InventoryLogSubscriber implements EventSubscriber
{
    protected $serviceContainer;
    
    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
    }
    
    public function getSubscribedEvents()
    {
         return array(
             'onFlush',
         );
    }
    
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof Inventory && !is_null($entity->getTransType())) {        
                $log = $this->generateInventoryLog($entity);
                            
                $classMetadata = $em->getClassMetadata(get_class($log));            
                $em->persist($log);
                
                $uow->computeChangeSet($classMetadata, $log);
            }
        }
    
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Inventory && !is_null($entity->getTransType())) {        
                $log = $this->generateInventoryLog($entity);
                            
                $classMetadata = $em->getClassMetadata(get_class($log));            
                $em->persist($log);
                
                $uow->computeChangeSet($classMetadata, $log);
            }
        }
    }
    
    protected function generateInventoryLog(Inventory $entity)
    {
        $user = $this->serviceContainer->get('security.context')->getToken()->getUser();
        
        $log = new InventoryLog();
        $log->setTransType($entity->getTransType())
            ->setInventory($entity)
            ->setQuantity($entity->getQtyChange())
            ->setBalance($entity->getQtyOnHand())
            ->setMemo($entity->getMemo())
            ->setCreatedBy($user)
        ;

        return $log;
    }
}