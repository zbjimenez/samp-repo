<?php
namespace CI\InventoryBundle\Helper;

use Twig_Extension;
use Twig_Function_Method;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FileHandlerExtension extends Twig_Extension
{
	protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
	
    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return array(
            'FHSingle' => new Twig_Function_Method(
                $this,
                'getSingleFile'//,
//                 array('pre_escape' => 'html', 'is_safe' => array('html'))
            ),
        	
        	'FHMulti' => new Twig_Function_Method($this,'getMultiFile')
        );
    }

    public function getSingleFile($entity)
    {
    	$fh = $this->container->get("ci.file_handler.service");
    	$file = $fh->getSingleFile($entity);
    	
        return $file;
    }
    
    public function getMultiFile($class, $ids)
    {
    	$fh = $this->container->get("ci.file_handler.service");
    	$file = $fh->getMultiFile($class, $ids);
    	 
    	return $file;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'ci_file_handler_twig';
    }
}
