<?php
namespace CI\InventoryBundle\Helper;

use TCPDF;

class CustomTCPDF extends TCPDF
{
	const FORM = 2;
	const REPORT = 1;
	const BLANK = 3;
	const QUOTATION = 4;
	const LANDSCAPE = 'L';
	const NONREPORT_LANDSCAPE = 'NRL';
	const CUSTOM_HEADER = 11;

	protected $title;
	protected $formNo;
	protected $type;
	protected $options;
	protected $tableHeaders;
	protected $params;
	protected $isApproved;

	public function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false, $pdfa=false)
	{
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
		$this->SetMargins(10, 50, 10, true);
		$this->SetHeaderMargin(10);
		$this->SetFooterMargin(15);
		$this->SetFont('helvetica', '', 10);
	}

	public function setType($type, $options = null)
	{
		switch ($type) {
			case CustomTCPDF::FORM:
				$this->type = self::FORM;
				$this->SetMargins(10, 30, 10, true);
				break;
			case self::LANDSCAPE:
				$this->type = self::LANDSCAPE;
				$this->setPageOrientation(self::LANDSCAPE);
				$this->SetMargins(10, 30, 10, true);
				break;
			case self::NONREPORT_LANDSCAPE:
				$this->type = self::NONREPORT_LANDSCAPE;
				$this->setPageOrientation(self::LANDSCAPE);
				$this->SetMargins(10, 30, 10, true);
				break;
			case self::BLANK:
				$this->type = self::BLANK;
				$this->SetMargins(10, 30, 10, true);
				break;
			case CustomTCPDF::QUOTATION:
				$this->type = self::QUOTATION;
				$this->SetMargins(10, 30, 10, true);
				break;
			default:
				$this->type = CustomTCPDF::REPORT;
				$this->SetMargins(10, 35, 10, true);
		}
	}

	public function setFormName($formName)
	{
		$this->formName = $formName;
	}

	public function setFormNo($formNo)
	{
		$this->formNo = $formNo;
	}

	public function setPageFormat($format, $orientation = 'P')
	{
		parent::setPageFormat($format, $orientation);
	}

	public function setTableHeaders($tableHeaders)
	{
		$this->tableHeaders = $tableHeaders;
	}

	public function setParams($params)
	{
		$this->params = $params;
	}

	public function setIsApproved($isApproved)
	{
		$this->isApproved = $isApproved;
	}

	public function Header()
	{
		if (self::FORM === $this->type) {
			$this->Image('../web/assets/img/comquest-logo.png', 56, 10, 100, 21);
		} elseif (self::LANDSCAPE === $this->type) {
			$this->Image('../web/assets/img/comquest-logo.png', 10, 6, 100, 21);
			if ($this->isApproved) {
				$this->Image('../web/img/watermark.png', 60, 70);
			}
			$this->SetFont('helvetica', 'B', 17);
			$this->Write(0, $this->formName, '', false, 'R', true);
			$this->SetFont('helvetica', '', 10);
			$this->Text(225, $this->getY(), 'Report Date: '.date('M d, Y g:i:s a'), false, false, true, 0, 1);
			$this->Text(225, $this->getY(), 'Page: ' . $this->PageNo().' of '.$this->getAliasNbPages());
		} elseif (self::NONREPORT_LANDSCAPE === $this->type) {
			$this->Image('../web/assets/img/comquest-logo.png', 10, 6, 100, 21);
		} elseif (self::BLANK === $this->type) {
			$this->SetAutoPageBreak(false, 0);
			// $this->Image('../web/img/deliveryreceipt.png', 0, 0, 240);
			//$this->Image('../web/img/new_si.png', 0, 0, 212.725);
			// $this->Image('../web/img/salesinvoice.png', 0, 0, 235, 296);
		} elseif (self::QUOTATION === $this->type) {
			$this->Image('../web/assets/img/comquest-logo.png', 56, 10, 100, 21);
			$this->Image('../web/img/watermark.png', 20, 80);
		} else {
			$this->Image('../web/assets/img/comquest-logo.png', 10, 10, 90, 18);
			$this->SetFont('helvetica', 'B', 17);
			$this->Write(0, $this->formName, '', false, 'R', true);
			$this->SetFont('helvetica', '', 10);
			$this->Text(138, $this->getY(), 'Report Date: '.date('M d, Y g:i:s a'), false, false, true, 0, 1);
			$this->Text(138, $this->getY(), 'Page: ' . $this->PageNo().' of '.$this->getAliasNbPages());
		}

		$this->SetTextColor(0, 0, 0);
 		$this->SetFont('helvetica', 'B', 8);

		//render search parameters
 		if (isset($this->params)) {
			$this->Ln();
			$this->Ln();
 			foreach ($this->params as $param => $value) {
 				$this->write(5, $param . ': ' . $value, '', false, 'L', true);
 			}
 			$x = 41 + (count($this->params)*5);
			$this->SetMargins(10, $x, 10, true);
 		}

		//render table headers
 		if (isset($this->tableHeaders)) {
 			$this->Ln();
 			foreach ($this->tableHeaders as $th) {
 				$this->Cell($th['width'], 5, $th['header'], 1, 0, 'C', false, '', 0, false, 'T', 'C');
 			}
 		}
	}

	public function Footer()
	{
		if ($this->type != self::BLANK) {
			$this->SetFont('helvetica', 'B', 8);
			$this->Write(0, 'UNIT 2406, UNIONBANK PLAZA, MERALCO AVE. CORNER ONYX ROAD, ORTIGAS CENTER, PASIG CITY 1605 PHILIPPINES', '', false, 'C', true);
			$this->Write(0, 'TEL NOS. (632) 706-9966 * FAX NO. (632) 706-0239', '', false, 'C', true);
			$this->Write(0, 'EMAIL: genoffice@comquest.com.ph; sales@comquest.com.ph', '', false, 'C');
		}
	}

	public function fits($h)
	{
		return $this->GetY() + $h <= $this->PageBreakTrigger;
	}
}
