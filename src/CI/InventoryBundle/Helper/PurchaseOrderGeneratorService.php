<?php

namespace CI\InventoryBundle\Helper;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use CI\InventoryBundle\Entity\PurchaseOrder;
use CI\InventoryBundle\Entity\PurchaseOrderItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PurchaseOrderGeneratorService
{
	protected $container;
	protected $em;

	public function __construct(ContainerInterface $container, EntityManager $em)
	{
		$this->container = $container;
		$this->em = $em;
	}

	public function checkThreshold($product)
	{
		$ordered = $this->em->getRepository('CIInventoryBundle:PurchaseOrderItem')->getExistingItemsQuantityWithProduct($product);

		if ($product->getReorderThreshold() > 0 && $product->getReorderQuantity() > 0) {
			$notYetOrdered = ($ordered < $product->getReorderQuantity());
			$reachedThreshold = ($product->getTotalOnHand() <= $product->getReorderThreshold());

			return $notYetOrdered && $reachedThreshold;
		} else {
			return false;
		}
	}

	public function generate($product)
	{
		$purchaseOrder = new PurchaseOrder();
		$items = new \Doctrine\Common\Collections\ArrayCollection();
		$item  = new PurchaseOrderItem();

		$ordered = $this->em->getRepository('CIInventoryBundle:PurchaseOrderItem')->getExistingItemsQuantityWithProduct($product);
		$quantity = $product->getReorderQuantity() - $ordered;
		if ($quantity < 1) {
			return;
		}

		$purchaseOrder->setStatus(PurchaseOrder::STATUS_DRAFT);

		$purchaseOrder->setPoId($this->container->get('ci.purchaseorder.model')->generateNumber());

		$purchaseOrder->setSupplier($product->getSupplier());

		$currency = $this->container->get('ci.currency.model')->getDefaultCurrency();
		$purchaseOrder->setCurrency($currency);

		if (!is_null($product->getDesignatedWarehouseNumber())) {
			$warehouse = $this->container->get('ci.warehouse.model')->getWarehouseByNumber($product->getDesignatedWarehouseNumber());
			$purchaseOrder->setWarehouse($warehouse);
		} else {
			$warehouse = $this->container->get('ci.warehouse.model')->getDefaultWarehouse();
			$purchaseOrder->setWarehouse($warehouse);
		}

		$item->setQuantity($quantity);
		$item->setProduct($product);
		$item->setUnitCost(1);
		$item->setLineTotal($quantity);

		$purchaseOrder->setTotal($quantity);

		$items[] = $item;

		$purchaseOrder->setItems($items);

		$this->em->persist($purchaseOrder);

		$this->container->get('session')->getFlashBag()->add('info', 'A new Purchase Order for ' . $product->getName() . ' has automatically been generated.');
	}
}