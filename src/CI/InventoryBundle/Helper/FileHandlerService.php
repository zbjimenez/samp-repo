<?php

namespace CI\InventoryBundle\Helper;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Form;
use CI\InventoryBundle\Entity\File;
use CI\CoreBundle\Helper\FileUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FileHandlerService
{
    protected $container;
	protected $em;
	
	
	public function __construct(ContainerInterface $container, EntityManager $em)
	{
	    $this->container = $container;
		$this->em = $em;
	}
	
	public function processSingleFile(Form $form, $entity, $formNamePrefix = '')
	{
	    $newFile = $form[$formNamePrefix . 'file']->getData();
	    
	    $className = get_class($entity);

	    // if the previous file was marked for deletion
	    // then delete the old file if it exists
		if ($form[$formNamePrefix . 'changeImage']->getData()) {
		    $previousFile = $this->em->getRepository("CIInventoryBundle:File")->findOneBy(array("class" => $className, "classID" => $entity->getId()));
		    if (!is_null($previousFile)) {
                $this->em->remove($previousFile);
		    }
		}

	    // if there's a new file uploaded
	    if (!is_null($newFile)) {

	        // get the configs
	        $config = $this->container->getParameter('ci_inventory.single_upload')[$className];
	        $allowedFileExt = $config['allowedFileExt'];
	        
		    if (count($allowedFileExt) == 0 || in_array(strtolower(FileUtils::getExtension($newFile->getClientOriginalName())), $allowedFileExt)) {
		        $file = new File();
		        $file->setClass($className);
		        $file->setClassID($entity->getId());
		        $file->setRelPath(FileUtils::getRealClass($entity) . "/" . $entity->getId());
		        $file->setFileName($newFile->getClientOriginalName());
		        $file->setHashName(uniqid("", true));
		        $this->em->persist($file);
		        
		        // move the file
		        $newFile->move($file->getUploadRootDir(), $file->getHashName() . '.' . $file->getExtension()); 	
		    } else {
		        throw new \Exception(FileUtils::getExtension($form[$formNamePrefix . "file"]->getData()->getClientOriginalName()) . " is not an allowed file.");
	        }
	    }
	}
	
	/**
	 * Used by the twig extension function
	 */
	public function getSingleFile($entity)
	{
		$file = $this->em->getRepository("CIInventoryBundle:File")->findOneBy(array("class" => get_class($entity), "classID" => $entity->getId()));
				
		return $file;
	}
	
	public function removeSingleFile($entity)
	{
		$file = $this->em->getRepository("CIInventoryBundle:File")->findOneBy(array("class" => get_class($entity), "classID" => $entity->getId()));
		if ($file) {		
		    $this->em->remove($file);
		}
	}
	
	/**
	 * Used by the twig extension function
	 */
	public function getMultiFile($class, $ids)
	{
		$files = $this->em->getRepository("CIInventoryBundle:File")->findMulti($class, $ids);
		$result = array();
		
		foreach ($files as $file) {
			$result[$file->getClassId()] = $file;
		}
	
		return $result;
	}
	
	public function removeMultiFile($class, $idsToRemove)
	{
		$files = $this->em->getRepository("CIInventoryBundle:File")->findMulti($class, $idsToRemove);
		foreach ($files as $file) {
			$this->em->remove($file);
		}
	}
}