<?php

namespace CI\InventoryBundle\Helper;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use CI\InventoryBundle\DependencyInjection\InventoryServiceEntityInterface;
use CI\InventoryBundle\DependencyInjection\InventoryServiceItemInterface;
use CI\InventoryBundle\Entity\WarehouseInventory;
use CI\InventoryBundle\Entity\Inventory;
use CI\InventoryBundle\Entity\InventoryLog;
use Symfony\Component\DependencyInjection\ContainerInterface;

class InventoryService
{
	protected $container;
	protected $em;

	public $hasGenerated = false;

	public function __construct(ContainerInterface $container, EntityManager $em)
	{
		$this->container = $container;
		$this->em = $em;
	}

	public function generatePurchaseOrderIfNeeded($product)
	{
		$purchaseOrderGenerator = $this->container->get('ci.purchase.order.generator.service');

		if ($purchaseOrderGenerator->checkThreshold($product)) {
			$purchaseOrderGenerator->generate($product);
			$this->hasGenerated = true;
		}
	}

	public function getHasGenerated()
	{
		return $this->hasGenerated;
	}

	public function isPalletIdValid($product, $storageLocation, $lotNumber = null, $palletId) 
	{
		if ($this->getExistingInventory($product, $storageLocation, $lotNumber, $palletId)) {
			return true;
		} else {
			return $this->em->getRepository('CIInventoryBundle:Inventory')->isPalletIdValid($palletId, $storageLocation->getWarehouse());
		}
	}

	public function isNewPalletIdValid($warehouse, $palletId, $product = null) 
	{
		return $this->em->getRepository('CIInventoryBundle:Inventory')->isPalletIdValid($palletId, $warehouse, $product);
	}

	public function getExistingInventory($product, $storageLocation, $lotNumber = null, $palletId = null) 
	{
		foreach ($product->getInventories() as $inv) {
			$matched = false;
			if ($inv->getStorageLocation()->getId() == $storageLocation->getId()) {
				$matched = true;
				if ($lotNumber && $palletId) {
					$matched = $inv->getLotNumber() == $lotNumber && $matched = $inv->getPalletId() == $palletId;
				} 
				if (!$lotNumber) {
					$matched = ($inv->getLotNumber()) ? false : true;
				} 
				if (!$palletId) {
					$matched = ($inv->getPalletId()) ? false : true;
				}
	
				if ($matched == true) {
					return $inv;
				}
			}
		}
		return null;
	}

	/**
	* Retrieve warehouse inventory if existing, return new WarehouseInventory when not existing
	*/
	public function getWarehouseInventory($product, $warehouse)
	{
		foreach ($product->getWarehouseInventories() as $inv) {
			if ($inv->getWarehouse()->getId() == $warehouse->getId()) {
				return $inv;
			}
		}

		$warehouseInventory = new WarehouseInventory();
		$warehouseInventory->setProduct($product);
		$warehouseInventory->setWarehouse($warehouse);
		
		return $warehouseInventory;
	}

	public function getInventory($product, $storageLocation, $lotNumber = null, $palletId = null)
	{
		$inventory = $this->getExistingInventory($product, $storageLocation, $lotNumber, $palletId);

		if (is_null($inventory)) {
			$inventory = new Inventory();
			$inventory->setProduct($product);
			$inventory->setStorageLocation($storageLocation);
			if ($lotNumber) {
				$inventory->setLotNumber($lotNumber);
			}
			if ($palletId) {
				$inventory->setPalletId($palletId);
			}
		}
		
		return $inventory;
	}
	
	/**
	 * Adds inventory stock for an item
	 */
	public function addItemStock($transType, $product, $storageLocation, $quantityChange, $memo = null, $lotNumber = null, $palletId = null, $productionDate = null, $generate = true)
	{
		$inventory = $this->getInventory($product, $storageLocation, $lotNumber, $palletId);

		if ($productionDate) {
			$inventory->setProductionDate($productionDate);
		}

		$inventory->setTransType($transType)
			->setQtyOnHand($inventory->getQtyOnHand() + $quantityChange)
			->setMemo($memo);

		if ($generate) {	
			$this->generatePurchaseOrderIfNeeded($product);
		}

		if (!$product->getInventories()->contains($inventory)) {
			$product->addInventory($inventory);
		}
		
		$product->updateTotalOnHand();

		$this->em->persist($product);
	}
	
	/**
	 * Deducts inventory stock for an item
	 */
	public function deductItemStock($transType, $product, $storageLocation, $quantityChange, $memo = null, $lotNumber = null, $palletId = null, $productionDate = null, $generate = true)
	{
		$inventory = $this->getInventory($product, $storageLocation, $lotNumber, $palletId);

		if ($productionDate) {
			$inventory->setProductionDate($productionDate);
		}

		$inventory->setTransType($transType)
			->setQtyOnHand($inventory->getQtyOnHand() + ($quantityChange * -1))
			->setMemo($memo);

		$purchaseOrderGenerator = $this->container->get('ci.purchase.order.generator.service');
		
		if ($generate) {	
			$this->generatePurchaseOrderIfNeeded($product);
		}
		
		if (!$product->getInventories()->contains($inventory)) {
			$product->addInventory($inventory);
		}

		$product->updateTotalOnHand();

		$this->em->persist($product);
	}

	/**
	 * Deletes inventory stock for an item
	 */
	public function deleteItemStock($product, $quantityChange) {
		$product->setTotalOnHand($product->getTotalOnHand() - $quantityChange);
		$this->em->persist($product);
	}
	
	/**
	 * Adds inventory stock for multiple items
	 */
	public function addMultipleItemStock($transType, InventoryServiceEntityInterface $entity, $memo = null)
	{
		$products = new ArrayCollection();
		$idsAdded = array();
		foreach ($entity->getISItems() as $item) {
			if (!in_array($item->getISProduct()->getId(), $idsAdded)) {
				$products[] = $item->getISProduct();
				$idsAdded[] = $item->getISProduct()->getId();
			}

			$lotNumber = null;
			if (method_exists($item, 'getLotNumber')) {
				$lotNumber = $item->getLotNumber();
			}

			$palletId = null;
			if (method_exists($item, 'getPalletId')) {
				$palletId = $item->getPalletId();
			}

			$productionDate = null;
			if (method_exists($item, 'getProductionDate')) {
				$productionDate = $item->getProductionDate();
			}

			$this->addItemStock($transType, $item->getISProduct(), $item->getStorageLocation(), $item->getISQuantity(), $memo, $lotNumber, $palletId, $productionDate, false);
		}

		foreach ($products as $product) {
			$this->generatePurchaseOrderIfNeeded($product);
		}
	}
	
	/**
	 * Deducts inventory stock for multiple items
	 */
	public function deductMultipleItemStock($transType, InventoryServiceEntityInterface $entity, $memo = null)
	{
		$products = new ArrayCollection();
		$idsAdded = array();
		foreach ($entity->getISItems() as $item) {
			if (!in_array($item->getISProduct()->getId(), $idsAdded)) {
				$products[] = $item->getISProduct();
				$idsAdded[] = $item->getISProduct()->getId();
			}

			$lotNumber = null;
			if (method_exists($item, 'getLotNumber')) {
				$lotNumber = $item->getLotNumber();
			}

			$palletId = null;
			if (method_exists($item, 'getPalletId')) {
				$palletId = $item->getPalletId();
			}

			$productionDate = null;
			if (method_exists($item, 'getProductionDate')) {
				$productionDate = $item->getProductionDate();
			}

			$this->deductItemStock($transType, $item->getISProduct(), $item->getStorageLocation(), $item->getISQuantity(), $memo, $lotNumber, $palletId, $productionDate, false);
		}

		foreach ($products as $product) {
			$this->generatePurchaseOrderIfNeeded($product);
		}
	}
	
	/**
	 * Allocate stocks in warehouseInventory.
	 * @param object $entity
	 */
	public function addAllocation(InventoryServiceEntityInterface $entity)
	{
		foreach ($entity->getISItems() as $item) {
			$product = $item->getISProduct();
			$warehouse = $entity->getWarehouse();

			$warehouseInventory = $this->getWarehouseInventory($product, $warehouse);

			$warehouseInventory->setQtyAllocated($warehouseInventory->getQtyAllocated() + $item->getISQuantity());

			$product->updateTotalAllocated();

			$this->em->persist($warehouseInventory);
		}
	}
	
	/**
	 * Deallocate stocks in warehouseInventory.
	 * @param object $entity
	 */
	public function deductAllocation(InventoryServiceEntityInterface $entity)
	{
		foreach ($entity->getISItems() as $item) {
			$product = $item->getISProduct();
			$warehouse = $entity->getWarehouse();

			$warehouseInventory = $this->getWarehouseInventory($product, $warehouse);

			$warehouseInventory->setQtyAllocated($warehouseInventory->getQtyAllocated() - $item->getISQuantity());

			$product->updateTotalAllocated();
			
			$this->em->persist($warehouseInventory);
		}
	}

	/**
	 * Stage stocks in warehouseInventory.
	 * @param object $entity
	 */
	public function addStaging(InventoryServiceEntityInterface $entity)
	{
		$warehouse = $entity->getWarehouse();
		$productQtyStaged = array();
		$storedIds = array();
		foreach ($entity->getISItems() as $item) {
			$product = $item->getISProduct();
			if (!in_array($product->getId(), $storedIds)) {
				$productQtyStaged[$product->getId()] = array(
					'product' => $product,
					'quantity' => $item->getISQuantity()
				);
				$storedIds[] = $product->getId();
			} else {
				$productQtyStaged[$product->getId()]['quantity'] = $productQtyStaged[$product->getId()]['quantity'] + $item->getISQuantity();
			}
		}

		foreach ($productQtyStaged as $staged) {
			$product = $staged['product'];
			$warehouseInventory = $this->getWarehouseInventory($product, $warehouse);

			$warehouseInventory->setQtyStaged($warehouseInventory->getQtyStaged() + $staged['quantity']);
			
			if (!$product->getWarehouseInventories()->contains($warehouseInventory)) {
				$product->addWarehouseInventory($warehouseInventory);
			}

			$product->updateTotalStaged();
			$product->setUpdatedAt(new \DateTime());
			$this->em->persist($product);
		}
	}
	
	/**
	 * Unstage stocks in warehouseInventory.
	 * @param object $entity
	 */
	public function deductStaging(InventoryServiceEntityInterface $entity)
	{
		$warehouse = $entity->getWarehouse();
		$productQtyStaged = array();
		$storedIds = array();
		foreach ($entity->getISItems() as $item) {
			$product = $item->getISProduct();
			if (!in_array($product->getId(), $storedIds)) {
				$productQtyStaged[$product->getId()] = array(
					'product' => $product,
					'quantity' => $item->getISQuantity()
				);
				$storedIds[] = $product->getId();
			} else {
				$productQtyStaged[$product->getId()]['quantity'] = $productQtyStaged[$product->getId()]['quantity'] + $item->getISQuantity();
			}
		}

		foreach ($productQtyStaged as $staged) {
			$product = $staged['product'];
			$warehouseInventory = $this->getWarehouseInventory($product, $warehouse);

			$warehouseInventory->setQtyStaged($warehouseInventory->getQtyStaged() - $staged['quantity']);
			
			if (!$product->getWarehouseInventories()->contains($warehouseInventory)) {
				$product->addWarehouseInventory($warehouseInventory);
			}

			$product->updateTotalStaged();
			$this->em->persist($product);
		}
	}

	/**
	 * Adds inventory staging for an item
	 */
	public function addItemStaging($transType, $product, $warehouse, $quantityChange, $memo = null)
	{
		$warehouseInventory = $this->getWarehouseInventory($product, $warehouse);

		$warehouseInventory->setTransType($transType)
			->setQtyStaged($warehouseInventory->getQtyStaged() + $quantityChange);

		$product->updateTotalStaged();

		if (!$product->getWarehouseInventories()->contains($warehouseInventory)) {
			$product->addWarehouseInventory($warehouseInventory);
		}
		
		$this->em->persist($product);
	}
	
	/**
	 * Deducts inventory stock for an item
	 */
	public function deductItemStaging($transType, $product, $warehouse, $quantityChange, $memo = null)
	{
		$warehouseInventory = $this->getWarehouseInventory($product, $warehouse);

		$warehouseInventory->setTransType($transType)
			->setQtyStaged($warehouseInventory->getQtyStaged() + ($quantityChange * -1));

		$product->updateTotalStaged();
		
		if (!$product->getWarehouseInventories()->contains($warehouseInventory)) {
			$product->addWarehouseInventory($warehouseInventory);
		}

		$this->em->persist($product);
	}

	/*
	* Transfer product from a storage location where it resides, to another storage location. When palletId is given, 
	* the quantity that is transfered is equal to the quantity of the previous inventory and the previous inventory will
	* be deleted because Pallet ID's of each inventory have to be unique.
	*
	* Returns an associative array of affected inventories.
	*/
	public function transferStorageLocation($originInventory, $targetStorageLocation, $quantity = 0) 
	{
		$affectedInventories = array(
			'origin' => null,
			'target' => null
		);

		if ($quantity == 0 && !$originInventory->getPalletId()) {
			return $affectedInventories;
		} else if ($originInventory->getStorageLocation()->getId() != $targetStorageLocation->getId()) {
			$targetInventory = $this->getInventory($originInventory->getProduct(), $targetStorageLocation, null, $originInventory->getPalletId() ? $originInventory->getPalletId() : null);
			$targetInventory->setLotNumber($originInventory->getLotNumber());
			$targetInventory->setProductionDate($originInventory->getProductionDate());
			$targetInventory->setFactoryDate($originInventory->getFactoryDate());

			if ($originInventory->getPalletId()) {
				$quantity = $originInventory->getQtyOnHand();
				$targetInventory->setPalletId($originInventory->getPalletId());
				$this->em->remove($originInventory);
			} else {
				$originInventory->setTransType(InventoryLog::TRANS_TRANSFER);
				$originInventory->setQtyOnHand($originInventory->getQtyOnHand() - $quantity);
				$affectedInventories['origin'] = $originInventory;
				$this->em->persist($originInventory);
			}

			$targetInventory->setTransType(InventoryLog::TRANS_TRANSFER);
			$targetInventory->setQtyOnHand($targetInventory->getQtyOnHand() + $quantity);
			$this->em->persist($targetInventory);
			$affectedInventories['target'] = $targetInventory;
			return $affectedInventories;
		} else {
			return $affectedInventories;
		}
	}
}