<?php

namespace CI\InventoryBundle\Twig;

use Twig_Extension;
use Twig_SimpleFunction;
use Twig_SimpleFilter;
use Twig_Filter_Method;

use CI\InventoryBundle\Entity\Shipping;

class RenderTagsExtension extends Twig_Extension
{
	/**
	 * {@inheritDoc}
	 */
	public function getFunctions()
	{
		$options = array('pre_escape' => 'html', 'is_safe' => array('html'));
		
		return array(
			new Twig_SimpleFunction('renderTags', array($this, 'renderTagsFunction'), $options),
			new Twig_SimpleFunction('renderStatusTag', array($this, 'renderStatusTagFunction'), $options),
			new Twig_SimpleFunction('renderActiveTag', array($this, 'renderActiveTagFunction'), $options),
			new Twig_SimpleFunction('renderNewTag', array($this, 'renderNewTagFunction'), $options),
			new Twig_SimpleFunction('renderUpdatedTag', array($this, 'renderNewTagFunction'), $options),
			new Twig_SimpleFunction('renderShippingTag', array($this, 'renderShippingTagFunction'), $options),
		);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getFilters()
	{
		$options = array('pre_escape' => 'html', 'is_safe' => array('html'));
		
		return array(
			new Twig_SimpleFilter('renderTags', array($this, 'renderTagsFunction'), $options),
			new Twig_SimpleFilter('renderStatusTag', array($this, 'renderStatusTagFunction'), $options),
			new Twig_SimpleFilter('renderActiveTag', array($this, 'renderActiveTagFunction'), $options),
			new Twig_SimpleFilter('renderNewTag', array($this, 'renderNewTagFunction'), $options),
			new Twig_SimpleFilter('renderUpdatedTag', array($this, 'renderUpdatedTagFunction'), $options),
			new Twig_SimpleFilter('renderShippingTag', array($this, 'renderShippingTagFunction'), $options),
		);
	}
	
	public function renderTagsFunction($data, $class = null)
	{
		if (is_object($data)) {
			$html = '';
			if (method_exists($data, 'getStatus')) {
				if ($data instanceof Shipping) {
					$html = $this->renderShippingTagFunction($data);
				} else {
					$result = $this->translateStatus(call_user_func(array($data, 'getStatus')));
					$html = $this->generateHTML($result["class"], $result["status"], $class);
				}
			} else if (method_exists($data, 'getActive')) {
				if (!$data->getActive()) {
					$html = $this->generateHTML('danger', 'Inactive', $class);
				}
			}
			
			if (method_exists($data, 'getCreatedAt')) {
				if ($data->getCreatedAt()->format('m/d/Y') == date('m/d/Y', strtotime('now'))) {
					$html .=  $this->generateHTML('warning', 'New', $class);
				}
			}
			
			if (method_exists($data, 'getUpdatedAt') && method_exists($data, 'getCreatedAt')) {
				if (
					!is_null($data->getUpdatedAt()) &&
					$data->getUpdatedAt()->format('m/d/Y') != $data->getCreatedAt()->format('m/d/Y') &&
					$data->getUpdatedAt()->format('m/d/Y') == date('m/d/Y', strtotime('now'))
				) {
					$html .= $this->generateHTML('warning', 'Updated', $class);
				}
			}
			
			return $html;
		} else {
			return $this->generateHTML('default', 'ERROR');
		}
	}
	
	public function renderStatusTagFunction($data, $class = null)
	{
		if (is_object($data) && method_exists($data, 'getStatus')) {
			$result = $this->translateStatus(call_user_func(array($data, 'getStatus')));
		} else if (!is_object($data)) {
			$result = $this->translateStatus($data);
		} else {
			$result = $this->translateStatus(0);
		}
		
		return $this->generateHTML($result["class"], $result["status"], $class);
	}
	
	public function renderActiveTagFunction($data, $class = null)
	{
		if (is_object($data) && method_exists($data, 'getActive')) {
			if (!$data->getActive()) {
				return $this->generateHTML('danger', 'Inactive', $class);
			}
		} else {
			return $this->generateHTML('default', 'ERROR');
		}
	}
	
	public function renderNewTagFunction($data, $class = null)
	{
		if (is_object($data) && method_exists($data, 'getCreatedAt')) {
			if ($data->getCreatedAt()->format('m/d/Y') == date('m/d/Y', strtotime('now'))) {
				return $this->generateHTML('warning', 'New', $class);
			}
		} else {
			return $this->generateHTML('default', 'ERROR');
		}
	}
	
	public function renderUpdatedTagFunction($data, $class = null)
	{
		if (is_object($data) && method_exists($data, 'getUpdatedAt') && method_exists($data, 'getCreatedAt')) {
			if (
				!is_null($data->getUpdatedAt()) &&
				$data->getUpdatedAt()->format('m/d/Y') != $data->getCreatedAt()->format('m/d/Y') &&
				$data->getUpdatedAt()->format('m/d/Y') == date('m/d/Y', strtotime('now'))
			) {
				return $this->generateHTML('warning', 'Updated', $class);
			}
		} else {
			return $this->generateHTML('default', 'ERROR');
		}
	}

	public function renderShippingTagFunction($data)
	{
		if ($data instanceof Shipping && method_exists($data, 'getStatus')) {
			$result = $data->translateStatus();
		}
		
		return $this->generateHTML($result['class'], $result['status']);
	}
	
	public function generateHTML($labelClass, $labelContent, $class = null)
	{
		return sprintf(' <span class="label label-%s%s">%s</span>', $labelClass, is_null($class) ? "" : " " . $class , $labelContent);
	}
	
	public function translateStatus($statusData)
	{
		$status = array();

		if ($statusData == 10 || $statusData == 'Draft' || $statusData == 'Pending for Approval' || $statusData == 15) {
			$status['class'] = 'default';
			$status['status'] = $statusData == 10 ? 'Draft' : ($statusData == 15 ? 'Pending' : $statusData);
		} elseif ($statusData == 17) {
			$status['class'] = 'info';
			$status['status'] = 'On Process';
		} elseif ($statusData == 19) {
			$status['class'] = 'warning';
			$status['status'] = 'Receiving';
		} elseif ($statusData == 20 || $statusData == 'Approved' || $statusData == 'Received') {
			$status['class'] = 'success';
			$status['status'] = $statusData !== 'Received' ? 'Approved' : 'Received';
		} elseif ($statusData == 21) {
			$status['class'] = 'success';
			$status['status'] = 'Submitted';
		} elseif ($statusData == 23) {
			$status['class'] = 'success';
			$status['status'] = 'Returned';
		} elseif ($statusData == 25) {
			$status['class'] = 'success';
			$status['status'] = 'Delivered';
		} elseif ($statusData == 26) {
			$status['class'] = 'info';
			$status['status'] = 'Fulfilled';
		} elseif ($statusData == 30 || $statusData == 'Void') {
			$status['class'] = 'danger';
			$status['status'] = 'Void';
		} elseif ($statusData == 40) {
			$status['class'] = 'default';
			$status['status'] = 'Closed';
		} elseif ($statusData == 'Successfully Put Away') {
            $status['class'] = 'success';
            $status['status'] = 'Successfully Put Away';
        } else {
			$status['class'] = 'default';
			$status['status'] = 'Error';
		}
		
		return $status;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getName()
	{
		return 'ci_render_tags';
	}
}