<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\PutAwayModel as Model;
use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Entity\PutAway;

/**
 * PutAway controller.
 *
 * @Route("/put-away")
 */
class PutAwayController extends BaseController
{
	const LINK_SHOW = 'putaway_show';

	public function getModel()
	{
		return $this->get('ci.putaway.model');
	}

	/**
	 * Lists all PutAway entities.
	 *
	 * @Route("/", name="putaway")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PUTAWAY_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page'),
			array('distinct' => true)
		);

		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 * Creates a new PutAway entity.
	 *
	 * @Route("/{type}", name="putaway_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:PutAway:new.html.twig")
	 * @Secure(roles="ROLE_PUTAWAY_CREATE")
	 */
	public function createAction(Request $request, $type)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$model->setType($type);
		$form = $model->getFormType($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Displays a form to create a new PutAway entity.
	 *
	 * @Route("/new/{transactionId}/{type}", name="putaway_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PUTAWAY_CREATE")
	 */
	public function newAction($transactionId, $type)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$model->setType($type);
		$model->setEntityData($entity, $transactionId, $type);
		$form = $model->getFormType($entity);

		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * @Route("/revision/{id}", name="putaway_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_PUTAWAY_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);

		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}

	/**
	 * @Route("/revision/{parentId}/{id}/log", name="putaway_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_PUTAWAY_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();

		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}

	/**
	 * Finds and displays a PutAway entity.
	 *
	 * @Route("/{id}/show", name="putaway_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PUTAWAY_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		return array(
			'entity' => $entity
		);
	}

	/**
	 * Displays a form to edit an existing PutAway entity.
	 *
	 * @Route("/{id}/edit", name="putaway_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PUTAWAY_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
		}

		$model->setEntityData($entity);
		$model->setType($entity->getPutAwayType());
		$editForm = $model->getFormType($entity);

		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Edits an existing PutAway entity.
	 *
	 * @Route("/{id}", name="putaway_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:PutAway:edit.html.twig")
	 * @Secure(roles="ROLE_PUTAWAY_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
		}

		$model->storePreviousStorageLocations($entity);
		$model->storePreviousPalletIds($entity);
		$model->setType($entity->getPutAwayType());
		$editForm = $model->getFormType($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	 /**
	 * @Route("/status/{id}/{status}/change", name="putaway_change_status")
	 */
	public function changeStatusAction($id, $status)
	{
		try {
			$model = $this->getModel();
			$status = $model->changeStatus($id, $status);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_STATUS_CHANGE) . $status . '.');
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}

		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 *
	 * @Route("/export/{id}", name="putaway_export")
	 */
	public function exportAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$rg = $this->get('ci.report.generator');

		return $rg->exportReport('Put Away # ' . $entity->getId(), null, $entity, 'CIInventoryBundle:PutAway:export.html.twig', null, CustomTCPDF::NONREPORT_LANDSCAPE);
	}

	/**
	 * Displays a form to upload files to PutAway.
	 *
	 * @Route("/{id}/file-upload", name="putaway_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_RECEIVEDORDER_FILE_UPLOAD")
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Updates the files for an existing PutAway entity.
	 *
	 * @Route("/{id}/file-upload", name="putaway_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:PutAway:file.html.twig")
	 * @Secure(roles="ROLE_RECEIVEDORDER_FILE_UPLOAD")
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="putaway_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_RECEIVEDORDER_FILE_DOWNLOAD")
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);
			
		return;
	}

	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="putaway_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_PUTAWAY_DELETE")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
	
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
			
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity),
		);
	}

	/**
	 * Deletes an Put Away entity.
	 *
	 * @Route("/{id}", name="putaway_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_PUTAWAY_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $this->createDeleteForm($id);
		
		if ($entity->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
			$receiveOrderId = $entity->getReceiveOrder()->getId();
		} else if ($entity->getPutAwayType() == PutAway::TYPE_BACKLOAD) {
			$backloadId = $entity->getBackload()->getId();
		}
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
	
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}

		if ($entity->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
			return $this->redirect($this->generateUrl('receiveorder_show', array('id' => $receiveOrderId)));
		} else if ($entity->getPutAwayType() == PutAway::TYPE_BACKLOAD) {
			return $this->redirect($this->generateUrl('backload_show', array('id' => $backloadId)));
		}
		
	}
}
