<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\ReceiveOrderModel as Model;
use CI\InventoryBundle\Helper\CustomTCPDF;

/**
 * ReceiveOrder controller.
 *
 * @Route("/receive-order")
 */
class ReceiveOrderController extends BaseController
{
	const LINK_SHOW = 'receiveorder_show';

	public function getModel()
	{
		return $this->get('ci.receiveorder.model');
	}

	/**
	 * Lists all ReceiveOrder entities.
	 *
	 * @Route("/", name="receiveorder")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_RECEIVEORDER_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);

		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 * Creates a new ReceiveOrder entity.
	 *
	 * @Route("/", name="receiveorder_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:ReceiveOrder:new.html.twig")
	 * @Secure(roles="ROLE_RECEIVEORDER_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
			'receivedPalletIds' => $model->getReceivedPalletIds()
		);
	}

	/**
	 * Displays a form to create a new ReceiveOrder entity.
	 *
	 * @Route("/new/{poId}", name="receiveorder_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_RECEIVEORDER_CREATE")
	 */
	public function newAction($poId)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$model->setEntityData($entity, $poId);
		$form = $model->getFormType($entity);

		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
			'receivedPalletIds' => $model->getReceivedPalletIds()
		);
	}

	/**
	 * @Route("/revision/{id}", name="receiveorder_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_RECEIVEORDER_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);

		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}

	/**
	 * @Route("/revision/{parentId}/{id}/log", name="receiveorder_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_RECEIVEORDER_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();

		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}

	/**
	 * Finds and displays a ReceiveOrder entity.
	 *
	 * @Route("/{id}/show", name="receiveorder_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_RECEIVEORDER_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		return array(
			'entity' => $entity
		);
	}

	/**
	 * Displays a form to edit an existing ReceiveOrder entity.
	 *
	 * @Route("/{id}/edit", name="receiveorder_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_RECEIVEORDER_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
		}

		$model->storeOriginalitems($entity);
		$model->setEntityData($entity, null, $model->getOriginalItems());
		$editForm = $model->getFormType($entity);

		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
			'receivedPalletIds' => $model->getReceivedPalletIds($entity)
		);
	}

	/**
	 * Edits an existing ReceiveOrder entity.
	 *
	 * @Route("/{id}", name="receiveorder_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:ReceiveOrder:edit.html.twig")
	 * @Secure(roles="ROLE_RECEIVEORDER_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
		}

		$model->storePreviousQuantities($entity);
		$editForm = $model->getFormType($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
			'receivedPalletIds' => $model->getReceivedPalletIds($entity)
		);
	}

	 /**
	 * @Route("/status/{id}/{status}/change", name="receiveorder_change_status")
	 */
	public function changeStatusAction($id, $status)
	{
		try {
			$model = $this->getModel();
			$status = $model->changeStatus($id, $status);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_STATUS_CHANGE) . $status . '.');
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}

		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 *
	 * @Route("/export/{id}", name="receiveorder_export")
	 */
	public function exportAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$rg = $this->get('ci.report.generator');

		return $rg->exportReport('Receive Order # ' . $entity->getId(), null, $entity, 'CIInventoryBundle:ReceiveOrder:export.html.twig', null, CustomTCPDF::NONREPORT_LANDSCAPE);
	}

	/**
	 * Displays a form to upload files to ReceiveOrder.
	 *
	 * @Route("/{id}/file-upload", name="receiveorder_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_RECEIVEDORDER_FILE_UPLOAD")
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);

		return array(
			'entity' => $entity,
			'form' => $form->createView()
		);
	}

	/**
	 * Updates the files for an existing ReceiveOrder entity.
	 *
	 * @Route("/{id}/file-upload", name="receiveorder_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:ReceiveOrder:file.html.twig")
	 * @Secure(roles="ROLE_RECEIVEDORDER_FILE_UPLOAD")
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'form' => $form->createView()
		);
	}

	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="receiveorder_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_RECEIVEDORDER_FILE_DOWNLOAD")
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);

		return;
	}
}
