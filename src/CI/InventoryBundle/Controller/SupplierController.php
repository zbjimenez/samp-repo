<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\SupplierModel as Model;

/**
 * Supplier controller.
 *
 * @Route("/supplier")
 */
class SupplierController extends BaseController
{
	const LINK_SHOW = 'supplier_show';
		
	public function getModel()
	{
		return $this->get('ci.supplier.model');
	}
	
	/**
	 * Lists all Supplier entities.
	 *
	 * @Route("/", name="supplier")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SUPPLIER_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page'),
			array('distinct' => true)
		); 

		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 * Creates a new Supplier entity.
	 *
	 * @Route("/", name="supplier_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Supplier:new.html.twig")
	 * @Secure(roles="ROLE_SUPPLIER_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity' => $entity,
			'form' => $form->createView(),
		);
	}
	
	/**
	 * Displays a form to create a new Supplier entity.
	 *
	 * @Route("/new", name="supplier_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SUPPLIER_CREATE")
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}
	
	/**
	 * Finds and displays a Supplier entity.
	 *
	 * @Route("/{id}/show", name="supplier_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SUPPLIER_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
			
		return array(
			'entity' => $entity,
		);
	}

	/**
	 * Displays a form to edit an existing Supplier entity.
	 *
	 * @Route("/{id}/edit", name="supplier_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SUPPLIER_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
	 * Edits an existing Supplier entity.
	 *
	 * @Route("/{id}", name="supplier_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Supplier:edit.html.twig")
	 * @Secure(roles="ROLE_SUPPLIER_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		$editForm = $model->getFormType($entity);
		
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
	 * Deletes a Supplier entity.
	 *
	 * @Route("/{id}/delete", name="supplier_confirm_delete")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_SUPPLIER_DELETE")
	 */
	public function confirmDeleteAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
		 
		return array(
			'params' => $model->getDeleteParams($entity),
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
		);
	}
	
	/**
	 * @Route("/{id}", name="supplier_delete")
	 * @Method("DELETE")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_SUPPLIER_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
		
		if ($deleteForm->handleRequest($request)->isSubmitted()) {
			if ($deleteForm->isValid()) {
				$model->deleteEntity($id);
				$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				return $this->redirect($this->generateUrl('supplier'));
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Oops! Unknown error occured. Please try again.');
			}
		}
		
		return array(
			'params' => $model->getDeleteParams($entity),
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
		);
	}

	/**
	 * Confirm status change of a Supplier entity.
	 *
	 * @Route("/{id}/confirm-change-status/{status}", name="supplier_confirm_change_status")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_SUPPLIER_CHANGE_STATUS")
	 */
	public function confirmChangeStatusAction($id, $status)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$name = '[Supplier] '. $entity->getName() . ' (ID# ' . $entity->getId() . ')';
	
		return $this->confirmChangeStatus($id, $status, $name, 'supplier_status_change', self::LINK_SHOW);
	}
	
	/**
	 * Change status of a Supplier entity.
	 *
	 * @Route("/{id}/status-change", name="supplier_status_change")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_SUPPLIER_CHANGE_STATUS")
	 */
	public function changeStatusAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		return $this->changeStatus($entity, 'supplier', self::LINK_SHOW);
	}
	
	/**
	 * @Route("/revision/{id}", name="supplier_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_SUPPLIER_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);

		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}
	
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="supplier_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_SUPPLIER_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();

		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}
	
	/**
	 *
	 * @Route("/{id}/get", name="supplier_get_json")
	 * @Method("GET")
	 */
	public function getJsonAction($id)
	{
		$model = $this->getModel();
		$data = $model->getJson($id);
	
		return new JsonResponse($data, 200);
	}

	/**
	 * Displays a form to upload files to Supplier.
	 *
	 * @Route("/{id}/file-upload", name="supplier_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SUPPLIER_FILE_UPLOAD")
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Updates the files for an existing Supplier entity.
	 *
	 * @Route("/{id}/file-upload", name="supplier_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Supplier:file.html.twig")
	 * @Secure(roles="ROLE_SUPPLIER_FILE_UPLOAD")
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
	
		return array(
			'entity' => $entity,
			'form' => $form->createView()
		);
	}
	
	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="supplier_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SUPPLIER_FILE_DOWNLOAD")
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);
			
		return;
	}
}