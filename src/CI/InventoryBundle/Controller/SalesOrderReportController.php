<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CI\InventoryBundle\Helper\CustomTCPDF;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

use CI\InventoryBundle\Model\InventoryReportModel as Model;

/**
 * SaleOrderReport controller.
 *
 * @Route("/report/sales-order")
 */
class SalesOrderReportController extends Controller
{
	public function getModel()
	{
		return $this->get('ci.sales.order.report.model');
	}

	/**
	 * @Route("/sales-order", name="get_sales_order_list")
	 * @Method("get")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_SALES_ORDER_REPORT')")
	 */
	public function getSalesOrderListAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getSalesOrderReport(Model::FILTER);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getSalesOrderReport(Model::INDEX, $params);

				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$qb,
					$this->get('request')->query->get('page', 1),
					$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}

		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 *
	 * @Route("/sales-order/export", name="get_sales_order_list_export")
	 */
	public function getSalesOrderListExportAction(Request $request)
	{
		$model = $this->getModel();
		$form  = $model->getSalesOrderReport(Model::FILTER);
		$form->handleRequest($request);

		$params = $form->getData();
		$pdf = $model->getSalesOrderReport(Model::PDF, $params);

		$rg = $this->get('ci.report.generator');
		
		return $rg->exportMulticellReport($pdf['title'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], null, CustomTCPDF::LANDSCAPE, $pdf['misc']);
	}

	/**
	 *
	 * @Route("/sales-by-customer-and-sales-agent", name="get_sales_by_customer_and_sales_agent_report")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_SALES_CUSTOMER_AGENT_REPORT')")
	 */
	public function getSalesByCustomerAndSalesAgentAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getSalesByCustomerAndSalesAgentReport(Model::FILTER);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$index = $model->getSalesByCustomerAndSalesAgentReport(Model::INDEX, $params);

				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
						$index['qb'],
						$this->get('request')->query->get('page', 1),
						$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}

		return array(
				'params' => isset($params) ? $params : null,
				'pagination' => isset($pagination) ? $pagination : null,
				'totals' => isset($index['totals']) ? $index['totals'] : null,
				'search_form' => $form->createView(),
		);
	}

	/**
	 *
	 * @Route("/sales-by-customer-and-sales-agent/export", name="get_sales_by_customer_and_sales_agent_export")
	 */
	public function getSalesByCustomerAndSalesAgentExportAction(Request $request)
	{
		$model = $this->getModel();
		$form  = $model->getSalesByCustomerAndSalesAgentReport(Model::FILTER);
		$form->handleRequest($request);

		$params = $form->getData();
		$pdf = $model->getSalesByCustomerAndSalesAgentReport(Model::PDF, $params);

		$rg = $this->get('ci.report.generator');
		
		return $rg->exportCellReport($pdf['title'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], null, CustomTCPDF::REPORT, $pdf['misc']);
	}

	/**
	 *
	 * @Route("/sales-by-customer-and-industry", name="get_sales_by_customer_and_industry_report")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_SALES_CUSTOMER_INDUSTRY_REPORT')")
	 */
	public function getSalesByCustomerAndIndustryAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getSalesByCustomerAndIndustryReport(Model::FILTER);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$index = $model->getSalesByCustomerAndIndustryReport(Model::INDEX, $params);

				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$index['qb'],
					$this->get('request')->query->get('page', 1),
					$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}

		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'totals' => isset($index['totals']) ? $index['totals'] : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 *
	 * @Route("/sales-by-customer-and-industry/export", name="get_sales_by_customer_and_industry_export")
	 * @Method("GET")
	 * @PreAuthorize("hasAnyRole('ROLE_SALES_CUSTOMER_INDUSTRY_REPORT')")
	 */
	public function getSalesByCustomerAndIndustryExportAction(Request $request)
	{
		$model = $this->getModel();
		$form  = $model->getSalesByCustomerAndIndustryReport(Model::FILTER);
		$form->handleRequest($request);

		$params = $form->getData();
		$pdf = $model->getSalesByCustomerAndIndustryReport(Model::PDF, $params);

		$rg = $this->get('ci.report.generator');
		
		return $rg->exportCellReport($pdf['title'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], null, CustomTCPDF::LANDSCAPE, $pdf['misc']);
	}
}