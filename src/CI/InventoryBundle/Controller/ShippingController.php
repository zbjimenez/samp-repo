<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\ReceiveOrderModel as Model;

/**
 * Shipping controller.
 *
 * @Route("/delivery")
 */
class ShippingController extends BaseController
{
	const LINK_SHOW = 'shipping_show';
	
	public function getModel()
	{
		return $this->get('ci.shipping.model');
	}
	
	/**
	 * Lists all Shipping entities.
	 *
	 * @Route("/", name="shipping")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SHIPPING_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();
		 
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		 
		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);
		 
		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 * Creates a new Shipping entity.
	 *
	 * @Route("/", name="shipping_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Shipping:new.html.twig")
	 * @Secure(roles="ROLE_SHIPPING_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		 
		return array(
			'entity' => $entity,
			'form' => $form->createView(),
		);
	}

	/**
	 * Displays a form to create a new Shipping entity.
	 *
	 * @Route("/new/{soId}", name="shipping_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SHIPPING_CREATE")
	 */
	public function newAction($soId)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$model->setEntityData($entity, $soId);
		$form = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}
	
	/**
	 * @Route("/revision/{id}", name="shipping_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_SHIPPING_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);

		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}
	
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="shipping_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_SHIPPING_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();

		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}

	/**
	 * Finds and displays a Shipping entity.
	 *
	 * @Route("/{id}/show", name="shipping_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SHIPPING_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
			
		return array(
			'entity' => $entity
		);
	}

	/**
	 * Displays a form to edit an existing Shipping entity.
	 *
	 * @Route("/{id}/edit", name="shipping_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SHIPPING_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		 
		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
		}
		 
		$model->storeOriginalitems($entity);
		$model->setEntityData($entity, null, $model->getOriginalItems());
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Edits an existing Shipping entity.
	 *
	 * @Route("/{id}", name="shipping_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Shipping:edit.html.twig")
	 * @Secure(roles="ROLE_SHIPPING_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		
		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
		}
		
		$model->storePreviousQuantities($entity);
		$editForm = $model->getFormType($entity);
		
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	 /**
	 * @Route("/status/{id}/{status}/change", name="shipping_change_status")
	 * @Secure(roles="ROLE_SHIPPING_DELIVERED, ROLE_SHIPPING_FULFILLED, ROLE_SHIPPING_VOID")
	 */
	public function changeStatusAction($id, $status)
	{
		try {
			$model = $this->getModel();
			$status = $model->changeStatus($id, $status);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_STATUS_CHANGE) . $status['status'] . '.');
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}
		 
		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 * @Route("/change-status-multiple", name="shipping_change_status_multiple")
	 * @Method("POST")
	 * @Secure(roles="ROLE_SHIPPING_DELIVERED, ROLE_SHIPPING_FULFILLED")
	 */
	public function changeStatusBulkAction(Request $request)
	{
		$model = $this->getModel();
		$ids = $request->request->get('ids');
		$status = $request->request->get('status');
		
		try {
			$results = $model->changeStatusBulk($ids, $status);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', 'An unknown error occured, please try again.');
		}

		return new JsonResponse($results);
	}
	
	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="shipping_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_SHIPPING_DELETE")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
	
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
			
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}
	
	/**
	 * Deletes a Shipping entity.
	 *
	 * @Route("/{id}", name="shipping_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_SHIPPING_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $this->createDeleteForm($id);
	
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
	
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}
	
		return $this->redirect($this->generateUrl('shipping'));
	}
	
	/**
	 * @Route("/export/{id}/{type}", name="shipping_export")
	 * @Secure(roles="ROLE_SHIPPING_PRINT_SI, ROLE_SHIPPING_PRINT_DR")
	 */
	public function exportAction($id, $type)
	{
		$model = $this->getModel();
		$pdf = $model->export($id, $type);
		
		return new StreamedResponse(function () use ($pdf) {
			$pdf['pdf']->Output($pdf['filename'] . '.pdf', 'I');
		});
	}

	/**
	 * Displays a form for the uploading of files to the given Shipping.
	 *
	 * @Route("/{id}/file-upload", name="shipping_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SHIPPING_FILE_UPLOAD")
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Updates the files of an existing Shipping entity.
	 *
	 * @Route("/{id}/file-upload", name="shipping_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Shipping:file.html.twig")
	 * @Secure(roles="ROLE_SHIPPING_FILE_UPLOAD")
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="shipping_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SHIPPING_FILE_DOWNLOAD")
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);
			
		return;
	}

	/**
	 * @Route("/find-product-by-item-id/{shippingItemId}", name="shipping_find_product_by_item_id")
	 * @Method("GET")
	 */
	public function findProductByItemIdAction(Request $request, $shippingItemId)
	{
		$model = $this->getModel();
		$results = $model->findProductByItemId($shippingItemId);

		return new JsonResponse($results);
	}
}
