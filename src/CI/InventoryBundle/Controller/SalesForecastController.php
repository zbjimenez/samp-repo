<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\SalesForecastModel as Model;
use CI\InventoryBundle\Helper\CustomTCPDF;

/**
 * SalesForecast controller.
 *
 * @Route("/sales-forecast")
 */
class SalesForecastController extends BaseController
{
	const LINK_SHOW = 'salesforecast_show';

	public function getModel()
	{
		return $this->get('ci.salesforecast.model');
	}

	/**
	 * Shows the SalesForecast Listing.
	 *
	 * @Route("/", name="salesforecast")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESFORECAST_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);

		return $this->render('CIInventoryBundle:SalesForecast:index.html.twig', array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView()
		));
	}

	/**
	 * @Route("/select", name="salesforecast_select")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESFORECAST_CREATE")
	 */
	public function selectAction()
	{
		$model = $this->getModel();
		$form = $model->getSelectType();

		return $this->render('CIInventoryBundle:SalesForecast:select.html.twig', array(
			'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/select-redirect", name="salesforecast_select_redirect")
	 * @Method("POST")
	 * @Secure(roles="ROLE_SALESFORECAST_CREATE")
	 */
	public function selectRedirectAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getSelectType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$data = $form->getData();

					return $this->redirect($this->generateUrl('salesforecast_new', array(
						'customerId' => $data['customer']->getId(),
						'productId' => $data['product']->getId(),
					)));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return $this->render('CIInventoryBundle:SalesForecast:select.html.twig', array(
			'form' => $form->createView(),
		));
	}

	/**
	 * Creates a new SalesForecast entity.
	 *
	 * @Route("/", name="salesforecast_create")
	 * @Method("POST")
	 * @Secure(roles="ROLE_SALESFORECAST_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return $this->render('CIInventoryBundle:SalesForecast:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView()
		));
	}

	/**
	 * Displays a form to create a new SalesForecast entity.
	 *
	 * @Route("/{customerId}/{productId}/new", name="salesforecast_new")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESFORECAST_CREATE")
	 */
	public function newAction($customerId, $productId)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity($customerId, $productId);
		$form = $model->getFormType($entity);

		return $this->render('CIInventoryBundle:SalesForecast:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView()
		));
	}

	/**
	 * Finds and displays a SalesForecast entity.
	 *
	 * @Route("/{id}/show", name="salesforecast_show")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESFORECAST_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		return $this->render('CIInventoryBundle:SalesForecast:show.html.twig', array(
			'entity' => $entity
		));
	}

	/**
	 * Displays a form to edit an existing SalesForecast entity.
	 *
	 * @Route("/{id}/edit", name="salesforecast_edit")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESFORECAST_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		$editForm = $model->getFormType($entity);

		return $this->render('CIInventoryBundle:SalesForecast:edit.html.twig', array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		));
	}

	/**
	 * Edits an existing SalesForecast entity.
	 *
	 * @Route("/{id}", name="salesforecast_update")
	 * @Method("PUT")
	 * @Secure(roles="ROLE_SALESFORECAST_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		$editForm = $model->getFormType($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return $this->render('CIInventoryBundle:SalesForecast:edit.html.twig', array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		));
	}

	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="salesforecast_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_SALESFORECAST_DELETE")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);

		return $this->render('CIInventoryBundle:Misc:delete.html.twig', array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		));
	}

	/**
	 * Deletes a SalesForecast entity.
	 *
	 * @Route("/{id}", name="salesforecast_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_SALESFORECAST_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$form = $this->createDeleteForm($id);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}

		return $this->redirect($this->generateUrl('salesforecast'));
	}

	/**
	 * @Route("/revision/{id}", name="salesforecast_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESFORECAST_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		
		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}

	/**
	 * @Route("/revision/{parentId}/{id}/log", name="salesforecast_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESFORECAST_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();

		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}

	/**
	 *
	 * @Route("/export/{id}", name="salesforecast_export")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESFORECAST_READ")
	 */
	public function exportAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$rg = $this->get('ci.report.generator');

		return $rg->exportReport('Sales Forecast # ' . $entity->getId(), null, $entity, 'CIInventoryBundle:SalesForecast:export.html.twig', null, CustomTCPDF::FORM);
	}

	/**
	 * Displays a form to upload files to SalesForecast.
	 *
	 * @Route("/{id}/file-upload", name="salesforecast_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESFORECAST_FILE_UPLOAD")
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Updates the files for an existing SalesForecast entity.
	 *
	 * @Route("/{id}/file-upload", name="salesforecast_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:SalesForecast:file.html.twig")
	 * @Secure(roles="ROLE_SALESFORECAST_FILE_UPLOAD")
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="salesforecast_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESFORECAST_FILE_UPLOAD")
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);
			
		return;
	}
}
