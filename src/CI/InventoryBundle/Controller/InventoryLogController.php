<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use CI\InventoryBundle\Form\Type\InventoryLogFilterType;

/**
 * InventoryLog controller.
 *
 * @Route("/inventory-log")
 */
class InventoryLogController extends Controller
{
    /**
     * Lists all InventoryLog entities.
     *
     * @Route("/", name="inventorylog")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(new InventoryLogFilterType());
    	$em = $this->getDoctrine()->getManager();
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    	    if ($form->isValid()) {
    			$params = $form->getData();
    			$em = $this->getDoctrine()->getManager();   			
    			$qb = $em->getRepository('CIInventoryBundle:InventoryLog')->findAll($params);
    		} else {
    			$this->get('session')->getFlashBag()->add('error', 'Please try again.');
    		}
    	}
    	
    	if (!isset($qb)) {
    		$qb = $em->getRepository('CIInventoryBundle:InventoryLog')->findAll();
    	}
    	
    	$paginator = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
    		$qb,
    		$this->get('request')->query->get('page', 1),
    		$this->container->getParameter('pagination_limit_per_page')
    	);

    	return array(
    		'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView()
    	);
    }
}