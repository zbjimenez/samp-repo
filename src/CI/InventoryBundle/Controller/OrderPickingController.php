<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Entity\OrderPicking;
use CI\InventoryBundle\Form\Type\OrderPickingType;
use CI\InventoryBundle\Model\OrderPickingModel as Model;

/**
 * OrderPicking controller.
 *
 * @Route("/order-picking")
 */
class OrderPickingController extends BaseController
{
	const LINK_SHOW = 'orderpicking_show';

	public function getModel()
	{
		return $this->get('ci.orderpicking.model');
	}

	/**
	 * Lists all OrderPicking entities.
	 *
	 * @Route("/", name="orderpicking")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_ORDERPICKING_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);

		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	/**
	 * Creates a new OrderPicking entity.
	 *
	 * @Route("/", name="orderpicking_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:OrderPicking:new.html.twig")
	 * @Secure(roles="ROLE_ORDERPICKING_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		 
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Displays a form to create a new OrderPicking entity.
	 *
	 * @Route("/new/{shippingId}", name="orderpicking_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_ORDERPICKING_CREATE")
	 */
	public function newAction($shippingId)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$model->setEntityData($entity, $shippingId);
		$form = $model->getFormType($entity);

		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Finds and displays a OrderPicking entity.
	 *
	 * @Route("/{id}/show", name="orderpicking_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_ORDERPICKING_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		return array(
			'entity' => $entity,
		);
	}

	/**
	 * Displays a form to edit an existing OrderPicking entity.
	 *
	 * @Route("/{id}/edit", name="orderpicking_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_ORDERPICKING_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
		}
		
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Edits an existing OrderPicking entity.
	 *
	 * @Route("/{id}", name="orderpicking_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:OrderPicking:edit.html.twig")
	 * @Secure(roles="ROLE_ORDERPICKING_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
		}

		$editForm = $model->getFormType($entity);
		$model->storeOriginalItems($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
	 *
	 * @Route("/{id}/check-barcode", name="orderpicking_check_barcode")
	 * @Method("POST")
	 */
	public function checkBarcodeAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$data = json_decode($request->getContent(), true);

		$barcode = $model->readBarcodeFile($entity, $data);

		if ($barcode) {
			$this->get('session')->getFlashBag()->add('success', 'Barcode file has successfully been scanned. The products have now been picked from the inventory.');
		} else {
			$this->get('session')->getFlashBag()->add('danger', 'There was an error encountered when the file was scanned. Kindly check the file and reupload it.');
		}

		return new JsonResponse(array('success' => $barcode));
	}

	/**
	 * @Route("/{id}/export", name="orderpicking_export")
	 * @Method("GET")
	 * @Secure(roles="ROLE_ORDERPICKING_READ")
	 */
	public function exportAction($id)
	{
		$model = $this->getModel();
		$pdf = $model->export($id);

		return new StreamedResponse(function () use ($pdf) {
			$pdf['pdf']->Output($pdf['filename'] . '.pdf', 'I');
		});
	}


	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="orderpicking_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_ORDERPICKING_DELETE")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
	
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
			
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity),
		);
	}
	
	/**
	 * Deletes an Order picking entity.
	 *
	 * @Route("/{id}", name="orderpicking_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_ORDERPICKING_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $this->createDeleteForm($id);
	
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
	
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}
	
		return $this->redirect($this->generateUrl('orderpicking'));
	}

	/**
	 * @Route("/revision/{id}", name="orderpicking_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_ORDERPICKING_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);

		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}
	
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="orderpicking_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_ORDERPICKING_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();

		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}
}
