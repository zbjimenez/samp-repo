<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\SalesActivityModel as Model;
use CI\InventoryBundle\Helper\CustomTCPDF;

/**
 * Sales Activity controller.
 *
 * @Route("/sales-activity")
 * 
 */
class SalesActivityController extends BaseController
{
	const LINK_SHOW = 'salesactivity_show';

	public function getModel()
	{
		return $this->get('ci.sales_activity.model');
	}

	/**
	 * Lists all Sales Activity.
	 *
	 * @Route("/", name="salesactivity")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESACTIVITY_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getIndex($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$qb = $model->getIndex();
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);

		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView()
		);
	}

	/**
	 * Creates a new Sales Activity entity.
	 *
	 * @Route("/", name="salesactivity_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:SalesActivity:new.html.twig")
	 * @Secure(roles="ROLE_SALESACTIVITY_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Displays a form to create a new Sales Activity entity.
	 *
	 * @Route("/new", name="salesactivity_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESACTIVITY_CREATE")
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);

		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Finds and displays a Sales Activity entity.
	 *
	 * @Route("/{id}/show", requirements={"id"="\d+"}, name="salesactivity_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESACTIVITY_READ")
	 */
	public function showAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		
		$previousUrl = $this->getRequest()->headers->get('referer');
		
		return array(
			'entity' => $entity,
			'previousUrl' => strpos($previousUrl, '/sales-activity/report') !== false ? $previousUrl : null
		);
	}

	/**
	 * Displays a form to edit an existing Sales Activity entity.
	 *
	 * @Route("/{id}/edit", requirements={"id"="\d+"}, name="salesactivity_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESACTIVITY_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		$editForm = $model->getFormType($entity);

		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}


	/**
	 * Edits an existing Sales Activity entity.
	 *
	 * @Route("/{id}", requirements={"id"="\d+"}, name="salesactivity_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:SalesActivity:edit.html.twig")
	 * @Secure(roles="ROLE_SALESACTIVITY_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		$editForm = $model->getFormType($entity);
		$model->storeOriginalItems($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Displays a form to upload files to sales activity.
	 *
	 * @Route("/{id}/file-upload", requirements={"id"="\d+"}, name="salesactivity_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESACTIVITY_FILE_UPLOAD")
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);

		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}

	/**
	 * Updates the files for an existing Sales Activity entity.
	 *
	 * @Route("/{id}/file-upload", requirements={"id"="\d+"}, name="salesactivity_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:SalesActivity:file.html.twig")
	 * @Secure(roles="ROLE_SALESACTIVITY_FILE_UPLOAD")
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}

	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", requirements={"id"="\d+"}, name="salesactivity_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESACTIVITY_FILE_DOWNLOAD")
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);

		return;
	}

	/**
	 * Preview image file.
	 *
	 * @Route("/{id}/preview", requirements={"id"="\d+"}, name="salesactivity_file_preview")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESACTIVITY_FILE_DOWNLOAD")
	 */
	public function previewAction($id)
	{
		return new JsonResponse($this->getModel()->previewImage($id), 200);
	}

	/**
	 * Deletes a Sales Activity entity.
	 *
	 * @Route("/{id}/delete", requirements={"id"="\d+"}, name="salesactivity_confirm_delete")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_SALESACTIVITY_DELETE")
	 */
	public function confirmDeleteAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);

		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}

	/**
	 * @Route("/{id}", requirements={"id"="\d+"}, name="salesactivity_delete")
	 * @Method("DELETE")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_SALESACTIVITY_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $this->createDeleteForm($id);

		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again later.');
			}
		}

		return $this->redirect($this->generateUrl('salesactivity'));
	}

	/**
	 * @Route("/revision/{id}", requirements={"id"="\d+"}, name="salesactivity_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_SALESACTIVITY_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}

	/**
	 * @Route("/revision/{parentId}/{id}/log", requirements={"id"="\d+"}, name="salesactivity_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_SALESACTIVITY_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();
		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}

	/**
	 * @Route("/status/{id}/{status}/change", name="salesactivity_change_status")
	 * @Secure(roles="ROLE_SALESACTIVITY_CHANGE_STATUS")
	 */
	public function changeStatusAction($id, $status)
	{
		$model = $this->getModel();

		try {
			$status = $model->changeStatus($id, $status);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_STATUS_CHANGE) . $status['status'] . '.');
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}

		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 * @Route("/{id}/read", name="salesactivity_toggle_read")
	 * @Secure(roles="ROLE_SALES_MANAGER")
	 */
	public function readAction(Request $request, $id)
	{
		$previousUrl = $request->query->get('previousUrl');
		$model = $this->getModel();
		$read = $model->toggleReadFlag($id);

		return $read && $previousUrl ? $this->redirect($previousUrl) : $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 * Sales Activity Report
	 *
	 * @Route("/report", name="salesactivity_report")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_SALES_ACTIVITY_REPORT')")
	 */
	public function reportAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getReportFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getReportIndex($params);

				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$qb,
					$this->get('request')->query->get('page', 1),
					$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
			'params' => isset($params) ? $params : null,
		);
	}

	/**
	 * @Route("/report/export", name="salesactivity_list_export")
	 * @Secure(roles="ROLE_SALESACTIVITY_READ")
	 *
	 */
	public function exportListAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getReportFilterFormType();
		$form->handleRequest($request);

		$params = $form->getData();
		$pdf = $model->getSalesActivityReport($params);
		
		$rg = $this->get('ci.report.generator');
		
		return  $rg->exportCellReport($pdf['title'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], null, CustomTCPDF::REPORT, $pdf['misc']);
	}

	/**
	 * Sales Activity Customer Report
	 *
	 * @Route("/report/customer", name="salesactivity_report_customer")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_SALES_ACTIVITY_CUSTOMER_REPORT')")
	 */
	public function customerReportAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getCustomerReportFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$data = $model->getCustomerReportIndex(Model::INDEX, $params);
				$qb = $data['query'];
				$count = $data['count'];
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$data = $model->getCustomerReportIndex(Model::INDEX);
			$qb = $data['query'];
			$count = $data['count'];
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);

		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
			'params' => isset($params) ? $params : null,
			'count' => isset($count) ? $count : null
		);
	}

	/**
	 * @Route("/report/customer/export", name="salesactivity_report_customer_export")
	 *
	 */
	public function customerReportExportAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getCustomerReportFilterFormType();
		$form->handleRequest($request);

		$params = $form->getData();
		$pdf = $model->getCustomerReportIndex(Model::PDF, $params);

		$filename = str_ireplace(' ', '_', 'Sales Activity Customer Report') . '_' . date('M-d-Y');

		return new StreamedResponse(function () use ($filename, $pdf) {
			$pdf->Output($filename . '.pdf', 'I');
		});
	}
	
	/**
	 *
	 * @Route("/export/{id}", name="salesactivity_export")
	 */
	public function exportAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$rg = $this->get('ci.report.generator');
	
		return $rg->exportReport('Sales Activity', null, $entity, 'CIInventoryBundle:SalesActivity:pdf.html.twig', null, CustomTCPDF::FORM);
	}
}
