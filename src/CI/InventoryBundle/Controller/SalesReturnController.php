<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\SalesReturnModel as Model;
use CI\InventoryBundle\Helper\CustomTCPDF;

/**
 * SalesReturn controller.
 *
 * @Route("/sales-return")
 */
class SalesReturnController extends BaseController
{
	const LINK_SHOW = 'salesreturn_show';

	public function getModel()
	{
		return $this->get('ci.salesreturn.model');
	}

	/**
	 * Shows the SalesReturn Listing.
	 *
	 * @Route("/", name="salesreturn")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESRETURN_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);

		return $this->render('CIInventoryBundle:SalesReturn:index.html.twig', array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView()
		));
	}

	/**
	 * Creates a new SalesReturn entity.
	 *
	 * @Route("/", name="salesreturn_create")
	 * @Method("POST")
	 * @Secure(roles="ROLE_SALESRETURN_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			try {
				$model->isCreatable($entity->getShipping()->getId());
			} catch (\Exception $e) {
				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirect($this->generateUrl('receiveorder_show', array('id' => $entity->getShipping()->getId())));
			}

			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return $this->render('CIInventoryBundle:SalesReturn:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView()
		));
	}

	/**
	 * Displays a form to create a new SalesReturn entity.
	 *
	 * @Route("/{shippingId}/new", name="salesreturn_new")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESRETURN_CREATE")
	 */
	public function newAction($shippingId)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity($shippingId);
		$form = $model->getFormType($entity);

		try {
			$model->isCreatable($shippingId);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl('shipping_show', array('id' => $shippingId)));
		}

		return $this->render('CIInventoryBundle:SalesReturn:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView()
		));
	}

	/**
	 * Finds and displays a SalesReturn entity.
	 *
	 * @Route("/{id}/show", name="salesreturn_show")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESRETURN_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		return $this->render('CIInventoryBundle:SalesReturn:show.html.twig', array(
			'entity' => $entity
		));
	}

	/**
	 * Displays a form to edit an existing SalesReturn entity.
	 *
	 * @Route("/{id}/edit", name="salesreturn_edit")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESRETURN_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		$editForm = $model->getFormType($entity);

		return $this->render('CIInventoryBundle:SalesReturn:edit.html.twig', array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		));
	}

	/**
	 * Edits an existing SalesReturn entity.
	 *
	 * @Route("/{id}", name="salesreturn_update")
	 * @Method("PUT")
	 * @Secure(roles="ROLE_SALESRETURN_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		$model->storeOriginalItems($entity);
		$editForm = $model->getFormType($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return $this->render('CIInventoryBundle:SalesReturn:edit.html.twig', array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		));
	}

	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="salesreturn_confirm_delete")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESRETURN_DELETE")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);

		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		return $this->render('CIInventoryBundle:Misc:delete.html.twig', array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		));
	}

	/**
	 * Deletes a SalesReturn entity.
	 *
	 * @Route("/{id}", name="salesreturn_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_SALESRETURN_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $this->createDeleteForm($id);

		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}

		return $this->redirect($this->generateUrl('salesreturn'));
	}

	/**
	 * @Route("/revision/{id}", name="salesreturn_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_SALESRETURN_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}

	/**
	 * @Route("/revision/{parentId}/{id}/log", name="salesreturn_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_SALESRETURN_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();
		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}

	/**
	 * @Route("/status/{id}/{status}/change", name="salesreturn_change_status")
	 * @Secure(roles="ROLE_SALESRETURN_CHANGE_STATUS")
	 */
	public function changeStatusAction($id, $status)
	{
		$model = $this->getModel();

		try {
			$status = $model->changeStatus($id, $status);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_STATUS_CHANGE) . $status . '.');
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}

		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 *
	 * @Route("/export/{id}", name="salesreturn_export")
	 * @Secure(roles="ROLE_SALESRETURN_READ")
	 */
	public function exportAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$rg = $this->get('ci.report.generator');

		return $rg->exportReport('Sales Return # ' . $entity->getId(), null, $entity, 'CIInventoryBundle:SalesReturn:export.html.twig', null, CustomTCPDF::FORM);
	}

	/**
	 * Displays a form to upload files to SalesReturn.
	 *
	 * @Route("/{id}/file-upload", name="salesreturn_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESRETURN_FILE_UPLOAD")
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
	
		return array(
				'entity' => $entity,
				'form'   => $form->createView()
		);
	}
	
	/**
	 * Updates the files for an existing SalesReturn entity.
	 *
	 * @Route("/{id}/file-upload", name="salesreturn_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:SalesReturn:file.html.twig")
	 * @Secure(roles="ROLE_SALESRETURN_FILE_UPLOAD")
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
	
		return array(
				'entity' => $entity,
				'form'   => $form->createView()
		);
	}
	
	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="salesreturn_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESRETURN_FILE_DOWNLOAD")
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);
			
		return;
	}
}
