<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * ProductCode controller.
 *
 * @Route("/product-code")
 */
class ProductCodeController extends BaseController
{	
	public function getModel()
	{
		return $this->get('ci.productcode.model');
	}
	
	/**
	 * Displays a form to create a new ProductCode entity for a Product entity
	 *
	 * @Route("/{id}/new", name="productcode_new", requirements={"id" = "\d+"})
	 * @Method("GET")
	 * @Template()
	 */
	public function newAction($id)
	{
		$model = $this->getModel();
		$entity = $model->getNewPreparedEntity($id);        
		$form = $model->getFormType($entity);
		
		return array(
			'form'  => $form->createView()
		);
	}


	/**
     * Creates a new ProductCode Entity for Product entity
     * 
     * @Route("/create", name="productcode_create")
     * @Method("POST")
     * @Template("CIInventoryBundle:ProductCode:new.html.twig")
    */
    public function createAction(Request $request)
    {
    	$model = $this->getModel();
        $entity = $model->getNewEntity();
        $form = $model->getFormType($entity);
        
        if ($form->handleRequest($request)->isSubmitted()) {
        	if ($form->isValid()) {
        		$responseData = $model->saveEntity($form, $entity);
        	}
        }
        
        if(!isset($responseData)) {
        	$responseData = $model->getErrorResponseData($form, $entity);
        }
        
        return new JsonResponse($responseData);
    }
	
	/**
	 * Displays a form to edit an existing ProductCode entity.
	 *
	 * @Route("/{id}/edit", name="productcode_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
     * Edits an existing ProductCode entity of a Product entity
     *
     * @Route("/{id}", name="productcode_update")
     * @Method("PUT")
     * @Template("CIInventoryBundle:ProductCode:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	$form = $model->getFormType($entity);
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid() && $entity->getCustomer()) {
    			$responseData = $model->saveEntity($form, $entity);
    		}
    	}
    	
    	if(!isset($responseData)) {
    		$responseData = $model->getErrorResponseData($form, $entity);
    	}
    	
    	return new JsonResponse($responseData);
    }
    
    /**
	 * Delete a ProductCode entity
	 * 
     * @Route("/{id}/delete", name="productcode_delete")
     */
    public function deleteAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	
    	return new JsonResponse($model->deleteEntity($id));
    }
}