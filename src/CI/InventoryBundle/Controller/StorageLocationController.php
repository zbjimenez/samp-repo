<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Entity\StorageLocation;
use CI\InventoryBundle\Model\StorageLocationModel as Model;

/**
 * StorageLocation controller.
 *
 * @Route("/storage-location")
 */
class StorageLocationController extends BaseController
{
	const LINK_SHOW = 'storagelocation_show';
	
	public function getModel()
	{
		return $this->get('ci.storagelocation.model');
	}
	
	/**
	 * Lists all StorageLocation entities.
	 *
	 * @Route("/", name="storagelocation")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_STORAGELOCATION_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getIndex($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (!isset($qb)) {
			$qb = $model->getIndex();
		}
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);
		
		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 * Creates a new StorageLocation entity.
	 *
	 * @Route("/", name="storagelocation_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:StorageLocation:new.html.twig")
	 * @Secure(roles="ROLE_STORAGELOCATION_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Displays a form to create a new StorageLocation entity.
	 *
	 * @Route("/new", name="storagelocation_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_STORAGELOCATION_CREATE")
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}
	
	/**
	 * Finds and displays a StorageLocation entity.
	 *
	 * @Route("/{id}/show", name="storagelocation_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_STORAGELOCATION_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		
		return array(
			'entity' => $entity
		);
	}

	/**
	 * Displays a form to edit an existing StorageLocation entity.
	 *
	 * @Route("/{id}/edit", name="storagelocation_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_STORAGELOCATION_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
	 * Edits an existing StorageLocation entity.
	 *
	 * @Route("/{id}", name="storagelocation_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:StorageLocation:edit.html.twig")
	 * @Secure(roles="ROLE_STORAGELOCATION_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		 
		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Confirm status change of a StorageLocation entity.
	 *
	 * @Route("/{id}/confirm-change-status/{status}", name="storagelocation_confirm_change_status")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_STORAGELOCATION_CHANGE_STATUS")
	 */
	public function confirmChangeStatusAction($id, $status)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$name = '[StorageLocation] '. $entity->getFullLocation() . ' (ID# ' . $entity->getId() . ')';

		if ((!(count($entity->getInventories()) != 0) && $entity->checkOrderPickingItems()  && $entity->checkPutAwayItems()) || !$entity->isActive()) {
			return $this->confirmChangeStatus($id, $status, $name, 'storagelocation_status_change', self::LINK_SHOW);
		} else {
			$this->get('session')->getFlashBag()->add('danger', 'This storage location currently have products stored or being transferred to. Please empty before setting to Inactive');
			return $this->redirect($this->generateUrl('storagelocation_show', array('id' => $id)));
		}
	}

	/**
	 * Change status of a StorageLocation entity.
	 *
	 * @Route("/{id}/status-change", name="storagelocation_status_change")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_STORAGELOCATION_CHANGE_STATUS")
	 */
	public function changeStatusAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		
		return $this->changeStatus($entity, 'storagelocation', self::LINK_SHOW);
	}
	
	/**
	 * Deletes a StorageLocation entity.
	 *
	 * @Route("/{id}/delete", name="storagelocation_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_STORAGELOCATION_DELETE")
	 */
	public function confirmDeleteAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
		
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}
	
	/**
	 * @Route("/{id}", name="storagelocation_delete")
	 * @Method("DELETE")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_STORAGELOCATION_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$form = $this->createDeleteForm($id);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again later.');
			}
		}
		
		return $this->redirect($this->generateUrl('storagelocation'));
	}
    
	/**
	 * @Route("/revision/{id}", name="storagelocation_revision")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_STORAGELOCATION_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		
		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
    }
    
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="storagelocation_log")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_STORAGELOCATION_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();
		
		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}

	/**
	 * @Route("/search", name="storagelocation_search", requirements={"id" = "\d+"})
	 * @Method("POST")
	 */
	public function searchAction(Request $request)
	{
		$model = $this->getModel();
		return new JsonResponse($model->search($request), 200);
	}

	/**
	 * Called by Select2 Ajax request
	 *
	 * @Route("/search-from-select", name="storagelocation_search_from_select")
	 * @Method("GET")
	 */
	public function searchTermAction(Request $request)
	{
		$model = $this->getModel();
		$results = $model->searchFromSelect($request);

		$response = new Response();
		$response->headers->set('Content-Type', 'application/json');
		
		return new JsonResponse($results, 200);
	}
	 
	/**
	 * @Route("/{id}/get", name="storagelocation_get_json")
	 * @Method("GET")
	 */
	public function getJsonAction($id)
	{
		$model = $this->getModel();
		$data = $model->getJson($id);
		
		return new JsonResponse($data, 200);
	}
}
