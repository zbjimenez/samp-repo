<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * SupplierReport controller.
 *
 * @Route("/report/supplier")
 */
class SupplierReportController extends Controller
{  
	public function getModel()
	{
		return $this->get('ci.supplier.report.model');
	}
	
	/**
	 * @Route("/purchase-order", name="get_purchase_order")
	 * @Method("get")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_PURCHASE_ORDER_REPORT')")
	 */
	public function getPurchaseOrderAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getPurchaseOrderReport('filter');
		 
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getPurchaseOrderReport('index', $params);

				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$qb,
					$this->get('request')->query->get('page', 1),
					$this->container->getParameter('pagination_limit_per_page'),
					array('distinct' => true)
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}

		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 *
	 * @Route("/purchase-order/export", name="get_purchase_order_export")
	 * @Secure(roles="ROLE_PURCHASEORDER_READ")
	 */
	public function getPurchaseOrderExportAction(Request $request)
	{
		$model = $this->getModel();
		$form  = $model->getPurchaseOrderReport('filter');
		$form->handleRequest($request);
	
		$params = $form->getData();
		$pdf = $model->getPurchaseOrderReport('pdf', $params);
		
		return new StreamedResponse(function () use ($pdf) {
			$pdf['pdf']->Output($pdf['filename'] . '.pdf', 'I');
		});
	}
}