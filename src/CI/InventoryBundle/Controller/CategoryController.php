<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\CategoryModel as Model;
use CI\InventoryBundle\Entity\Category;

/**
 * Category controller.
 *
 * @Route("/category")
 */
class CategoryController extends BaseController
{
	const LINK_SHOW = 'category_show';
	
	public function getModel()
	{
		return $this->get('ci.category.model');
	}
	
	/**
	 * Lists all Category entities.
	 *
	 * @Route("/", name="category")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CATEGORY_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
        $form = $model->getFilterFormType();
        
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getIndex($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (!isset($qb)) {
			$qb = $model->getIndex();
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
        	$qb,
        	$this->get('request')->query->get('page', 1),
        	$this->container->getParameter('pagination_limit_per_page'),
        	array("distinct" => true)
        );

        return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
        );
	}
	
	/**
	 * Creates a new Category entity.
	 *
	 * @Route("/", name="category_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Category:new.html.twig")
	 * @Secure(roles="ROLE_CATEGORY_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					
					$this->get('session')->getFlashBag()->add('success', $model->getMessages('create'));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}
	
	/**
	 * Displays a form to create a new Category entity.
	 *
	 * @Route("/new", name="category_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CATEGORY_CREATE")
	 */
	public function newAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}
	
	/**
	 * Finds and displays a Category entity.
	 *
	 * @Route("/{id}/show", name="category_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CATEGORY_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
			
		return array(
			'entity' => $entity,
		);
	}
	
	/**
	 * Displays a form to edit an existing Category entity.
	 *
	 * @Route("/{id}/edit", name="category_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CATEGORY_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
	 * Updates a new Category entity.
	 *
	 * @Route("/{id}", name="category_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Category:edit.html.twig")
	 * @Secure(roles="ROLE_CATEGORY_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					if ($model->restrictCategoryHeirarchy($entity)) {
						$model->saveEntity($editForm, $entity);
						$this->get('session')->getFlashBag()->add('success', $model->getMessages('update'));
					}
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
	 * Confirm status change of a Category entity.
	 *
	 * @Route("/{id}/confirm-change-status/{status}", name="category_confirm_change_status")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_CATEGORY_CHANGE_STATUS")
	 */
	public function confirmChangeStatusAction($id, $status)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$name = '[Category] '. $entity->getName() . ' (ID# ' . $entity->getId() . ')';
	
		return $this->confirmChangeStatus($id, $status, $name, 'category_status_change', self::LINK_SHOW);
	}
	
	/**
	 * Change status of a Category entity.
	 *
	 * @Route("/{id}/status-change", name="category_status_change")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_CATEGORY_CHANGE_STATUS")
	 */
	public function changeStatusAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
	
		return $this->changeStatus($entity, 'category', self::LINK_SHOW);
	}
	
	/**
	 * @Route("/revision/{id}", name="category_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_CATEGORY_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		return $this->entityRevision($revision["route"], $revision["type"], $revision["class"]);
	}
	
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="category_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_CATEGORY_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();
		return $this->entityLog($parentId, $log["route"], $id, $log["type"], $log["classes"]);
	}
	
	/**
	 * Deletes a Category entity.
	 *
	 * @Route("/{id}/delete", name="category_confirm_delete")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_CATEGORY_DELETE")
	 */
	public function confirmDeleteAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
	
		return $this->confirmDelete($entity, $model->getDeleteParams($entity), 'category');
	}
	
	/**
	 * @Route("/{id}", name="category_delete")
	 * @Method("DELETE")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_CATEGORY_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
	
		return $this->delete($entity, $model->getDeleteParams($entity), $request, 'Category', 'category');
	}
}