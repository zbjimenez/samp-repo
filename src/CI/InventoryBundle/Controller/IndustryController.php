<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Entity\Industry;
use CI\InventoryBundle\Form\Type\IndustryType;
use CI\InventoryBundle\Model\IndustryModel as Model;

/**
 * Industry controller.
 *
 * @Route("/industry")
 */
class IndustryController extends BaseController
{
	const LINK_SHOW = 'industry_show';
	
	public function getModel()
	{
		return $this->get('ci.industry.model');
	}

	/**
	 * Lists all Industry entities.
	 *
	 * @Route("/", name="industry")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_INDUSTRY_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getIndex($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (!isset($qb)) {
			$qb = $model->getIndex();
		}
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);
		
		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 * Creates a new Industry entity.
	 *
	 * @Route("/", name="industry_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Industry:new.html.twig")
	 * @Secure(roles="ROLE_INDUSTRY_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Displays a form to create a new Industry entity.
	 *
	 * @Route("/new", name="industry_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_INDUSTRY_CREATE")
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Finds and displays a Industry entity.
	 *
	 * @Route("/{id}/show", name="industry_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_INDUSTRY_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		return array(
			'entity' => $entity,
		);
	}

	/**
	 * Displays a form to edit an existing Industry entity.
	 *
	 * @Route("/{id}/edit", name="industry_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_INDUSTRY_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Edits an existing Industry entity.
	 *
	 * @Route("/{id}", name="industry_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Industry:edit.html.twig")
	 * @Secure(roles="ROLE_INDUSTRY_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		 
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		 
		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Confirm status change of a Industry entity.
	 *
	 * @Route("/{id}/confirm-change-status/{status}", name="industry_confirm_change_status")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_INDUSTRY_CHANGE_STATUS")
	 */
	public function confirmChangeStatusAction($id, $status)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$name = '[Industry] '. $entity->getName() . ' (ID# ' . $entity->getId() . ')';
		
		return $this->confirmChangeStatus($id, $status, $name, 'industry_status_change', self::LINK_SHOW);
	}
	
	/**
	 * Change status of a Industry entity.
	 *
	 * @Route("/{id}/status-change", name="industry_status_change")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_INDUSTRY_CHANGE_STATUS")
	 */
	public function changeStatusAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		
		return $this->changeStatus($entity, 'industry', self::LINK_SHOW);
	}

	/**
	 * Confirms to delete an Industry entity.
	 *
	 * @Route("/{id}/delete", name="industry_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_INDUSTRY_DELETE")
	 */
	public function confirmDeleteAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
		
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}

	/**
	 * Deletes an Industry entity.
	 *
	 * @Route("/{id}", name="industry_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_INDUSTRY_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$form = $this->createDeleteForm($id);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again later.');
			}
		}
		
		return $this->redirect($this->generateUrl('industry'));
	}

	/**
	 * @Route("/revision/{id}", name="industry_revision")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_INDUSTRY_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		
		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}
	
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="industry_log")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_INDUSTRY_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();
		
		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}
}
