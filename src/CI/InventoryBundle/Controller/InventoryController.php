<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Inventory controller.
 *
 * @Route("/inventory")
 */
class InventoryController extends BaseController
{	
	public function getModel()
	{
		return $this->get('ci.inventory.model');
	}
	
	/**
	 * Displays a form to create a new Inventory entity for a Product entity
	 *
	 * @Route("/{id}/new", name="inventory_new", requirements={"id" = "\d+"})
	 * @Method("GET")
	 * @Template()
	 */
	public function newAction($id)
	{
		$model = $this->getModel();
		$entity = $model->getNewPreparedEntity($id);        
		$form = $model->getFormType($entity);
		
		return array(
			'form'  => $form->createView()
		);
	}


	/**
	 * Creates a new Inventory Entity for Product entity
	 * 
	 * @Route("/create", name="inventory_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Inventory:new.html.twig")
	*/
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$responseData = $model->saveEntity($form, $entity);
			}
		}
		
		if(!isset($responseData)) {
			$responseData = $model->getErrorResponseData($form, $entity);
		}
		
		return new JsonResponse($responseData);
	}
	
	/**
	 * Displays a form to edit an existing Inventory entity.
	 *
	 * @Route("/{id}/edit", name="inventory_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
	 * Edits an existing Inventory entity of a Product entity
	 *
	 * @Route("/{id}", name="inventory_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Inventory:edit.html.twig")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid() && $entity->getCustomer()) {
				$responseData = $model->saveEntity($form, $entity);
			}
		}
		
		if(!isset($responseData)) {
			$responseData = $model->getErrorResponseData($form, $entity);
		}
		
		return new JsonResponse($responseData);
	}
	
	/**
	 * Delete a Inventory entity
	 * 
	 * @Route("/{id}/delete", name="inventory_delete")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		
		return new JsonResponse($model->deleteEntity($id));
	}

	/**
	 * @Route("/find-stocks-by-location", name="inventory_find_stocks_by_location")
	 * @Method("GET")
	 */
	public function findStocksByLocationAction(Request $request)
	{
		$model = $this->getModel();
		$warehouseNumber = strtoupper($request->query->get('warehouseNumber'));
		$lane = $request->query->get('lane');

		$results = $model->findStocksByLocation($warehouseNumber, $lane);

		return new JsonResponse($results);
	}

	/**
	 * @Route("/find-lot-numbers/{productId}", name="inventory_find_lot_numbers")
	 * @Method("GET")
	 */
	public function findLotNumbersAction(Request $request, $productId)
	{
		$model = $this->getModel();

		$results = $model->findLotNumbers($productId);

		return new JsonResponse($results);
	}

	/**
	 * @Route("/find-production-date/{lotNumber}", name="inventory_find_production_date")
	 * @Method("GET")
	 */
	public function findProductionDateAction(Request $request, $lotNumber)
	{
		$model = $this->getModel();

		$results = $model->findProductionDate($lotNumber);

		return new JsonResponse($results);
	}

	/**
	 * @Route("/find-pallet-ids/{productId}/{lotNumber}", name="inventory_find_pallet_ids")
	 * @Method("GET")
	 */
	public function findPalletIdsAction(Request $request, $productId, $lotNumber)
	{
		$model = $this->getModel();

		$results = $model->findPalletIds($productId, $lotNumber);

		return new JsonResponse($results);
	}

	/**
	 * @Route("/find-storage-location/{productId}/{lotNumber}/{palletId}", name="inventory_find_storage_location")
	 * @Method("GET")
	 */
	public function findStorageLocationAction(Request $request, $productId, $lotNumber, $palletId)
	{
		$model = $this->getModel();

		$results = $model->findStorageLocation($productId, $lotNumber, $palletId);

		return new JsonResponse($results);
	}

	/**
	 * Transfer storage location 
	 * 
	 * @Route("/transfer-storage-location/{id}/{storageLocationId}/{quantity}", name="inventory_transfer_storage_location_update", requirements={"id" = "\d+", "storageLocationId" = "\d+", "quantity" = "|\d+"})
	 * @Method("POST")
	 * @Secure(roles="ROLE_PRODUCT_ADJUST_INVENTORY_STOCKS")
	*/
	public function transferStorageLocationUpdateAction($id, $storageLocationId, $quantity)
	{
		$model = $this->getModel();
		
		$responseData = $model->transferStorageLocation($id, $storageLocationId, $quantity);
		
		return new JsonResponse($responseData);
	}
}