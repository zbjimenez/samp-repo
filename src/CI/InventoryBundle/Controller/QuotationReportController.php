<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Model\QuotationReportModel as Model;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

/**
 * ProductReport controller.
 *
 * @Route("/report/quotation")
 */
class QuotationReportController extends Controller
{
	public function getModel()
	{
		return $this->get('ci.quotation.report.model');
	}
	
	/**
	 * @Route("/price-history", name="quotationreport_pricehistory")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_PRICE_LIST_REPORT')")
	 */
	public function getPriceHistoryAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getPriceHistoryReport(Model::FILTER);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getPriceHistoryReport(Model::INDEX, $params);
			
				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
						$qb,
						$this->get('request')->query->get('page', 1),
						$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}
		
		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 *
	 * @Route("/price-history/export", name="quotationreport_pricehistory_export")
	 */
	public function getPriceHistoryExportAction(Request $request)
	{
		$model = $this->getModel();
		$form  = $model->getPriceHistoryReport(Model::FILTER);
		$form->handleRequest($request);
	
		$params = $form->getData();
		$pdf = $model->getPriceHistoryReport(Model::PDF, $params);
	
		$rg = $this->get('ci.report.generator');
		
		return $rg->exportCellReport($pdf['title'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], null, CustomTCPDF::REPORT, $pdf['misc']);
	}
}