<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\ProductModel as Model;
use CI\InventoryBundle\Entity\ProductCode;

/**
 * Product controller.
 *
 * @Route("/product")
 */
class ProductController extends BaseController
{
	const LINK_SHOW = 'product_show';
	
	public function getModel()
	{
		return $this->get('ci.product.model');
	}
	
	/**
	 * Lists all Product.
	 *
	 * @Route("/", name="product")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PRODUCT_LIST")
	 */
	public function indexAction(Request $request) 
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page'),
			array('distinct' => true)
		);
		
		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 * Creates a new Product entity.
	 *
	 * @Route("/", name="product_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Product:new.html.twig")
	 * @Secure(roles="ROLE_PRODUCT_CREATE")
	 */
	public function createAction(Request $request) 
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Displays a form to create a new Product entity.
	 *
	 * @Route("/new", name="product_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PRODUCT_CREATE")
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$entity->addCode(new ProductCode());
		$form = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Finds and displays a Product entity.
	 *
	 * @Route("/{id}/show", name="product_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PRODUCT_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		
		return array(
			'entity' => $entity
		);
	}

	 /**
	 * Displays a form to edit an existing Product entity.
	 *
	 * @Route("/{id}/edit", name="product_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PRODUCT_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}


	/**
	 * Edits an existing Product entity.
	 *
	 * @Route("/{id}", name="product_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Product:edit.html.twig")
	 * @Secure(roles="ROLE_PRODUCT_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		$editForm = $model->getFormType($entity);
		
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Deletes a Product entity.
	 *
	 * @Route("/{id}/delete", name="product_confirm_delete")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_PRODUCT_DELETE")
	 */
	public function confirmDeleteAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
			
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}
	
	/**
	 * @Route("/{id}", name="product_delete")
	 * @Method("DELETE")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_PRODUCT_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$form = $this->createDeleteForm($id);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again later.');
			}
		}
		
		return $this->redirect($this->generateUrl('product'));
	}
	
	/**
	 * Confirm status change of a Product entity.
	 *
	 * @Route("/{id}/confirm-change-status/{status}", name="product_confirm_change_status")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_PRODUCT_CHANGE_STATUS")
	 */
	public function confirmChangeStatusAction($id, $status)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$name = '[Product] '. $entity->getSku() . ' '  . $entity->getName() . ' (ID# ' . $entity->getId() . ')';
	
		return $this->confirmChangeStatus($id, $status, $name, 'product_status_change', self::LINK_SHOW);
	}
	
	/**
	 * Change status of a Product entity.
	 *
	 * @Route("/{id}/status-change", name="product_status_change")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_PRODUCT_CHANGE_STATUS")
	 */
	public function changeStatusAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
	
		return $this->changeStatus($entity, 'product', self::LINK_SHOW);
	}
	
	/**
	 * @Route("/revision/{id}", name="product_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_PRODUCT_READ") 
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		
		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}
	
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="product_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_PRODUCT_READ") 
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();

		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}
	
	/**
	 * @Route("/search", name="product_search")
	 * @Method("POST")
	 */
	public function searchAction(Request $request)
	{
		$model = $this->getModel();

		return new JsonResponse($model->search($request), 200);
	}
	
	/**
	 * @Route("/customer/search", name="product_customer_search")
	 * @Method("POST")
	 */
	public function customerSearchAction(Request $request)
	{
		$model = $this->getModel();

		return new JsonResponse($model->customerSearch($request), 200);
	}
	
	/**
	 * Displays a form for the uploading of files to the given Product.
	 *
	 * @Route("/{id}/file-upload", name="product_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PRODUCT_FILE_UPLOAD")
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
	
		return array(
			'entity' => $entity,
			'form' => $form->createView()
		);
	}
	
	/**
	 * Updates the files of an existing Product entity.
	 *
	 * @Route("/{id}/file-upload", name="product_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Product:file.html.twig")
	 * @Secure(roles="ROLE_PRODUCT_FILE_UPLOAD")
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="product_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_PRODUCT_FILE_DOWNLOAD")
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);
			
		return;
	}
	
	/**
	 *
	 * @Route("/inventory/{id}/adjust", name="product_inventory_adjust")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Product:adjustInventory.html.twig")
	 * @Secure(roles="ROLE_PRODUCT_ADJUST_INVENTORY_STOCKS")
	 */
	public function adjustInventoryAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getAdjustmentFormType();
	
		return array(
			'entity' => $entity,
			'form' => $form->createView()
		);
	}
	
	/**
	 *
	 * @Route("/inventory/{id}", name="product_inventory_adjust_update")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Product:adjustInventory.html.twig")
	 * @Secure(roles="ROLE_PRODUCT_ADJUST_INVENTORY_STOCKS")
	 */
	public function adjustInventoryUpdateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getAdjustmentFormType();
			
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$responseData = $model->adjustInventory($form, $entity);
			}
		}
			
		if(!isset($responseData)) {
			$responseData = $model->getErrorResponseData($form, $entity);
		}
			
		return new JsonResponse($responseData);
	}
}