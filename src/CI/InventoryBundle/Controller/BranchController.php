<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Branch controller.
 *
 * @Route("/branch")
 */
class BranchController extends BaseController
{	
	public function getModel()
	{
		return $this->get('ci.branch.model');
	}
	
	/**
	 * Displays a form to create a new Branch entity for a Customer entity
	 *
	 * @Route("/{id}/new", name="branch_new", requirements={"id" = "\d+"})
	 * @Method("GET")
	 * @Template()
	 */
	public function newAction($id)
	{
		$model = $this->getModel();
		$entity = $model->getNewPreparedEntity($id);        
		$form = $model->getFormType($entity);
		
		return array(
			'form'  => $form->createView()
		);
	}


	/**
     * Creates a new Branch Entity for Customer entity
     * 
     * @Route("/create", name="branch_create")
     * @Method("POST")
     * @Template("CIInventoryBundle:Branch:new.html.twig")
    */
    public function createAction(Request $request)
    {
    	$model = $this->getModel();
        $entity = $model->getNewEntity();
        $form = $model->getFormType($entity);
        
        if ($form->handleRequest($request)->isSubmitted()) {
        	if ($form->isValid()) {
        		$responseData = $model->saveEntity($form, $entity);
        	}
        }
        
        if(!isset($responseData)) {
        	$responseData = $model->getErrorResponseData($form, $entity);
        }
        
        return new JsonResponse($responseData);
    }
	
	/**
	 * Displays a form to edit an existing Branch entity.
	 *
	 * @Route("/{id}/edit", name="branch_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
     * Edits an existing Branch entity of a Customer entity
     *
     * @Route("/{id}", name="branch_update")
     * @Method("PUT")
     * @Template("CIInventoryBundle:Branch:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	$form = $model->getFormType($entity);
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid() && $entity->getCustomer()) {
    			$responseData = $model->saveEntity($form, $entity);
    		}
    	}
    	
    	if(!isset($responseData)) {
    		$responseData = $model->getErrorResponseData($form, $entity);
    	}
    	
    	return new JsonResponse($responseData);
    }
    
    /**
	 * Delete a Branch entity
	 * 
     * @Route("/{id}/delete", name="branch_delete")
     */
    public function deleteAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	
    	return new JsonResponse($model->deleteEntity($id));
    }
    
    /**
     * @Route("/{id}/get", name="branch_get_json")
     * @Method("GET")
     */
    public function getJsonAction($id)
    {
    	$model = $this->getModel();
    	$response = $model->getJson($id);
    
    	return new JsonResponse($response, 200);
    }
}