<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\PurchaseOrderModel as Model;
use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Entity\PurchaseOrder;

/**
 * PurchaseOrder controller.
 *
 * @Route("/purchase-order")
 */
class PurchaseOrderController extends BaseController
{
	const LINK_SHOW = 'purchaseorder_show';

	public function getModel()
	{
		return $this->get('ci.purchaseorder.model');
	}

	/**
	 * Shows the PurchaseOrder Listing.
	 *
	 * @Route("/", name="purchaseorder")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PURCHASEORDER_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page'),
			array("distinct" => true)
		);

		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 * Creates a new PurchaseOrder entity.
	 *
	 * @Route("/", name="purchaseorder_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:PurchaseOrder:new.html.twig")
	 * @Secure(roles="ROLE_PURCHASEORDER_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'form' => $form->createView()
		);
	}

	/**
	 * Displays a form to create a new PurchaseOrder entity.
	 *
	 * @Route("/new", name="purchaseorder_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PURCHASEORDER_CREATE")
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$entity->setPoId($model->generateNumber());
		$form = $model->getFormType($entity);

		return array(
			'entity' => $entity,
			'form' => $form->createView()
		);
	}

	/**
	 * Finds and displays a PurchaseOrder entity.
	 *
	 * @Route("/{id}/show", name="purchaseorder_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PURCHASEORDER_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		return array(
			'entity' => $entity
		);
	}

	/**
	 * Displays a form to edit an existing PurchaseOrder entity.
	 *
	 * @Route("/{id}/edit", name="purchaseorder_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PURCHASEORDER_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		$editForm = $model->getFormType($entity);

		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		);
	}

	/**
	 * Edits an existing PurchaseOrder entity.
	 *
	 * @Route("/{id}", name="purchaseorder_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:PurchaseOrder:edit.html.twig")
	 * @Secure(roles="ROLE_PURCHASEORDER_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		$model->storeOriginalItems($entity);
		$editForm = $model->getFormType($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		);
	}

	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="purchaseorder_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_PURCHASEORDER_DELETE")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);

		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}

	/**
	 * Deletes a PurchaseOrder entity.
	 *
	 * @Route("/{id}", name="purchaseorder_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_PURCHASEORDER_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $this->createDeleteForm($id);

		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}

		return $this->redirect($this->generateUrl('purchaseorder'));
	}

	/**
	 * @Route("/revision/{id}", name="purchaseorder_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_PURCHASEORDER_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);

		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}

	/**
	 * @Route("/revision/{parentId}/{id}/log", name="purchaseorder_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_PURCHASEORDER_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();

		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}

	/**
	 * @Route("/status/{id}/{status}/change", name="purchaseorder_change_status")
	 * @Secure(roles="ROLE_PURCHASEORDER_APPROVE, ROLE_PURCHASEORDER_VOID, ROLE_PURCHASEORDER_CLOSE")
	 */
	public function changeStatusAction($id, $status)
	{
		$model = $this->getModel();
		try {
			$status = $model->changeStatus($id, $status);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_STATUS_CHANGE) . $status . '.');
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}

		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 * @Route("/change-status-multiple", name="purchaseorder_change_status_multiple")
	 * @Method("POST")
	 * @Secure(roles="ROLE_PURCHASEORDER_APPROVE, ROLE_PURCHASEORDER_VOID, ROLE_PURCHASEORDER_CLOSE")
	 */
	public function changeStatusBulkAction(Request $request)
	{
		$model = $this->getModel();
		$ids = $request->request->get('ids');
		$status = $request->request->get('status');

		try {
			$results = $model->changeStatusBulk($ids, $status);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', 'An unknown error occured, please try again.');
		}

		return new JsonResponse($results);
	}

	/**
	 *
	 * @Route("/shipping-information/{id}/edit", name="purchaseorder_shipping_edit")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:PurchaseOrder:shippingInformation.html.twig")
	 */
	public function editShippingInformationAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getShippingFormType($entity);

		return array(
			'entity' => $entity,
			'form' => $form->createView()
		);
	}

	/**
	 *
	 * @Route("/shipping-information/{id}", name="purchaseorder_shipping_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:PurchaseOrder:shippingInformation.html.twig")
	 */
	public function updateShippingInformationAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getShippingFormType($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$responseData = $model->saveShippingInformation($form, $entity);
			}
		}

		if(!isset($responseData)) {
			$responseData = $model->getErrorResponseData($form, $entity);
		}

		return new JsonResponse($responseData);
	}

	/**
	 * @Route("/shipping-information/status/{id}/{status}/change", name="purchaseorder_change_shipping_status")
	 */
	public function changeShippingStatusAction($id, $status)
	{
		$model = $this->getModel();

		try {
			$status = $model->changeShippingStatus($id, $status);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_SHIPPING_STATUS_CHANGE) . $status . '.');
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}

		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 *
	 * @Route("/export/{id}", name="purchaseorder_export")
	 * @Secure(roles="ROLE_PURCHASEORDER_PRINT"))
	 */
	public function exportAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$rg = $this->get('ci.report.generator');

		if ($entity->getStatus() == PurchaseOrder::STATUS_APPROVED) {
			$isApproved = true;
		} else {
			$isApproved = false;
		}

		return $rg->exportReport('Purchase Order # ' . $entity->getPoId(), null, $entity, 'CIInventoryBundle:PurchaseOrder:export.html.twig', null, CustomTCPDF::LANDSCAPE, $isApproved);
	}


	/**
	 * Displays a form to upload files to PurchaseOrder.
	 *
	 * @Route("/{id}/file-upload", name="purchaseorder_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PURCHASEORDER_FILE_UPLOAD") 
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Updates the files for an existing PurchaseOrder entity.
	 *
	 * @Route("/{id}/file-upload", name="purchaseorder_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:PurchaseOrder:file.html.twig")
	 * @Secure(roles="ROLE_PURCHASEORDER_FILE_UPLOAD")
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="purchaseorder_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_PURCHASEORDER_FILE_DOWNLOAD")
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);
			
		return;
	}
}
