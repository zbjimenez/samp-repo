<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\CustomerModel as Model;
use CI\InventoryBundle\Entity\Branch;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Customer controller.
 *
 * @Route("/customer")
 */
class CustomerController extends BaseController
{
	const LINK_SHOW = 'customer_show';
	
	public function getModel()
	{
		return $this->get('ci.customer.model');
	}
	
	/**
	 * Lists all Customer.
	 *
	 * @Route("/", name="customer")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CUSTOMER_LIST")
	 */
	public function indexAction(Request $request) 
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);
		
		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 * Creates a new Customer entity.
	 *
	 * @Route("/", name="customer_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Customer:new.html.twig")
	 * @Secure(roles="ROLE_CUSTOMER_CREATE")
	 */
	public function createAction(Request $request) 
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Displays a form to create a new Customer entity.
	 *
	 * @Route("/new", name="customer_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CUSTOMER_CREATE")
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$entity->addBranch(new Branch());
		$form = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Finds and displays a Customer entity.
	 *
	 * @Route("/{id}/show", name="customer_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CUSTOMER_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		$recentTransactions = $model->getLatestTransactions($entity);
			
		return array(
			'entity' => $entity,
			'recentTransactions' => $recentTransactions,
		);
	}

	 /**
	 * Displays a form to edit an existing Customer entity.
	 *
	 * @Route("/{id}/edit", name="customer_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CUSTOMER_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
		
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}


	/**
	 * Edits an existing Customer entity.
	 *
	 * @Route("/{id}", name="customer_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Customer:edit.html.twig")
	 * @Secure(roles="ROLE_CUSTOMER_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		$editForm = $model->getFormType($entity);
		
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Deletes a Customer entity.
	 *
	 * @Route("/{id}/delete", name="customer_confirm_delete")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_CUSTOMER_DELETE")
	 */
	public function confirmDeleteAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
			
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}
	
	/**
	 * @Route("/{id}", name="customer_delete")
	 * @Method("DELETE")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_CUSTOMER_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$form = $this->createDeleteForm($id);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again later.');
			}
		}
		
		return $this->redirect($this->generateUrl('customer'));
	}
	
	/**
	 * Confirm status change of a Customer entity.
	 *
	 * @Route("/{id}/confirm-change-status/{status}", name="customer_confirm_change_status")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_CUSTOMER_CHANGE_STATUS")
	 */
	public function confirmChangeStatusAction($id, $status)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$name = '[Customer] '. $entity->getName() . ' (ID# ' . $entity->getId() . ')';
		
		return $this->confirmChangeStatus($id, $status, $name, 'customer_status_change', self::LINK_SHOW);
	}
	
	/**
	 * Change status of a Customer entity.
	 *
	 * @Route("/{id}/status-change", name="customer_status_change")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_CUSTOMER_CHANGE_STATUS")
	 */
	public function changeStatusAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		
		return $this->changeStatus($entity, 'customer', self::LINK_SHOW);
	}
	
	/**
	 * @Route("/revision/{id}", name="customer_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_CUSTOMER_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}
	
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="customer_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_CUSTOMER_READ") 
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();
		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}
	
	/**
	 * @Route("/{id}/get", name="customer_get_json")
	 * @Method("GET")
	 */
	public function getJsonAction($id)
	{
		$model = $this->getModel();
		$response = $model->getJson($id);
	
		return new JsonResponse($response, 200);
	}

	/**
	 * Displays a form to upload files to Customer.
	 *
	 * @Route("/{id}/file-upload", name="customer_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CUSTOMER_FILE_UPLOAD") 
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
	
		return array(
				'entity' => $entity,
				'form'   => $form->createView()
		);
	}
	
	/**
	 * Updates the files for an existing Customer entity.
	 *
	 * @Route("/{id}/file-upload", name="customer_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Customer:file.html.twig")
	 * @Secure(roles="ROLE_CUSTOMER_FILE_UPLOAD") 
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
	
		return array(
				'entity' => $entity,
				'form'   => $form->createView()
		);
	}
	
	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="customer_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_CUSTOMER_FILE_DOWNLOAD") 
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);
			
		return;
	}
}