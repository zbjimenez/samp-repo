<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Entity\Currency;
use CI\InventoryBundle\Model\CurrencyModel as Model;

/**
 * Currency controller.
 *
 * @Route("/currency")
 */
class CurrencyController extends BaseController
{
	const LINK_SHOW = 'currency_show';
	
	public function getModel()
	{
		return $this->get('ci.currency.model');
	}
	
	/**
	 * Lists all Currency entities.
	 *
	 * @Route("/", name="currency")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CURRENCY_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getIndex($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (!isset($qb)) {
			$qb = $model->getIndex();
		}
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);
		
		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 * Creates a new Currency entity.
	 *
	 * @Route("/", name="currency_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Currency:new.html.twig")
	 * @Secure(roles="ROLE_CURRENCY_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Displays a form to create a new Currency entity.
	 *
	 * @Route("/new", name="currency_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CURRENCY_CREATE")
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}
	
	/**
	 * Finds and displays a Currency entity.
	 *
	 * @Route("/{id}/show", name="currency_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CURRENCY_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		
		return array(
			'entity' => $entity
		);
	}

	/**
	 * Displays a form to edit an existing Currency entity.
	 *
	 * @Route("/{id}/edit", name="currency_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_CURRENCY_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
	 * Edits an existing Currency entity.
	 *
	 * @Route("/{id}", name="currency_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Currency:edit.html.twig")
	 * @Secure(roles="ROLE_CURRENCY_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		 
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		 
		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
	 * Deletes a Currency entity.
	 *
	 * @Route("/{id}/delete", name="currency_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_CURRENCY_DELETE")
	 */
	public function confirmDeleteAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
		
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}
	
	/**
	 * @Route("/{id}", name="currency_delete")
	 * @Method("DELETE")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_CURRENCY_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$form = $this->createDeleteForm($id);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again later.');
			}
		}
		
		return $this->redirect($this->generateUrl('currency'));
	}
    
	/**
	 * Confirm status change of a Currency entity.
	 *
	 * @Route("/{id}/confirm-change-status/{status}", name="currency_confirm_change_status")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_CURRENCY_CHANGE_STATUS")
	 */
	public function confirmChangeStatusAction($id, $status)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$name = '[Currency] '. $entity->getCurrencyName() . ' (ID# ' . $entity->getId() . ')';
		
		return $this->confirmChangeStatus($id, $status, $name, 'currency_status_change', self::LINK_SHOW);
    }
    
	/**
	 * Change status of a Currency entity.
	 *
	 * @Route("/{id}/status-change", name="currency_status_change")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @Secure(roles="ROLE_CURRENCY_CHANGE_STATUS")
	 */
	public function changeStatusAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		
		return $this->changeStatus($entity, 'currency', self::LINK_SHOW);
	}
    
	/**
	 * @Route("/revision/{id}", name="currency_revision")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_CURRENCY_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		
		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
    }
    
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="currency_log")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_CURRENCY_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();
		
		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}
	 
	/**
	 * @Route("/{id}/get", name="currency_get_json")
	 * @Method("GET")
	 */
	public function getJsonAction($id)
	{
		$model = $this->getModel();
		$data = $model->getJson($id);
		
		return new JsonResponse($data, 200);
	}
}
