<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\PurchaseReturnModel as Model;
use CI\InventoryBundle\Helper\CustomTCPDF;

/**
 * PurchaseReturn controller.
 *
 * @Route("/shipment-discrepancy-notice")
 */
class PurchaseReturnController extends BaseController
{
	const LINK_SHOW = 'purchasereturn_show';
	
	public function getModel()
	{
		return $this->get('ci.purchasereturn.model');
	}
	
	/**
	 * Shows the PurchaseReturn Listing.
	 *
	 * @Route("/", name="purchasereturn")
	 * @Method("GET")
	 * @Secure(roles="ROLE_PURCHASERETURN_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);
		
		return $this->render('CIInventoryBundle:PurchaseReturn:index.html.twig', array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView()
		));
	}
	
	/**
	 * Creates a new PurchaseReturn entity.
	 *
	 * @Route("/", name="purchasereturn_create")
	 * @Method("POST")
	 * @Secure(roles="ROLE_PURCHASERETURN_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			try {
				$model->isCreatable($entity->getReceiveOrder()->getId());
			} catch (\Exception $e) {
				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirect($this->generateUrl('receiveorder_show', array('id' => $entity->getReceiveOrder()->getId())));
			}
			
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return $this->render('CIInventoryBundle:PurchaseReturn:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView()
		));
	}
	
	/**
	 * Displays a form to create a new PurchaseReturn entity.
	 *
	 * @Route("/{receiveOrderId}/new", name="purchasereturn_new")
	 * @Method("GET")
	 * @Secure(roles="ROLE_PURCHASERETURN_CREATE")
	 */
	public function newAction($receiveOrderId)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity($receiveOrderId);
		$form = $model->getFormType($entity);
		
		try {
			$model->isCreatable($receiveOrderId);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl('receiveorder_show', array('id' => $receiveOrderId)));
		}
		
		return $this->render('CIInventoryBundle:PurchaseReturn:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView()
		));
	}
	
	/**
	 * Finds and displays a PurchaseReturn entity.
	 *
	 * @Route("/{id}/show", name="purchasereturn_show")
	 * @Method("GET")
	 * @Secure(roles="ROLE_PURCHASERETURN_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		
		return $this->render('CIInventoryBundle:PurchaseReturn:show.html.twig', array(
			'entity' => $entity
		));
	}
	
	/**
	 * Displays a form to edit an existing PurchaseReturn entity.
	 *
	 * @Route("/{id}/edit", name="purchasereturn_edit")
	 * @Method("GET")
	 * @Secure(roles="ROLE_PURCHASERETURN_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		
		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
		
		$editForm = $model->getFormType($entity);
		
		return $this->render('CIInventoryBundle:PurchaseReturn:edit.html.twig', array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		));
	}
	
	/**
	 * Edits an existing PurchaseReturn entity.
	 *
	 * @Route("/{id}", name="purchasereturn_update")
	 * @Method("PUT")
	 * @Secure(roles="ROLE_PURCHASERETURN_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		
		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
		
		$model->storeOriginalItems($entity);
		$editForm = $model->getFormType($entity);
		
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return $this->render('CIInventoryBundle:PurchaseReturn:edit.html.twig', array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		));
	}
	
	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="purchasereturn_confirm_delete")
	 * @Method("GET")
	 * @Secure(roles="ROLE_PURCHASERETURN_DELETE")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
			
		return $this->render('CIInventoryBundle:Misc:delete.html.twig', array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		));
	}
	
	/**
	 * Deletes a PurchaseReturn entity.
	 *
	 * @Route("/{id}", name="purchasereturn_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_PURCHASERETURN_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}
		
		return $this->redirect($this->generateUrl('purchasereturn'));
	}
	
	/**
	 * @Route("/revision/{id}", name="purchasereturn_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_PURCHASERETURN_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}
	
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="purchasereturn_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_PURCHASERETURN_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();
		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}
	
	/**
	 * @Route("/status/{id}/{status}/change", name="purchasereturn_change_status")
	 * @Secure(roles="ROLE_PURCHASERETURN_VOID, ROLE_PURCHASERETURN_CLOSE")
	 */
	public function changeStatusAction($id, $status)
	{
		$model = $this->getModel();
		
		try {
			$status = $model->changeStatus($id, $status);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_STATUS_CHANGE) . $status . '.');
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}
		
		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}
	
	/**
	 * @Route("/export/{id}", name="purchasereturn_export")
	 */
	public function exportAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$rg = $this->get('ci.report.generator');
	
		return $rg->exportReport('Shipment Discrepancy Notice # ' . $entity->getId(), null, $entity, 'CIInventoryBundle:PurchaseReturn:export.html.twig', null, CustomTCPDF::FORM);
	}

	/**
	 * Displays a form to upload files to PurchaseReturn.
	 *
	 * @Route("/{id}/file-upload", name="purchasereturn_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_PURCHASERETURN_FILE_UPLOAD")
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Updates the files for an existing PurchaseReturn entity.
	 *
	 * @Route("/{id}/file-upload", name="purchasereturn_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:PurchaseReturn:file.html.twig")
	 * @Secure(roles="ROLE_PURCHASERETURN_FILE_UPLOAD")
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
	
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="purchasereturn_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_PURCHASERETURN_FILE_DOWNLOAD")
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);
			
		return;
	}
}