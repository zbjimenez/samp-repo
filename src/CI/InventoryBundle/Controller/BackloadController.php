<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Entity\Backload;
use CI\InventoryBundle\Form\Type\BackloadType;
use CI\InventoryBundle\Model\BackloadModel as Model;

/**
 * Backload controller.
 *
 * @Route("/backload")
 */
class BackloadController extends BaseController
{
	const LINK_SHOW = 'backload_show';

	public function getModel()
	{
		return $this->get('ci.backload.model');
	}

	/**
	 * Lists all Backload entities.
	 *
	 * @Route("/", name="backload")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_BACKLOAD_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getIndex($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$qb = $model->getIndex();
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);

		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 * Creates a new Backload entity.
	 *
	 * @Route("/", name="backload_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Backload:new.html.twig")
	 * @Secure(roles="ROLE_BACKLOAD_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'form' => $form->createView(),
		);
	}

	/**
	 * Displays a form to create a new Backload entity.
	 *
	 * @Route("/new/{shippingId}", name="backload_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_BACKLOAD_CREATE")
	 */
	public function newAction($shippingId)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$model->setEntityData($entity, $shippingId);
		$form = $model->getFormType($entity);

		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Finds and displays a Backload entity.
	 *
	 * @Route("/{id}/show", name="backload_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_BACKLOAD_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		return array(
			'entity' => $entity,
		);
	}

	/**
	 * Displays a form to edit an existing Backload entity.
	 *
	 * @Route("/{id}/edit", name="backload_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_BACKLOAD_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
		}

		$model->setEntityData($entity);
		$editForm = $model->getFormType($entity);

		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Edits an existing Backload entity.
	 *
	 * @Route("/{id}", name="backload_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Backload:edit.html.twig")
	 * @Secure(roles="ROLE_BACKLOAD_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
		}
		
		$editForm = $model->getFormType($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 *
	 * @Route("/{id}/for-put-away", name="backload_for_put_away")
	 * @Method("GET")
	 * @Secure(roles="ROLE_BACKLOAD_CHANGE_STATUS")
	 */
	public function forPutAwayAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		try {
			$status = $model->forPutAway($entity);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_FOR_PUT_AWAY));
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}

		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="backload_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_BACKLOAD_DELETE")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
	
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
			
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity),
		);
	}
	
	/**
	 * Deletes a backload entity.
	 *
	 * @Route("/{id}", name="backload_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_BACKLOAD_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $this->createDeleteForm($id);
	
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
	
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}
	
		return $this->redirect($this->generateUrl('backload'));
	}

	 /**
	 * @Route("/status/{id}/{status}/change", name="backload_change_status")
	 * @Method("GET")
	 * @Secure(roles="ROLE_BACKLOAD_CHANGE_STATUS")
	 */
	public function changeStatusAction($id, $status)
	{
		try {
			$model = $this->getModel();
			$status = $model->changeStatus($id, $status);
			$this->get('session')->getFlashBag()->add('danger', $model->getMessages(Model::ACTION_CHANGE_STATUS));
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}

		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 * @Route("/revision/{id}", name="backload_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_BACKLOAD_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);

		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}
	
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="backload_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_BACKLOAD_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();

		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}
}
