<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Model\InventoryReportModel as Model;

/**
 * InventoryReport controller.
 *
 * @Route("/report/inventory")
 */
class InventoryReportController extends Controller
{
	public function getModel()
	{
		return $this->get('ci.inventory.report.model');
	}

	/**
	 * @Route("/list", name="get_inventory_list")
	 * @Method("get")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_INVENTORY_REPORT')")
	 */
	public function getInventoryListAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getInventoryListReport(Model::FILTER);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getInventoryListReport(Model::INDEX, $params);

				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$qb,
					$this->get('request')->query->get('page', 1),
					$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}

		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 * @Route("/warehouse", name="get_warehouse_inventory")
	 * @Method("get")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_WAREHOUSE_INVENTORY_REPORT')")
	 */
	public function getWarehouseInventoryAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getWarehouseInventoryReport(Model::FILTER);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$results = $model->getWarehouseInventoryReport(Model::INDEX, $params);

				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$results['qb']->getQuery(),
					$this->get('request')->query->get('page', 1),
					$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}

		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'productTotalsResult' => isset($results) ? $results['productTotalsResult'] : null,
			'lotNumberTotalsResult' => isset($results) ? $results['lotNumberTotalsResult'] : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 *
	 * @Route("/list/export", name="get_inventory_list_export")
	 * @PreAuthorize("hasAnyRole('ROLE_INVENTORY_REPORT')")
	 */
	public function getInventoryListExportAction(Request $request)
	{
		$model = $this->getModel();
		$form  = $model->getInventoryListReport(Model::FILTER);
		$form->handleRequest($request);

		$params = $form->getData();
		$pdf = $model->getInventoryListReport(Model::PDF, $params);

		$rg = $this->get('ci.report.generator');
		
		return $rg->exportMultiCellReport($pdf['title'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], null, CustomTCPDF::LANDSCAPE, $pdf['misc']);
	}

	/**
	 *
	 * @Route("/warehouse/export", name="get_warehouse_inventory_export")
	 * @PreAuthorize("hasAnyRole('ROLE_WAREHOUSE_INVENTORY_REPORT')")
	 */
	public function getWarehouseInventoryExportAction(Request $request)
	{
		$model = $this->getModel();
		$form  = $model->getWarehouseInventoryReport(Model::FILTER);
		$form->handleRequest($request);

		$params = $form->getData();
		$pdf = $model->getWarehouseInventoryReport(Model::PDF, $params);

		$rg = $this->get('ci.report.generator');
		
		return $rg->exportMultiCellReport($pdf['title'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], null, CustomTCPDF::LANDSCAPE, $pdf['misc']);
	}
}
