<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\PackagingModel as Model;

/**
 * Packaging controller.
 *
 * @Route("/packaging")
 */
class PackagingController extends BaseController
{
	const LINK_SHOW = 'packaging_show';
	
	public function getModel()
	{
		return $this->get('ci.packaging.model');
	}
	
    /**
     * Lists all Packaging.
     *
     * @Route("/", name="packaging")
     * @Method("GET")
     * @Template()
	 * @Secure(roles="ROLE_PACKAGING_LIST")
     */
    public function indexAction(Request $request) 
    {
    	$model = $this->getModel();
    	$form = $model->getFilterFormType();
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			$params = $form->getData();
    			$qb = $model->getIndex($params);
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
    		}
    	}
    	
    	if (!isset($qb)) {
    		$qb = $model->getIndex();
    	}
    	
    	$paginator = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
    		$qb,
    		$this->get('request')->query->get('page', 1),
    		$this->container->getParameter('pagination_limit_per_page'),
    		array('distinct' => true)
    	);
    	
    	return array(
    		'pagination' => isset($pagination) ? $pagination : null,
    		'search_form' => $form->createView(),
    	);
    }

    /**
     * Creates a new Packaging entity.
     *
     * @Route("/", name="packaging_create")
     * @Method("POST")
     * @Template("CIInventoryBundle:Packaging:new.html.twig")
	 * @Secure(roles="ROLE_PACKAGING_CREATE")
     */
    public function createAction(Request $request) 
    {
    	$model = $this->getModel();
    	$entity = $model->getNewEntity();
    	$form = $model->getFormType($entity);
    	
    	$flag = false;
    	if ($request->isXmlHttpRequest()) {
    		$flag = true;
    	}
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			if ($flag === true) {
    				$responseData = $model->saveModalEntity($form, $entity);
    				return new JsonResponse($responseData, 200);
    			}
    			try {
    				$model->saveEntity($form, $entity);
    				$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
    				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    			} catch (\Exception $e) {
    				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    			}
    		} else {
    			if ($flag == true) {
    				if(!isset($responseData)) {
    					$responseData = $model->getErrorResponseData($form, $entity);
    				}
    				return new JsonResponse($responseData);
    			}
    			$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
    		}
    	}
    	
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView(),
    	);
    }
    
    /**
     * Displays a form to create a new Packaging entity.
     *
     * @Route("/new", name="packaging_new")
     * @Method("GET")
     * @Template()
	 * @Secure(roles="ROLE_PACKAGING_CREATE")
     */
    public function newAction(Request $request)
    {
    	$model = $this->getModel();
    	$entity = $model->getNewEntity();
    	$form = $model->getFormType($entity);
    	
    	if ($request->isXmlHttpRequest()) {
    		$form = $this->renderView('CIInventoryBundle:Packaging:modal.html.twig', array(
    				'form' => $form->createView(),
    				'entity' => $entity,
    				'prefix' => 'packaging'
    		));
    			
    		return new JsonResponse($form, 200);
    	}
    	
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView(),
    	);
    }
    
    /**
     * Finds and displays a Packaging entity.
     *
     * @Route("/{id}/show", name="packaging_show")
     * @Method("GET")
     * @Template()
	 * @Secure(roles="ROLE_PACKAGING_READ")
     */
    public function showAction($id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    		
    	return array(
    		'entity' => $entity
    	);
    }

     /**
     * Displays a form to edit an existing Packaging entity.
     *
     * @Route("/{id}/edit", name="packaging_edit")
     * @Method("GET")
     * @Template()
	 * @Secure(roles="ROLE_PACKAGING_UPDATE")
     */
	 
    public function editAction($id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	$editForm = $model->getFormType($entity);
    	
    	return array(
    		'entity'    => $entity,
    		'edit_form' => $editForm->createView(),
    	);
    }


    /**
     * Edits an existing Packaging entity.
     *
     * @Route("/{id}", name="packaging_update")
     * @Method("PUT")
     * @Template("CIInventoryBundle:Packaging:edit.html.twig")
	 * @Secure(roles="ROLE_PACKAGING_UPDATE")
     */
    public function updateAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	$editForm = $model->getFormType($entity);
    	
    	if ($editForm->handleRequest($request)->isSubmitted()) {
    		if ($editForm->isValid()) {
    			try {
    				$model->saveEntity($editForm, $entity);
    				$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
    				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    			} catch (\Exception $e) {
    				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    			}
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
    		}
    	}
    	
    	return array(
    		'entity'    => $entity,
    		'edit_form' => $editForm->createView(),
    	);
    }

    /**
     * Deletes a Packaging entity.
     *
     * @Route("/{id}/delete", name="packaging_confirm_delete")
     * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_PACKAGING_DELETE")
     */
    public function confirmDeleteAction($id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	$deleteForm = $this->createDeleteForm($id);
    	
    	try {
    		$model->isDeletable($entity);
    	} catch (\Exception $e) {
    		$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    	}
    		
    	return array(
    		'entity' => $entity,
    		'delete_form' => $deleteForm->createView(),
    		'params' => $model->getDeleteParams($entity)
    	);
    }
    
    /**
     * @Route("/{id}", name="packaging_delete")
     * @Method("DELETE")
     * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_PACKAGING_DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$form = $this->createDeleteForm($id);
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			try {
    				$model->deleteEntity($id);
    				$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
    			} catch (\Exception $e) {
    				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    			}
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again later.');
    		}
    	}
    	
    	return $this->redirect($this->generateUrl('packaging'));
    }
    
	/**
     * Confirm status change of a Packaging entity.
     *
     * @Route("/{id}/confirm-change-status/{status}", name="packaging_confirm_change_status")
     * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
     * @Secure(roles="ROLE_PACKAGING_CHANGE_STATUS")
     */
    public function confirmChangeStatusAction($id, $status)
    {
        $model = $this->getModel();
		$entity = $model->findExistingEntity($id);
        $name = '[Packaging] '. $entity->getName() . ' (ID# ' . $entity->getId() . ')';
    
        return $this->confirmChangeStatus($id, $status, $name, 'packaging_status_change', self::LINK_SHOW);
    }
    
    /**
     * Change status of a Packaging entity.
     *
     * @Route("/{id}/status-change", name="packaging_status_change")
     * @Method("PUT")
     * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
     * @Secure(roles="ROLE_PACKAGING_CHANGE_STATUS")
     */
    public function changeStatusAction($id)
    {
        $model = $this->getModel();
		$entity = $model->findExistingEntity($id);
	
		return $this->changeStatus($entity, 'packaging', self::LINK_SHOW);
    }
    
    /**
     * @Route("/revision/{id}", name="packaging_revision")
     * @Template("CIInventoryBundle:Log:index.html.twig")
     */
    public function revisionAction($id)
    {
    	$model = $this->getModel();
    	$revision = $model->getRevision($id);
    	return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
    }
    
    /**
     * @Route("/revision/{parentId}/{id}/log", name="packaging_log")
     * @Template("CIInventoryBundle:Log:show.html.twig")
     */
    public function logAction($parentId, $id)
    {
    	$model = $this->getModel();
    	$log = $model->getLog();
    	return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
    }
    
    /**
     *
     * @Route("/{id}/get", name="packaging_get_json")
     * @Method("GET")
     */
    public function getJsonAction($id)
    {
    	$model = $this->getModel();
    	$data = $model->getJson($id);
    
    	return new JsonResponse($data, 200);
    }
}