<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use CI\InventoryBundle\Model\QuotationModel as Model;
use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Entity\Quotation;

/**
 * Quotation controller.
 *
 * @Route("/quotation")
 */
class QuotationController extends BaseController
{
	const LINK_SHOW = 'quotation_show';
	const LINK_LISTING = 'quotation';

	public function getModel()
	{
		return $this->get('ci.quotation.model');
	}

	/**
	 * Shows the Quotation Listing.
	 *
	 * @Route("/", name="quotation")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_QUOTATION_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page'),
			array('distinct' => true)
		);

		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 * Creates a new Quotation entity.
	 *
	 * @Route("/", name="quotation_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Quotation:new.html.twig")
	 * @Secure(roles="ROLE_QUOTATION_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}

	/**
	 * Displays a form to create a new Quotation entity.
	 *
	 * @Route("/new", name="quotation_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_QUOTATION_CREATE")
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);

		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}

	/**
	 * Finds and displays a Quotation entity.
	 *
	 * @Route("/{id}/show", name="quotation_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_QUOTATION_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->checkUserAccessEntity($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_LISTING));
		}

		$model->setHasRead($entity);

		return array(
			'entity' => $entity
		);
	}

	/**
	 * Displays a form to edit an existing Quotation entity.
	 *
	 * @Route("/{id}/edit", name="quotation_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_QUOTATION_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		$currentUserCanView = false;
		try {
			$currentUserCanView = $model->checkUserAccessEntity($entity);
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			if ($currentUserCanView) {
				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
			} else {
				return $this->redirect($this->generateUrl(self::LINK_LISTING));
			}
		}

		$editForm = $model->getFormType($entity);

		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		);
	}

	/**
	 * Edits an existing Quotation entity.
	 *
	 * @Route("/{id}", name="quotation_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Quotation:edit.html.twig")
	 * @Secure(roles="ROLE_QUOTATION_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		$currentUserCanView = false;	
		try {
			$currentUserCanView = $model->checkUserAccessEntity($entity);
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			if ($currentUserCanView) {
				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
			} else {
				return $this->redirect($this->generateUrl(self::LINK_LISTING));
			}
		}
		
		$editForm = $model->getFormType($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		);
	}

	/**
	 * Displays a form to upload files to Quotation.
	 *
	 * @Route("/{id}/file-upload", name="quotation_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_QUOTATION_FILE_UPLOAD")
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		try {
			$model->checkUserAccessEntity($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_LISTING));
		}

		$form = $model->getFileFormType($entity);

		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}

	/**
	 * Updates the files for an existing Quotation entity.
	 *
	 * @Route("/{id}/file-upload", name="quotation_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Quotation:file.html.twig")
	 * @Secure(roles="ROLE_QUOTATION_FILE_UPLOAD") 
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		try {
			$model->checkUserAccessEntity($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_LISTING));
		}

		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}

	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="quotation_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_QUOTATION_FILE_DOWNLOAD")
	 */
	public function downloadAction($id)
	{
		$this->getModel()->downloadFile($id);

		return;
	}

	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="quotation_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_QUOTATION_DELETE")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$currentUserCanView = false;
		try {
			$currentUserCanView = $model->checkUserAccessEntity($entity);
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			if ($currentUserCanView) {
				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
			} else {
				return $this->redirect($this->generateUrl(self::LINK_LISTING));
			}
		}

		$deleteForm = $this->createDeleteForm($id);

		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}

	/**
	 * Deletes a Quotation entity.
	 *
	 * @Route("/{id}", name="quotation_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_QUOTATION_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$currentUserCanView = false;
		try {
			$currentUserCanView = $model->checkUserAccessEntity($entity);
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			if ($currentUserCanView) {
				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
			} else {
				return $this->redirect($this->generateUrl(self::LINK_LISTING));
			}
		}

		$form = $this->createDeleteForm($id);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}

		return $this->redirect($this->generateUrl('quotation'));
	}

	/**
	 * @Route("/revision/{id}", name="quotation_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_QUOTATION_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);
		try {
			$model->checkUserAccessEntity($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_LISTING));
		}
		$revision = $model->getRevision($id);

		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}

	/**
	 * @Route("/revision/{parentId}/{id}/log", name="quotation_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_QUOTATION_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($parentId);
		try {
			$model->checkUserAccessEntity($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_LISTING));
		}
		$log = $model->getLog();

		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}

	/**
	 * @Route("/status/{id}/{status}/change", name="quotation_change_status")
	 *  @Secure(roles="ROLE_QUOTATION_CHANGE_STATUS")
	 */
	public function changeStatusAction($id, $status)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		$currentUserCanView = false;
		try {
			$currentUserCanView = $model->checkUserAccessEntity($entity);
			$status = $model->changeStatus($id, $status);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_STATUS_CHANGE) . $status . '.');
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			if ($currentUserCanView) {
				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
			} else {
				return $this->redirect($this->generateUrl(self::LINK_LISTING));
			}
		}

		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 * @Route("/change-status-multiple", name="quotation_change_status_multiple")
	 * @Method("POST")
	 * @Secure(roles="ROLE_QUOTATION_CHANGE_STATUS")
	 */
	public function changeStatusBulkAction(Request $request)
	{
		$model = $this->getModel();
		$ids = $request->request->get('ids');
		$status = $request->request->get('status');

		try {
			$results = $model->changeStatusBulk($ids, $status);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', 'An unknown error occured, please try again.');
		}

		return new JsonResponse($results);
	}

	/**
	 *
	 * @Route("/export/{id}", name="quotation_export")
	 */
	public function exportAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$rg = $this->get('ci.report.generator');
		
		if ($entity->getStatus() == Quotation::STATUS_APPROVED) {
			$type = CustomTCPDF::QUOTATION;
		} else {
			$type = CustomTCPDF::FORM;
		}

		return $rg->exportReport('Quotation # ' . $entity->getReferenceNumber(), null, $entity, 'CIInventoryBundle:Quotation:export.html.twig', null, $type);
	}

	/**
	 * @Route("/search", name="quotation_search")
	 * @Method("POST")
	 */
	public function searchAction(Request $request)
	{
		$model = $this->getModel();
		
		return new JsonResponse($model->search($request), 200);
	}
}
