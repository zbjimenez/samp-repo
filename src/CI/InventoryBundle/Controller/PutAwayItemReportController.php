<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Model\InventoryReportModel as Model;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

/**
 * PutAwayItemReport controller.
 *
 * @Route("/report/put-away-item")
 */
class PutAwayItemReportController extends Controller
{  
	public function getModel()
	{
		return $this->get('ci.put.away.item.report.model');
	}
	
	/**
	 * @Route("/put-away", name="get_put_away_item_putaway_list")
	 * @Method("get")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_PUTAWAY_REPORT')")
	 */
	public function getPutAwayAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getPutAwayReport(Model::FILTER);
		 
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getPutAwayReport(Model::INDEX, $params);
				
				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$qb,
					$this->get('request')->query->get('page', 1),
					$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}

		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 *
	 * @Route("/put-away/export", name="get_put_away_item_putaway_list_export")
	 */
	public function getPutAwayExportAction(Request $request)
	{
		$model = $this->getModel();
		$form  = $model->getPutAwayReport(Model::FILTER);
		$form->handleRequest($request);
	
		$params = $form->getData();
		$pdf = $model->getPutAwayReport(Model::PDF, $params);

		$rg = $this->get('ci.report.generator');
		
		return $rg->exportMultiCellReport($pdf['title'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], null, CustomTCPDF::LANDSCAPE, $pdf['misc']);
	}
}