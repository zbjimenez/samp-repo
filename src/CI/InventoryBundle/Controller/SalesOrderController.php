<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CI\InventoryBundle\Model\SalesOrderModel as Model;
use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Entity\SalesOrder;

/**
 * Sales Order controller.
 *
 * @Route("/sales-order")
 */
class SalesOrderController extends BaseController
{
	const LINK_SHOW = 'salesorder_show';

	public function getModel()
	{
		return $this->get('ci.sales_order.model');
	}

	/**
	 * Lists all Sales Orders.
	 *
	 * @Route("/", name="salesorder")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESORDER_LIST")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->loadEntities($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$qb = $model->loadEntities();
		}

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);

		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 * Creates a new Sales Order entity.
	 *
	 * @Route("/", name="salesorder_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:SalesOrder:new.html.twig")
	 * @Secure(roles="ROLE_SALESORDER_CREATE")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));

					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'form' => $form->createView(),
		);
	}

	/**
	 * Displays a form to create a new Sales Order entity.
	 *
	 * @Route("/new", name="salesorder_new")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESORDER_CREATE")
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$entity->setRefCode($model->generateSoId());
		$form = $model->getFormType($entity);

		return array(
			'entity' => $entity,
			'form' => $form->createView(),
		);
	}

	/**
	 * Finds and displays a Sales Order entity.
	 *
	 * @Route("/{id}/show", requirements={"id"="\d+"}, name="salesorder_show")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESORDER_READ")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->currentUserCanView($entity);
			($entity->getStatus() == SalesOrder::STATUS_DRAFT) ? $model->currentUserCanViewDrafts($entity) : true;
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl("salesorder"));
		}

		return array(
			'entity' => $entity,
		);
	}

	/**
	 * Displays a form to edit an existing Sales Order entity.
	 *
	 * @Route("/{id}/edit", requirements={"id"="\d+"}, name="salesorder_edit")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESORDER_UPDATE")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->currentUserCanView($entity);
			($entity->getStatus() == SalesOrder::STATUS_DRAFT) ? $model->currentUserCanViewDrafts($entity) : true;
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl("salesorder"));
		}

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		$editForm = $model->getFormType($entity);

		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Edits an existing Sales Order entity.
	 *
	 * @Route("/{id}", requirements={"id"="\d+"}, name="salesorder_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:SalesOrder:edit.html.twig")
	 * @Secure(roles="ROLE_SALESORDER_UPDATE")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->loadEntity($id);

		try {
			$model->isEditable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		$editForm = $model->getFormType($entity);
		$model->storeOriginalItems($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));

					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Deletes a Sales Order entity.
	 *
	 * @Route("/{id}/delete", requirements={"id"="\d+"}, name="salesorder_confirm_delete")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @Secure(roles="ROLE_SALESORDER_DELETE")
	 */
	public function confirmDeleteAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);

		try {
			$model->currentUserCanView($entity);
			($entity->getStatus() == SalesOrder::STATUS_DRAFT) ? $model->currentUserCanViewDrafts($entity) : true;
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl("salesorder"));
		}

		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}

		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity),
		);
	}

	/**
	 * @Route("/{id}", requirements={"id"="\d+"}, name="salesorder_delete")
	 * @Method("DELETE")
	 * @Secure(roles="ROLE_SALESORDER_DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$form = $this->createDeleteForm($id);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again later.');
			}
		}

		return $this->redirect($this->generateUrl('salesorder'));
	}

	/**
	 * @Route("/revision/{id}", requirements={"id"="\d+"}, name="salesorder_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @Secure(roles="ROLE_SALESORDER_READ")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		try {
			$entity = $model->loadEntity($id);
			$model->currentUserCanView($entity);
			($entity->getStatus() == SalesOrder::STATUS_DRAFT) ? $model->currentUserCanViewDrafts($entity) : true;
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl("salesorder"));
		}

		$revision = $model->getRevision($id);

		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}

	/**
	 * @Route("/revision/{parentId}/{id}/log", requirements={"id"="\d+"}, name="salesorder_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @Secure(roles="ROLE_SALESORDER_READ")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();

		try {
			$entity = $model->loadEntity($parentId);
			$model->currentUserCanView($entity);
			($entity->getStatus() == SalesOrder::STATUS_DRAFT) ? $model->currentUserCanViewDrafts($entity) : true;
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl("salesorder"));
		}

		$log = $model->getLog();

		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}

	/**
	 * @Route("/status/{id}/{status}/change", name="salesorder_change_status")
	 * @Secure(roles="ROLE_SALESORDER_VOID, ROLE_SALESORDER_APPROVE, ROLE_SALESORDER_CLOSE")
	 */
	public function changeStatusAction($id, $status)
	{
		$model = $this->getModel();

		try {
			$entity = $model->loadEntity($id);
			$model->currentUserCanView($entity);
			($entity->getStatus() == SalesOrder::STATUS_DRAFT) ? $model->currentUserCanViewDrafts($entity) : true;
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl("salesorder"));
		}

		try {
			$status = $model->changeStatus($id, $status);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_STATUS_CHANGE) . $status . '.');
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}

		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $id)));
	}

	/**
	 * @Route("/export/{id}", name="salesorder_export")
	 * @Secure(roles="ROLE_SALESORDER_READ")
	 */
	public function exportAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		try {
			$model->currentUserCanView($entity);
			($entity->getStatus() == SalesOrder::STATUS_DRAFT) ? $model->currentUserCanViewDrafts($entity) : true;
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl("salesorder"));
		}

		$rg = $this->get('ci.report.generator');

		return $rg->exportReport('Sales Order # ' . $entity->getRefCode(), null, $entity, 'CIInventoryBundle:SalesOrder:export.html.twig', null, CustomTCPDF::LANDSCAPE);
	}

	/**
	 * Displays a form to upload files to SalesOrder.
	 *
	 * @Route("/{id}/file-upload", name="salesorder_file")
	 * @Method("GET")
	 * @Template()
	 * @Secure(roles="ROLE_SALESORDER_FILE_UPLOAD")
	 */
	public function fileAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);

		try {
			$model->currentUserCanView($entity);
			($entity->getStatus() == SalesOrder::STATUS_DRAFT) ? $model->currentUserCanViewDrafts($entity) : true;
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl("salesorder"));
		}

		$form = $model->getFileFormType($entity);

		return array(
			'entity' => $entity,
			'form' => $form->createView(),
		);
	}

	/**
	 * Updates the files for an existing SalesOrder entity.
	 *
	 * @Route("/{id}/file-upload", name="salesorder_file_upload")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:SalesOrder:file.html.twig")
	 * @Secure(roles="ROLE_SALESORDER_FILE_UPLOAD")
	 */
	public function fileUploadAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $model->getFileFormType($entity);
		$model->storeOriginalFiles($entity);

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->updateFiles($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));

					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}

		return array(
				'entity' => $entity,
				'form' => $form->createView(),
		);
	}

	/**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="salesorder_file_download")
	 * @Method("GET")
	 * @Secure(roles="ROLE_SALESORDER_FILE_DOWNLOAD")
	 */
	public function downloadAction($id)
	{
		try {
			$model = $this->getModel();
			$entity = $model->loadEntity($id);
			$model->currentUserCanView($entity);
			($entity->getStatus() == SalesOrder::STATUS_DRAFT) ? $model->currentUserCanViewDrafts($entity) : true;
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());

			return $this->redirect($this->generateUrl("salesorder"));
		}

		$this->getModel()->downloadFile($id);

		return;
	}
}
