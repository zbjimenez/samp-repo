<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

use CI\InventoryBundle\Helper\CustomTCPDF;

/**
 * ProductReport controller.
 *
 * @Route("/report/product")
 */
class ProductReportController extends Controller
{
	public function getModel()
	{
		return $this->get('ci.product.report.model');
	}

	/**
	 * @Route("/sales-item-supplier", name="get_sales_item_supplier")
	 * @Method("get")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_SALES_ITEM_SUPPLIER_REPORT')")
	 */
	public function getSalesItemSupplierAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getSalesItemSupplierReport('filter');

		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$data = $model->getSalesItemSupplierReport('index', $params);

				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$data['qb'],
					$this->get('request')->query->get('page', 1),
					$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}

		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'total' => isset($data['total']) ? $data['total'] : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 *
	 * @Route("/sales-item-supplier/export", name="get_sales_item_supplier_export")
	 */
	public function getSalesItemSupplierExportAction(Request $request)
	{
		$model = $this->getModel();
		$form  = $model->getSalesItemSupplierReport('filter');
		$form->handleRequest($request);

		$params = $form->getData();
		$pdf = $model->getSalesItemSupplierReport('pdf', $params);

		$rg = $this->get('ci.report.generator');
		
		return $rg->exportCellReport($pdf['title'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], null, CustomTCPDF::REPORT, $pdf['misc']);
	}
}