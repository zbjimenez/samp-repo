<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Model\OrderPickingReportModel as Model;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

/**
 * Order Picking Report controller.
 *
 * @Route("/report/order-picking")
 */
class OrderPickingReportController extends Controller
{
	public function getModel()
	{
		return $this->get('ci.order.picking.report.model');
	}
	
	/**
	 * @Route("/", name="get_order_picking_list")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_ORDER_PICKING_REPORT')")
	 */
	public function getOrderPickingListAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getOrderPickingReport(Model::FILTER);
		 
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getOrderPickingReport(Model::INDEX, $params);

				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$qb,
					$this->get('request')->query->get('page', 1),
					$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}

		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}

	/**
	 *
	 * @Route("/export", name="get_order_picking_list_export")
	 */
	public function getOrderPickingListExportAction(Request $request)
	{
		$model = $this->getModel();
		$form  = $model->getOrderPickingReport(Model::FILTER);
		$form->handleRequest($request);

		$params = $form->getData();	
		$pdf = $model->getOrderPickingReport(Model::PDF, $params);
		$rg = $this->get('ci.report.generator');
		
		return $rg->exportMulticellReport($pdf['title'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], null, CustomTCPDF::LANDSCAPE);
	}

}