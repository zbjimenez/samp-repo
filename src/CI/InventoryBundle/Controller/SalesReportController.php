<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Entity\SalesForecast;

/**
 * Sales Report controller.
 *
 * @Route("/report/sales")
 */
class SalesReportController extends Controller
{
	public function getModel()
	{
		return $this->get('ci.salesreport.model');
	}
	
	/**
	 * @Route("/delivery-schedule", name="salesreport_deliveryschedule")
	 * @Method("GET")
	 * @PreAuthorize("hasAnyRole('ROLE_DELIVERY_REPORT')")
	 */
	public function deliveryScheduleReportAction(Request $request)
	{
		$model = $this->getModel()->deliveryScheduleReport();
		$form = $model->getFilter();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getData($params);
				
				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$qb,
					$this->get('request')->query->get('page', 1),
					$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}
		
		return $this->render('CIInventoryBundle:SalesReport:deliveryScheduleReport.html.twig', array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		));
	}
	
	/**
	 * @Route("/delivery-schedule/export", name="salesreport_deliveryschedule_export")
	 * @Method("GET")
	 */
	public function deliveryScheduleReportExportAction(Request $request)
	{
		$model = $this->getModel()->deliveryScheduleReport();
		$form = $model->getFilter();
		$form->handleRequest($request);
		 
		$params = $form->getData();
		$pdf = $model->getPdf($params);
	
		$rg = $this->get('ci.report.generator');
		
		return $rg->exportMultiCellReport($pdf['name'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], '', CustomTCPDF::LANDSCAPE, $pdf['misc']);
	}
	
	/**
	 * @Route("/sales-forecast", name="salesreport_salesforecast")
	 * @Method("GET")
	 * @PreAuthorize("hasAnyRole('ROLE_SALES_FORECAST_REPORT')")
	 */
	public function salesForecastReportAction(Request $request)
	{
		$model = $this->getModel()->salesForecastReport();
		$form = $model->getFilter();
	
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$data = $model->getData($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}
	
		return $this->render('CIInventoryBundle:SalesReport:salesForecastReport.html.twig', array(
			'params' => isset($params) ? $params : null,
			'data' => isset($data) ? $data : null,
			'search_form' => $form->createView(),
			'months' => SalesForecast::getMonths(),
		));
	}
	
	/**
	 * @Route("/sales-forecast/export", name="salesreport_salesforecast_export")
	 * @Method("GET")
	 */
	public function salesForecastReportExportAction(Request $request)
	{
		$model = $this->getModel()->salesForecastReport();
		$form = $model->getFilter();
		$form->handleRequest($request);
			
		$params = $form->getData();
		$pdf = $model->getPdf($params);
	
		$rg = $this->get('ci.report.generator');
		
		return $rg->exportMultiCellReport($pdf['name'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], '', CustomTCPDF::LANDSCAPE, $pdf['misc']);
	}
}