<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Model\InventoryReportModel as Model;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

/**
 * ReceiveOrderItemReport controller.
 *
 * @Route("/report/receive-order-item")
 */
class ReceiveOrderItemReportController extends Controller
{  
	public function getModel()
	{
		return $this->get('ci.receive.order.item.report.model');
	}
	
	/**
	 * @Route("/receiving", name="get_receive_order_item_receiving_list")
	 * @Method("get")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_RECEIVING_REPORT')")
	 */
	public function getReceivingAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getReceivingReport(Model::FILTER);
		 
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getReceivingReport(Model::INDEX, $params);
				
				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$qb,
					$this->get('request')->query->get('page', 1),
					$this->container->getParameter('pagination_limit_per_page')
				);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}

		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 *
	 * @Route("/receiving/export", name="get_receive_order_item_receiving_list_export")
	 */
	public function getReceivingExportAction(Request $request)
	{
		$model = $this->getModel();
		$form  = $model->getReceivingReport(Model::FILTER);
		$form->handleRequest($request);
	
		$params = $form->getData();
		$pdf = $model->getReceivingReport(Model::PDF, $params);

		$rg = $this->get('ci.report.generator');
		
		return $rg->exportMultiCellReport($pdf['title'], $pdf['parsedParams'], $pdf['rows'], $pdf['tableFormat'], null, CustomTCPDF::LANDSCAPE, $pdf['misc']);
	}
}