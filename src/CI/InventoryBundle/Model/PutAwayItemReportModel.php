<?php

namespace CI\InventoryBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\InventoryBundle\Form\Type\InventoryReportFilterType;
use CI\InventoryBundle\Form\Type\PutAwayItemPutAwayReportFilterType;

class PutAwayItemReportModel
{
	private $container;
	private $formFactory;
	private $em;
	
	const FILTER = 'filter';
	const INDEX = 'index';
	const PDF = 'pdf';
	const XLS = 'xls';
	
	public function __construct(ContainerInterface $container, FormFactoryInterface $formFactory)
	{
		$this->container = $container;
		$this->formFactory = $formFactory;
		$this->em = $container->get('doctrine')->getManager();
	}
	
	public function getPutAwayReport($type, $params = null)
	{
		switch ($type) {
			case self::FILTER:
				return $this->formFactory->create(new PutAwayItemPutAwayReportFilterType(), null, array('method' => 'GET'));
			case self::INDEX:
				$params['status'] = true;
				return $this->em->getRepository('CIInventoryBundle:PutAwayItem')->findAll($params);
			case self::PDF:
				$parsedParams = array();
				$params['status'] = true;

				if (!empty($params['pwNo'])) {
					$parsedParams['PW #'] = $params['pwNo'];
				}
				
				if (!empty($params['roNo'])) {
					$parsedParams['RO #'] = $params['roNo'];
				}

				if (!empty($params['lotNumber'])) {
					$parsedParams['Lot #'] = $params['lotNumber'];
				}

				if (!empty($params['poId'])) {
					$parsedParams['PO #'] = $params['poId'];
				}

				if (!empty($params['product'])) {
					$parsedParams['Product'] = $params['product']->getSku() . ' ' . $params['product']->getName();
				}

				if (!empty($params['dateFrom'])) {
					$parsedParams['Date From'] = date_format($params['dateFrom'],"F d, Y");
				}

				if (!empty($params['dateTo'])) {
					$parsedParams['Date To'] = date_format($params['dateTo'],"F d, Y");
				}

				$qb = $this->getPutAwayReport(self::INDEX, $params);
				$items = $qb->getResult();
				$arr = array();
				foreach ($items as $item) {
					$receiveOrder = $item->getReceiveOrderItem()->getReceiveOrder();
					$arr[] = array(
						'pwNo' => $item->getPutAway()->getId(),
						'roNo' => $receiveOrder->getId(),
						'dateReceived' => date_format($receiveOrder->getDateReceived(),"M d, Y"),
						'barcode' => $item->getBarcode(),
						'product' => $item->getProduct()->getSku() . ' ' . $item->getProduct()->getName(),
						'lotNumber' => $item->getLotNumber(),
						'productionDate' => date_format($item->getProductionDate(),"M d, Y"),
						'quantity' => $item->getQuantity(),
						'palletId' => $item->getPalletId(),
						'storageLocation' => $item->getStorageLocation()->getFullLocation(),
					);
				}

				return array(
					'rows' => $arr,
					'parsedParams' => $parsedParams,
					'title' => 'Put Away Report',
					'misc' => array(
						'excludeColumns' => array(),
					),
					'tableFormat' => array(
						array('header' => "Put\nAway #", 'width' => 0.06, 'align' => 'C', 'number' => null),
						array('header' => "RO #\n ", 'width' => 0.06, 'align' => 'C', 'number' => null),
						array('header' => "RO DATE\n ", 'width' => 0.08, 'align' => 'L', 'number' => null),
						array('header' => "BARCODE\n ", 'width' => 0.10, 'align' => 'R', 'number' => null),
						array('header' => "ITEM DESCRIPTION\n ", 'width' => 0.3, 'align' => 'L', 'number' => null),
						array('header' => "LOT #\n ", 'width' => 0.07, 'align' => 'C', 'number' => null),
						array('header' => "PRODUCTION DATE", 'width' => 0.08, 'align' => 'L', 'number' => null),
						array('header' => "QUANTITY\n ", 'width' => 0.10, 'align' => 'R', 'number' => 2),
						array('header' => "PALLET ID\n ", 'width' => 0.07, 'align' => 'L', 'number' => null),
						array('header' => "STORAGE LOCATION\n ", 'width' => 0.08, 'align' => 'L', 'number' => null),
					)
				);
		}
	}
}