<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Entity\StorageLocation;
use CI\InventoryBundle\Form\Type\StorageLocationType;
use CI\InventoryBundle\Form\Type\SearchFilterType;
use Symfony\Component\HttpFoundation\Request;

class StorageLocationModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	
	public function getNewEntity()
	{
		return new StorageLocation();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new StorageLocationType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new SearchFilterType('Full Location', 'Search'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE:
				return 'New Storage Location has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Storage Location has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Storage Location has been deleted.';
			default:
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'storagelocation_delete',
			'return_path' => 'storagelocation_show',
			'name' => '[StorageLocation] ' . $entity->getFullLocation() . ' (ID #' . $entity->getId() . ')'
		);
	}
	
	public function isDeletable(StorageLocation $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This Storage Location ' . $entity->getFullLocation() . ' has already been added to a transaction and therefore can no longer be deleted.');
		}
	}

	/*
	* This simply finds the first Storage Location ever created,
	* presumably that's the default one.
	*/
	public function getDefaultStoragelocation()
	{
		return $this->getRepository()->getDefaultStoragelocation();
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'StorageLocation'
		);
	
		$options = array(
			'route' => 'storagelocation',
			'name' => 'Storage Location',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'storagelocation',
			'name' => 'Storage Location',
			'classes' => array(
				'CI\InventoryBundle\Entity\StorageLocation'
			)
		);
	}

	public function search(Request $request, $excludedId = null)
	{
		$data = json_decode($request->getContent(), true);
		$params = array('searchString' => $data['searchString']);

		if ($warehouseId = $data['warehouseId']) {
			$params['warehouseId'] = $warehouseId;
		}

		if ($excludedId) {
			$params['excludedId'] = $excludedId;
		}
	
		$results = $this->getRepository()->search($params);
	
		$storageLocations = array();
		if (count($results) > 0) {
			foreach ($results as $result) {
				$storageLocations[] = array(
					'id' => $result['id'],
					'fullLocation' => $result['fullLocation'],
					'label' => $result['fullLocation']
				);
			}
		}
	
		return array('storageLocations' => $storageLocations);
	}

	public function searchFromSelect(Request $request)
	{
		$params = array(
			'searchString' => $request->query->get('searchString'),
			'excludedId' => $request->query->get('excludedId')
		);
	
		$results = $this->getRepository()->search($params);
	
		$storageLocations = array();
		if (count($results) > 0) {
			foreach ($results as $result) {
				$storageLocations[] = array(
					'id' => $result['id'],
					'fullLocation' => $result['fullLocation'],
				);
			}
		}
	
		return array('items' => $storageLocations);
	}
	
	public function getJson($id)
	{
		$entity = $this->findExistingEntity($id);
	
		return array(
			'warehouseNumber' => $entity->getWarehouseNumber(),
			'lane' => $entity->getLane(),
			'row' => $entity->getRow(),
			'highLevelNumber' => $entity->getHighLevelNumber()
		);
	}
}