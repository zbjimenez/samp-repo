<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\InventoryBundle\Form\Type\SalesOrderReportFilterType;
use CI\InventoryBundle\Form\Type\SalesByCustomerAndIndustryFilterType;
use CI\InventoryBundle\Form\Type\SalesByCustomerAndSalesAgentFilterType;

class SalesOrderReportModel
{
	private $container;
	private $formFactory;
	private $em;

	const FILTER = 'filter';
	const INDEX = 'index';
	const PDF = 'pdf';
	const XLS = 'xls';

	public function __construct(ContainerInterface $container, FormFactoryInterface $formFactory)
	{
		$this->container = $container;
		$this->formFactory = $formFactory;
		$this->em = $container->get('doctrine')->getManager();
	}

	public function getSalesOrderReport($type, $params = null)
	{
		switch ($type) {
			case self::FILTER:
				return $this->formFactory->create(new SalesOrderReportFilterType(), null, array('method' => 'GET'));
			case self::INDEX:
				return $this->em->getRepository('CIInventoryBundle:SalesOrderItem')->getSalesOrderReports($params);
			case self::PDF:
				$parsedParams = array();

				if (!empty($params['poId'])) {
					$parsedParams = array('PO #' => $params['poId']);
				}

				if (!empty($params['soId'])) {
					$parsedParams['SO #'] = $params['soId'];
				}

				if (!empty($params['dateFrom'])) {
					$parsedParams['Date From'] = $params['dateFrom']->format('M d, Y');
				}

				if (!empty($params['dateTo'])) {
					$parsedParams['Date To'] = $params['dateTo']->format('M d, Y');
				}

				if (!empty($params['customer'])) {
					$parsedParams['Customer'] = $params['customer']->getId();
				}
				
				$rows = $this->getSalesOrderReport(self::INDEX, $params)->getScalarResult();

				for ($i = 0; $i < count($rows); $i++) {
					$rows[$i]['date'] = (new \DateTime($rows[$i]['date']))->format('M d, Y');
					$rows[$i]['deliveryDate'] = (new \DateTime($rows[$i]['deliveryDate']))->format('M d, Y');
				}

				return array(
					'rows' => $rows,
					'parsedParams' => $parsedParams,
					'title' => 'Sales Order Report',
					'misc' => array(
						'excludeColumns' => array('soi_id'),
					),
					'tableFormat' => array(
						array('header' => "SO #\n ", 'width' => 0.06, 'align' => 'C', 'number' => null),
						array('header' => "SO DATE\n ", 'width' => 0.07, 'align' => 'L', 'number' => null),
						array('header' => "CUSTOMER\n ", 'width' => 0.17, 'align' => 'L', 'number' => null),
						array('header' => "PO #\n ", 'width' => 0.06, 'align' => 'C', 'number' => null),
						array('header' => "DELIVERY DATE", 'width' => 0.08, 'align' => 'L', 'number' => null),
						array('header' => "PRODUCT\n ", 'width' => 0.26, 'align' => 'L', 'number' => null),
						array('header' => "BALANCE\n ", 'width' => 0.1, 'align' => 'R', 'number' => 2),
						array('header' => "UNIT PRICE\n ", 'width' => 0.1, 'align' => 'R', 'number' => 2),
						array('header' => "AMOUNT\n ", 'width' => 0.1, 'align' => 'R', 'number' => 2),
					)
				);
		}
	}

	public function getSalesByCustomerAndSalesAgentReport($type, $params = null)
	{
		switch ($type) {
			case self::FILTER:
				return $this->formFactory->create(new SalesByCustomerAndSalesAgentFilterType(), null, array('method' => 'GET'));
			case self::INDEX:
				return array(
					'qb' => $this->em->getRepository('CIInventoryBundle:Customer')->getSalesByCustomerAndSalesAgent($params),
					'totals' => $this->em->getRepository('CIInventoryBundle:Customer')->getSalesByCustomerAndSalesAgentTotal($params)
				);
			case self::PDF:
				$parsedParams = array();

				if (!empty($params['customer'])) {
					$parsedParams = array('Customer' => $params['customer']->getShortName());
				}

				if (!empty($params['agent'])) {
					$parsedParams['Sales Agent'] = $params['agent']->getName();
				}

				return array(
						'rows' => $this->getSalesByCustomerAndSalesAgentReport(self::INDEX, $params)['qb']->getScalarResult(),
						'parsedParams' => $parsedParams,
						'title' => "Sales By Customer \n & Sales Agent Report",
						'misc' => array(
								'excludeColumns' => array('c_id', 'c_shortName', 'agentId'),
						),
						'tableFormat' => array(
								array('header' => 'CUSTOMER', 'width' => 0.33, 'align' => 'L', 'number' => null),
								array('header' => 'SALES AGENT', 'width' => 0.33, 'align' => 'L', 'number' => null),
								array('header' => 'TOTAL SALES', 'width' => 0.33, 'align' => 'R', 'number' => 2, 'total' => 0),
						)
				);
		}
	}

	public function getSalesByCustomerAndIndustryReport($type, $params = null)
	{
		switch ($type) {
			case self::FILTER:
				return $this->formFactory->create(new SalesByCustomerAndIndustryFilterType(), null, array('method' => 'GET'));
			case self::INDEX:
				return array(
					'qb' => $this->em->getRepository('CIInventoryBundle:SalesOrderItem')->getSalesByCustomerAndIndustry($params),
					'totals' => $this->em->getRepository('CIInventoryBundle:SalesOrderItem')->getSalesByCustomerAndIndustryTotal($params)
				);
			case self::PDF:
				$parsedParams = array();

				if (!empty($params['customer'])) {
					$parsedParams = array('Customer' => $params['customer']->getShortName());
				}

				if (!empty($params['industry'])) {
					$parsedParams['Industry'] = $params['industry']->getName();
				}

				return array(
					'rows' => $this->getSalesByCustomerAndIndustryReport(self::INDEX, $params)['qb']->getScalarResult(),
					'parsedParams' => $parsedParams,
					'title' => 'Sales by Customer & by Industry Report',
					'misc' => array(
						'excludeColumns' => array('soi_id'),
					),
					'tableFormat' => array(
						array('header' => 'CUSTOMER', 'width' => 0.4, 'align' => 'L', 'number' => null),
						array('header' => 'INDUSTRY', 'width' => 0.4, 'align' => 'L', 'number' => null),
						array('header' => 'TOTAL SALES', 'width' => 0.2, 'align' => 'R', 'number' => 2, 'total' => 0),
					)
				);
		}
	}
}
