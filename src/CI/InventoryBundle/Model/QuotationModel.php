<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\Form\Form;
use CI\InventoryBundle\Entity\Quotation;
use CI\InventoryBundle\Entity\ReadReceipt;
use CI\InventoryBundle\Form\Type\QuotationType;
use CI\InventoryBundle\Form\Type\QuotationFilterType;

use CI\InventoryBundle\Entity\QuotationFile;
use CI\InventoryBundle\Form\Type\QuotationFileContainerType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

class QuotationModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_STATUS_CHANGE = 'statusChange';
	const ACTION_UPLOAD = 'upload';
	const ACTION_SHOW = 'show';
	const ACTION_VIEW_LOGS = 'viewLogs';

	private $originalFiles;

	public function getNewEntity()
	{
		return new Quotation();
	}

	public function loadEntities($params = null)
	{
		return $this->getRepository()->loadAll($params, $this->getSecurityContext());
	}

	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new QuotationType($this->getSecurityContext()), $entity, !$entity->getId() ? array() : array('method' => 'PUT'));
	}

	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new QuotationFilterType($this->getSecurityContext()), null, array('method' => 'GET'));
	}

	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE:
				return 'New Quotation has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Quotation has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Specified Quotation has been deleted.';
			case self::ACTION_STATUS_CHANGE:
				return 'Quotation status has been succesfully changed to ';
			default:
				throw new \Exception('Invalid action parameter.');
		}
	}

	public function isEditable($entity)
	{
		if (!$entity->isEditable() && !$this->getSecurityContext()->isGranted('ROLE_QUOTATION_MEMO')) {
			throw new \Exception('Only Quotations set to status of Draft can be edited.');
		}
	}

	public function isDeletable($entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('Specified Quotation is not a draft and cannot be deleted.');
		}
	}

	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		$sc = $this->getSecurityContext();
		
		if (null === $entity->getId()) {
			$user = $sc->getToken()->getUser();
			$entity->setSalesAgent($user);
			$em->persist($entity);
			$em->flush();
		}

		$today = new \DateTime();
		$ref = substr($today->format('Y'), -2) . '-' . str_pad($entity->getId(), 5, '0', STR_PAD_LEFT);
		$entity->setReferenceNumber($ref);
		$em->persist($entity);
		$em->flush();
	}

	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'quotation_delete',
			'return_path' => 'quotation_show',
			'name' => '[Quotation]  #' . $entity->getReferenceNumber()
		);
	}

	public function changeStatus($id, $status)
	{
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);

		if (Quotation::STATUS_PENDING == $status) {
			if (Quotation::STATUS_APPROVED == $entity->getStatus() && !$entity->getActiveSalesOrder()) {
				$entity->setStatus(Quotation::STATUS_PENDING);
			} else {
				throw new \Exception('Only Quotations set to status Approved and without active Sales Order can be set back to Pending.');
			}
		} elseif (Quotation::STATUS_APPROVED == $status) {			
			if (Quotation::STATUS_PENDING == $entity->getStatus()) {
				$entity->setStatus(Quotation::STATUS_APPROVED);
			} else {
				throw new \Exception('Only Pending for Approval Quotations can be approved.');
			}
		} elseif (Quotation::STATUS_VOID == $status) {
			if (Quotation::STATUS_APPROVED == $entity->getStatus()) {
				$entity->setStatus(Quotation::STATUS_VOID);
			} else {
				throw new \Exception('Only Quotations set to status Approved can be voided.');
			}
		} else {
			throw new \Exception('Invalid operation.');
		}

		$em->persist($entity);
		$em->flush();

		return $entity->translateStatus();
	}

	public function changeStatusBulk($ids, $status) 
	{
		$results = array();
		foreach ($ids as $id) {
			try {
				$this->changeStatus($id, $status);
				$results[] = array('id' => $id, 'success' => true, 'message' => 'Success');
			} catch (\Exception $e) {
				$results[] = array('id' => $id, 'success' => false, 'message' => $e->getMessage());
			}
		}
		return $results;
	}

	public function getRevision($id)
	{
		$class = array(
			'id' => $id,
			'class' => 'Quotation'
		);

		$options = array(
			'route' => 'quotation',
			'name' => 'Quotation',
			'class' => $class,
		);

		return $options;
	}

	public function getLog()
	{
		return array(
			'route' => 'quotation',
			'name' => 'Quotation',
			'classes' => array(
				'CI\InventoryBundle\Entity\Quotation',
				'CI\InventoryBundle\Entity\QuotationFile'
			)
		);
	}

	//file handling
	public function getFileFormType(Quotation $entity)
	{
		return $this->getFormFactory()->create(new QuotationFileContainerType(), $entity, array('method' => 'PUT'));
	}

	public function storeOriginalFiles(Quotation $entity)
	{
		$this->originalFiles = new ArrayCollection();

		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}

	public function updateFiles(Form $form, Quotation $entity)
	{
		$em = $this->getEM();

		foreach ($this->originalFiles as $file) {
			if (false === $entity->getFiles()->contains($file)) {
				$file->setPrevId($entity->getId());
				$file->setQuotation(null);
				$em->remove($file);
			}
		}

		$entity->setUpdatedAt(new \DateTime());

		$em->persist($entity);
		$em->flush();
	}

	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:QuotationFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();

		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();

		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename='.$file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}

	public function search(Request $request)
	{
		$data = json_decode($request->getContent(), true);
		$params = array('searchString' => $data['searchString']);

		if ($customerId = $data['customerId']) {
			$params['customerId'] = $customerId;
		}

		$params['included'] = $data['included'];

		$results = $this->getRepository()->search($params);

		$quotations = array();
		if (count($results) > 0) {
			foreach ($results as $result) {
				$date = date("M d, Y", strtotime($result["endDate"]));
				$price = $result['isForeign'] ? $result['price'] * $result['exchangeRate'] : $result['price'];

				$quotations[] = array(
					'id' => $result['id'],
					'refNum' => $result['refNum'],
					'endDate' => $date,
					'productName' => $result['sku'] . ' ' . $result['name'],
					'quantity' => $result['quantity'],
					'onHand' => $result['qtyOnHand'],
					'price' => $price,
					'noResults' => false,
					"htmlLabel" => $result["refNum"] . " <small>(" . $date . ")</small>" . "<br><small>Product: <strong>" . $result["sku"] . "</strong> " . $result["name"] . "</small>"
				);
			}
		}

		return array('quotations' => $quotations);
	}

	public function setHasRead($quotation)
	{
		$securityContext = $this->getContainer()->get('security.context');
		$em = $this->getEM();
		$user = $securityContext->getToken()->getUser();

		if (($securityContext->isGranted('ROLE_ADMIN') || $securityContext->isGranted('ROLE_QUOTATION_SET_VIEW_RECEIPT')) && (is_null($quotation->getReadName()) && is_null($quotation->getReadDateTime()))) {
			$quotation->setReadName($user->getName());
			$quotation->setReadDateTime(new \DateTime('now'));

			$em->flush();
		}
	}

	public function checkUserAccessEntity($quotation)
	{
		$securityContext = $this->getContainer()->get('security.context');
		if (!$securityContext->isGranted('ROLE_QUOTATION_VIEW_ALL') && $securityContext->getToken()->getUser() != $quotation->getSalesAgent()) {
			throw new \Exception('Access denied. You do not have enough permission to proceed.');
		}

		return true;
	}
}
