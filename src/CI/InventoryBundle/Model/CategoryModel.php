<?php

namespace CI\InventoryBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

use CI\InventoryBundle\Entity\Category as EntityType;
use CI\InventoryBundle\Form\Type\CategoryType as EntityFormType;
use CI\InventoryBundle\Form\Type\SearchFilterType as FilterFormType;

class CategoryModel extends BaseEntityModel
{
	public function getNewEntity()
	{
		return new EntityType();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new EntityFormType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new FilterFormType('category name', 'category'), null, array("method" => "GET"));
	}
	
	public function getList($keyword)
	{
		return $this->getRepository()->getList($keyword)->getResult();
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case 'create': return 'New Category has been created successfully.';
			case 'update': return 'Category has been updated successfully.';
			case 'statusChange': return 'Category status has been succesfully changed to ';
			default: throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function restrictCategoryHeirarchy(EntityType $entity)
	{
		if (count($entity->getChildren()) > 0 && $entity->getParent()) {
			$this->getSession()->getFlashBag()->add('danger', 'This category has already been set as a Parent Category.');
			
			return false;
		}
		
		return true;
	}
	
	public function getDeleteParams($entity)
	{
		if ($entity instanceof EntityType) {
			return array(
				'path' => 'category_delete',
				'return_path' => 'category_show',
				'name' => '[Category] ' . $entity->getName() . ' (ID# ' . $entity->getId() . ')'
			);
		}
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Category'
		);
	
		$options = array(
			"route" => "category",
			"type" => "Category",
			"class" => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			"route" => "category",
			"type" => "Category",
			"classes" => array(
				'CI\InventoryBundle\Entity\Category',
			)
		);
	}
}