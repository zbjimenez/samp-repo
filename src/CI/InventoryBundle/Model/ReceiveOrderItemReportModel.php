<?php

namespace CI\InventoryBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\InventoryBundle\Form\Type\InventoryReportFilterType;
use CI\InventoryBundle\Form\Type\ReceiveOrderItemReceivingReportFilterType;
use CI\InventoryBundle\Form\Type\StagingInventoryReportFilterType;

class ReceiveOrderItemReportModel
{
	private $container;
	private $formFactory;
	private $em;
	
	const FILTER = 'filter';
	const INDEX = 'index';
	const PDF = 'pdf';
	const XLS = 'xls';
	
	public function __construct(ContainerInterface $container, FormFactoryInterface $formFactory)
	{
		$this->container = $container;
		$this->formFactory = $formFactory;
		$this->em = $container->get('doctrine')->getManager();
	}
	
	public function getReceivingReport($type, $params = null)
	{
		switch ($type) {
			case self::FILTER:
				return $this->formFactory->create(new ReceiveOrderItemReceivingReportFilterType(), null, array('method' => 'GET'));
			case self::INDEX:
				$params['status'] = true;
				return $this->em->getRepository('CIInventoryBundle:ReceiveOrderItem')->findAll($params);
			case self::PDF:
				$parsedParams = array();
				
				if (!empty($params['roNo'])) {
					$parsedParams['RO #'] = $params['roNo'];
				}

				if (!empty($params['lotNumber'])) {
					$parsedParams['Lot #'] = $params['lotNumber'];
				}

				if (!empty($params['poId'])) {
					$parsedParams['PO #'] = $params['poId'];
				}

				if (!empty($params['product'])) {
					$parsedParams['Product'] = $params['product']->getSku() . ' ' . $params['product']->getName();
				}

				if (!empty($params['dateFrom'])) {
					$parsedParams['Date From'] = date_format($params['dateFrom'],"F d, Y");
				}

				if (!empty($params['dateTo'])) {
					$parsedParams['Date To'] = date_format($params['dateTo'],"F d, Y");
				}

				$qb = $this->getReceivingReport(self::INDEX, $params);
				$items = $qb->getResult();
				$arr = array();
				foreach ($items as $item) {
					$arr[] = array(
						'id' => $item->getId(),
						'roNo' => $item->getReceiveOrder()->getId(),
						'dateReceived' => date_format($item->getReceiveOrder()->getDateReceived(),"M d, Y"),
						'supplierName' => $item->getReceiveOrder()->getSupplierName(),
						'poId' => $item->getPurchaseOrderItem()->getPurchaseOrder()->getPoId(),
						'barcode' => $item->getBarcode(),
						'product' => $item->getProduct()->getSku() . ' ' . $item->getProduct()->getName(),
						'lotNumber' => $item->getLotNumber(),
						'productionDate' => date_format($item->getProductionDate(),"M d, Y"),
						'quantity' => $item->getQuantity(),
					);
				}

				return array(
					'rows' => $arr,
					'parsedParams' => $parsedParams,
					'title' => 'Receiving Report',
					'misc' => array(
						'excludeColumns' => array('id'),
					),
					'tableFormat' => array(
						array('header' => "RO #.\n ", 'width' => 0.05, 'align' => 'C', 'number' => null),
						array('header' => "RO DATE\n ", 'width' => 0.08, 'align' => 'L', 'number' => null),
						array('header' => "SUPPLIER\n ", 'width' => 0.10, 'align' => 'L', 'number' => null),
						array('header' => "PO #\n ", 'width' => 0.05, 'align' => 'C', 'number' => null),
						array('header' => "BARCODE\n ", 'width' => 0.10, 'align' => 'L', 'number' => null),
						array('header' => "ITEM DESCRIPTION\n ", 'width' => 0.34, 'align' => 'L', 'number' => null),
						array('header' => "LOT #\n ", 'width' => 0.10, 'align' => 'C', 'number' => null),
						array('header' => "PRODUCTION DATE", 'width' => 0.08, 'align' => 'L', 'number' => null),
						array('header' => "QUANTITY\n ", 'width' => 0.10, 'align' => 'R', 'number' => 2)
					)
				);
		}
	}

	public function getStagingInventoryReport($type, $params = null)
	{
		switch ($type) {
			case self::FILTER:
				return $this->formFactory->create(new StagingInventoryReportFilterType(), null, array('method' => 'GET'));
			case self::INDEX:
				return $this->em->getRepository('CIInventoryBundle:ReceiveOrderItem')->getStagingInventoryReports($params);
			case self::PDF:
				$parsedParams = array();

				if (!empty($params['poId'])) {
					$parsedParams = array('PO #' => $params['poId']);
				}

				if (!empty($params['roId'])) {
					$parsedParams['SO #'] = $params['roId'];
				}

				if (!empty($params['dateFrom'])) {
					$parsedParams['Date From'] = $params['dateFrom']->format('M d, Y');
				}

				if (!empty($params['dateTo'])) {
					$parsedParams['Date To'] = $params['dateTo']->format('M d, Y');
				}

				$rows = $this->getStagingInventoryReport(self::INDEX, $params)->getScalarResult();

				for ($i = 0; $i < count($rows); $i++) {
					$rows[$i]['productionDate'] = (new \DateTime($rows[$i]['productionDate']))->format('M d, Y');
				}

				return array(
					'rows' => $rows,
					'parsedParams' => $parsedParams,
					'title' => 'Staging Inventory Report',
					'misc' => array(
						'excludeColumns' => array('id', 'roi_id'),
					),
					'tableFormat' => array(
						array('header' => "PO #\n ", 'width' => 0.06, 'align' => 'C', 'number' => null),
						array('header' => "RO #\n ", 'width' => 0.06, 'align' => 'C', 'number' => null),
						array('header' => "Quantity\n ", 'width' => 0.1, 'align' => 'R', 'number' => 2),
						array('header' => "Product\n ", 'width' => 0.41, 'align' => 'L', 'number' => null),
						array('header' => "Lot #\n ", 'width' => 0.19, 'align' => 'C', 'number' => null),
						array('header' => "Production Date", 'width' => 0.08, 'align' => 'L', 'number' => null),
						array('header' => "Pallet ID\n ", 'width' => 0.1, 'align' => 'L', 'number' => null),
					)
				);
		}
	}
}