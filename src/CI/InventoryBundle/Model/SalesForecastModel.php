<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

use CI\InventoryBundle\Entity\SalesForecast as EntityType;
use CI\InventoryBundle\Form\Type\SalesForecastType as EntityFormType;
use CI\InventoryBundle\Form\Type\SalesForecastFileContainerType as FileFormType;
use CI\InventoryBundle\Form\Type\SalesForecastFilterType as FilterFormType;

class SalesForecastModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	
	public function getNewEntity($customerId = null, $productId = null)
	{
		$entity = new EntityType();
		if (!empty($customerId) && !empty($productId)) {
			$customer = $this->getRepository("CIInventoryBundle:Customer")->find($customerId);
			$product = $this->getRepository("CIInventoryBundle:Product")->find($productId);
			$entity->setCustomer($customer);
			$entity->setProduct($product);
		}
		
		return $entity;
	}
	
	public function getSelectType()
	{
		return $this->createFormBuilder()
		->add('customer', 'entity', array(
			'class' => 'CIInventoryBundle:Customer',
			'property' => 'shortName',
			'empty_value' => 'Choose a customer',
			'attr' => array('select2' => true),
			'constraints' => array(new NotBlank(array('message' => 'Please choose a customer.'))),
			'query_builder' => function($repository) {
				return $repository->findAllQb();
			}
		))
		->add('product', 'entity', array(
			'class' => 'CIInventoryBundle:Product',
			'property' => 'displayName',
			'empty_value' => 'Choose a product',
			'attr' => array('select2' => true),
			'constraints' => array(new NotBlank(array('message' => 'Please choose a product.'))),
			'query_builder' => function($repository) {
				return $repository->findAllQb();
			}
		))
		->add('next', 'submit', array(
			'label' => 'Next',
			'attr' => array(
				'class' => 'btn btn-info submit-button',
				'data-loading-text' => "Loading..."
			)
		))
		->getForm();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new EntityFormType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new FilterFormType());
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE: return 'Sales forecast created successfully.';
			case self::ACTION_UPDATE: return 'Sales forecast updated successfully.';
			case self::ACTION_DELETE: return 'Sales forecast has been deleted.';
			default: throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		
		if ($form->get('save')->isClicked()) {
			
		} else {
			throw new \Exception("Invalid operation. Please try again.");
		}
		
		$em->persist($entity);
		$em->flush();
	}
	
	public function getDeleteParams($entity)
	{
		return array(
    		'path' => 'salesforecast_delete',
    		'return_path' => 'salesforecast_show',
    		'name' => '[Sales Forecast] # ' . $entity->getId()
    	);
	}
	
	public function deleteEntity($id)
	{
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);
		
		$em->remove($entity);
		$em->flush();
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'SalesForecast'
		);
	
		$options = array(
			'route' => 'salesforecast',
			'name' => 'Sales Forecast',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'salesforecast',
			'name' => 'Sales Forecast',
			'classes' => array(
				'CI\InventoryBundle\Entity\SalesForecast',
				'CI\InventoryBundle\Entity\SalesForecastFile',
			)
		);
	}

	//file handling
	public function getFileFormType(EntityType $entity)
	{
		return $this->getFormFactory()->create(new FileFormType(), $entity, array('method' => 'PUT'));
	}
	
	public function storeOriginalFiles(EntityType $entity)
	{
		$this->originalFiles = new ArrayCollection();
	
		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}
	
	public function updateFiles(Form $form, EntityType $entity)
	{
		$em = $this->getEM();
	
		foreach ($this->originalFiles as $file) {
			if (false === $entity->getFiles()->contains($file)) {
				$file->setPrevId($entity->getId());
				$file->setSalesForecast(null);
				$em->remove($file);
			}
		}

		$em->persist($entity);
		$em->flush();
	}
	
	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:SalesForecastFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();
	
		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();
	
		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename='.$file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}
}