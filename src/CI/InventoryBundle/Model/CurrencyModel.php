<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Entity\Currency;
use CI\InventoryBundle\Form\Type\CurrencyType;
use CI\InventoryBundle\Form\Type\SearchFilterType;

class CurrencyModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	
	public function getNewEntity()
	{
		return new Currency();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new CurrencyType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new SearchFilterType('currency name', 'currency name'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE:
				return 'New Currency has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Currency has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Currency has been deleted.';
			default:
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'currency_delete',
			'return_path' => 'currency_show',
			'name' => '[Currency] ' . $entity->getCurrencyName() . ' (ID #' . $entity->getId() . ')'
		);
	}
	
	public function isDeletable(Currency $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This currency has already been added to a transaction and therefore can no longer be deleted.');
		}
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Currency'
		);
	
		$options = array(
			'route' => 'currency',
			'name' => 'Currency',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'currency',
			'name' => 'Currency',
			'classes' => array(
				'CI\InventoryBundle\Entity\Currency'
			)
		);
	}
	
	public function getJson($id)
	{
		$entity = $this->findExistingEntity($id);
	
		return array(
			'currencyCode' => $entity->getCurrencyCode()
		);
	}

	/*
	* This simply finds the first currency ever created,
	* presumably that's the default one.
	*/
	public function getDefaultCurrency()
	{
		return $this->getRepository()->getDefaultCurrency();
	}
}