<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;

use CI\InventoryBundle\Entity\ReceiveOrder;
use CI\InventoryBundle\Entity\PurchaseOrder;
use CI\InventoryBundle\Entity\ReceiveOrderItem;
use CI\InventoryBundle\Form\Type\ReceiveOrderType;
use CI\InventoryBundle\Form\Type\ReceiveOrderFileContainerType;
use CI\InventoryBundle\Form\Type\ReceiveOrderFilterType;
use CI\InventoryBundle\Helper\InventoryService;
use CI\InventoryBundle\Entity\InventoryLog;
use CI\InventoryBundle\Entity\Product;

class ReceiveOrderModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_STATUS_CHANGE = 'statusChange';
	
	private $originalItems;
	
	public function getNewEntity()
	{
		return new ReceiveOrder();
	}
	
	public function storeOriginalitems($entity)
	{
		$this->originalItems = array();
	
		foreach ($entity->getItems() as $item) {
			$this->originalItems[] = $item->getPurchaseOrderItem()->getId();
		}
	}
	
	public function storePreviousQuantities($entity)
	{
		foreach ($entity->getItems() as $item) {
			$item->setPreviousQuantity($item->getISQuantity());
		}
	}
	
	public function getOriginalItems()
	{
		return $this->originalItems;
	}
	
	public function getInventoryService()
	{
		return $this->get('ci.inventory.service');
	}
	
	public function setEntityData($entity, $poId = null, $originalItems = null) 
	{
		if (!is_null($poId)) {
			$purchaseOrder = $this->getEM()->getRepository('CIInventoryBundle:PurchaseOrder')->find($poId);
			$entity->setPurchaseOrder($purchaseOrder);
			$entity->setSupplierName($purchaseOrder->getSupplier()->getName());
			
			foreach ($entity->getPurchaseOrder()->getItems() as $item) {
				$roItem = new ReceiveOrderItem();
				$roItem->setQuantity($item->getMaxQtyAllowedToReceive());
				$roItem->setPurchaseOrderItem($item);
				$roItem->setBarcode($item->getProduct()->getBarcode());
				$entity->getItems()->add($roItem);
			}
		} else {
			foreach ($entity->getPurchaseOrder()->getItems() as $item) {
				if (!in_array($item->getId(), $originalItems)) {
					$roItem = new ReceiveOrderItem();
					$roItem->setIsIncluded(false);
					$roItem->setQuantity($item->getMaxQtyAllowedToReceive());
					$roItem->setPurchaseOrderItem($item);
					$entity->getItems()->add($roItem);
				}
			}
		}
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new ReceiveOrderType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new ReceiveOrderFilterType(), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch ($action) {
			case self::ACTION_CREATE:
				return 'New Receive Order has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Receive Order has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Receive Order has been deleted successfully.';
			case self::ACTION_STATUS_CHANGE:
				return 'Receive Order status has been succesfully changed to ';
		}
	}
	
	public function getRevision($id)
	{
		$class = array(
			'id' => $id,
			'class' => 'ReceiveOrder'
		);
		
		$options = array(
			'route' => 'receiveorder',
			'name' => 'Receive Order',
			'class' => $class,
		);
		
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'receiveorder',
			'name' => 'Receive Order',
			'classes' => array(
				'CI\InventoryBundle\Entity\ReceiveOrder',
				'CI\InventoryBundle\Entity\ReceiveOrderItem',
				'CI\InventoryBundle\Entity\ReceiveOrderFile'
			)
		);
	}

	//file handling
	public function getFileFormType(ReceiveOrder $entity)
	{
		return $this->getFormFactory()->create(new ReceiveOrderFileContainerType(), $entity, array('method' => 'PUT'));
	}
	
	public function storeOriginalFiles(ReceiveOrder $entity)
	{
		$this->originalFiles = new ArrayCollection();
	
		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}
	
	public function updateFiles(Form $form, ReceiveOrder $entity)
	{
		$em = $this->getEM();
	
		foreach ($this->originalFiles as $file) {
			if (false === $entity->getFiles()->contains($file)) {
				$file->setPrevId($entity->getId());
				$file->setReceiveOrder(null);
				$em->remove($file);
			}
		}
		$entity->setUpdatedAt(new \DateTime());

		$em->persist($entity);
		$em->flush();
	}
	
	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:ReceiveOrderFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();
	
		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();
	
		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename='.$file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}
	
	public function getDeleteParams($entity)
	{
		return;
	}
	
	public function saveEntity(Form $form, $entity = null)
	{
		$em = $this->getEM();
		$memo = 'Supplier: '.$entity->getPurchaseOrder()->getSupplier()->getName();
		$productRepository = $this->getEM()->getRepository('CIInventoryBundle:Product');
		$invSvc = $this->get('ci.inventory.service');
		$receivedPalletIds = $this->getReceivedPalletIds($entity);
		$palletIds = array();

		foreach ($entity->getItems() as $item) {
			if ($item->getIsIncluded() === true) {
				if (in_array($item->getPalletId(), $palletIds)) {
					throw new \Exception('Pallet ID\'s must be unique within ' . $entity->getWarehouse()->getName() . ', you have multiple items with the same Pallet ID of ' . $item->getPalletId() . '.');
				} else {
					array_push($palletIds, $item->getPalletId());	
				}

				$product = $productRepository->findOneByBarcode($item->getBarcode());
				if (!$product) {
					throw new \Exception('We don\'t have any products with this barcode.');
				}

				if (!$invSvc->isNewPalletIdValid($entity->getWarehouse(), $item->getPalletId(), null)) {
					throw new \Exception('Cannot stage a product with Pallet ID ' . $item->getPalletId() . ' because it already used within ' . $entity->getWarehouse()->getName() . '. Pallets must be unique within this warehouse compound.');
				}
				$palletCounter = 0;

				if (in_array($item->getPalletId(), $receivedPalletIds)) {
					throw new \Exception("Pallet ID " . $item->getPalletId() . " is currently used in another active Receive Order. To use this Pallet ID, please exclude this item or void that Receive Order.");
				}

				$invalidDateMessage = 'Production date \'' . $item->getProductionDateFromScan() . '\' is not valid, must be MM/DD/YYYY with leading zeroes and the year must be four digits.';
				if ($item->getProductionDateFromScan()) {
					try {
						if (\DateTime::createFromFormat('m/d/Y', $item->getProductionDateFromScan())) {
							$item->setProductionDate(\DateTime::createFromFormat('m/d/Y', $item->getProductionDateFromScan()));
						} else {
							throw new \Exception($invalidDateMessage);
						}
					} catch (\Exception $e) {
						throw new \Exception($invalidDateMessage);
					}
				}
			}
		}

		if (!$entity->getId()) {
			foreach ($entity->getItems() as $item) {
				if ($item->getIsIncluded() === false) {
					$entity->removeItem($item);
					$em->remove($item);
				}
			}
		} else {	
			foreach ($entity->getItems() as $item) {
				if ($item->getIsIncluded() === false) {
					if ($item->getId()) {
						$quantityChange = $item->getPreviousQuantity();
					}
					$entity->removeItem($item);
					$em->remove($item);

					$invSvc->deductItemStaging(InventoryLog::TRANS_RECEIVE, $item->getISProduct(), $item->getWarehouse(), $item->getQuantity());
				}
			}
		}

		$invSvc->addStaging($entity);
		$entity->setUpdatedAt(new \DateTime());

		$em->persist($entity);
		$em->flush();
	}
	
	public function isEditable($entity)
	{
		if (!$entity->isEditable()) {
			throw new \Exception('Only Receive Orders with status set to Received and have no active Put Aways can be edited.');
		}
	}
	
	public function changeStatus($id, $status)
	{
		$receiveOrder = $this->findExistingEntity($id);
		$invSvc = $this->getInventoryService();
		
		if (ReceiveOrder::STATUS_INACTIVE === $status) {
			if ($this->getSecurityContext()->isGranted("ROLE_RECEIVEDORDER_CHANGE_STATUS")) {
				if (PurchaseOrder::STATUS_APPROVED === $receiveOrder->getPurchaseOrder()->getStatus()) {
					if ($receiveOrder->getActivePurchaseReturns()->count() == 0 && $receiveOrder->getActivePutAways()->count() == 0) {
						if (ReceiveOrder::STATUS_ACTIVE == $receiveOrder->getActive()) {
							$receiveOrder->setActive(ReceiveOrder::STATUS_INACTIVE);
							$memo = 'Supplier: ' . $receiveOrder->getPurchaseOrder()->getSupplier()->getName();
							$invSvc->deductStaging($receiveOrder);
						} else {
							throw new \Exception('This Receive Order has already been voided.');
						}
					} else if ($receiveOrder->getActivePurchaseReturns()->count() > 0) {
						throw new \Exception('Only Receive Orders without Draft/Returned Shipment Discrepancy Notices are allowed to be voided.');
					} else if ($receiveOrder->getActivePutAways()->count() > 0) {
						throw new \Exception('Only Receive Orders without Active Put Aways are allowed to be voided.');
					}
				} else {
					throw new \Exception('The Purchase Order of this Receive Order has already been set to ' . $receiveOrder->getPurchaseOrder()->translateStatus() . '.');
				}
			} else {
				throw new \Exception('You are not allowed to change the status of this item.');
			}
		} else {
			throw new \Exception('Invalid operation.');
		}
		
		$em = $this->getEM();
		$em->persist($receiveOrder);
		$em->flush();
		
		return $receiveOrder->translateStatus();
	}

	public function getReceivedPalletIds($excludedReceiveOrder = null) {
		return $this->getEM()->getRepository('CIInventoryBundle:ReceiveOrderItem')->getReceivedPalletIds($excludedReceiveOrder);
	}
}