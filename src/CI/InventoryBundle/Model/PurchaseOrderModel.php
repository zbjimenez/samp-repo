<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use CI\InventoryBundle\Entity\PurchaseOrder;
use CI\InventoryBundle\Entity\PurchaseOrderItem;
use CI\InventoryBundle\Form\Type\PurchaseOrderType;
use CI\InventoryBundle\Form\Type\PurchaseOrderFileContainerType;
use CI\InventoryBundle\Form\Type\PurchaseOrderFilterType;
use CI\InventoryBundle\Form\Type\PurchaseOrderShippingType;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;


class PurchaseOrderModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_STATUS_CHANGE = 'statusChange';
	const ACTION_SHIPPING_STATUS_CHANGE = 'shippingStatusChange';

	private $originalCollections;

	public function generateNumber()
	{
		$max = $this->getRepository()->getLatest();
		if (!is_null($max)) {
			$matches = array();

			//group series of letters, numbers and hyphen.
			preg_match_all("/[-]|[0-9]+|[[:upper:][:lower:]]+/", $max, $matches);

			//get last element of the array.
			$last = end($matches[0]);

			//if element has zero padding, add zeroes after increment.
			$last = is_numeric($last) ? str_pad(++$last, strlen(end($matches[0])), '0', STR_PAD_LEFT) : ++$last;

			//join array elements + modified last element.
			return implode('', array_slice($matches[0], 0, -1)) . $last;
		}

		return 1;
	}

	public function getNewEntity()
	{
		$entity = new PurchaseOrder();

		return $entity;
	}

	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new PurchaseOrderType(), $entity, !$entity->getId() ? array() : array('method' => 'PUT'));
	}

	public function getShippingFormType($entity)
	{
		return $this->getFormFactory()->create(new PurchaseOrderShippingType(), $entity, array('method' => 'PUT'));
	}

	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new PurchaseOrderFilterType(), null, array('method' => 'GET'));
	}

	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE:
				return 'New Purchase Order has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Purchase Order has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Specified Purchase Order has been deleted.';
			case self::ACTION_STATUS_CHANGE:
				return 'Purchase Order status has been succesfully changed to ';
			case self::ACTION_SHIPPING_STATUS_CHANGE:
				return 'Purchase Order shipping status has been succesfully changed to ';
			default:
				throw new \Exception('Invalid action parameter.');
		}
	}

	public function isEditable($entity)
	{
		if (!$entity->isEditable()) {
			throw new \Exception('Only Purchase Orders set to status of Draft or Approved and have no active Receive Orders can be edited.');
		}
	}

	public function isDeletable($entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('Specified Purchase Order is not a draft and cannot be deleted.');
		}
	}

	public function storeOriginalItems($entity)
	{
		$this->originalCollections = array(
			'items' => new ArrayCollection(),
			'adjustments' => new ArrayCollection()
		);

		foreach ($entity->getItems() as $item) {
			$this->originalCollections['items']->add($item);
		}

		foreach ($entity->getAdjustments() as $item) {
			$this->originalCollections['adjustments']->add($item);
		}
	}

	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		$sc = $this->getSecurityContext();

		if ($entity->getId()) {
			foreach ($this->originalCollections['items'] as $item) {
				if (false === $entity->getItems()->contains($item)) {
					$em->remove($item);
				}
			}

			foreach ($this->originalCollections['adjustments'] as $item) {
				if (false === $entity->getAdjustments()->contains($item)) {
					$em->remove($item);
				}
			}

			$entity->setUpdatedAt(new \DateTime());
		}

		if ($form->get('saveAsDraft')->isClicked()) {
			$entity->setStatus(PurchaseOrder::STATUS_DRAFT);
		} elseif ($form->get('saveAsApproved')->isClicked()) {
			if ($this->getSecurityContext()->isGranted("ROLE_PURCHASEORDER_APPROVE")) {
				$entity->setStatus(PurchaseOrder::STATUS_APPROVED);
			} else {
				throw new \Exception('You cannot approve Purchase Order.');
			}
		} else {
			throw new \Exception('Invalid operation. Please try again.');
		}

		$em->persist($entity);
		$em->flush();
	}

	public function saveShippingInformation(Form $form, PurchaseOrder $entity)
	{
		$em = $this->getEM();
		$entity->setUpdatedAt(new \DateTime());
		$em->persist($entity);
		$em->flush();

		return array(
			'status' => 'success',
			'message' => 'Shipping information has been updated.',
		);
	}

	public function getErrorResponseData(Form $form, PurchaseOrder $entity)
	{
		$errors = array();

		foreach ($form as $fieldName => $formField) {
			foreach ($formField->getErrors(true) as $error) {
				$errors[$fieldName] = $error->getMessage();
			}
		}

		return array(
				'status' => 'error',
				'message' => 'Something went wrong. Please try again.',
				'errors' => $errors
		);
	}

	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'purchaseorder_delete',
			'return_path' => 'purchaseorder_show',
			'name' => '[Purchase Order] PO #' . $entity->getPoId()
		);
	}

	public function changeStatus($id, $status)
	{
		$em = $this->getEM();
		$sc = $this->getSecurityContext();
		$entity = $this->findExistingEntity($id);
		
	
		if (PurchaseOrder::STATUS_DRAFT == $status) {
			if ($this->getSecurityContext()->isGranted("ROLE_PURCHASEORDER_APPROVE")) {
				if (PurchaseOrder::STATUS_APPROVED == $entity->getStatus() && count($entity->getActiveReceiveOrders()) == 0) {
					$entity->setStatus(PurchaseOrder::STATUS_DRAFT);
					$entity->setShippingStatus(PurchaseOrder::SHIPPING_STATUS_PENDING);
				} else {
					throw new \Exception('Only Purchase Orders set to status Approved and have no active Receive Orders can be set back to Draft.');
				}
			} else {
				throw new \Exception('You are not allowed to disapprove this item.');
			}
		} elseif (PurchaseOrder::STATUS_APPROVED == $status) {
			if ($this->getSecurityContext()->isGranted("ROLE_PURCHASEORDER_APPROVE")) {
				if (PurchaseOrder::STATUS_DRAFT == $entity->getStatus()) {
					$entity->setStatus(PurchaseOrder::STATUS_APPROVED);
				} else {
					throw new \Exception('Only Draft Purchase Orders can be approved.');
				}
			} else {
				throw new \Exception('You are not allowed to approve this item.');
			}
		} elseif (PurchaseOrder::STATUS_VOID == $status) {
			if ($this->getSecurityContext()->isGranted("ROLE_PURCHASEORDER_VOID")) {
				if (PurchaseOrder::STATUS_APPROVED == $entity->getStatus() && count($entity->getActiveReceiveOrders()) == 0) {
					$entity->setStatus(PurchaseOrder::STATUS_VOID);
				} else {
					throw new \Exception('Only Purchase Orders set to status Approved and have no active Receive Orders can be voided.');
				}
			} else {
				throw new \Exception('You are not allowed to void this item.');
			}
		} else if (PurchaseOrder::STATUS_CLOSED == $status) {
			if ($this->getSecurityContext()->isGranted("ROLE_PURCHASEORDER_CLOSE")) {
				if (PurchaseOrder::STATUS_APPROVED == $entity->getStatus()) {
					$entity->setStatus(PurchaseOrder::STATUS_CLOSED);
				} else {
					throw new \Exception('Only Purchase Orders set to status Approved can be closed.');
				}
			}
		} else {
			throw new \Exception('Invalid operation.');
		}

		$em->persist($entity);
		$em->flush();

		return $entity->translateStatus();
	}

	public function changeStatusBulk($ids, $status) 
	{
		$results = array();
		foreach ($ids as $id) {
			try {
				$this->changeStatus($id, $status);
				$results[] = array('id' => $id, 'success' => true, 'message' => 'Success');
			} catch (\Exception $e) {
				$results[] = array('id' => $id, 'success' => false, 'message' => $e->getMessage());
			}
		}
		return $results;
	}

	public function changeShippingStatus($id, $status)
	{
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);

		if ($entity->getStatus() == PurchaseOrder::STATUS_APPROVED) {
			if (PurchaseOrder::SHIPPING_STATUS_PENDING == $status) {
				if (PurchaseOrder::SHIPPING_STATUS_PENDING != $entity->getShippingStatus()) {
					$entity->setShippingStatus(PurchaseOrder::SHIPPING_STATUS_PENDING);
				} else {
					throw new \Exception('This Purchase Order shipping status has already been set to Pending.');
				}
			} elseif (PurchaseOrder::SHIPPING_STATUS_ON_PROCESS == $status) {
				if (PurchaseOrder::SHIPPING_STATUS_ON_PROCESS != $entity->getShippingStatus()) {
					$entity->setShippingStatus(PurchaseOrder::SHIPPING_STATUS_ON_PROCESS);
				} else {
					throw new \Exception('This Purchase Order shipping status has already been set to On Progress.');
				}
			} elseif (PurchaseOrder::SHIPPING_STATUS_RECEIVING == $status) {
				if (PurchaseOrder::SHIPPING_STATUS_RECEIVING != $entity->getShippingStatus()) {
					$entity->setShippingStatus(PurchaseOrder::SHIPPING_STATUS_RECEIVING);
				} else {
					throw new \Exception('This Purchase Order shipping status has already been set to Receiving.');
				}
			} else {
				throw new \Exception('Invalid operation.');
			}
		} else {
			throw new \Exception('You cannot update the shipping status of Purchase Orders that are not set as Approved.');
		}

		$em->persist($entity);
		$em->flush();

		return $entity->translateShippingStatus();
	}

	public function getRevision($id)
	{
		$class = array(
			'id' => $id,
			'class' => 'PurchaseOrder'
		);

		$options = array(
			'route' => 'purchaseorder',
			'name' => 'Purchase Order',
			'class' => $class,
		);

		return $options;
	}

	public function getLog()
	{
		return array(
			'route' => 'purchaseorder',
			'name' => 'Purchase Order',
			'classes' => array(
				'CI\InventoryBundle\Entity\PurchaseOrder',
				'CI\InventoryBundle\Entity\PurchaseOrderItem',
				'CI\InventoryBundle\Entity\PurchaseOrderFile',
				'CI\InventoryBundle\Entity\Adjustment'
			)
		);
	}

	//file handling
	public function getFileFormType(PurchaseOrder $entity)
	{
		return $this->getFormFactory()->create(new PurchaseOrderFileContainerType(), $entity, array('method' => 'PUT'));
	}
	
	public function storeOriginalFiles(PurchaseOrder $entity)
	{
		$this->originalFiles = new ArrayCollection();
	
		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}
	
	public function updateFiles(Form $form, PurchaseOrder $entity)
	{
		$em = $this->getEM();
	
		foreach ($this->originalFiles as $file) {
			if (false === $entity->getFiles()->contains($file)) {
				$file->setPrevId($entity->getId());
				$file->setPurchaseOrder(null);
				$em->remove($file);
			}
		}

		$em->persist($entity);
		$em->flush();
	}
	
	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:PurchaseOrderFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();
	
		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();
	
		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename='.$file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}
}
