<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;

use CI\InventoryBundle\Entity\Backload;
use CI\InventoryBundle\Entity\BackloadItem;
use CI\InventoryBundle\Form\Type\BackloadFilterType;
use CI\InventoryBundle\Form\Type\BackloadType;

class BackloadModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_FOR_PUT_AWAY = 'putAway';
	const ACTION_CHANGE_STATUS = 'changeStatus';
	
	private $originalItems;
	
	public function getNewEntity()
	{
		return new Backload();
	}

	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new BackloadType($this->getEM()), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}

	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new BackloadFilterType($this->getSecurityContext()), null, array('method' => 'GET'));
	}

	public function getMessages($action)
	{
		switch ($action) {
			case self::ACTION_CREATE:
				return 'New Backload has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Backload has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Backload has been deleted successfully.';
			case self::ACTION_FOR_PUT_AWAY:
				return 'Backload has successfully been set for put away.';
			case self::ACTION_CHANGE_STATUS:
				return 'Backload has been set to void.';
		}
	}

	public function isEditable($entity)
	{
		if (!$entity->isEditable()) {
			throw new \Exception("Only backloads that are not set to void and not for put away can be edited.");
		}
	}

	public function isDeletable($entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception("Only backloads that are not for put away can be deleted.");
		}
	}

	public function storeOriginalItems(Backload $entity)
	{
		$this->originalItems = new ArrayCollection();
	
		foreach ($entity->getItems() as $item) {
			$this->originalItems->add($item);
		}
	}

	public function setEntityData(Backload $entity, $shippingId = null) 
	{
		$originalItems = array();

		if (!is_null($shippingId)) {
			$shipping = $this->getEM()->getRepository('CIInventoryBundle:Shipping')->find($shippingId);
			$entity->setShipping($shipping);

			foreach ($entity->getShipping()->getItems() as $item) {
				$backloadItem = new BackloadItem();
				$backloadItem->setQuantity($item->getQuantity());
				$backloadItem->setShippingItem($item);
				$entity->getItems()->add($backloadItem);
			}
		} else {
			$shipping = $this->getEM()->getRepository('CIInventoryBundle:Shipping')->find($entity->getShipping()->getId());

			foreach ($entity->getItems() as $item) {
				$originalItems[] = $item->getShippingItem()->getId();
			}

			foreach ($shipping->getItems() as $item) {
				if (!in_array($item->getId(), $originalItems)) {
					$backloadItem = new BackloadItem();
					$backloadItem->setIsIncluded(false);
					$backloadItem->setQuantity($item->getQuantity());
					$backloadItem->setShippingItem($item);
					$entity->getItems()->add($backloadItem);
				}
			}
		}
	}

	public function processItems(Backload $entity)
	{
		$em = $this->getEM();
		$palletIds = array();
		$invSvc = $this->get('ci.inventory.service');

		foreach ($entity->getItems() as $item) {
			if ($item->getIsIncluded() === true) {
				if (!$invSvc->isNewPalletIdValid($entity->getShipping()->getSalesOrder()->getWarehouse(), $item->getPalletId(), null)) {
					throw new \Exception('Cannot save the backload because the Pallet ID ' . $item->getPalletId() . ' is already used within ' . $entity->getShipping()->getSalesOrder()->getWarehouse()->getName() . '.');
				}	
				if (!is_null($item->getPalletId())) {
					if (in_array($item->getPalletId(), $palletIds)) {
						throw new \Exception('Pallet ID\'s must be unique, you have multiple items with the same Pallet ID of ' . $item->getPalletId() . '.');
					} else {
						array_push($palletIds, $item->getPalletId());	
					}
				}
			} else {
				$entity->getItems()->removeElement($item);
				$em->remove($item);
			}
		}
	}

	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();

		$this->processItems($entity);

		$em->persist($entity);
		$em->flush();
	}

	public function forPutAway(Backload $entity)
	{
		if ($entity->getForPutAway()) {
			throw new \Exception("This backload has already been set to be for put away.");
		}

		if (!$entity->checkForPutAway()) {
			throw new \Exception("This backload can't be processed for the put away operation because one or more of its items may not have a Pallet ID. A Pallet ID is required for every item that is to be put away in the inventory.");
		}

		$em = $this->getEM();

		$entity->setForPutAway(true);
		$em->flush();
	}

	public function changeStatus($id, $status)
	{
		$backload = $this->findExistingEntity($id);
		
		if (Backload::STATUS_INACTIVE === $status) {
			if ($this->getSecurityContext()->isGranted("ROLE_BACKLOAD_CHANGE_STATUS")) {
				if (Backload::STATUS_ACTIVE == $backload->getActive()) {
					$backload->setActive(Backload::STATUS_INACTIVE);
				}
			} else {
				throw new \Exception('You are not allowed to change the status of this backload.');
			}
		} else {
			throw new \Exception('Invalid operation.');
		}
		
		$em = $this->getEM();
		$em->flush();
	}

	public function getRevision($id)
	{
		$class = array(
			'id' => $id,
			'class' => 'Backload'
		);
		
		$options = array(
			'route' => 'backload',
			'name' => 'Backload',
			'class' => $class,
		);
		
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'backload',
			'name' => 'Backload',
			'classes' => array(
				'CI\InventoryBundle\Entity\Backload',
				'CI\InventoryBundle\Entity\BackloadItem'
			)
		);
	}

	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'backload_delete',
			'return_path' => 'backload_show',
			'name' => '[Backload] ID #' . $entity->getId(),
		);
	}
}