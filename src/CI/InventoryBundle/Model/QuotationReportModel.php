<?php

namespace CI\InventoryBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\InventoryBundle\Form\Type\InventoryReportFilterType;
use CI\InventoryBundle\Form\Type\PriceHistoryReportFilterType;

class QuotationReportModel
{
	private $container;
	private $formFactory;
	private $em;
	
	const FILTER = 'filter';
	const INDEX = 'index';
	const PDF = 'pdf';
	const XLS = 'xls';
	
	public function __construct(ContainerInterface $container, FormFactoryInterface $formFactory)
	{
		$this->container = $container;
		$this->formFactory = $formFactory;
		$this->em = $container->get('doctrine')->getManager();
	}
	
	public function getPriceHistoryReport($type, $params = null)
	{
		switch ($type) {
			case self::FILTER:
				return $this->formFactory->create(new PriceHistoryReportFilterType(), null, array('method' => 'GET'));
			case self::INDEX:
				return $this->em->getRepository('CIInventoryBundle:Quotation')->getPriceHistory($params);
			case self::PDF:
				$parsedParams = array();
				
				if (!empty($params['product'])) {
					$parsedParams = array(
						'Product Code' => $params['product']->getSku(),
					);
				}
				return array(
					'rows' => $this->getPriceHistoryReport(self::INDEX, $params)->getScalarResult(),
					'parsedParams' => $parsedParams,
					'title' => 'Price History Report',
					'misc' => array(
						'excludeColumns' => array('q_id'),
					),
					'tableFormat' => array(
						array('header' => 'Product Code', 'width' => 0.50, 'align' => 'L', 'number' => null),
						array('header' => 'Date', 'width' => 0.20, 'align' => 'L', 'number' => null),
						array('header' => 'Price', 'width' => 0.30, 'align' => 'R', 'number' => 2),
					)
				);
		}
	}
}