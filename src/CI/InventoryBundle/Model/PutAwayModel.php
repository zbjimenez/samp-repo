<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;

use CI\InventoryBundle\Entity\PutAway;
use CI\InventoryBundle\Entity\PurchaseOrder;
use CI\InventoryBundle\Entity\PutAwayItem;
use CI\InventoryBundle\Entity\ReceiveOrder;
use CI\InventoryBundle\Entity\ReceiveOrderItem;
use CI\InventoryBundle\Form\Type\PutAwayType;
use CI\InventoryBundle\Form\Type\PutAwayFileContainerType;
use CI\InventoryBundle\Form\Type\PutAwayFilterType;
use CI\InventoryBundle\Helper\InventoryService;
use CI\InventoryBundle\Entity\InventoryLog;
use CI\InventoryBundle\Entity\Product;

class PutAwayModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_STATUS_CHANGE = 'statusChange';

	protected $type;
	
	public function getNewEntity()
	{
		return new PutAway();
	}

	public function setType($type)
	{
		$this->type = $type;
	}

	public function storePreviousStorageLocations($entity)
	{
		foreach ($entity->getItems() as $item) {
			$item->setPreviousStorageLocation($item->getStorageLocation());
		}
	}
	
	public function storePreviousPalletIds($entity)
	{
		foreach ($entity->getItems() as $item) {
			$item->setPreviousPalletId($item->getPalletId());
		}
	}
	
	public function getOriginalItems()
	{
		return $this->originalItems;
	}
	
	public function getInventoryService()
	{
		return $this->get('ci.inventory.service');
	}
	
	public function setEntityData($entity, $transactionId = null, $type = null) 
	{
		if (!is_null($transactionId) && !is_null($type)) {
			$entity->setPutAwayType($type);

			if ($type == PutAway::TYPE_RECEIVE_ORDER) {
				$receiveOrder = $this->getEM()->getRepository('CIInventoryBundle:ReceiveOrder')->find($transactionId);
				$entity->setReceiveOrder($receiveOrder);
			} else {
				$backload = $this->getEM()->getRepository('CIInventoryBundle:Backload')->find($transactionId);
				$entity->setBackload($backload);
			}
		}
	}
	
	public function getFormType($entity = null)
	{
		$type = $this->type;
		
		return $this->getFormFactory()->create(new PutAwayType($type), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new PutAwayFilterType(), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch ($action) {
			case self::ACTION_CREATE:
				return 'New Put Away has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Put Away has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Put Away has been deleted successfully.';
			case self::ACTION_STATUS_CHANGE:
				return 'Put Away status has been succesfully changed to ';
		}
	}
	
	public function getRevision($id)
	{
		$class = array(
			'id' => $id,
			'class' => 'PutAway'
		);
		
		$options = array(
			'route' => 'putaway',
			'name' => 'Put Away',
			'class' => $class,
		);
		
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'putaway',
			'name' => 'Put Away',
			'classes' => array(
				'CI\InventoryBundle\Entity\PutAway',
				'CI\InventoryBundle\Entity\PutAwayItem'
			)
		);
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'putaway_delete',
			'return_path' => 'putaway_show',
			'name' => '[Put Away] ID #' . $entity->getId(),
		);
	}
	
	public function saveEntity(Form $form, $entity = null)
	{
		$em = $this->getEM();
		$invSvc = $this->getInventoryService();

		if ($entity->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
			$memo =  'From RO# ' . $entity->getReceiveOrder()->getId();
		} else {
			$memo =  'From Backload # ' . $entity->getBackload()->getId();
		}

		$productRepository = $this->getEM()->getRepository('CIInventoryBundle:Product');
		$storageLocationRepository = $this->getEM()->getRepository('CIInventoryBundle:StorageLocation');
		$warehouseModel = $this->get('ci.warehouse.model');
		$palletIds = array();

		foreach ($entity->getItems() as $item) {
			if ($item->getIsIncluded() == true) {
				try {
					$storageLocation = $storageLocationRepository->findOneByFullLocation($item->getStorageLocationFromScan());
					if (is_null($storageLocation)) {
						throw new \Exception('Storage Location '. $item->getStorageLocationFromScan() .' does not exist.');
					}
					if ($storageLocation->getWarehouse()->getId() != $entity->getWarehouse()->getId()) {
						throw new \Exception('Storage Location '. $item->getStorageLocationFromScan() .' is not from ' . $entity->getWarehouse()->getName());
					}
				} catch (\Exception $e) {
					throw new \Exception('Storage Location '. $item->getStorageLocationFromScan() .' does not exist.');
				}
				$item->setStorageLocation($storageLocation);
				$storageLocation->addPutAwayItems($item);
				$em->persist($storageLocation);
			}
		}
		foreach ($entity->getItems() as $item) {
			if ($item->getIsIncluded() == true) {
				if (in_array($item->getPalletId(), $palletIds)) {
					throw new \Exception('Pallet ID\'s must be unique within ' . $entity->getWarehouse()->getName() . ', you have multiple items with the same Pallet ID of ' . $item->getPalletId() . '.');
				} else {
					array_push($palletIds, $item->getPalletId());	
				}

				$putAwayItem = $this->getEM()->getRepository('CIInventoryBundle:PutAwayItem')->findOneByPalletId($item->getPalletId());
				if (!is_null($putAwayItem)) {
					throw new \Exception('Cannot Put Away this Pallet ' . $item->getPalletId() . ' because it has already been put awayed.');
				}

				if ($entity->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
					$receiveOrderItemPalletId = $this->getEM()->getRepository('CIInventoryBundle:ReceiveOrderItem')->getReceiveOrderItem($entity->getReceiveOrder() , $item->getPalletId(), $item->getLotNumber(), $item->getProduct());
					if (empty($receiveOrderItemPalletId)) {
						throw new \Exception('Receive Order Item with Pallet ID ' . $item->getPalletId() . ' and Lot Number ' . $item->getLotNumber() . ' does\'nt exist.');
					}

					if ($item->getReceiveOrderItem()->getTotalReturned() > 0) {
						throw new \Exception('Cannot Put Away this Pallet ' . $item->getPalletId() . ' because it has already been returned.');
					}
				} else if ($entity->getPutAwayType() == PutAway::TYPE_BACKLOAD) {
					$backloadItemPalletId = $this->getEM()->getRepository('CIInventoryBundle:BackloadItem')->getBackloadItem($entity->getBackload() , $item->getPalletId(), $item->getLotNumber(), $item->getProduct());
					if (empty($backloadItemPalletId)) {
						throw new \Exception('Backload Item with Pallet ID ' . $item->getPalletId() . ' and Lot Number ' . $item->getLotNumber() . ' does\'nt exist.');
					}
				}
			}
		}
		
		if (!$entity->getId()) {
			foreach ($entity->getItems() as $item) {
				if ($item->getIsIncluded() === false) {
					$entity->removeItem($item);
					$em->remove($item);
				}
			}
			$invSvc->addMultipleItemStock(InventoryLog::TRANS_PUTAWAY, $entity, $memo);
			if ($entity->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
				$invSvc->deductStaging($entity);
			}
			
		} else {	
			foreach ($entity->getItems() as $item) {
				if ($item->getIsIncluded() === false) {
					if ($item->getId()) {
						$invSvc->deductItemStock(
							InventoryLog::TRANS_PUTAWAY,
							$item->getISProduct(),
							$item->getStorageLocation(),
							$item->getQuantity(),
							null,
							$item->getLotNumber(),
							$item->getPalletId(),
							$item->getProductionDate(),
							true
						);
						if ($entity->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
							$invSvc->addItemStaging(InventoryLog::TRANS_RECEIVE, $item->getReceiveOrderItem()->getISProduct(), $item->getReceiveOrderItem()->getWarehouse(), $item->getQuantity());
						}
					}
					$entity->removeItem($item);
					$em->remove($item);
				} else if ($item->getPreviousStorageLocation()->getId() != $item->getStorageLocation()->getId() || $item->getPreviousPalletId() != $item->getPalletId()) {
					$invSvc->deductItemStock(
						InventoryLog::TRANS_PUTAWAY,
						$item->getISProduct(),
						$item->getPreviousStorageLocation(),
						$item->getQuantity(),
						$memo,
						$item->getLotNumber(),
						$item->getPreviousPalletId(),
						$item->getProductionDate(),
						true
					);
					$invSvc->addItemStock(
						InventoryLog::TRANS_PUTAWAY,
						$item->getISProduct(),
						$item->getStorageLocation(),
						$item->getQuantity(),
						$memo,
						$item->getLotNumber(),
						$item->getPalletId(),
						$item->getProductionDate(),
						true
					);
				}
			}
		}
		$entity->setUpdatedAt(new \DateTime());

		$em->persist($entity);
		$em->flush();
	}
	
	public function changeStatus($id, $status)
	{
		$putAway = $this->findExistingEntity($id);
		$invSvc = $this->getInventoryService();
		
		if (PutAway::STATUS_INACTIVE === $status) {
			if ($this->getSecurityContext()->isGranted("ROLE_PUTAWAY_CHANGE_STATUS")) {
				if ($putAway->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER && $putAway->getReceiveOrder()->getActive() == true) {
					if (PutAway::STATUS_ACTIVE == $putAway->getActive()) {
						$memo =  'From RO# ' . $putAway->getReceiveOrder()->getId();
						$putAway->setActive(PutAway::STATUS_INACTIVE);
						$invSvc->deductMultipleItemStock(InventoryLog::TRANS_PUTAWAY, $putAway, $memo);
						$invSvc->addStaging($putAway);
					} else {
						throw new \Exception('This Put Away has already been voided.');
					}
				} else if ($putAway->getPutAwayType() == PutAway::TYPE_BACKLOAD && $putAway->getBackload()->getActive() == true) {
					$memo =  'From Backload # ' . $putAway->getBackload()->getId();
					$putAway->setActive(PutAway::STATUS_INACTIVE);
					$invSvc->deductMultipleItemStock(InventoryLog::TRANS_PUTAWAY, $putAway, $memo);
				} else {
					throw new \Exception('The Receive Order of this Put Away has already been set to ' . $putAway->getReceiveOrder()->translateStatus() . '.');
				}
			} else {
				throw new \Exception('You are not allowed to change the status of this item.');
			}
		} else {
			throw new \Exception('Invalid operation.');
		}
		
		$em = $this->getEM();
		$em->persist($putAway);
		$em->flush();
		
		return $putAway->translateStatus();
	}

	public function deleteEntity($id)
	{
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);
		$invSvc = $this->getInventoryService();

		if ($entity->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
			$memo =  'From RO# ' . $entity->getReceiveOrder()->getId();
			$invSvc->deductMultipleItemStock(InventoryLog::TRANS_PUTAWAY, $entity, $memo);
			$invSvc->addStaging($entity);
		} else if ($entity->getPutAwayType() == PutAway::TYPE_BACKLOAD){
			$memo =  'From Backload # ' . $entity->getBackload()->getId();
			$invSvc->deductMultipleItemStock(InventoryLog::TRANS_PUTAWAY, $entity, $memo);
		}
		
		$em->remove($entity);
		$em->flush();
	}

	public function isEditable($entity)
	{
		if (!$entity->isEditable()) {
			throw new \Exception('Only Put Aways with status set to Successfully Put Away can be edited.');
		}
	}

	public function isDeletable($entity)
	{
		return true;
	}
}