<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Entity\Product;
use CI\InventoryBundle\Entity\ProductFile;
use CI\InventoryBundle\Form\Type\ProductType;
use CI\InventoryBundle\Form\Type\SearchFilterType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use CI\InventoryBundle\Form\Type\ProductFileContainerType;
use CI\InventoryBundle\Form\Type\InventoryAdjustmentType;
use CI\InventoryBundle\Entity\Inventory;
use CI\InventoryBundle\Entity\InventoryLog;

class ProductModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_UPLOAD = 'upload';
	
	private $originalFiles;
	
	public function getNewEntity()
	{
		return new Product();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new ProductType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getAdjustmentFormType()
	{
		return $this->getFormFactory()->create(new InventoryAdjustmentType());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new SearchFilterType('product sku', 'product'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE: 
				return 'New Product has been created successfully.';
			case self::ACTION_UPDATE: 
				return 'Product has been updated successfully.';
			case self::ACTION_DELETE: 
				return 'Product has been deleted.';
			default: 
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(Product $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This product has already been added to a transaction and therefore can no longer be deleted.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
    		'path' => 'product_delete',
    		'return_path' => 'product_show',
    		'name' => '[Product] ' . $entity->getSku() . ' ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
    	);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Product'
		);
	
		$options = array(
			'route' => 'product',
			'name' => 'Product',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'product',
			'name' => 'Product',
			'classes' => array(
				'CI\InventoryBundle\Entity\Product',
				'CI\InventoryBundle\Entity\ProductCode',
				'CI\InventoryBundle\Entity\ProductFile'
			)
		);
	}
	
	public function search(Request $request)
	{
		$data = json_decode($request->getContent(), true);
		$params = array('searchString' => $data['searchString']);
		
		if ($supplierId = $data['supplierId']) {
			$params['supplierId'] = $supplierId;
		}
	
		$results = $this->getRepository()->search($params);
	
		$products = array();
		if (count($results) > 0) {
			foreach ($results as $result) {
				$products[] = array(
					'id' => $result['id'],
					'sku' => $result['sku'],
					'name' => $result['name'],
					'label' => $result['sku'] . ' ' . $result['name']
				);
			}
		}
	
		return array('products' => $products);
	}
	
	public function customerSearch(Request $request)
	{
		$data = json_decode($request->getContent(), true);
		$params = array('searchString' => $data['searchString']);
	
		if ($customerId = $data['customerId']) {
			$params['customerId'] = $customerId;
		}
	
		$results = $this->getRepository()->customerSearch($params);
	
		$products = array();
		if (count($results) > 0) {
			foreach ($results as $result) {
				$products[] = array(
					'id' => $result['id'],
					'sku' => $result['sku'],
					'name' => $result['name'],
					'label' => $result['sku'] . ' ' . $result['name']
				);
			}
		}
	
		return array('products' => $products);
	}
	
	//file handling
	public function getFileFormType(Product $entity)
	{
		return $this->getFormFactory()->create(new ProductFileContainerType(), $entity, array('method' => 'PUT'));
	}
	
	public function storeOriginalFiles(Product $entity)
	{
		$this->originalFiles = new ArrayCollection();
	
		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}
	
	public function updateFiles(Form $form, Product $entity)
	{
		$em = $this->getEM();
	
		foreach ($this->originalFiles as $file) {
			if (false === $entity->getFiles()->contains($file)) {
				$file->setPrevId($entity->getId());
				$file->setProduct(null);
				$em->remove($file);
			}
		}
		$entity->setUpdatedAt(new \DateTime());

		$em->persist($entity);
		$em->flush();
	}
	
	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:ProductFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();
	
		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();
	
		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename='.$file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}

	public function getInventoryService()
	{
		return $this->get('ci.inventory.service');
	}
	
	public function adjustInventory(Form $form, $entity)
	{
		$em = $this->getEM();
		
		$invSvc = $this->getInventoryService();
		$quantity = $form->get('quantity')->getData();
		$type = $form->get('type')->getData();
		$lotNumber = $form->get('lotNumber')->getData();
		$palletId = $form->get('palletId')->getData();
		$productionDate = $form->get('productionDate')->getData();
		$factoryDate = $form->get('factoryDate')->getData();
		$storageLocation = $form->get('storageLocation')->getData();
		
		if ($quantity != 0) {
			if ($palletId && !$invSvc->isPalletIdValid($entity, $storageLocation, $lotNumber, $palletId)) {
				$inventoryStorageLocation = $em->getRepository('CIInventoryBundle:Inventory')->getInventoryStorageLocationOfPalletId($palletId, $storageLocation->getWarehouse());
				if (count($inventoryStorageLocation) > 0) {
					if ($inventoryStorageLocation[0]['fullLocation'] === $storageLocation->getFullLocation()) {
						return array(
						'status' => 'failure',
						'message' => 'Cannot create new inventory entry with Pallet ID ' .$palletId. ' and Lot Number ' .$lotNumber. ' because the pallet already contains a different Lot Number ' .$inventoryStorageLocation[0]['i_lotNumber']. '.'
						);
					}
					return array(
						'status' => 'failure',
						'message' => 'Cannot create new inventory entry with Pallet ID ' . $palletId . ' because it already exists in Storage Location ' . $inventoryStorageLocation[0]['fullLocation']
						);
				}
			}
			if ($type == Inventory::ADJUSTMENT_IN) {
				$invSvc->addItemStock(
					InventoryLog::TRANS_ADJUST,
					$entity,
					$storageLocation,
					$quantity,
					null,
					$lotNumber,
					$palletId,
					$productionDate,
					true
				);
			} else {
				$invSvc->deductItemStock(
					InventoryLog::TRANS_ADJUST,
					$entity,
					$storageLocation,
					$quantity,
					null,
					$lotNumber,
					$palletId,
					$productionDate,
					true
				);
			}

			$inventory = $invSvc->getInventory($entity, $storageLocation, $lotNumber, $palletId);
			$inventory->setPalletId($palletId);
			$inventory->setLotNumber($lotNumber);
			$inventory->setProductionDate($productionDate);
			$inventory->setFactoryDate($factoryDate);

			$entity->setUpdatedAt(new \DateTime());
			$inventory->setUpdatedAt(new \DateTime());
			$em->persist($inventory);
			$em->persist($entity);
			$em->flush();
		} else {
			return array(
				'status' => 'failure',
				'message' => 'Quantity must not be blank.'
				);
		}

		$serialized = $inventory->serializeToArray();
		$serialized['isMismatched'] = false;
	
		return array(
			'status' => 'success',
			'totalOnHand' => array(
				'kg' => $entity->getTotalOnHand(), 
				'packageCount' => $entity->getQtyOnHandPackage()
			),
			'totalAllocated' => array(
				'kg' => $entity->getTotalAllocated(), 
				'packageCount' => $entity->getQtyAllocatedPackage()
			),
			'changedInventory' => $serialized,
			'hasGenerated' => $invSvc->getHasGenerated(),
			'message' => 'On hand quantity in Storage Location ' . $storageLocation->getFullLocation() . ' has been updated.',
		);
	}
	
	public function getErrorResponseData(Form $form, $entity)
	{
		$errors = array();
	
		foreach ($form as $fieldName => $formField) {
			foreach ($formField->getErrors(true) as $error) {
				$errors[$fieldName] = $error->getMessage();
			}
		}
	
		return array(
			'status' => 'error',
			'message' => 'Something went wrong. Please try again.',
			'errors' => $errors
		);
	}
}