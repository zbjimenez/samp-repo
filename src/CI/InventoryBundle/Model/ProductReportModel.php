<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface;

use CI\InventoryBundle\Form\Type\SalesItemSupplierReportFilterType;

class ProductReportModel
{
	private $container;
	private $formFactory;
	private $em;
	
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
		$this->formFactory = $container->get('form.factory');
		$this->em = $container->get('doctrine')->getManager();
	}
	
	public function getSalesItemSupplierReport($type, $params = null)
	{
		switch ($type) {
			case 'filter':
				return $this->formFactory->create(new SalesItemSupplierReportFilterType(), null, array('method' => 'GET'));
			case 'index':
				return array(
					'qb' => $this->em->getRepository('CIInventoryBundle:Product')->getSalesItemSupplierReport($params),
					'total' => $this->em->getRepository('CIInventoryBundle:Product')->getSalesItemSupplierReportTotal($params)
				);
			case 'pdf':
				$parsedParams = array();
				
				if (!empty($params['product'])) {
					$parsedParams['Product'] = $params['product']->getSku();
				}
				
				if (!empty($params['supplier'])) {
					$parsedParams['Supplier'] = $params['supplier']->getShortName();
				}
				
				return array(
					'rows' => $this->getSalesItemSupplierReport('index', $params)['qb']->getScalarResult(),
					'parsedParams' => $parsedParams,
					'title' => 'Sales by Item & by Supplier Report',
					'misc' => array(
						'excludeColumns' => array('p_id', 'p_sku'),
					),
					'tableFormat' => array(
						array('header' => 'PRODUCT', 'width' => 0.4, 'align' => 'L', 'number' => null),
						array('header' => 'SUPPLIER', 'width' => 0.4, 'align' => 'L', 'number' => null),
						array('header' => 'TOTAL SALES', 'width' => 0.2, 'align' => 'R', 'number' => 2, 'total' => 0),
					)
				);
		}
	}
}