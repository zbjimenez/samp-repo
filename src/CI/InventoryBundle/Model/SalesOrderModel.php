<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use CI\InventoryBundle\Entity\SalesOrder as EntityType;
use CI\InventoryBundle\Form\Type\SalesOrderType as EntityFormType;
use CI\InventoryBundle\Form\Type\SalesOrderFileContainerType as FileFormType;
use CI\InventoryBundle\Form\Type\SalesOrderFilterType as FilterFormType;

class SalesOrderModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_STATUS_CHANGE = 'statusChange';

	private $originalItems;

	public function getNewEntity()
	{
		return new EntityType();
	}

	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new EntityFormType($this->getSecurityContext()), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}

	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new FilterFormType($this->getSecurityContext()), null, array('method' => 'GET'));
	}

	public function getMessages($action)
	{
		switch ($action) {
			case self::ACTION_CREATE: return 'Sales order created successfully.';
			case self::ACTION_UPDATE: return 'Sales order updated successfully.';
			case self::ACTION_DELETE: return 'Sales order has been deleted.';
			case self::ACTION_STATUS_CHANGE: return 'Sales order status has been updated to ';
			default: throw new \Exception('Invalid action parameter.');
		}
	}

	public function isDeletable(EntityType $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This sales order is not a draft.');
		}
	}

	public function isEditable(EntityType $entity)
	{
		if (!$entity->isEditable()) {
			throw new \Exception('This sales order cannot be edited.');
		}
	}

	public function storeOriginalItems(EntityType $entity)
	{
		$this->originalItems = new ArrayCollection();

		foreach ($entity->getItems() as $item) {
			$this->originalItems->add($item);
		}
	}

	public function loadEntity($id)
	{
		return $this->getRepository()->load($id, $this->getSecurityContext());
	}

	public function loadEntities($params = null)
	{
		return $this->getRepository()->loadAll($params, $this->getSecurityContext());
	}

	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();

		if ($entity->getId()) {
			foreach ($this->originalItems as $item) {
				if ($entity->getItems()->contains($item) === false) {
					$item->setSalesOrder(null);
					$em->remove($item);
				}
			}

			$entity->setUpdatedAt(new \DateTime('now'));
		} else {
			$entity->setOwnedBy($this->getSecurityContext()->getToken()->getUser());
		}

		if ($form->get('saveDraft')->isClicked()) {
			$entity->setStatus(EntityType::STATUS_DRAFT);
		} elseif ($form->get('saveApproved')->isClicked()) {
			if ($this->getSecurityContext()->isGranted("ROLE_SALESORDER_APPROVE")) {
				foreach ($entity->getItems() as $item) {
					if ($item->getQuotation()->getActiveSalesOrder() !== null) {
						throw new \Exception('One of the quotations is already added to an approved/closed sales order.');
					}
				}

				$entity->setStatus(EntityType::STATUS_APPROVED);
				$IS = $this->get('ci.inventory.service');
				$IS->addAllocation($entity);
			} else {
				throw new \Exception('Only users with the user role Sales Manager can approve Sales Orders.');
			}
		} else {
			throw new \Exception("Invalid operation. Please try again.");
		}

		$em->persist($entity);
		$em->flush();
	}

	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'salesorder_delete',
			'return_path' => 'salesorder_show',
			'name' => '[Sales Order] SO# ' . $entity->getRefCode(),
		);
	}

	public function changeStatus($id, $status)
	{
		$em = $this->getEM();
		$IS = $this->get('ci.inventory.service');
		$entity = $this->findExistingEntity($id);

		if ($status == EntityType::STATUS_DRAFT) {
			if ($this->getSecurityContext()->isGranted("ROLE_SALESORDER_APPROVE")) {
				if ($entity->getStatus() == EntityType::STATUS_APPROVED && count($entity->getActiveShipping()) === 0) {
					$IS->deductAllocation($entity);
					$entity->setStatus(EntityType::STATUS_DRAFT);
				} else {
					throw new \Exception('Only Sales Orders set to status Approved and does not have any shipping can be set back to Draft.');
				}
			} else {
				throw new \Exception('You are not allowed to draft this item.');
			}
		} elseif ($status == EntityType::STATUS_APPROVED) {
			if ($this->getSecurityContext()->isGranted("ROLE_SALESORDER_APPROVE")) {
				if ($entity->getStatus() == EntityType::STATUS_DRAFT) {
					foreach ($entity->getItems() as $item) {
						if ($item->getQuotation()->getActiveSalesOrder() !== null) {
							throw new \Exception('One of the quotations is already added to an approved/closed sales order.');
						}
					}

					$IS->addAllocation($entity);
					$entity->setStatus(EntityType::STATUS_APPROVED);
				} else {
					throw new \Exception('Only Draft Sales Orders can be approved.');
				}
			} else {
				throw new \Exception('You are not allowed to approve this item.');
			}
		} elseif ($status == EntityType::STATUS_VOID) {
			if ($this->getSecurityContext()->isGranted("ROLE_SALESORDER_VOID")) {
				if ($entity->getStatus() == EntityType::STATUS_APPROVED && count($entity->getActiveShipping()) === 0) {
					$IS->deductAllocation($entity);
					$entity->setStatus(EntityType::STATUS_VOID);
				} else {
					throw new \Exception('Only Sales Orders set to status Approved can be voided.');
				}
			} else {
				throw new \Exception('You are not allowed to void this item.');
			}
		} elseif ($status == EntityType::STATUS_CLOSED && count($entity->getActiveShipping()) > 0) {
			if ($this->getSecurityContext()->isGranted("ROLE_SALESORDER_CLOSE")) {
				if ($entity->getStatus() == EntityType::STATUS_APPROVED) {
					$entity->setStatus(EntityType::STATUS_CLOSED);
				} else {
					throw new \Exception('Only Sales Orders set to status Approved and has been delivered can be closed.');
				}
			} else {
				throw new \Exception('You are not allowed to close this item.');
			}
		} else {
			throw new \Exception('Invalid operation.');
		}

		$em->persist($entity);
		$em->flush();

		return $entity->translateStatus();
	}

	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'SalesOrder',
		);

		$options = array(
			'route' => 'salesorder',
			'name' => 'Sales Order',
			'class' => $classes,
		);

		return $options;
	}

	public function getLog()
	{
		return array(
			'route' => 'salesorder',
			'name' => 'Sales Order',
			'classes' => array(
				'CI\InventoryBundle\Entity\SalesOrder',
				'CI\InventoryBundle\Entity\SalesOrderItem',
				'CI\InventoryBundle\Entity\SalesOrderFile',
			),
		);
	}

	//file handling
	public function getFileFormType(EntityType $entity)
	{
		return $this->getFormFactory()->create(new FileFormType(), $entity, array('method' => 'PUT'));
	}

	public function storeOriginalFiles(EntityType $entity)
	{
		$this->originalFiles = new ArrayCollection();

		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}

	public function updateFiles(Form $form, EntityType $entity)
	{
		$em = $this->getEM();

		foreach ($this->originalFiles as $file) {
			if ($entity->getFiles()->contains($file) === false) {
				$file->setPrevId($entity->getId());
				$file->setSalesOrder(null);
				$em->remove($file);
			}
		}

		$em->persist($entity);
		$em->flush();
	}

	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:SalesOrderFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();

		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();

		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename=' . $file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}

	public function generateSoId()
	{
		$max = $this->getRepository()->getLatest();

		if (!is_null($max)) {
			$matches = array();
			preg_match_all("/[-]|[0-9]+|[[:upper:][:lower:]]+/", $max, $matches);
			$last = end($matches[0]);
			$last = is_numeric($last) ? str_pad(++$last, strlen(end($matches[0])), '0', STR_PAD_LEFT) : ++$last;

			return implode('', array_slice($matches[0], 0, -1)) . $last;
		}

		return 1;
	}

	public function currentUserCanView($salesOrder)
	{
		$securityContext = $this->getContainer()->get('security.context');
		if ($securityContext->isGranted('ROLE_SALESORDER_VIEW_FROM_ALL_USERS') || $securityContext->getToken()->getUser() == $salesOrder->getOwnedBy() || $securityContext->getToken()->getUser() == $salesOrder->getCustomer()->getSalesAgent() || $securityContext->isGranted('ROLE_ADMIN')) {
			return true;
		}

		throw new \Exception('Access denied. You do not have enough permission to proceed.');
	}

	public function currentUserCanViewDrafts($salesOrder)
	{
		$securityContext = $this->getContainer()->get('security.context');
		if ($securityContext->isGranted('ROLE_SALESORDER_VIEW_DRAFTS') || $securityContext->getToken()->getUser() == $salesOrder->getOwnedBy() || $securityContext->getToken()->getUser() == $salesOrder->getCustomer()->getSalesAgent() || $securityContext->isGranted('ROLE_ADMIN')) {
			return true;
		}

		throw new \Exception('Access denied. You do not have enough permission to proceed.');
	}
}
