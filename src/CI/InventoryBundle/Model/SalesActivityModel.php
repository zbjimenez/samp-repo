<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

use CI\InventoryBundle\Entity\SalesActivity as EntityType;
use CI\InventoryBundle\Entity\SalesActivityItem as EntityItemType;
use CI\InventoryBundle\Form\Type\SalesActivityType as EntityFormType;
use CI\InventoryBundle\Form\Type\SalesActivityFilterType as FilterFormType;
use CI\InventoryBundle\Entity\SalesActivityFile;
use CI\InventoryBundle\Form\Type\SalesActivityFileContainerType;
use Symfony\Component\HttpFoundation\File\File;
use CI\InventoryBundle\Form\Type\SalesActivityReportFilterType;
use CI\InventoryBundle\Form\Type\SalesActivityCustomerReportFilterType;
use CI\InventoryBundle\Entity\SalesActivity;
use CI\InventoryBundle\Helper\CustomTCPDF;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;

class SalesActivityModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_UPLOAD = 'upload';
	const ACTION_STATUS_CHANGE = 'statusChange';

	const INDEX = 'index';
	const PDF = 'pdf';

	private $originalItems;
	private $originalFiles;

	public function getNewEntity()
	{
		$entity = new EntityType();

		$entity->addItem(new EntityItemType());

		return $entity;
	}

	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new EntityFormType($this->getSecurityContext(), $entity ? $entity->getAgent() : null), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}

	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new FilterFormType(), null, array('method' => 'GET'));
	}

	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE:
				return 'Sales activity registered successfully.';
			case self::ACTION_UPDATE:
				return 'Sales activity updated successfully.';
			case self::ACTION_DELETE:
				return 'Sales activity has been deleted.';
			case self::ACTION_UPLOAD:
				return 'Sales activity files updated successfully.';
			case self::ACTION_STATUS_CHANGE:
				return 'Sales activity status has been succesfully changed to ';
			default:
				throw new \Exception('Invalid action parameter.');
		}
	}

	public function isEditable($entity)
	{
		if (!$entity->isEditable() && !($this->getSecurityContext()->isGranted(array(new Expression('hasAnyRole("ROLE_SALES_MANAGER")'))))) {
			throw new \Exception('Only Sales Activities set to status of Draft can be edited.');
		}
	}

	public function isDeletable($entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('Only Sales Activities set to status of Draft can be deleted.');
		}
	}

	public function storeOriginalItems(EntityType $entity)
	{
		$this->originalItems = new ArrayCollection();

		foreach ($entity->getItems() as $item) {
			$this->originalItems->add($item);
		}
	}

	public function getIndex($params = null)
	{
		return $this->getRepository()->findAll($params, $this->getSecurityContext()->getToken()->getUser());
	}

	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		$user = $this->getSecurityContext()->getToken()->getUser();

		if ($entity->getId()) {
			foreach ($this->originalItems as $item) {
				if (false === $entity->getItems()->contains($item)) {
					$item->setSalesActivity(null);
					$em->remove($item);
				}
			}

			$entity->setUpdatedAt(new \DateTime('now'));
		} else {
			$entity->setAgent($user);
		}

		if ($form->get('saveAsDraft')->isClicked()) {
			$entity->setStatus(SalesActivity::STATUS_DRAFT);
		} elseif ($form->get('saveAsApproved')->isClicked()) {
			$entity->setStatus(SalesActivity::STATUS_SUBMITTED);
		} else {
			throw new \Exception('Invalid operation. Please try again.');
		}

		$em->persist($entity);
		$em->flush();
	}

	public function getDeleteParams($entity)
	{
		return array(
    		'path' => 'salesactivity_delete',
    		'return_path' => 'salesactivity_show',
    		'name' => '[Sales Activity] for ' . $entity->getDate()->format('M. d, Y') . ' (ID #' . $entity->getId() . ')'
    	);
	}

	public function changeStatus($id, $status)
	{
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);

		if (SalesActivity::STATUS_DRAFT == $status) {
			if (SalesActivity::STATUS_SUBMITTED == $entity->getStatus() && !($entity->hasComments())) {
				$entity->setStatus(SalesActivity::STATUS_DRAFT);
			} else {
				throw new \Exception('Only Sales Activities set to status Submitted without comments can be set back to Draft.');
			}
		} elseif (SalesActivity::STATUS_SUBMITTED == $status) {
			if (SalesActivity::STATUS_DRAFT == $entity->getStatus()) {
				$entity->setStatus(SalesActivity::STATUS_SUBMITTED);
			} else {
				throw new \Exception('Only Draft Sales Activities can be submitted.');
			}
		} else {
			throw new \Exception('Invalid operation.');
		}

		$em->persist($entity);
		$em->flush();

		return $entity->translateStatus();
	}

	public function toggleReadFlag($id)
	{
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);

		if ($entity->getReadFlag()) {
			$entity->setReadFlag(false);
		} else {
			$entity->setReadFlag(true);
		}

		$em->persist($entity);
		$em->flush();

		return $entity->getReadFlag();
	}

	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'SalesActivity'
		);

		$options = array(
			'route' => 'salesactivity',
			'name' => 'Sales Activity',
			'class' => $classes,
		);

		return $options;
	}

	public function getLog()
	{
		return array(
			'route' => 'salesactivity',
			'name' => 'Sales Activity',
			'classes' => array(
				'CI\InventoryBundle\Entity\SalesActivity',
				'CI\InventoryBundle\Entity\SalesActivityItem',
				'CI\InventoryBundle\Entity\SalesActivityFile'
			)
		);
	}

	//report
	public function getReportFilterFormType()
	{
		return $this->getFormFactory()->create(new SalesActivityReportFilterType($this->getSecurityContext()), null, array('method' => 'GET'));
	}

	public function getCustomerReportFilterFormType()
	{
		return $this->getFormFactory()->create(new SalesActivityCustomerReportFilterType(), null, array('method' => 'GET'));
	}

	public function getReportIndex($params = null)
	{	
		if (empty($params['agent'])) {
			$params['agent'] = $this->getSecurityContext()->getToken()->getUser();
		}
		return $this->getRepository()->getReport($params);
	}

	public function getCustomerReportIndex($type, $params = null)
	{
		switch ($type) {
			case self::INDEX:
				$userId = null;
				if (!$this->getSecurityContext()->isGranted('ROLE_SALES_MANAGER')) {
					$userId = $this->getSecurityContext()->getToken()->getUser()->getId();
				}
				
				$query = $this->getEM()->getRepository('CIInventoryBundle:SalesActivityItem')->getCustomerReport($params, $userId);
				$results = $query->getResult();

				$categories = array();
				foreach ($results as $result) {
					$year = $result->getSalesActivity()->getDate()->format('Y');
					$month = $result->getSalesActivity()->getDate()->format('M');

					if (!array_key_exists($year, $categories)) {
						$categories[$year] = array();
					}

					if (!array_key_exists($month, $categories[$year])) {
						$categories[$year][$month] = 0;
					}

					$categories[$year][$month]+= 1;
				}

				return array(
					'query' => $query,
					'count' => $categories
				);
			case self::PDF:
				$pdf = $this->getContainer()->get("white_october.tcpdf")->create();
				$rows = $this->getCustomerReportIndex(self::INDEX, $params)['query']->getResult();
				$count = $this->getCustomerReportIndex(self::INDEX, $params)['count'];
				$pdf->setType('L');
				$pageMargins = $pdf->getMargins();
				$pageWidth = $pdf->getPageWidth() - $pageMargins['left'] - $pageMargins['right'];

				$parsedParams = array();
				if (!is_null($params['customer'])) {
					$parsedParams['Customer'] = $params['customer']->getShortName();
				}

				$parsedParams['Date From'] = $params['dateFrom']->format('M d, Y');
				$parsedParams['Date To'] = $params['dateTo']->format('M d, Y');

				$tableHeaders = array(
					array('width' => 0.07 * $pageWidth, 'header' => 'Date'),
					array('width' => 0.06 * $pageWidth, 'header' => 'Day Type'),
					array('width' => 0.05 * $pageWidth, 'header' => 'Start'),
					array('width' => 0.05 * $pageWidth, 'header' => 'End'),
					array('width' => 0.15 * $pageWidth, 'header' => 'Agent'),
					array('width' => 0.20 * $pageWidth, 'header' => 'Contact'),
					array('width' => 0.21 * $pageWidth, 'header' => 'Description'),
					array('width' => 0.21 * $pageWidth, 'header' => 'Comments'),
				);
				$pdf->setTableHeaders($tableHeaders);
				$pdf->setParams($parsedParams);

				$pdf->setFormName('Sales Activity Customer Report');
				$pdf->SetFont('helvetica', 'B', 8);
				$pdf->AddPage();

				if (count($rows) > 0) {
					foreach ($rows as $row) {

						$cellText = $row->getDescription();
						$colWidth = 0.21 * $pageWidth;
						$padding = 3;
						$numLines = $pdf->getNumLines($cellText, $colWidth);
						$lineHeight = $pdf->GetFontSize() * $pdf->getCellHeightRatio();
						$cellHeight = $numLines * $lineHeight + $padding;

						$height = $pdf->getStringHeight(0.21 * $pageWidth, $row->getDescription(), false, true, '', 0);
						$pdf->Cell(0.07 * $pageWidth, 5, $row->getSalesActivity()->getDate()->format('M d, Y'), 0, 0, 'L');
						$pdf->Cell(0.06 * $pageWidth, 5, $row->getSalesActivity()->getDayType(), 0, 0, 'L');
						$pdf->Cell(0.05 * $pageWidth, 5, $row->getStartTime()->format('g:i A'), 0, 0, 'L');
						$pdf->Cell(0.05 * $pageWidth, 5, $row->getEndTime()->format('g:i A'), 0, 0, 'L');
						$pdf->Cell(0.15 * $pageWidth, 5, $row->getSalesActivity()->getAgent()->getName(), 0, 0, 'L');
						$pdf->Cell(0.20 * $pageWidth, 5, $row->getContact(), 0, 0, 'L');
						$pdf->MultiCell(0.21 * $pageWidth, 3, $row->getDescription(), 0, 'L', false,0);
						$pdf->MultiCell(0.21 * $pageWidth, $cellHeight, $row->getComments(), 0, 'L', false);
					}
				}

				$pdf->Ln();

				foreach ($count as $year => $cnt) {
					foreach ($cnt as $month => $num) {
						$pdf->Cell(0.15 * $pageWidth, 5, $month . ' ' . $year . ' = ' . $num, 0, 0, 'L');
						$pdf->Ln();
					}
				}

				return $pdf;
		}
	}

	public function getSalesActivityReport($params)
	{
		$parsedParams = array(
			'Sales Agent' => $params['agent']->getName(),
			'Date From' => $params['dateFrom']->format('M d, Y'),
			'Date To' => $params['dateTo']->format('M d, Y')
		);
		
		$results = $this->getReportIndex($params)->getResult();
		$rows = array();
		
		foreach ($results as $result) {
			$rows[] = array(
				'date' => $result['date']->format('M d, Y'),
				'status' => $result['status'] == EntityType::STATUS_DRAFT ? 'Draft' : 'Approved',
				'dayType' => $result['dayType']
			);
		}

		return array(
			'rows' => $rows,
			'parsedParams' => $parsedParams,
			'title' => 'Sales Activity Report',
			'misc' => array(
				'excludeColumns' => array('sa_id', 'id'),
			),
			'tableFormat' => array(
				array('header' => 'DATE', 'width' => 0.20, 'align' => 'L', 'number' => null),
				array('header' => 'STATUS', 'width' => 0.10, 'align' => 'L', 'number' => null),
				array('header' => 'DAY TYPE', 'width' => 0.20, 'align' => 'L', 'number' => null),
			)
		);
	}

	//might be temporary, so grouped them together for easier deletion
	public function getFileFormType(EntityType $entity)
	{
		return $this->getFormFactory()->create(new SalesActivityFileContainerType(), $entity, array('method' => 'PUT'));
	}

	public function storeOriginalFiles(EntityType $entity)
	{
		$this->originalFiles = new ArrayCollection();

		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}

	public function updateFiles(Form $form, EntityType $entity)
	{
		$em = $this->getEM();

		foreach ($this->originalFiles as $file) {
			if (false === $entity->getFiles()->contains($file)) {
				$file->setPrevId($entity->getId());
				$file->setSalesActivity(null);
				$em->remove($file);
			}
		}
		$entity->setUpdatedAt(new \DateTime());

		$em->persist($entity);
		$em->flush();
	}

	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:SalesActivityFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();

		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();

		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename='.$file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}

	public function previewImage($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:SalesActivityFile')->find($id);
		$imgExt = array('jpg', 'jpeg', 'png');
		$ext = strtolower($file->getExtension());

		if (in_array($ext, $imgExt)) {
			return $this->getTemplateEngine()->render('CIInventoryBundle:SalesActivity:preview.html.twig', array('file' => $file));
		} else {
			return null;
		}
	}
}
