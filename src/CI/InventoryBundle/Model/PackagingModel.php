<?php

namespace CI\InventoryBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

use CI\InventoryBundle\Entity\Packaging as EntityType;
use CI\InventoryBundle\Form\Type\PackagingType as EntityFormType;
use CI\InventoryBundle\Form\Type\SearchFilterType as FilterFormType;
use CI\InventoryBundle\Entity\Packaging;

class PackagingModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	
	public function getNewEntity()
	{
		return new EntityType();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new EntityFormType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new FilterFormType('packaging name', 'packaging'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE: return 'New Packaging has been created successfully.';
			case self::ACTION_UPDATE: return 'Packaging has been updated successfully.';
			case self::ACTION_DELETE: return 'Packaging has been deleted.';
			default: throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(EntityType $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('Packaging can not be deleted. Set status to inactive instead.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
    		'path' => 'packaging_delete',
    		'return_path' => 'packaging_show',
    		'name' => '[Packaging] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
    	);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Packaging'
		);
	
		$options = array(
			'route' => 'packaging',
			'name' => 'Packaging',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'packaging',
			'name' => 'Packaging',
			'classes' => array(
				'CI\InventoryBundle\Entity\Packaging',
			)
		);
	}
	
	public function getJson($id)
	{
		$entity = $this->findExistingEntity($id);
	
		return array(
				'kgsUnit' => $entity->getKgsUnit(),
				'name' => $entity->getName(),
		);
	}
	
	public function saveModalEntity(Form $form, Packaging $entity)
	{
		$em = $this->getEM();
		$em->persist($entity);
		$em->flush();
	
		return array(
				'entityId' => $entity->getId(),
				'status' => 'success',
				'message' => 'New packaging added.',
				'name' => $entity->getName(),
		);
	}
	
	public function getErrorResponseData(Form $form, Packaging $entity)
	{
		$errors = array();
	
		foreach ($form as $fieldName => $formField) {
			foreach ($formField->getErrors(true) as $error) {
				$errors[$fieldName] = $error->getMessage();
			}
		}
	
		return array(
				'status' => 'error',
				'message' => 'Something went wrong. Please try again.',
				'errors' => $errors
		);
	}
}