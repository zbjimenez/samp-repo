<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\Form\Form;
use CI\InventoryBundle\Entity\ProductCode;
use CI\InventoryBundle\Form\Type\ProductCodeType;

class ProductCodeModel extends BaseEmptyEntityModel
{
	public function getNewEntity()
	{
		return new ProductCode();
	}
	
	public function getNewPreparedEntity($id)
	{
		$em = $this->getEM();
		$entity = new ProductCode();
		$product = $em->getRepository('CIInventoryBundle:Product')->find($id);
		$entity->setProduct($product);
	
		return $entity;
	}
	
	public function findExistingEntity($id)
	{
		return $this->getRepository()->find($id);
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new ProductCodeType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function isDeletable(ProductCode $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This product code is already being used by some records and therefor cannot be deleted.');
		}
	}
	
	public function getErrorResponseData(Form $form, ProductCode $entity, $type = null)
	{
		$errors = array();
		
	    foreach ($form as $fieldName => $formField) {
	        foreach ($formField->getErrors(true) as $error) {
	            $errors[$fieldName] = $error->getMessage();
	        }
	    }
		
		return array(
			'status' => 'error',
			'message' => 'Something went wrong. Please try again.',
			'errors' => $errors
		);
	}
	
	public function saveEntity(Form $form, ProductCode $entity)
	{
		$em = $this->getEM();
		$parentEntity = $entity->getProduct()->setUpdatedAt(new \DateTime());
		$em->persist($entity);
		$isCreate = $entity->getId() ? false : true;
		$em->persist($parentEntity);
		$em->flush();
	
		return array(
			'entityId' => $entity->getId(),
			'customerName' => $entity->getCustomer()->getName(),
			'status' => 'success',
			'message' => !$isCreate ? 'Product code has been updated.' : 'New product code added.',
		);
	}
	
	public function deleteEntity($id)
	{
		$data = array();
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);
		$parentEntity = $entity->getProduct();
		
		try {
			$this->isDeletable($entity);
			$parentEntity->setUpdatedAt(new \DateTime());
			$em->persist($parentEntity);
			$em->remove($entity);
			$em->flush();
			
			$data = array(
				'success' => true,
				'message' => 'Product code has been deleted.'
			);
		} catch (\Exception $e) {
			$data = array(
				'success' => false,
				'message' => $e->getMessage()
			);
		}
		
		return $data;
	}
}