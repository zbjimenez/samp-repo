<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use CI\InventoryBundle\Entity\PurchaseOrder;
use CI\InventoryBundle\Entity\ReceiveOrderItem;
use CI\InventoryBundle\Form\Type\ReceiveOrderType;
use CI\InventoryBundle\Form\Type\ReceiveOrderFilterType;
use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Helper\InventoryService;
use CI\InventoryBundle\Entity\InventoryLog;
use CI\InventoryBundle\Entity\Product;
use CI\InventoryBundle\Entity\Shipping;
use CI\InventoryBundle\Entity\ShippingItem;
use CI\InventoryBundle\Form\Type\ShippingType;
use CI\InventoryBundle\Form\Type\ShippingFileContainerType;
use CI\InventoryBundle\Form\Type\ShippingFilterType;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;

class ShippingModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_STATUS_CHANGE = 'statusChange';
	
	private $originalItems;
	
	public function getNewEntity()
	{
		return new Shipping();
	}
	
	public function storeOriginalitems(Shipping $entity)
	{
		$this->originalItems = array();
	
		foreach ($entity->getItems() as $item) {
			$this->originalItems[] = $item->getSalesOrderItem()->getId();
		}
	}
	
	public function storePreviousQuantities(Shipping $entity)
	{
		foreach ($entity->getItems() as $item) {
			$item->setPreviousQuantity($item->getISQuantity());
		}
	}
	
	public function getOriginalItems()
	{
		return $this->originalItems;
	}
	
	public function getInventoryService()
	{
		return $this->get('ci.inventory.service');
	}
	
	public function setEntityData(Shipping $entity, $soId = null, $originalItems = null) 
	{
		if (!is_null($soId)) {
			$salesOrder = $this->getEM()->getRepository('CIInventoryBundle:SalesOrder')->find($soId);
			$entity->setSalesOrder($salesOrder);
			foreach ($entity->getSalesOrder()->getItems() as $item) {
				$shippingItem = new ShippingItem();
				$shippingItem->setQuantity($item->getMaxQtyAllowedToDeliver());
				$shippingItem->setSalesOrderItem($item);
				$entity->addItem($shippingItem);
			}
		} else {
			foreach ($entity->getSalesOrder()->getItems() as $item) {
				if (!in_array($item->getId(), $originalItems)) {
					$shippingItem = new ShippingItem();
					$shippingItem->setIsIncluded(false);
					$shippingItem->setQuantity($item->getMaxQtyAllowedToDeliver());
					$shippingItem->setSalesOrderItem($item);
					$entity->addItem($shippingItem);
				}
			}
		}
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new ShippingType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new ShippingFilterType(), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch ($action) {
			case self::ACTION_CREATE:
				return 'New Delivery has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Delivery has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Delivery has been deleted successfully.';
			case self::ACTION_STATUS_CHANGE:
				return 'Delivery status has been succesfully changed to ';
		}
	}
	
	public function getRevision($id)
	{
		$class = array(
			'id' => $id,
			'class' => 'Shipping'
		);
		
		$options = array(
			'route' => 'shipping',
			'name' => 'Delivery',
			'class' => $class,
		);
		
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'shipping',
			'name' => 'Delivery',
			'classes' => array(
				'CI\InventoryBundle\Entity\Shipping',
				'CI\InventoryBundle\Entity\ShippingItem'
			)
		);
	}

	//file handling
	public function getFileFormType(Shipping $entity)
	{
		return $this->getFormFactory()->create(new ShippingFileContainerType(), $entity, array('method' => 'PUT'));
	}
	
	public function storeOriginalFiles(Shipping $entity)
	{
		$this->originalFiles = new ArrayCollection();
	
		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}
	
	public function updateFiles(Form $form, Shipping $entity)
	{
		$em = $this->getEM();
	
		foreach ($this->originalFiles as $file) {
			if (false === $entity->getFiles()->contains($file)) {
				$file->setPrevId($entity->getId());
				$file->setShipping(null);
				$em->remove($file);
			}
		}

		$em->persist($entity);
		$em->flush();
	}
	
	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:ShippingFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();
	
		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();
	
		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename='.$file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}
	
	public function saveEntity(Form $form, $entity = null)
	{
		$em = $this->getEM();
		
		if ($form->get('saveAsDraft')->isClicked()) {
			$this->removeItems($entity);
			$entity->setStatus(Shipping::STATUS_DRAFT);
		
		} else if ($form->get('saveAsPending')->isClicked()) {
			$this->removeItems($entity);
			$entity->setStatus(Shipping::STATUS_PENDING);
		
		} else if ($form->get('saveAsDelivered')->isClicked()) {
			
			if ($entity->getAttention()) {
				throw new \Exception('Only deliveries with no attention can be saved as delivered.');
			} else {
				foreach ($entity->getItems() as $item) {
					if ($item->getIsIncluded() === false) {
						$entity->removeItem($item);
						$em->remove($item);
					}
				}

				$entity->setStatus(Shipping::STATUS_DELIVERED);
			}
			
		} else {
			throw new \Exception('Invalid operation.');
		}
		
		$em->persist($entity);
		$em->flush();
	}
	
	public function removeItems($entity) {
		$em = $this->getEM();
		foreach ($entity->getItems() as $item) {
			if ($item->getIsIncluded() === false) {
				$entity->removeItem($item);
				$em->remove($item);
			}
		}
	}
	
	public function isEditable($entity)
	{
		if (!$entity->isEditable()) {
			throw new \Exception('Only deliveries with status set to For Delivery and without an order picking can be edited.');
		}
	}
	
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'shipping_delete',
			'return_path' => 'shipping_show',
			'name' => '[Delivery] DR #' . $entity->getDrId(),
		);
	}

	public function isDeletable($entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('Only deliveries with status set to Draft can be deleted.');
		}
	}
	
	public function changeStatus($id, $status)
	{
		$shipping = $this->findExistingEntity($id);
		$invSvc = $this->getInventoryService();

		if (Shipping::STATUS_PENDING == $status) {
			if ($shipping->getStatus() === Shipping::STATUS_DRAFT && $shipping->getAttention() == false) {
				$shipping->setStatus($status);
			} else {
				throw new \Exception('Only deliveries with the status "Draft" and have no attention can be saved as "For Delivery".');
			}
		} else if (Shipping::STATUS_DELIVERED == $status) {
			if ($shipping->getStatus() === Shipping::STATUS_PENDING && $shipping->isFullyPicked()) {
				$shipping->setStatus($status);
			} else {
				throw new \Exception('Only deliveries with the status "For Delivery" and with its items fully picked can be saved as delivered.');
			}
		} else if (Shipping::STATUS_FULFILLED == $status) {
			if ($shipping->getStatus() === Shipping::STATUS_DELIVERED) {
				$shipping->setStatus($status);
			} else {
				throw new \Exception('Only deliveries that have the status "Delivered" can be set to "Fulfilled".');
			}
		} else {
    		throw new \Exception('Invalid operation.');
		}
    	
		$em = $this->getEM();
		$em->persist($shipping);
		$em->flush();
		
		return $shipping->translateStatus();
	}

	public function changeStatusBulk($ids, $status) 
	{
		$results = array();

		foreach ($ids as $id) {
			try {
				$this->changeStatus($id, $status);
				$results[] = array('id' => $id, 'success' => true, 'message' => 'Success');
			} catch (\Exception $e) {
				$results[] = array('id' => $id, 'success' => false, 'message' => $e->getMessage());
			}
		}

		return $results;
	}
	
	/**
	 * 
	 * @param integer $id
	 * @param integer $type: 1 = DR, 2 = SI
	 */
	public function export($id, $type)
	{
		$entity = $this->findExistingEntity($id);
		$salesOrder = $entity->getSalesOrder();
		$customer = $salesOrder->getCustomer();
		$branch = $entity->getBranch();
		
		$pdf = $this->get('white_october.tcpdf')->create();
		$pdf->setType(CustomTCPDF::BLANK);
		
		if ($type == Shipping::TYPE_DR) {
			$filename = 'Delivery_Receipt_' . $entity->getId() . '_' . date('M-d-Y');
			
			$pdf->setPageFormat(array(240, 280));
			$pdf->addPage();
			$pdf->SetFont('helvetica', '', 10);
				
			$pdf->setXY(42, 51, true);
			$pdf->MultiCell(108, 5, $customer->getName(), 0 ,'L');
			$pdf->setXY(170, 51, true);
			$pdf->Cell(30, 5, $entity->getDeliveryDate()->format('M d, Y'));
			$pdf->setXY(170, 57, true);
			$pdf->Cell(30, 5, $salesOrder->getTerms() . ' days');
			$pdf->setXY(28, 58, true);
			$pdf->Cell(30, 5, $customer->getTin());
			$pdf->setXY(35, 64, true);
			$pdf->MultiCell(115, 5, $branch->getAddress(), 0 ,'L');
			$pdf->setXY(155, 64, true);
			$pdf->Cell(30, 5, 'PO #:');
			$pdf->setXY(170, 64, true);
			$pdf->Cell(30, 5, $salesOrder->getPoRefCode());
			
			$widthStart = 90;
			
			foreach ($entity->getItems() as $item) {
				$soi = $item->getSalesOrderItem();
				$packaging = $soi->getQuotation()->getProduct()->getPackaging();
				$unitPerPackage = round($item->getQuantity() / $packaging->getKgsUnit(), 2);
				$pdf->setXY(20, $widthStart, true);
				$pdf->Cell(25, 4.25, number_format($item->getQuantity(), 2), 0, 0, 'C');
				$pdf->setXY(47, $widthStart, true);
				$pdf->Cell(22, 4.25, 'kg',0 , 0, 'C');
				$pdf->setXY(72, $widthStart, true);
				$pdf->MultiCell(148, 4.25, $soi->getQuotation()->getProductName() . ' (' . $unitPerPackage .' x ' .$packaging->getDescription() . ')', 0, 'L');
				$widthStart += $pdf->getLastH()+2;
			}
			
			$pdf->setXY(17, 234, true);
			$pdf->Cell(30, 5, $entity->getDriver());
		} else {
			$filename = 'Sales_Invoice_' . $entity->getId() . '_' . date('M-d-Y');
			
			$pdf->setPageFormat(array(235, 296));
			$pdf->addPage();

			$pdf->SetFont('helvetica', '', 10);
			
			$pdf->setXY(31, 51, true);
			$pdf->Cell(117, 5, $customer->getName(), 0);
			$pdf->setXY(162, 51, true);
			$pdf->Cell(50, 5, $salesOrder->getDate()->format('M d, Y'), 0);
			$pdf->setXY(163, 57, true);
			$pdf->Cell(50, 5, $salesOrder->getTerms() . ' days', 0);
			$pdf->setXY(32, 58, true);
			$pdf->MultiCell(117, 10, $branch->getAddress(), 0, 'L');
			$pdf->setXY(25, 70, true);
			$pdf->Cell(50, 5, $customer->getTin(), 0);
			
			$widthStart = 89;
			$drTotal = 0;
				
			foreach ($entity->getItems() as $item) {
				$soi = $item->getSalesOrderItem();
				$packaging = $soi->getQuotation()->getProduct()->getPackaging();
				$pdf->setXY(15, $widthStart, true);
				$pdf->Cell(25, 4.25, number_format($item->getQuantity(), 2), 0, 0, 'R');
				$pdf->setXY(40, $widthStart, true);
				$pdf->Cell(13, 4.25, 'kg', 0, 0, 'C');

				$pdf->setXY(54, $widthStart, true);
				$pdf->MultiCell(98, 4.25, $soi->getQuotation()->getProductName() . ' (' . $packaging->getName() . ')', 0, 'L');
				$productNameCellHeight = $pdf->getLastH();
				$pdf->setXY(151, $widthStart, true);
				$pdf->Cell(28, 4.25, number_format($soi->getUnitPrice(), 2), 0, 0, 'R');
				$pdf->setXY(179, $widthStart, true);
				$lineTotal = $item->getQuantity() * $soi->getUnitPrice();
				$pdf->Cell(39, 4.25, number_format($lineTotal, 2), 0, 0, 'R');
				$drTotal += $lineTotal;
				$widthStart += $productNameCellHeight + 2;
			}
			
			$vat = $drTotal - ($drTotal / 1.12);

			//PO No
			$pdf->setXY(54, 209, true);
			$pdf->Cell(70, 4.25, 'PO NO. ' . $salesOrder->getPoRefCode(), 0);

			//DR Ref No
			$pdf->setXY(54, 214, true);
			$pdf->Cell(70, 4.25, 'DR REF NO. ' . $entity->getDrId(), 0);
			
			//Total Sales
			$pdf->setXY(179, 209, true);
			$pdf->Cell(39, 4.25, number_format(round($drTotal + $vat, 2), 2), 0, 0, 'R');
			//Less VAT
			$pdf->setXY(179, 215, true);
			$pdf->Cell(39, 4.25, number_format(round($vat, 2), 2), 0, 0, 'R');
			//Amount: Net of VAT
			$pdf->setXY(179, 222, true);
			$pdf->Cell(39, 4.25, number_format($drTotal, 2), 0, 0, 'R');
			//Amount Due
			$pdf->setXY(179, 234, true);
			$pdf->Cell(39, 4.25, number_format($drTotal, 2), 0, 0, 'R');
			//Add VAT
			$pdf->setXY(179, 241, true);
			$pdf->Cell(39, 4.25, number_format(round($vat, 2), 2), 0, 0, 'R');
			//Total Amount Due
			$pdf->setXY(179, 248, true);
			$pdf->Cell(39, 4.25, number_format(round($drTotal + $vat, 2), 2), 0, 0, 'R');
			//VATable Sales
			$pdf->setXY(85, 221, true);
			$pdf->Cell(39, 4.25, number_format($drTotal, 2), 0, 0, 'R');
			//VAT Amount
			$pdf->setXY(85, 240, true);
			$pdf->Cell(39, 4.25, number_format(round($vat, 2), 2), 0, 0, 'R');
		}
		
		return array('pdf' => $pdf, 'filename' => $filename);
	}

	public function findProductByItemId($shippingItemId)
	{
		return $this->getEM()->getRepository('CIInventoryBundle:ShippingItem')->findProductByItemId($shippingItemId);
	}
}