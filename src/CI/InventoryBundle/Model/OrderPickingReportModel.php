<?php

namespace CI\InventoryBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\InventoryBundle\Form\Type\OrderPickingReportFilterType;

class OrderPickingReportModel
{
	private $container;
	private $formFactory;
	private $em;

	const FILTER = 'filter';
	const INDEX = 'index';
	const PDF = 'pdf';
	const XLS = 'xls';

	public function __construct(ContainerInterface $container, FormFactoryInterface $formFactory)
	{
		$this->container = $container;
		$this->formFactory = $formFactory;
		$this->em = $container->get('doctrine')->getManager();
	}

	public function getOrderPickingReport($type, $params = null)
	{
		switch ($type) {
			case self::FILTER:
				return $this->formFactory->create(new OrderPickingReportFilterType(), null, array('method' => 'GET'));
			case self::INDEX:
				return $this->em->getRepository('CIInventoryBundle:OrderPickingItem')->getOrderPickingItemReports($params);
			case self::PDF:
				$parsedParams = array();
				if (!empty($params['warehouse'])) {
					$parsedParams = array('Warehouse Number' => $params['warehouse']);
				}

				if (!empty($params['customer'])) {
					$parsedParams['Customer'] = $params['customer']->getShortName();
				}

				if (!empty($params['date'])) {
					$parsedParams['date'] = $params['date']->format('M d, Y');
				}

				$qb = $this->getOrderPickingReport(self::INDEX, $params);
				$items = $qb->getResult();
				$arr = array();
				foreach ($items as $item) {
					$arr[] = array(
						'id' => $item['opNumber'],
						'drNo' => $item['id'],
						'warehouseNumber' => $item['warehouseNumber'],
						'supplierName' => $item['shortName'],
						'product' => $item['name'],
						'lotNumber' => $item['lotNumber'],
						'productionDate' => date_format($item['productionDate'],"M d, Y"),
						'palletId' => $item['palletId'],
						'storageLocation' => $item['fullLocation'],
						'quantity' => $item['quantity'],
						'opDate' => date_format($item['date'],"M d, Y"),
					);
				}
				
				return array(
					'rows' => $arr,
					'parsedParams' => $parsedParams,
					'title' => 'Order Picking Report',
					'tableFormat' => array(
						array('header' => "ORDER PICKING #", 'width' => 0.06, 'align' => 'C', 'number' => null),
						array('header' => "DR #\n ", 'width' => 0.06, 'align' => 'C', 'number' => null),
						array('header' => "WAREHOUSE NUMBER", 'width' => 0.08, 'align' => 'R', 'number' => null),
						array('header' => "CUSTOMER\n ", 'width' => 0.14, 'align' => 'L', 'number' => null),
						array('header' => "PRODUCT\n ", 'width' => 0.2, 'align' => 'L', 'number' => null),
						array('header' => "LOT #\n ", 'width' => 0.06, 'align' => 'C', 'number' => null),
						array('header' => "PRODUCTION DATE", 'width' => 0.08, 'align' => 'R', 'number' => null),
						array('header' => "PALLET ID\n ", 'width' => 0.06, 'align' => 'R', 'number' => null),
						array('header' => "STORAGE LOCATION", 'width' => 0.1, 'align' => 'R', 'number' => null),
						array('header' => "QUANTITY\n ", 'width' => 0.06, 'align' => 'R', 'number' => null),
						array('header' => "ORDER PICKING DATE", 'width' => 0.1, 'align' => 'R', 'number' => null),

					)
				);
		}
	}
}