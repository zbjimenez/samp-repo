<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Entity\Warehouse;
use CI\InventoryBundle\Form\Type\WarehouseType;
use CI\InventoryBundle\Form\Type\SearchFilterType;

class WarehouseModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	
	public function getNewEntity()
	{
		return new Warehouse();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new WarehouseType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new SearchFilterType('Name or Warehouse ID', 'Search'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE:
				return 'New Warehouse has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Warehouse has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Warehouse has been deleted.';
			default:
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'warehouse_delete',
			'return_path' => 'warehouse_show',
			'name' => '[Warehouse] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
		);
	}
	
	public function isDeletable(Warehouse $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This Warehouse ' . $entity->getName() . ' has already been added to a transaction and therefore can no longer be deleted.');
		}
	}

	/*
	* This simply finds the first Warehouse ever created,
	* presumably that's the default one.
	*/
	public function getDefaultWarehouse()
	{
		return $this->getRepository()->getDefaultWarehouse();
	}

	public function getWarehouseByNumber($warehouseNumber)
	{
		return $this->getRepository()->determineWarehouseByNumber($warehouseNumber);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Warehouse'
		);
	
		$options = array(
			'route' => 'warehouse',
			'name' => 'Warehouse ',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'warehouse',
			'name' => 'Warehouse ',
			'classes' => array(
				'CI\InventoryBundle\Entity\Warehouse'
			)
		);
	}
	
	public function getJson($id)
	{
		$entity = $this->findExistingEntity($id);
	
		return array(
			'name' => $entity->getName(),
			'address' => $entity->getAddress()
		);
	}

	public function determineWarehouseByNumber($warehouseNumber)
	{
		try {
			return $this->getRepository()->determineWarehouseByNumber($warehouseNumber);
		} catch(NotFoundHttpException $e) {
			return $this->getDefaultWarehouse();
		}
	}
}