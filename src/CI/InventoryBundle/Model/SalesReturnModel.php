<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

use CI\InventoryBundle\Entity\SalesReturn as EntityType;
use CI\InventoryBundle\Entity\SalesReturnItem as EntityItemType;
use CI\InventoryBundle\Form\Type\SalesReturnType as EntityFormType;
use CI\InventoryBundle\Form\Type\SalesReturnFileContainerType as FileFormType;
use CI\InventoryBundle\Form\Type\SalesReturnFilterType as FilterFormType;
use CI\InventoryBundle\Entity\InventoryLog;

class SalesReturnModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_STATUS_CHANGE = 'statusChange';
	
	private $originalItems;
	
	public function getInventoryService()
	{
		return $this->get('ci.inventory.service');
	}
	
	public function getNewEntity($shippingId = null)
	{
		$entity = new EntityType();
		if (!empty($shippingId)) {
			$s = $this->getRepository("CIInventoryBundle:Shipping")->find($shippingId);
			$entity->setShipping($s);
		}
		return $entity;
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new EntityFormType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new FilterFormType());
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE: return 'Sales return created successfully.';
			case self::ACTION_UPDATE: return 'Sales return updated successfully.';
			case self::ACTION_DELETE: return 'Sales return has been deleted.';
			case self::ACTION_STATUS_CHANGE: return 'Sales return status has been succesfully changed to ';
			default: throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isCreatable($shippingId)
	{
		$ro = $this->getRepository("CIInventoryBundle:Shipping")->find($shippingId);
	
		if (!$ro->getActive()) {
			throw new \Exception('Only delivery receipts that are received can create sales returns.');
		}
	}
	
	public function isEditable(EntityType $entity)
	{
		if (!$entity->isEditable()) {
			throw new \Exception('Only sales returns that are drafts can be edited.');
		}
	}
	
	public function isDeletable(EntityType $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('Only sales returns that are drafts can be deleted.');
		}
	}
	
	public function storeOriginalItems(EntityType $entity)
	{
		$this->originalItems = new ArrayCollection();
	
		foreach ($entity->getItems() as $item) {
			$this->originalItems->add($item);
		}
	}
	
	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		
		if ($entity->getId()) {
			foreach ($this->originalItems as $item) {
				if (false === $entity->getItems()->contains($item)) {
					$item->setSalesReturn(null);
					$em->remove($item);
				}
			}
		}
		
		if ($form->get('saveDraft')->isClicked()) {
			$entity->setStatus(EntityType::STATUS_DRAFT);
		} else if ($form->get('saveReturned')->isClicked()) {
			$entity->setStatus(EntityType::STATUS_RETURNED);
		} else {
			throw new \Exception("Invalid operation. Please try again.");
		}
		
		$em->persist($entity);
		$em->flush();
	}
	
	public function getDeleteParams($entity)
	{
		return array(
    		'path' => 'salesreturn_delete',
    		'return_path' => 'salesreturn_show',
    		'name' => '[Sales Return] # ' . $entity->getId()
    	);
	}
	
	public function deleteEntity($id)
	{
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);
		
		$em->remove($entity);
		$em->flush();
	}
	
	public function changeStatus($id, $status)
	{
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);

		if (EntityType::STATUS_RETURNED == $status) {
			if ($this->getSecurityContext()->isGranted("ROLE_SALESRETURN_CHANGE_STATUS")) {
				if (EntityType::STATUS_DRAFT == $entity->getStatus()) {
					$entity->setStatus(EntityType::STATUS_RETURNED);
				} else {
					throw new \Exception('Only draft sales returns can be returned.');
				}
			} else {
				throw new \Exception('You dont have access to change the status of this item.');
			}
			
		} elseif (EntityType::STATUS_VOID == $status) {
			if ($this->getSecurityContext()->isGranted("ROLE_SALESRETURN_CHANGE_STATUS")) {
				if (EntityType::STATUS_RETURNED == $entity->getStatus()) {
					$entity->setStatus(EntityType::STATUS_VOID);
				} else {
					throw new \Exception('Only returned sales returns can be voided.');
				}
			} else {
				throw new \Exception('You dont have access to change the status of this item.');
			}			
		} else {
			throw new \Exception('Invalid operation.');
		}
	
		$em->persist($entity);
		$em->flush();
			
		return $entity->translateStatus();
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'SalesReturn'
		);
	
		$options = array(
			'route' => 'salesreturn',
			'name' => 'Sales Return',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'salesreturn',
			'name' => 'Sales Return',
			'classes' => array(
				'CI\InventoryBundle\Entity\SalesReturn',
				'CI\InventoryBundle\Entity\SalesReturnItem',
				'CI\InventoryBundle\Entity\SalesReturnFile'
			)
		);
	}

	//file handling
	public function getFileFormType(EntityType $entity)
	{
		return $this->getFormFactory()->create(new FileFormType(), $entity, array('method' => 'PUT'));
	}
	
	public function storeOriginalFiles(EntityType $entity)
	{
		$this->originalFiles = new ArrayCollection();
	
		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}
	
	public function updateFiles(Form $form, EntityType $entity)
	{
		$em = $this->getEM();
	
		foreach ($this->originalFiles as $file) {
			if (false === $entity->getFiles()->contains($file)) {
				$file->setPrevId($entity->getId());
				$file->setSalesReturn(null);
				$em->remove($file);
			}
		}
		$entity->setUpdatedAt(new \DateTime());

		$em->persist($entity);
		$em->flush();
	}
	
	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:SalesReturnFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();
	
		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();
	
		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename='.$file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}
}