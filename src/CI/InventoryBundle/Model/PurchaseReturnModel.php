<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;

use CI\InventoryBundle\Entity\PurchaseReturn as EntityType;
use CI\InventoryBundle\Entity\PurchaseReturnItem as EntityItemType;
use CI\InventoryBundle\Form\Type\PurchaseReturnType as EntityFormType;
use CI\InventoryBundle\Form\Type\PurchaseReturnFileContainerType as FileFormType;
use CI\InventoryBundle\Form\Type\PurchaseReturnFilterType as FilterFormType;
use CI\InventoryBundle\Entity\InventoryLog;

class PurchaseReturnModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_STATUS_CHANGE = 'statusChange';
	
	private $originalItems;
	
	public function getInventoryService()
	{
		return $this->get('ci.inventory.service');
	}
	
	public function getNewEntity($receiveOrderId = null)
	{
		$entity = new EntityType();
		if (!empty($receiveOrderId)) {
			$ro = $this->getRepository("CIInventoryBundle:ReceiveOrder")->find($receiveOrderId);
			$entity->setReceiveOrder($ro);
			$entity->setSupplier($ro->getPurchaseOrder()->getSupplier());
		}
		return $entity;
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new EntityFormType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new FilterFormType());
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE: return 'Shipment discrepancy notice created successfully.';
			case self::ACTION_UPDATE: return 'Shipment discrepancy notice updated successfully.';
			case self::ACTION_DELETE: return 'Shipment discrepancy notice has been deleted.';
			case self::ACTION_STATUS_CHANGE: return 'Shipment discrepancy notice status has been succesfully changed to ';
			default: throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isCreatable($receiveOrderId)
	{
		$ro = $this->getRepository("CIInventoryBundle:ReceiveOrder")->find($receiveOrderId);
	
		if (!$ro->getActive()) {
			throw new \Exception('Only receive orders that are received can create shipment discrepancy notices.');
		}
	}
	
	public function isEditable(EntityType $entity)
	{
		if (!$entity->isEditable()) {
			throw new \Exception('Only shipment discrepancy notices that are drafts can be edited.');
		}
	}
	
	public function isDeletable(EntityType $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('Only shipment discrepancy notices that are drafts can be deleted.');
		}
	}
	
	public function storeOriginalItems(EntityType $entity)
	{
		$this->originalItems = new ArrayCollection();
	
		foreach ($entity->getItems() as $item) {
			$this->originalItems->add($item);
		}
	}
	
	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		$invSvc = $this->getInventoryService();
		$memo = 'Supplier: ' . $entity->getReceiveOrder()->getPurchaseOrder()->getSupplier()->getName();

		foreach ($entity->getItems() as $item) {
			if ($item->getReceiveOrderItem()->getTotalPutAway() > 0) {
				throw new \Exception('Cannot Return this Pallet ' . $item->getPalletId() . ' because it has already been put away.');
			}
		}
		
		if ($entity->getId()) {
			foreach ($this->originalItems as $item) {
				if (false === $entity->getItems()->contains($item)) {
					$item->setPurchaseReturn(null);
					$em->remove($item);
				}
			}
		}
		
		if ($form->get('saveDraft')->isClicked()) {
			$entity->setStatus(EntityType::STATUS_DRAFT);
		} else if ($form->get('saveReturned')->isClicked()) {
			$entity->setStatus(EntityType::STATUS_RETURNED);
			$invSvc->deductStaging($entity);
		} else {
			throw new \Exception("Invalid operation. Please try again.");
		}
		
		$em->persist($entity);
		$em->flush();
	}
	
	public function getDeleteParams($entity)
	{
		return array(
    		'path' => 'purchasereturn_delete',
    		'return_path' => 'purchasereturn_show',
    		'name' => '[Shipment Discrepancy Notice] # ' . $entity->getId()
    	);
	}
	
	public function deleteEntity($id)
	{
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);
		
		$em->remove($entity);
		$em->flush();
	}
	
	public function changeStatus($id, $status)
	{
		$em = $this->getEM();
		$invSvc = $this->getInventoryService();
		$entity = $this->findExistingEntity($id);
		$memo = 'Supplier: ' . $entity->getReceiveOrder()->getPurchaseOrder()->getSupplier()->getName();
	
		if (EntityType::STATUS_RETURNED == $status) {
			if ($this->getSecurityContext()->isGranted(array(new Expression('hasAnyRole("ROLE_INVENTORY_CLERK","ROLE_PURCHASING", "ROLE_SALES_MANAGER")')))) {
				if (EntityType::STATUS_DRAFT == $entity->getStatus()) {
					$entity->setStatus(EntityType::STATUS_RETURNED);
					$invSvc->deductStaging($entity);
				} else {
					throw new \Exception('Only draft shipment discrepancy notices can be returned.');
				}
			} else {
				throw new \Exception('You are not allowed to mark this item as Returned');
			}
		} elseif (EntityType::STATUS_VOID == $status) {
			if ($this->getSecurityContext()->isGranted(array(new Expression('hasAnyRole("ROLE_INVENTORY_CLERK", "ROLE_PURCHASING", "ROLE_SALES_MANAGER")')))) {			
				if (EntityType::STATUS_RETURNED == $entity->getStatus()) {
					$entity->setStatus(EntityType::STATUS_VOID);
					$invSvc->addStaging($entity);
				} else {
					throw new \Exception('Only returned shipment discrepancy notices can be voided.');
				} 
			} else {
					throw new \Exception('You are not allowed to void this item');	
			}
		} else {
			throw new \Exception('Invalid operation.');
		}
	
		$em->persist($entity);
		$em->flush();
			
		return $entity->translateStatus();
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'PurchaseReturn'
		);
	
		$options = array(
			'route' => 'purchasereturn',
			'name' => 'Shipment Discrepancy Notice',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'purchasereturn',
			'name' => 'Shipment Discrepancy Notice',
			'classes' => array(
				'CI\InventoryBundle\Entity\PurchaseReturn',
				'CI\InventoryBundle\Entity\PurchaseReturnFile',
				'CI\InventoryBundle\Entity\PurchaseReturnItem'
			)
		);
	}

	//file handling
	public function getFileFormType(EntityType $entity)
	{
		return $this->getFormFactory()->create(new FileFormType(), $entity, array('method' => 'PUT'));
	}
	
	public function storeOriginalFiles(EntityType $entity)
	{
		$this->originalFiles = new ArrayCollection();
	
		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}
	
	public function updateFiles(Form $form, EntityType $entity)
	{
		$em = $this->getEM();
	
		foreach ($this->originalFiles as $file) {
			if (false === $entity->getFiles()->contains($file)) {
				$file->setPrevId($entity->getId());
				$file->setPurchaseReturn(null);
				$em->remove($file);
			}
		}

		$em->persist($entity);
		$em->flush();
	}
	
	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:PurchaseReturnFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();
	
		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();
	
		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename='.$file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}
}