<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Helper\CustomTCPDF;
use CI\InventoryBundle\Form\Type\PurchaseOrderReportFilterType;

class SupplierReportModel extends BaseEmptyEntityModel
{
	public function getPurchaseOrderReport($type, $params = null)
	{
		switch ($type) {
			case 'filter':
				return $this->getFormFactory()->create(new PurchaseOrderReportFilterType());
			case 'index':
				return $this->getRepository('CIInventoryBundle:PurchaseOrder')->getPurchaseOrderReport($params);
			case 'pdf':
				$parsedParams = array();
				
				if (!empty($params['poId'])) {
					$parsedParams['PO #'] = $params['poId'];
				}
				
				if (!empty($params['supplier'])) {
					$parsedParams['Supplier'] = $params['supplier']->getName();
				}
				
				if (!empty($params['products'])) {
					$products = '';
					foreach ($params['products'] as $p) {
						$products .= $p->getDisplayName() . ', ';
					}
					$parsedParams['Products'] = trim($products, ', ');
				}
				
				if (!empty($params['dateFrom'])) {
					$parsedParams['Date From'] = $params['dateFrom']->format('M d, Y');
				}
				
				if (!empty($params['dateTo'])) {
					$parsedParams['Date To'] = $params['dateTo']->format('M d, Y');
				}
				
				$rows = $this->getPurchaseOrderReport('index', $params)->getResult();
				
				$pdf = $this->get("white_october.tcpdf")->create();
				$pdf->setFormName('Purchase Order Report');
				$pdf->setType(CustomTCPDF::LANDSCAPE);
				$pdf->SetFont('helvetica', 'B', 8);
				$pdf->AddPage('L');
				
				$pageMargins = $pdf->getMargins();
				$pageWidth = $pdf->getPageWidth() - $pageMargins['left'] - $pageMargins['right'];
				
				//params
				foreach ($parsedParams as $param => $value) {
					if (!is_null($value)) {
						$pdf->write(5, $param . ': ' . $value, '', false, 'L', true);
					}
				}
				$pdf->Ln();
				
				//headers
				$tableHeaders = array(
						array('width' => 0.06, 'header' => "PO #\n "),
						array('width' => 0.07, 'header' => "PO DATE\n "),
						array('width' => 0.1, 'header' => "REF. NO.\n "),
						array('width' => 0.2, 'header' => "SUPPLIER\n "),
						array('width' => 0.43, 'header' => "ITEMS\n "),
						array('width' => 0.05, 'header' => "TERMS\n "),
						array('width' => 0.09, 'header' => 'ESTIMATED DELIVERY DATE')
				);
				foreach ($tableHeaders as $header) {
					$pdf->MultiCell($header["width"] * $pageWidth, 5,$header["header"],1,'C',false,0,'','',true,0,false,true,10,'T',false);
				}
				$pdf->Ln();
				
				if (count($rows) > 0) {
					foreach ($rows as $row) {
						$maxHeight = 0;
						foreach ($rows as $col) {
							$items = '';
							foreach ($col->getItems() as $item) {
								$items .= $item->getProduct()->getDisplayName() . ' - ' . number_format($item->getQuantity()) . ' kg' . "\n";
							}
							$height = $pdf->getStringHeight(0.43 * $pageWidth, $items, false, true, '', 0);

							if ($height > $maxHeight) {
								$maxHeight = $height;
							}
						}
						if (!$pdf->fits($maxHeight)) {
							$pdf->AddPage();
						}
						$pdf->Cell(0.06 * $pageWidth, 5, $row->getPoId(), 0, 0, 'C');
						$pdf->Cell(0.07 * $pageWidth, 5, $row->getPoDate()->format('M d, Y'));
						$pdf->Cell(0.1 * $pageWidth, 5, $row->getCnPf());
						$pdf->Cell(0.2 * $pageWidth, 5, $row->getSupplier()->getName());
						
						$items = '';
						foreach ($row->getItems() as $item) {
							$items .= $item->getProduct()->getDisplayName() . ' - ' . number_format($item->getQuantity()) . ' kg' . "\n";
						}
						$skips = $pdf->MultiCell(0.43 * $pageWidth, 5, $items, 0, 'L', false, 0, $pdf->GetX(), $pdf->GetY(), false);
						
						$pdf->Cell(0.05 * $pageWidth, 5, $row->getTerms(), 0, 0, 'R');
						$pdf->Cell(0.09 * $pageWidth, 5, $row->getDeliveryDate() ? $row->getDeliveryDate()->format('M d, Y') : '');
						
						$i = 0;
						while ($i < $skips) {
							$pdf->Ln();
							$i++;
						}
					}
				} else {
					$pdf->Write(10, 'No results found.', '', false, 'C');
				}
				
				$pdf->lastPage();
				
				return array('pdf' => $pdf, 'filename' => str_ireplace(' ', '_', 'Purchase Order Report'). '_' . date('M-d-Y'));
		}
	}
}