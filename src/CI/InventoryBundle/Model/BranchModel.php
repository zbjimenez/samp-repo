<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\Form\Form;
use CI\InventoryBundle\Entity\Branch;
use CI\InventoryBundle\Form\Type\BranchType;

class BranchModel extends BaseEmptyEntityModel
{
	public function getNewEntity()
	{
		return new Branch();
	}
	
	public function getNewPreparedEntity($id)
	{
		$em = $this->getEM();
		$entity = new Branch();
		$customer = $em->getRepository('CIInventoryBundle:Customer')->find($id);
		$entity->setCustomer($customer);
	
		return $entity;
	}
	
	public function findExistingEntity($id)
	{
		return $this->getRepository()->find($id);
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new BranchType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function isDeletable(Branch $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('<p>A branch can only be deleted if:</p>
					<ol>
						<li>Not set as main branch.</li>
						<li>There is more than 1 branch specified for the customer.</li>
					<ol>');
		}
	}
	
	public function getErrorResponseData(Form $form, Branch $entity, $type = null)
	{
		$errors = array();
		
	    foreach ($form as $fieldName => $formField) {
	        foreach ($formField->getErrors(true) as $error) {
	            $errors[$fieldName] = $error->getMessage();
	        }
	    }
		
		return array(
			'status' => 'error',
			'message' => 'Something went wrong. Please try again.',
			'errors' => $errors
		);
	}
	
	public function saveEntity(Form $form, Branch $entity)
	{
		try {
			$entity->validate();
		} catch (\Exception $e) {
			return array(
				'success' => false,
				'message' => $e->getMessage()
			);
		}
		
		$em = $this->getEM();
		$parentEntity = $entity->getCustomer()->setUpdatedAt(new \DateTime());
		$em->persist($entity);
		$isCreate = !$entity->getId() ? true : false;
		$em->persist($parentEntity);
		$em->flush();
		
		$message = !$isCreate ? 'Branch has been updated.' : 'New branch added.';
		if (count($entity->getCustomer()->getMain()) == 0) {
			$message .= '<p><i>(Attention: Please select a main branch for this customer.)</i></p>';
		}
	
		return array(
			'entityId' => $entity->getId(),
			'status' => 'success',
			'message' => $message,
		);
	}
	
	public function deleteEntity($id)
	{
		$data = array();
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);
		$parentEntity = $entity->getCustomer();
		
		try {
			$this->isDeletable($entity);
			$parentEntity->setUpdatedAt(new \DateTime());
			$em->persist($parentEntity);
			$em->remove($entity);
			$em->flush();
			
			$message = 'Branch has been deleted.';
			if (count($entity->getCustomer()->getMain()) == 0) {
				$message .= '<p><i>(Attention: Please select a main branch for this customer.)</i></p>';
			}
			
			$data = array(
				'success' => true,
				'message' => $message
			);
		} catch (\Exception $e) {
			$data = array(
				'success' => false,
				'message' => $e->getMessage()
			);
		}
		
		return $data;
	}
	
	public function getJson($id)
	{
		$entity = $this->findExistingEntity($id);
	
		return array(
			'name' => $entity->getName(),
			'address' => nl2br($entity->getAddress())
		);
	}
}