<?php

namespace CI\InventoryBundle\Model;

class WarehouseInventoryModel extends BaseEmptyEntityModel
{
	public function findExistingEntity($id)
	{
		return $this->getRepository()->find($id);
	}

	public function getInventoryService()
	{
		return $this->get('ci.inventory.service');
	}

	public function deleteEntity($id)
	{
		$data = array();
		$em = $this->getEM();

		$entity = $this->findExistingEntity($id);
		
		try {
			$em->remove($entity);

			$em->flush();
			
			$data = array(
				'success' => true,
				'message' => 'Warehouse Inventory has been deleted.'
			);
		} catch (\Exception $e) {
			$data = array(
				'success' => false,
				'message' => $e->getMessage()
			);
		}
		
		return $data;
	}
}