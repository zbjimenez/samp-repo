<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;

use CI\InventoryBundle\Entity\OrderPicking;
use CI\InventoryBundle\Entity\OrderPickingItem;
use CI\InventoryBundle\Entity\InventoryLog;
use CI\InventoryBundle\Form\Type\OrderPickingFilterType;
use CI\InventoryBundle\Form\Type\OrderPickingType;
use CI\InventoryBundle\Helper\CustomTCPDF;

class OrderPickingModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';

	private $originalItems;

	public function getNewEntity()
	{
		return new OrderPicking();
	}

	public function getInventoryService()
	{
		return $this->get('ci.inventory.service');
	}

	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new OrderPickingType($this->getEM()), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}

	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new OrderPickingFilterType(), null, array('method' => 'GET'));
	}

	public function getMessages($action)
	{
		switch ($action) {
			case self::ACTION_CREATE:
				return 'New Order Picking has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Order Picking has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Order Picking has been deleted successfully.';
		}
	}

	public function isEditable($entity)
	{
		if (!$entity->isEditable()) {
			throw new \Exception("Only order pickings that have an item that hasn't been picked can be edited.");
		}
	}

	public function isDeletable($entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception("Only order pickings that have an item that hasn't been picked can be deleted.");
		}
	}

	public function storeOriginalItems(OrderPicking $entity)
	{
		$this->originalItems = new ArrayCollection();

		foreach ($entity->getItems() as $item) {
			$this->originalItems->add($item);
		}
	}

	public function setEntityData(OrderPicking $entity, $shippingId = null)
	{
		if (!is_null($shippingId)) {
			$shipping = $this->getEM()->getRepository('CIInventoryBundle:Shipping')->find($shippingId);
			$entity->setShipping($shipping);
		}
	}

	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		$palletId = array();
		$product = array();

		if (!$entity->getId() && $entity->getShipping()->getAttention())
		{
			throw new \Exception('Order pickings can only be created for deliveries that have no attention.');
		}

		if (bccomp($entity->getShipping()->getSalesOrder()->getQtyDelivered(), $entity->getShipping()->getSalesOrder()->getQtyOrdered(), 2) === 0) {
			throw new \Exception('There is no remaining balance. All items are fully delivered.');
		}

		foreach ($entity->getItems() as $item) {
			if (in_array($item->getPalletId(),$palletId)) {
				throw new \Exception('Pallet IDs must be unique.');
			}
			array_push($palletId, $item->getPalletId());

			if (isset($product[$item->getISProduct()->getId()])) {
				$product[$item->getISProduct()->getId()]['total'] += $item->getQuantity();
			} else {
				$product[$item->getISProduct()->getId()] = array();
				$product[$item->getISProduct()->getId()]['total'] = $item->getQuantity();
				$product[$item->getISProduct()->getId()]['maxQty'] = $item->getShippingItem()->getMaxQtyAllowedToPick();
				$product[$item->getISProduct()->getId()]['name'] = $item->getISProduct()->getName();

			}
		}

		foreach ($product as $productId) {
			if ($productId['total'] > $productId['maxQty']) {
				throw new \Exception('Limit reached for item'. ' '.$productId['name']);
			}
		}

		if ($entity->getId()) {
			foreach ($this->originalItems as $item) {
				if (false === $entity->getItems()->contains($item)) {
					$item->setOrderPicking(null);
					$em->remove($item);
				}
			}
		}

		$em->persist($entity);
		$em->flush();
	}

	public function readBarcodeFile(OrderPicking $entity, $data)
	{
		$em = $this->getEM();
		$invSvc = $this->getInventoryService();
		$success = false;

		foreach ($data as $key => $row) {
			$data[$key]['storageLocation'] = trim($row['storageLocation']);
			$data[$key]['quantity'] = trim($row['quantity']);
			$data[$key]['barcode'] = trim($row['barcode']);
			$data[$key]['lotNumber'] = trim($row['lotNumber']);
			$data[$key]['palletId'] = trim($row['palletId']);
			$data[$key]['productionDate'] = trim($row['productionDate']);
		}

		foreach ($entity->getItems() as $key => $item) {
			foreach ($data as $row) {
				$product = $item->getShippingItem()->getSalesOrderItem()->getQuotation()->getProduct();
				$quantityMatched = bccomp($row['quantity'], $item->getQuantity(), 2) === 0;
				$barCodeMatched = $product->getBarcode() === $row['barcode'];
				$storageLocationMatched = $item->getStorageLocation()->getFullLocation() === $row['storageLocation'];
				$palletIdMatched = $item->getPalletId() === $row['palletId'];
				$lotNumberMatched = $item->getLotNumber() === $row['lotNumber'];
				$productionDateMatched = $item->getProductionDate()->format('m/d/Y') === $row['productionDate'];

				if ($item->getIsPicked() === false && $quantityMatched && $barCodeMatched && $storageLocationMatched && $palletIdMatched && $lotNumberMatched && $productionDateMatched) {
					$invSvc->deductItemStock(
						InventoryLog::TRANS_DELIVER,
						$item->getISProduct(),
						$item->getStorageLocation(),
						$item->getISQuantity(),
						null,
						$item->getLotNumber(),
						$item->getPalletId(),
						false
					);

					$warehouseInv = $invSvc->getWarehouseInventory($item->getISProduct(), $entity->getWarehouse());
					$warehouseInv->setQtyAllocated($warehouseInv->getQtyAllocated() - $item->getISQuantity());
					$item->getISProduct()->updateTotalAllocated();

					$item->setIsPicked(true);
					$success = true;

					break;
				}
			}
		}

		if ($success === true) {
			$entity->updatePickedQuantity();
			$entity->setUpdatedAt(new \DateTime('now'));
			$em->flush();
		}

		return $success;
	}

	/**
	 *
	 * @param integer $id
	 */
	public function export($id)
	{
		$entity = $this->findExistingEntity($id);

		$pdf = $this->get('white_october.tcpdf')->create();
		$pdf->setType(CustomTCPDF::LANDSCAPE);
		$pdf->setFormName('Order Picking List');

		$pdf->addPage('L');

		$pdf->SetFont('helvetica', 'B', 11);
		$pdf->setXY(0, 40, true);
		$pdf->Cell(30, 5, 'Quantity', 0, 0, 'R');

		$pdf->setXY(30, 40, true);
		$pdf->Cell(30, 5, 'Product');

		$pdf->setXY(135, 40, true);
		$pdf->Cell(30, 5, 'Lot #');

		$pdf->setXY(165, 40, true);
		$pdf->Cell(30, 5, 'Prod. Date');

		$pdf->setXY(200, 40, true);
		$pdf->Cell(30, 5, 'Pallet ID');

		$pdf->setXY(235, 40, true);
		$pdf->Cell(30, 5, 'Warehouse Location');

		$pdf->Line(10, 45, 290, 45);

		$pdf->SetFont('helvetica', '', 11);

		$y = 45;

		foreach ($entity->getItems() as $item) {
			$pdf->setXY(0, $y, true);
			$pdf->Cell(30, 5, $item->getQuantity(), 0, 0, 'R');

			$pdf->setXY(30, $y, true);
			$pdf->MultiCell(90, 10, $item->getShippingItem()->getProductName(), 0, 'L', false, 0);

			$pdf->setXY(135, $y, true);
			$pdf->Cell(30, 5, $item->getLotNumber());

			$pdf->setXY(165, $y, true);
			$pdf->Cell(30, 5, $item->getProductionDate()->format('M d, Y'));

			$pdf->setXY(200, $y, true);
			$pdf->Cell(30, 5, $item->getPalletId());

			$pdf->setXY(235, $y, true);
			$pdf->Cell(30, 5, $item->getStorageLocation()->getFullLocation());

			$y += 10;

			$pdf->Line(10, $y, 290, $y);
		}

		$filename = 'Order_Picking_' . $entity->getId() . '_' . date('M-d-Y');
		$pdf->lastPage();

		return array('pdf' => $pdf, 'filename' => $filename);
	}

	public function getRevision($id)
	{
		$class = array(
			'id' => $id,
			'class' => 'OrderPicking'
		);

		$options = array(
			'route' => 'orderpicking',
			'name' => 'OrderPicking',
			'class' => $class,
		);

		return $options;
	}

	public function getLog()
	{
		return array(
			'route' => 'orderpicking',
			'name' => 'Order Picking',
			'classes' => array(
				'CI\InventoryBundle\Entity\OrderPicking',
				'CI\InventoryBundle\Entity\OrderPickingItem'
			)
		);
	}

	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'orderpicking_delete',
			'return_path' => 'orderpicking_show',
			'name' => '[Order Picking] ID #' . $entity->getId(),
		);
	}
}