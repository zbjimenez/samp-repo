<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\Form\Form;
use CI\InventoryBundle\Entity\Inventory;
use CI\InventoryBundle\Entity\InventoryLog;

class InventoryModel extends BaseEmptyEntityModel
{
	public function findExistingEntity($id)
	{
		return $this->getRepository()->find($id);
	}

	public function getInventoryService()
	{
		return $this->get('ci.inventory.service');
	}

	public function deleteEntity($id)
	{
		$data = array();
		$em = $this->getEM();
		$invSvc = $this->getInventoryService();

		$entity = $this->findExistingEntity($id);
		
		try {
			$invSvc->deleteItemStock($entity->getProduct(), $entity->getQtyOnHand());

			$em->remove($entity);

			$em->flush();
			
			$data = array(
				'success' => true,
				'message' => 'Inventory has been deleted.',
				'totalOnHand' => array(
					'kg' => $entity->getProduct()->getTotalOnHand(), 
					'packageCount' => $entity->getProduct()->getQtyOnHandPackage()
				),
				'hasGenerated' => $invSvc->getHasGenerated()
			);
		} catch (\Exception $e) {
			$data = array(
				'success' => false,
				'message' => $e->getMessage(),
				'hasGenerated' => $invSvc->getHasGenerated()
			);
		}
		
		return $data;
	}

	public function findStocksByLocation($warehouseNumber, $lane)
	{
		$results = $this->getRepository()->findStocksByLocation($warehouseNumber, $lane);

		foreach ($results as $key => $row) {
			$productionDate = new \DateTime($row['productionDate']);
			$factoryDate = new \DateTime($row['factoryDate']);
			
			if (!is_null($results[$key]['productionDate'])) {
				$results[$key]['productionDate'] = $productionDate->format('M d, Y');
			}
			if (!is_null($results[$key]['factoryDate'])) {
				$results[$key]['factoryDate'] = $factoryDate->format('M d, Y');
			}
		}

		return $results;
	}

	public function findLotNumbers($productId)
	{
		$results = $this->getRepository()->findLotNumbers($productId);

		return $results;
	}

	public function findProductionDate($lotNumber)
	{
		$results = $this->getRepository()->findProductionDate($lotNumber);

		return $results;
	}

	public function findPalletIds($productId, $lotNumber)
	{
		$results = $this->getRepository()->findPalletIds($productId, $lotNumber);

		return $results;
	}

	public function findStorageLocation($productId, $lotNumber, $palletId)
	{
		$results = $this->getRepository()->findStorageLocation($productId, $lotNumber, $palletId);

		return $results;
	}

	public function transferStorageLocation($inventoryId, $storageLocationId, $quantity = 0)
	{
		$em = $this->getEM();
		$invSvc = $this->getInventoryService();

		$originInventory = $em->getRepository('CIInventoryBundle:Inventory')->find($inventoryId);
		$storageLocation = $em->getRepository('CIInventoryBundle:StorageLocation')->find($storageLocationId);

		$entityPass = ($originInventory && $storageLocation);
		$storageLocationPass = ($entityPass && $originInventory->getStorageLocation()->getId() != $storageLocation->getId());
		$quantityPass = is_null($originInventory->getPalletId()) ? ($quantity > 0) : true;

		if ($entityPass && $storageLocationPass && $quantityPass) {
			$affectedInventories = $invSvc->transferStorageLocation($originInventory, $storageLocation, $quantity);
			$em->flush();

			if ($affectedInventories['target']) {
				$originInventoryData = $affectedInventories['origin'] ? $affectedInventories['origin']->serializeToArray() : null;
				$targetInventoryData = $affectedInventories['target']->serializeToArray();

				$data = array(
					'success' => true,
					'message' => 'Inventory successfully transferred.',
					'affectedInventories' => array(
						'origin' => $originInventoryData,
						'target' => $targetInventoryData
					),
				);
			} else {
				$data = array(
					'success' => false,
					'message' => 'An error occured, please try again.',
					'affectedInventories' => array()
				);
			}
		} else if (!$entityPass) {
			$data = array(
				'success' => false,
				'message' => 'An error occured, please try again.',
				'affectedInventories' => array()
			);
		} else if (!$storageLocationPass) {
			$data = array(
				'success' => false,
				'message' => 'Must select a different storage location.',
				'affectedInventories' => array()
			);
		} else if (!$quantityPass) {
			$data = array(
				'success' => false,
				'message' => 'Quantity must be greater than 0',
				'affectedInventories' => array()
			);
		} else {
			$data = array(
				'success' => false,
				'message' => 'An error occured, please try again.',
				'affectedInventories' => array()
			);
		}

		return $data;
	}
}