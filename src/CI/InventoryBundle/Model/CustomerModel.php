<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Entity\Customer;
use CI\InventoryBundle\Form\Type\CustomerType;
use CI\InventoryBundle\Form\Type\CustomerFileContainerType;
use CI\InventoryBundle\Form\Type\SearchFilterType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;

class CustomerModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	
	public function getNewEntity()
	{
		return new Customer();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new CustomerType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new SearchFilterType('customer short name', 'customer'), null, array('method' => 'GET'));
	}
	
	public function getLatestTransactions(Customer $entity)
	{
		$em = $this->getEM();
		$recentSalesOrders = $em->getRepository('CIInventoryBundle:SalesOrder')->getRecent($entity);
		$recentDeliveryReceipts = $em->getRepository('CIInventoryBundle:Shipping')->getRecent($entity);
		return array(
			'salesOrders' => $recentSalesOrders,
			'deliveryReceipts' => $recentDeliveryReceipts, 
		);
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE: 
				return 'New Customer has been created successfully.';
			case self::ACTION_UPDATE: 
				return 'Customer has been updated successfully.';
			case self::ACTION_DELETE: 
				return 'Customer has been deleted.';
			default: 
				throw new \Exception('Invalid action parameter.');
		}
	}

	public function isEditable(Customer $entity)
	{
		if (!$entity->isEditable()) {
			throw new \Exception('Cannot edit customer.');
		}
	}

	public function isDeletable(Customer $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This customer has already been added to a transaction and therefore can no longer be deleted.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
    		'path' => 'customer_delete',
    		'return_path' => 'customer_show',
    		'name' => '[Customer] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
    	);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Customer'
		);
	
		$options = array(
			'route' => 'customer',
			'name' => 'Customer',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'customer',
			'name' => 'Customer',
			'classes' => array(
				'CI\InventoryBundle\Entity\Customer',
				'CI\InventoryBundle\Entity\Branch',
				'CI\InventoryBundle\Entity\CustomerFile'
			)
		);
	}

	//file handling
	public function getFileFormType(Customer $entity)
	{
		return $this->getFormFactory()->create(new CustomerFileContainerType(), $entity, array('method' => 'PUT'));
	}
	
	public function storeOriginalFiles(Customer $entity)
	{
		$this->originalFiles = new ArrayCollection();
	
		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}
	
	public function updateFiles(Form $form, Customer $entity)
	{
		$em = $this->getEM();
	
		foreach ($this->originalFiles as $file) {
			if (false === $entity->getFiles()->contains($file)) {
				$file->setPrevId($entity->getId());
				$file->setCustomer(null);
				$em->remove($file);
			}
		}

		$entity->setUpdatedAt(new \DateTime());

		$em->persist($entity);
		$em->flush();
	}
	
	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:CustomerFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();
	
		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();
	
		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename='.$file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}
	
	public function getJson($id)
	{
		try {
			$entity = $this->findExistingEntity($id);
		
			return array(
				'success' => true,
				'name' => $entity->getName(),
				'shortName' => $entity->getShortName(),
				'terms' => $entity->getTerms(),
				'ewt' => $entity->getEwt(),
				'ewtLabel' => $entity->translateEwt(),
			);
		} catch(\Exception $e) {
			return array('success' => false);
		}
	}
}