<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

use CI\InventoryBundle\Form\Type\DeliveryScheduleReportFilterType;
use CI\InventoryBundle\Form\Type\SalesForecastReportFilterType;
use CI\InventoryBundle\Entity\SalesForecast;

class SalesReportModel extends BaseEmptyEntityModel
{
	const TYPE_DELIVERY_SCHEDULE = 'delivery.schedule';
	const TYPE_SALES_FORECAST = 'sales.forecast';
	
	private $type = null;
	
	public function deliveryScheduleReport()
	{
		$this->type = self::TYPE_DELIVERY_SCHEDULE;
		
		return $this;
	}
	
	public function salesForecastReport()
	{
		$this->type = self::TYPE_SALES_FORECAST;
	
		return $this;
	}
	
	public function getFilter()
	{
		switch ($this->type) {
			case self::TYPE_DELIVERY_SCHEDULE: return $this->getFormFactory()->create(new DeliveryScheduleReportFilterType());
			case self::TYPE_SALES_FORECAST: return $this->getFormFactory()->create(new SalesForecastReportFilterType());
			default: throw new \Exception('Make sure a report type is set.');
		}
	}
	
	public function getData($params)
	{
		if (empty($params)) {
			throw new \Exception('Variable $params must be set.');
		}
		
		switch ($this->type) {
			case self::TYPE_DELIVERY_SCHEDULE: return $this->getRepository('CIInventoryBundle:ShippingItem')->getDeliveryScheduleReport($params);
			case self::TYPE_SALES_FORECAST: return $this->getRepository('CIInventoryBundle:SalesForecast')->loadAll($params, true)->orderBy('c.name', 'ASC')->addOrderBy('p.sku', 'ASC')->getQuery()->getResult();
			default: throw new \Exception('Make sure a report type is set.');
		}
	}
	
	public function getPdf($params)
	{
		if (self::TYPE_DELIVERY_SCHEDULE === $this->type) {
			$parsedParams = array();
			$parsedParams['Date From'] = $params['dateFrom']->format('m/d/Y');
			$parsedParams['Date To'] = $params['dateTo']->format('m/d/Y');
			
			if (!empty($params['customer'])) {
				$parsedParams['Customer'] = $params['customer']->getName();
			}
			
			if (!empty($params['product'])) {
				$parsedParams['Product'] = $params['product']->getDisplayName();
			}
			
			$data = $this->getData($params)->getArrayResult();
			$procData = array();
			foreach ($data as $key => $datum) {
				$procData[$key][] = $datum['deliveryDate']->format('M d, Y');
				$procData[$key][] = $datum['drRefCode'];
				$procData[$key][] = $datum['siRefCode'];
				$procData[$key][] = $datum['customerName'];
				$procData[$key][] = $datum['productName'];
				$procData[$key][] = $datum['quantity'] . " kg (" . ($datum['quantity'] / $datum['kgsUnit']) . " " . $datum['kgsName'] . ")";
				$procData[$key][] = $datum['quantity'];
				$procData[$key][] = $datum['unitPrice'];
				$procData[$key][] = $datum['quantity'] * $datum['unitPrice'];
			}
			
			return array(
				'name' => 'Delivery Schedule Report',
				'rows' => $procData,
				'parsedParams' => $parsedParams,
				'misc' => array(),
				'tableFormat' => array(
					array('header' => 'DATE', 'width' => 0.07, 'align' => 'L', 'number' => null),
					array('header' => 'DR#', 'width' => 0.06, 'align' => 'C', 'number' => null),
					array('header' => 'SI#', 'width' => 0.06, 'align' => 'C', 'number' => null),
					array('header' => 'DELIVERED TO', 'width' => 0.15, 'align' => 'L', 'number' => null),
					array('header' => 'ITEM', 'width' => 0.21, 'align' => 'L', 'number' => null),
					array('header' => 'DESCRIPTION', 'width' => 0.15, 'align' => 'L', 'number' => null),
					array('header' => 'QTY (KGS)', 'width' => 0.1, 'align' => 'R', 'number' => 2),
					array('header' => 'PRICE', 'width' => 0.1, 'align' => 'R', 'number' => 4),
					array('header' => 'LINE TOTAL', 'width' => 0.1, 'align' => 'R', 'number' => 2),
				)
			);
		} elseif (self::TYPE_SALES_FORECAST === $this->type) {
			$parsedParams = array();
			$parsedParams['Year'] = $params['year'];
			
			if (!empty($params['month'])) {
				$parsedParams['Month'] = $params['month'];
			}
			
			if (!empty($params['customer'])) {
				$parsedParams['Customer'] = $params['customer']->getName();
			}
				
			if (!empty($params['product'])) {
				$parsedParams['Product'] = $params['product']->getDisplayName();
			}
			
			$months = SalesForecast::getMonths();
			$data = $this->getData($params);
			$procData = array();
			$totalQty = $totalAmt = 0;
			foreach ($months as $month) {
				if (empty($params['month']) || $month == $params['month']) {
					$monthQty = $monthAmt = 0;
					foreach ($data as $key => $datum) {
						$monthValue = $datum->getMonthValue($month);
						$monthQty += $monthValue['qty'];
						$monthAmt += $monthValue['amt'];
						$procData[] = array(
							$month,
							$datum->getCustomer()->getShortName(),
							$datum->getProduct()->getSku(),
							$monthValue['qty'],
							$monthValue['amt'],
						);
					}
					
					$procData[] = array(
						" ", " ",
						"SUBTOTAL",
						$monthQty,
						$monthAmt,
					);
					$totalQty += $monthQty;
					$totalAmt += $monthAmt;
				}
			}
			
			$procData[] = array(
				" ", " ",
				"TOTAL",
				$totalQty,
				$totalAmt,
			);
				
			return array(
				'name' => 'Sales Forecast Report',
				'rows' => $procData,
				'parsedParams' => $parsedParams,
				'misc' => array(),
				'tableFormat' => array(
					array('header' => 'MONTH', 'width' => 0.07, 'align' => 'L', 'number' => null),
					array('header' => 'CUSTOMER', 'width' => 0.25, 'align' => 'L', 'number' => null),
					array('header' => 'PRODUCT', 'width' => 0.38, 'align' => 'L', 'number' => null),
					array('header' => 'QTY (KGS)', 'width' => 0.15, 'align' => 'R', 'number' => 2),
					array('header' => 'AMT (PHP)', 'width' => 0.15, 'align' => 'R', 'number' => 2),
				)
			);
		} else {
			throw new \Exception('Make sure a report type is set.');
		}
	}
}