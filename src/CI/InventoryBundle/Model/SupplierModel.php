<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\InventoryBundle\Entity\Supplier;
use CI\InventoryBundle\Form\Type\SupplierFileContainerType;
use CI\InventoryBundle\Form\Type\SupplierType;
use CI\InventoryBundle\Form\Type\SearchFilterType;

class SupplierModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	
	public function getNewEntity()
	{
		return new Supplier();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new SupplierType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new SearchFilterType('supplier short name', 'supplier'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch ($action) {
			case self::ACTION_CREATE:
				return 'New Supplier has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Supplier has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Supplier has been deleted successfully.';
		}
	}
	
	public function getRevision($id)
	{
		$class = array(
			'id' => $id,
			'class' => 'Supplier'
		);
		
		$options = array(
			'route' => 'supplier',
			'name' => 'Supplier',
			'class' => $class,
		);
		
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'supplier',
			'name' => 'Supplier',
			'classes' => array(
				'CI\InventoryBundle\Entity\Supplier',
				'CI\InventoryBundle\Entity\SupplierFile'
			)
		);
	}

	//file handling
	public function getFileFormType(Supplier $entity)
	{
		return $this->getFormFactory()->create(new SupplierFileContainerType(), $entity, array('method' => 'PUT'));
	}
	
	public function storeOriginalFiles(Supplier $entity)
	{
		$this->originalFiles = new ArrayCollection();
	
		foreach ($entity->getFiles() as $file) {
			$this->originalFiles->add($file);
		}
	}
	
	public function updateFiles(Form $form, Supplier $entity)
	{
		$em = $this->getEM();
	
		foreach ($this->originalFiles as $file) {
			if (false === $entity->getFiles()->contains($file)) {
				$file->setPrevId($entity->getId());
				$file->setSupplier(null);
				$em->remove($file);
			}
		}
		$entity->setUpdatedAt(new \DateTime());

		$em->persist($entity);
		$em->flush();
	}
	
	public function downloadFile($id)
	{
		$em = $this->getEM();
		$file = $em->getRepository('CIInventoryBundle:SupplierFile')->find($id);
		$filePath = $file->getAbsoluteFilePath();
	
		$fileObj = new File($filePath);
		$mimeType = $fileObj->getMimeType();
	
		if (file_exists($filePath)) {
			header("Content-Type: " . $mimeType);
			header('Content-Disposition: attachment; filename='.$file->getName());
			header('Content-Length: ' . filesize($filePath));
			readfile($filePath);
			exit;
		}
	}
	
	public function isDeletable($entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This supplier has already been associated with several transactions.');
		}
	}
	
	public function deleteEntity($id)
	{
		$em = $this->getEM();
		$entity = $this->findExistingEntity($id);
		$em->remove($entity);
		$em->flush();
	}
	
	public function getDeleteParams($entity)
	{
		if ($entity instanceof Supplier) {
			return array(
	    		'path' => 'supplier_delete',
	    		'return_path' => 'supplier_show',
	    		'name' => '[Supplier] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
	    	);
		}
	}
	
	public function getJson($id)
	{
		$entity = $this->findExistingEntity($id);
	
		return array(
			'terms' => $entity->getTerms() ? $entity->getTerms() : 0
		);
	}
}