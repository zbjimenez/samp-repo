<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Entity\Industry;
use CI\InventoryBundle\Form\Type\IndustryType;
use CI\InventoryBundle\Form\Type\SearchFilterType;

class IndustryModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	
	public function getNewEntity()
	{
		return new Industry();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new IndustryType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new SearchFilterType('industry name', 'industry name'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE:
				return 'New Industry has been created successfully.';
			case self::ACTION_UPDATE:
				return 'Industry has been updated successfully.';
			case self::ACTION_DELETE:
				return 'Industry has been deleted.';
			default:
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'industry_delete',
			'return_path' => 'industry_show',
			'name' => '[Industry] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
		);
	}
	
	public function isDeletable(Industry $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This industry has already been added to a product and therefore can no longer be deleted.');
		}
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Industry'
		);
	
		$options = array(
			'route' => 'industry',
			'name' => 'Industry',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'industry',
			'name' => 'Industry',
			'classes' => array(
				'CI\InventoryBundle\Entity\Industry'
			)
		);
	}
}