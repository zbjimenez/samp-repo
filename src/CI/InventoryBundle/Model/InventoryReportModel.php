<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\InventoryBundle\Form\Type\InventoryReportFilterType;
use CI\InventoryBundle\Form\Type\WarehouseInventoryReportFilterType;

class InventoryReportModel
{
	private $container;
	private $formFactory;
	private $em;

	const FILTER = 'filter';
	const INDEX = 'index';
	const PDF = 'pdf';
	const XLS = 'xls';

	public function __construct(ContainerInterface $container, FormFactoryInterface $formFactory)
	{
		$this->container = $container;
		$this->formFactory = $formFactory;
		$this->em = $container->get('doctrine')->getManager();
	}

	public function getInventoryListReport($type, $params = null)
	{
		switch ($type) {
			case self::FILTER:
				return $this->formFactory->create(new InventoryReportFilterType(), null, array('method' => 'GET'));
			case self::INDEX:
				return $this->em->getRepository('CIInventoryBundle:Product')->getInventory($params);
			case self::PDF:

				$parsedParams = array();

				if (!empty($params['sku'])) {
					$parsedParams = array(
							'SKU' => $params['sku']
					);
				}

				return array(
					'rows' => $this->getInventoryListReport(self::INDEX, $params)->getScalarResult(),
					'parsedParams' => $parsedParams,
					'title' => 'Inventory Report',
					'misc' => array(
						'excludeColumns' => array('p_id'),
					),
					'tableFormat' => array(
						array('header' => 'SKU.', 'width' => 0.15, 'align' => 'L', 'number' => null),
						array('header' => 'PRODUCT NAME', 'width' => 0.7, 'align' => 'L', 'number' => null),
						array('header' => 'CATEGORY', 'width' => 0.15, 'align' => 'L', 'number' => null)
					)
				);
		}
	}

	public function getWarehouseInventoryReport($type, $params = null)
	{
		switch ($type) {
			case self::FILTER:
				return $this->formFactory->create(new WarehouseInventoryReportFilterType(), null, array('method' => 'GET'));
			case self::INDEX:
				return $this->em->getRepository('CIInventoryBundle:Inventory')->getWarehouseInventoryReport($params);
			case self::PDF:
				$parsedParams = array();

				if (!empty($params['warehouse'])) {
					$parsedParams['Warehouse'] = $params['warehouse']->getName();
				}

				if (!empty($params['supplier'])) {
					$parsedParams['Supplier'] = $params['supplier']->getName();
				}

				if (!empty($params['product'])) {
					$parsedParams['Product'] = $params['product']->getName();
				}

				if (!empty($params['lotNumber'])) {
					$parsedParams['Lot Number'] = $params['lotNumber'];
				}

				if (!empty($params['productionDate'])) {
					$parsedParams['Production Date'] = date_format($params['productionDate'], "M d, Y");
				}

				$results = $this->getWarehouseInventoryReport(self::INDEX, $params);

				$inventories = $results['qb']->getQuery()->getResult();
				$rows = array();
				foreach ($inventories as $inventory) {
					$arr = array();

					$product = $inventory->getProduct();

					$arr['supplier'] = $product->getSupplier()->getName();
					$arr['product'] = $product->getSku() . ' ' . $product->getName();
					$arr['warehouse'] = $inventory->getStorageLocation()->getWarehouse()->getName();
					$arr['barcode'] = $product->getBarcode();
					$arr['qtyOnHand'] = $inventory->getQtyOnHand();
					$arr['lotNumber'] = $inventory->getLotNumber() ? $inventory->getLotNumber() : "";
					$arr['productionDate'] = $inventory->getProductionDate() ? date_format($inventory->getProductionDate(), 'M d, Y') : "";
					$arr['product_total'] =  $results['productTotalsResult'][$product->getId()];
					if ($inventory->getLotNumber()) {
						$arr['lot_number_total'] =  $results['lotNumberTotalsResult'][$inventory->getLotNumber()];
					} else {
						$arr['lot_number_total'] = '';
					}
					$rows[] = $arr;
				}

				return array(
					'rows' => $rows,
					'parsedParams' => $parsedParams,
					'title' => 'Warehouse Inventory Report',
					'misc' => array(
						'excludeColumns' => array(),
					),
					'tableFormat' => array(
						array("header" => "SUPPLIER\n ", "width" => 0.10, "align" => "L", "number" => null),
						array("header" => "PRODUCT\n ", "width" => 0.24, "align" => "L", "number" => null),
						array("header" => "WAREHOUSE\n ", "width" => 0.10, "align" => "L", "number" => null),
						array("header" => "BARCODE\n ", "width" => 0.12, "align" => "L", "number" => null),
						array("header" => "ACTUAL STOCK\n ", "width" => 0.10, "align" => "R", "number" => 2),
						array("header" => "LOT NUMBER", "width" => 0.06, "align" => "R", "number" => null),
						array("header" => "PRODUCTION DATE", "width" => 0.08, "align" => "L", "number" => null),
						array("header" => "TOTAL COUNT\nPER ITEM", "width" => 0.10, "align" => "R", "number" => 2),
						array("header" => "TOTAL COUNT\nPER LOT NUMBER", "width" => 0.10, "align" => "R", "number" => 2),
					)
				);
		}
	}
}