<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\CoreBundle\Entity\User;

class DashboardModel
{
	private $container;
	private $em;
	private $securityContext;
	
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
		$this->securityContext = $container->get('security.context');
		$this->em = $container->get('doctrine')->getManager();
	}
	
	public function getIndex()
	{
		$records = array();
		if ($this->securityContext->isGranted('ROLE_SALES_ACTIVITY')) {
			$records['salesActivities'] = $this->getSalesActivies();
		}
		
		if ($this->securityContext->isGranted('ROLE_LIST_SALES_AGENT')) {
			$records['salesAgent'] = $this->getSalesAgent();
		}
			
		if ($this->securityContext->isGranted('ROLE_LIST_SHIPPING')) {
			$records['shippingAttention'] = $this->getAllShippingWithAttention();
		}
		
		return array(
// 			'salesOrders' => $this->getSalesOrders(),
// 			'productions' => $this->getProductions()
			'salesAgentActivities' => $this->getSalesActiviesResult()
		) + $records;
	}

	private function getSalesActiviesResult()
	{
		$user = $this->securityContext->getToken()->getUser();
		return $this->em->getRepository('CIInventoryBundle:SalesActivity')->firstSevenDaysLatestResult($user->getId());
	}

	private function getSalesActivies()
	{
		$user = $this->securityContext->getToken()->getUser();
		return $this->em->getRepository('CIInventoryBundle:SalesActivity')->firstSevenDaysLatest($user->getId());
	}
	
	private function getAllShippingWithAttention()
	{
		return $this->em->getRepository('CIInventoryBundle:Shipping')->findAllAttention();
	}
	
	private function getSalesAgent()
	{
		return $this->em->getRepository('CICoreBundle:User')->getInactive();
	}
	
	private function getSalesOrders()
	{
		return $this->em->getRepository('CIInventoryBundle:SalesOrder')->getDashboard();
	}
	
	private function getProductions()
	{
		return $this->em->getRepository('CIInventoryBundle:Production')->getDashboard();
	}
}