<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ProductCode
 *
 * @ORM\Table(name="product_code")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"product", "customer"}, errorPath="customer", message="Duplicate customer is not allowed.")
 */
class ProductCode extends BaseEntity
{
	/**
	 * @ORM\Column(name="code", type="string", length=255, nullable=true)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $code;
	
	/**
	 * @ORM\Column(name="alias", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $alias;

	/**
	 * @ORM\ManyToOne(targetEntity="Customer", inversedBy="codes")
	 * @ASsert\NotBlank(message="Customer must not be blank.")
	 */
	private $customer;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="codes")
	 */
	private $product;
	
    /**
     * Constructor
     */
    public function __construct()
    {
    	//empty construct, just for self-fulfillment purposes
    }

    /**
     * Set code
     *
     * @param string $code
     * @return ProductCode
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return ProductCode
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set customer
     *
     * @param \CI\InventoryBundle\Entity\Customer $customer
     * @return ProductCode
     */
    public function setCustomer(\CI\InventoryBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;
    
        return $this;
    }

    /**
     * Get customer
     *
     * @return \CI\InventoryBundle\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set product
     *
     * @param \CI\InventoryBundle\Entity\Product $product
     * @return ProductCode
     */
    public function setProduct(\CI\InventoryBundle\Entity\Product $product = null)
    {
        $this->product = $product;
    
        return $this;
    }

    /**
     * Get product
     *
     * @return \CI\InventoryBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
    
    //for future modifications
    public function isDeletable()
    {
    	return true;
    }
    
    public function getLog()
    {
    	return array(
    		'Customer' => $this->getCustomer()->getName(),
    		'Code' => $this->getCode(),
    		'Alias' => $this->getAlias(),
    	);
    }
}