<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

use CI\InventoryBundle\DependencyInjection\InventoryServiceItemInterface;

/**
 * ShippingItem
 *
 * @ORM\Table(name="shipping_item")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\ShippingItemRepository")
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"validate"})
 */
class ShippingItem implements InventoryServiceItemInterface
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @var boolean
	 */
	private $isIncluded = true;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="quantity", type="decimal", scale=2, precision=11)
	 */
	private $quantity;

	private $previousQuantity = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="picked_quantity", type="decimal", scale=2, precision=11)
	 */
	private $pickedQuantity = 0;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="remarks", type="string", length=255, nullable=true)
	 */
	private $remarks;

	/**
	 * @ORM\ManyToOne(targetEntity="Shipping", inversedBy="items")
	 */
	private $shipping;
	
	/**
	 * @ORM\ManyToOne(targetEntity="SalesOrderItem", inversedBy="shippingItems")
	 */
	private $salesOrderItem;
	
	/**
	 * @ORM\OneToMany(targetEntity="SalesReturnItem", mappedBy="shippingItem", cascade={"persist"})
	 */
	private $salesReturnItems;

	/**
	 * @ORM\OneToMany(targetEntity="OrderPickingItem", mappedBy="shippingItem", cascade={"persist"})
	 */
	private $orderPickingItems;

	/**
	 * @ORM\OneToMany(targetEntity="BackloadItem", mappedBy="shippingItem", cascade={"persist"})
	 */
	private $backloadItems;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->quantity = 0;
		$this->salesReturnItems = new \Doctrine\Common\Collections\ArrayCollection();
		$this->orderPickingItems = new \Doctrine\Common\Collections\ArrayCollection();
		$this->backloadItems = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
	/**
	 * Set isIncluded
	 *
	 * @param boolean $isIncluded
	 * @return ReceiveOrderItem
	 */
	public function setIsIncluded($isIncluded)
	{
		$this->isIncluded = $isIncluded;
	
		return $this;
	}
	
	/**
	 * Get isIncluded
	 *
	 * @return boolean
	 */
	public function getIsIncluded()
	{
		return $this->isIncluded;
	}
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * Set quantity
	 *
	 * @param float $quantity
	 * @return ShippingItem
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	
		return $this;
	}

	/**
	 * Get quantity
	 *
	 * @return float 
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}
	
	/**
	 * Set remarks
	 *
	 * @param float $remarks
	 * @return ShippingItem
	 */
	public function setRemarks($remarks)
	{
		$this->remarks = $remarks;
	
		return $this;
	}
	
	/**
	 * Get remarks
	 *
	 * @return float
	 */
	public function getRemarks()
	{
		return $this->remarks;
	}
	
	/**
	 * Set shipping
	 *
	 * @param \CI\InventoryBundle\Entity\Shipping $shipping
	 * @return ShippingItem
	 */
	public function setShipping(\CI\InventoryBundle\Entity\Shipping $shipping = null)
	{
		$this->shipping = $shipping;
	
		return $this;
	}
	
	/**
	 * Get shipping
	 *
	 * @return \CI\InventoryBundle\Entity\Shipping
	 */
	public function getShipping()
	{
		return $this->shipping;
	}
	
	/**
	 * Set salesOrderItem
	 *
	 * @param \CI\InventoryBundle\Entity\SalesOrderItem $salesOrderItem
	 * @return ShippingItem
	 */
	public function setSalesOrderItem(\CI\InventoryBundle\Entity\SalesOrderItem $salesOrderItem = null)
	{
		$this->salesOrderItem = $salesOrderItem;
	
		return $this;
	}
	
	/**
	 * Get salesOrderItem
	 *
	 * @return \CI\InventoryBundle\Entity\SalesOrderItem
	 */
	public function getSalesOrderItem()
	{
		return $this->salesOrderItem;
	}
	
	public function getSalesReturnItems()
	{
		return $this->salesReturnItems;
	}
	
	/**
	 * Set previousQuantity
	 *
	 * @param float $previousQuantity
	 * @return ReceiveOrderItem
	 */
	public function setPreviousQuantity($previousQuantity)
	{
		$this->previousQuantity = $previousQuantity;
	
		return $this;
	}
	
	/**
	 * Get previousQuantity
	 *
	 * @return float
	 */
	public function getPreviousQuantity()
	{
		return $this->previousQuantity;
	}

	/**
	 * Set pickedQuantity
	 *
	 * @param string $pickedQuantity
	 * @return ShippingItem
	 */
	public function setPickedQuantity($pickedQuantity)
	{
		$this->pickedQuantity = $pickedQuantity;
	
		return $this;
	}

	/**
	 * Get pickedQuantity
	 *
	 * @return string 
	 */
	public function getPickedQuantity()
	{
		return $this->pickedQuantity;
	}

	/**
	 * Get orderPickingItems
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getOrderPickingItems()
	{
		return $this->orderPickingItems;
	}

	/**
	 * Get backloadItems
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getBackloadItems()
	{
		return $this->backloadItems;
	}
	
	public function validate(ExecutionContextInterface $context)
	{
		if ($this->getIsIncluded() == true) {
			if (is_null($this->getQuantity())) {
				$context->addViolationAt('quantity', 'Quantity must not be blank.');
			}
	
			if (!is_numeric($this->getQuantity())) {
				$context->addViolationAt('quantity', 'Quantity is not a valid number.');
			} else {
				if ($this->getQuantity() < 0.01) {
					$context->addViolationAt('quantity', 'Minimum value is 0.01.');
				}
	
				if ($this->getQuantity() > 9999999.99) {
					$context->addViolationAt('quantity', 'Maximum value is 9999999.99.');
				}
	
				if ($this->getQuantity() > $this->getSalesOrderItem()->getMaxQtyAllowedToDeliver($this)) {
					$context->addViolationAt('quantity', 'Limit reached.');
				}
			}
		}
	}
	
	public function getTotalReturned(SalesReturnItem $excluded = null)
	{
		$totalReturned = 0;
		 
		foreach ($this->getSalesReturnItems() as $item) {
			if ($excluded && $excluded->getId() == $item->getId()) {
				continue;
			}
	
			if (SalesReturn::STATUS_RETURNED == $item->getSalesReturn()->getStatus()) {
				$totalReturned += $item->getQuantity();
			}
		}
	
		return $totalReturned;
	}

	public function getTotalPicked($excludeOrderPickingItem = null)
	{
		$totalPicked = 0;
		$prevId = null;
	
		foreach ($this->getOrderPickingItems() as $item) {
			if ($excludeOrderPickingItem && $excludeOrderPickingItem->getId() == $item->getId()) {
				continue;
			}

			if ($item->getIsPicked() === true && $prevId !== $item->getId()) {
				$prevId = $item->getId();
				$totalPicked += $item->getQuantity();
			}
		}
	
		return $totalPicked;
	}

	public function getTotalBackloaded($excludeBackloadItem = null)
	{
		$totalBackloaded = 0;
	
		foreach ($this->getBackloadItems() as $item) {
			if ($excludeBackloadItem && $excludeBackloadItem->getId() == $item->getId()) {
				continue;
			}

			if ($item->getBackload()->isActive() == 1) {
				$totalBackloaded += $item->getQuantity();
			}
		}
	
		return $totalBackloaded;
	}

	public function getMaxQtyAllowedToPick(OrderPickingItem $opItem = null)
	{
		return $this->getQuantity() - $this->getTotalPicked($opItem);
	}

	public function getMaxQtyAllowedToBackload(BackloadItem $backloadItem = null)
	{
		return $this->getQuantity() - $this->getTotalBackloaded($backloadItem);
	}
	
	public function getLog()
	{
		return array(
			'Product' => $this->getSalesOrderItem()->getQuotation()->getProduct()->getName(),
			'Quantity' => $this->getQuantity() . ' kg',
			'Packaging' => $this->getSalesOrderItem()->getQuotation()->getProduct()->getPackaging()->getName(),
			'Remarks' => $this->getRemarks(),
		);
	}
	
	public function getISProduct()
	{
		return $this->getSalesOrderItem()->getQuotation()->getProduct();
	}
	
	public function getISQuantity()
	{
		return $this->getQuantity();
	}

	public function getProductName()
	{
		return $this->getSalesOrderItem()->getQuotation()->getProduct()->getDisplayName();
	}
}