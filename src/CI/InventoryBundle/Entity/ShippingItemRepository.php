<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use CI\InventoryBundle\Entity\Shipping;

/**
 * ShippingItemRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ShippingItemRepository extends EntityRepository
{
	public function getDeliveryScheduleReport($params)
	{
		$qb = $this->createQueryBuilder('si')
		->select(
			'PARTIAL si.{id}', 's.deliveryDate', 's.id AS shippingId', 's.drId AS drRefCode', 's.siRefCode', 'c.name AS customerName',
			"CONCAT(CONCAT(p.sku, ' '), p.name) AS productName", 'si.quantity', 'pkg.kgsUnit', 'pkg.name AS kgsName', 'soi.unitPrice'
		)
		->join('si.shipping', 's')
		->join('s.salesOrder', 'so')
		->join('so.customer', 'c')
		->join('si.salesOrderItem', 'soi')
		->join('soi.quotation', 'q')
		->join('q.product', 'p')
		->join('p.packaging', 'pkg')
		->where('s.status = :pending')
		->andWhere('s.deliveryDate BETWEEN :from AND :to')
		->setParameter('pending', Shipping::STATUS_PENDING)
		->setParameter('from', $params['dateFrom'])
		->setParameter('to', $params['dateTo'])
		->groupBy('si.id')
		->orderBy('s.deliveryDate', 'DESC')
		->addOrderBy('s.drId', 'DESC')
		->addOrderBy('s.siRefCode', 'DESC')
		->addOrderBy('c.name', 'ASC')
		->addOrderBy('p.name', 'ASC')
		->addOrderBy('si.id', 'DESC')
		;
		
		if (!empty($params['customer'])) {
			$qb->andWhere('c.id = :customer')
			->setParameter('customer', $params['customer']->getId());
		}
		
		if (!empty($params['product'])) {
			$qb->andWhere('p.id = :product')
			->setParameter('product', $params['product']->getId());
		}
		
		return $qb->getQuery();
	}

	public function getItemsQb($shippingId)
	{
		return $this->createQueryBuilder('si')
			->select('si')
			->join('si.shipping', 's', 'WITH', 's.id = :shippingId')
			->setParameter('shippingId', $shippingId)
		;
	}

	public function findProductByItemId($shippingItemId)
	{
		$qb = $this->createQueryBuilder('si')
			->select('si.id', 'p.id')
			->join('si.salesOrderItem', 'soi')
			->join('soi.quotation', 'q')
			->join('q.product', 'p')
			->where('si.id = :shippingItemId')
			->setParameter('shippingItemId', $shippingItemId)
		;

		return $qb->getQuery()->getOneOrNullResult();
	}

	public function findShipping($shippingItemId)
	{
		$qb = $this->createQueryBuilder('si')
			->select('si.id', 's.id as shippingId')
			->join('si.shipping', 's')
			->where('si.id = :shippingItemId')
			->setParameter('shippingItemId', $shippingItemId)
		;

		return $qb->getQuery()->getScalarResult();
	}
}