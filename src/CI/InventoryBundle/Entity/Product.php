<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use CI\InventoryBundle\Validator\Constraints as CIAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\Criteria;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\ProductRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="sku", message="This SKU is already in use.")
 */
class Product extends BaseEntity
{
	/**
	 * @ORM\Column(name="sku", type="string", length=255)
	 * @Assert\NotBlank(message="Sku must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $sku;

	/**
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Assert\NotBlank(message="Name must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $name;

	/**
	 * @ORM\Column(name="description", type="text")
	 * @Assert\NotBlank(message="Description must not be blank.")
	 * @Assert\Type(type="string")
	 */
	private $description;

	/**
	 * @ORM\Column(name="barcode", type="string", length=255, nullable=true)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $barcode;

	/**
	 * @ORM\Column(name="storage_temp", type="string", length=255, nullable=true)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $storageTemp;

	/**
	 * @ORM\Column(name="shelf_life", type="string", nullable=true)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $shelfLife;

	/**
	 * @ORM\Column(name="other_product_info", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $otherProductInfo;

	/**
	 * @ORM\Column(name="is_halal", type="boolean")
	 */
	private $isHalal;

	/**
	 * @ORM\Column(name="supplier_product_name", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 * @Assert\Length(min=1, max=255)
	 */
	private $supplierProductName;

	/**
	 * @ORM\Column(name="supplier_product_code", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 * @Assert\Length(min=1, max=255)
	 */
	private $supplierProductCode;

	/**
	 * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
	 * @Assert\NotBlank(message="Category must not be blank.")
	 */
	private $category;

	/**
	 * @ORM\ManyToOne(targetEntity="Industry", inversedBy="products")
	 * @ORM\JoinColumn(name="industry_id", referencedColumnName="id", nullable=false)
	 * @Assert\NotBlank(message="Industry must not be blank.")
	 */
	private $industry;

	/**
	 * @ORM\ManyToOne(targetEntity="Supplier", inversedBy="products")
	 * @Assert\NotBlank(message="Supplier must not be blank.")
	 */
	private $supplier;

	/**
	 * @ORM\OneToMany(targetEntity="ProductCode", mappedBy="product", cascade={"persist", "remove"})
	 * @ORM\OrderBy({"alias"="ASC"})
	 * @Assert\Valid
	 * @Assert\All(constraints={
	 *     @CIAssert\UniqueInCollection(propertyPath="customer", message="Duplicate customer is not allowed.")
	 * })
	 */
	private $codes;

	/**
	 * @ORM\OneToMany(targetEntity="Quotation", mappedBy="product", cascade={"persist", "remove"})
	 */
	private $quotations;

	/**
	 * @ORM\OneToMany(targetEntity="PurchaseOrderItem", mappedBy="product", cascade={"persist", "remove"})
	 */
	private $purchaseOrderItems;

	/**
	 * @ORM\OneToMany(targetEntity="SalesForecast", mappedBy="product", cascade={"persist"})
	 */
	private $salesForecasts;

	/**
	 * @ORM\OneToMany(targetEntity="ProductFile", mappedBy="product", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;

	/**
	 * @ORM\ManyToOne(targetEntity="Packaging", inversedBy="products")
	 * @ORM\JoinColumn(name="packaging_id", referencedColumnName="id", nullable=false)
	 * @Assert\NotBlank(message="Packaging must not be blank.")
	 */
	private $packaging;

	/**
	 * @ORM\OneToMany(targetEntity="WarehouseInventory", mappedBy="product", cascade={"persist", "remove"})
	 */
	private $warehouseInventories;

	/**
	 * @ORM\OneToMany(targetEntity="Inventory", mappedBy="product", cascade={"persist", "remove"})
	 */
	private $inventories;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="total_allocated", type="decimal", scale=2, precision=13)
	 */
	private $totalAllocated;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="total_staged", type="decimal", scale=2, precision=13)
	 */
	private $totalStaged;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="total_onhand", type="decimal", scale=2, precision=13)
	 */
	private $totalOnHand;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="reorder_threshold", type="decimal", scale=2, precision=13)
	 */
	private $reorderThreshold;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="reorder_quantity", type="decimal", scale=2, precision=13)
	 */
	private $reorderQuantity;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_tracked", type="boolean")
	 */
	private $isTracked;

	/**
	 * @ORM\Column(name="designated_warehouse_number", type="string", length=255, nullable=true)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $designatedWarehouseNumber;

	private $qtyChange;
	private $memo;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->qtyChange = 0;
		$this->transType = null;
		$this->codes = new \Doctrine\Common\Collections\ArrayCollection();
		$this->quotations = new \Doctrine\Common\Collections\ArrayCollection();
		$this->purchaseOrderItems = new \Doctrine\Common\Collections\ArrayCollection();
		$this->inventoryLog = new \Doctrine\Common\Collections\ArrayCollection();
		$this->salesOrderItems = new \Doctrine\Common\Collections\ArrayCollection();
		$this->files = new \Doctrine\Common\Collections\ArrayCollection();
		$this->warehouseInventories = new \Doctrine\Common\Collections\ArrayCollection();
		$this->inventories = new \Doctrine\Common\Collections\ArrayCollection();
		$this->isHalal = false;
		$this->salesForecasts = new \Doctrine\Common\Collections\ArrayCollection();
		$this->totalAllocated = 0;
		$this->totalStaged = 0;
		$this->totalOnHand = 0;
		$this->reorderThreshold = 0;
		$this->reorderQuantity = 0;
		$this->isTracked = true;
	}

	/**
	 * Set memo
	 *
	 * @param string $memo
	 * @return Product
	 */
	public function setMemo($memo)
	{
		$this->memo = $memo;

		return $this;
	}

	/**
	 * Get memo
	 *
	 * @return string
	 */
	public function getMemo()
	{
		return $this->memo;
	}

	/**
	 * Set sku
	 *
	 * @param string $sku
	 * @return Product
	 */
	public function setSku($sku)
	{
		$this->sku = $sku;

		return $this;
	}

	/**
	 * Get sku
	 *
	 * @return string
	 */
	public function getSku()
	{
		return $this->sku;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Product
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Product
	 */
	public function setDescription($description)
	{
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set barcode
	 *
	 * @param string $barcode
	 * @return Product
	 */
	public function setBarcode($barcode)
	{
		$this->barcode = $barcode;

		return $this;
	}

	/**
	 * Get barcode
	 *
	 * @return string
	 */
	public function getBarcode()
	{
		return $this->barcode;
	}

	/**
	 * Set category
	 *
	 * @param \CI\InventoryBundle\Entity\Category $category
	 * @return Product
	 */
	public function setCategory(\CI\InventoryBundle\Entity\Category $category = null)
	{
		$this->category = $category;

		return $this;
	}

	/**
	 * Get category
	 *
	 * @return \CI\InventoryBundle\Entity\Category
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * Set supplier
	 *
	 * @param \CI\InventoryBundle\Entity\Supplier $supplier
	 * @return Product
	 */
	public function setSupplier(\CI\InventoryBundle\Entity\Supplier $supplier = null)
	{
		$this->supplier = $supplier;

		return $this;
	}

	/**
	 * Get supplier
	 *
	 * @return \CI\InventoryBundle\Entity\Supplier
	 */
	public function getSupplier()
	{
		return $this->supplier;
	}

	/**
	 * Add codes
	 *
	 * @param \CI\InventoryBundle\Entity\ProductCode $codes
	 * @return Product
	 */
	public function addCode(\CI\InventoryBundle\Entity\ProductCode $codes)
	{
		$codes->setProduct($this);
		$this->codes[] = $codes;

		return $this;
	}

	/**
	 * Remove codes
	 *
	 * @param \CI\InventoryBundle\Entity\ProductCode $codes
	 */
	public function removeCode(\CI\InventoryBundle\Entity\ProductCode $codes)
	{
		$this->codes->removeElement($codes);
	}

	/**
	 * Get codes
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getCodes()
	{
		return $this->codes;
	}


	/**
	 * Set qtyChange
	 *
	 * @param integer $qtyChange
	 * @return Product
	 */
	public function setQtyChange($qtyChange)
	{
		$this->qtyChange += $qtyChange;

		return $this;
	}

	/**
	 * Get qtyChange
	 *
	 * @return integer
	 */
	public function getQtyChange()
	{
		return $this->qtyChange;
	}

	/**
	 * Add quotations
	 *
	 * @param \CI\InventoryBundle\Entity\Quotation $quotations
	 * @return ProductCode
	 */
	public function addQuotation(\CI\InventoryBundle\Entity\Quotation $quotations)
	{
		$this->quotations[] = $quotations;

		return $this;
	}

	/**
	 * Remove quotations
	 *
	 * @param \CI\InventoryBundle\Entity\Quotation $quotations
	 */
	public function removeQuotation(\CI\InventoryBundle\Entity\Quotation $quotations)
	{
		$this->quotations->removeElement($quotations);
	}

	/**
	 * Get quotations
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getQuotations()
	{
		return $this->quotations;
	}

	/**
	 * Add purchaseOrderItems
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrderItem $purchaseOrderItems
	 * @return Product
	 */
	public function addPurchaseOrderItem(\CI\InventoryBundle\Entity\PurchaseOrderItem $purchaseOrderItems)
	{
		$this->purchaseOrderItems[] = $purchaseOrderItems;

		return $this;
	}

	/**
	 * Remove purchaseOrderItems
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrderItem $purchaseOrderItems
	 */
	public function removePurchaseOrderItem(\CI\InventoryBundle\Entity\PurchaseOrderItem $purchaseOrderItems)
	{
		$this->purchaseOrderItems->removeElement($purchaseOrderItems);
	}

	/**
	 * Get purchaseOrderItems
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getPurchaseOrderItems()
	{
		return $this->purchaseOrderItems;
	}

	/**
	 * Add files
	 *
	 * @param \CI\InventoryBundle\Entity\ProductFile $files
	 * @return Product
	 */
	public function addFile(\CI\InventoryBundle\Entity\ProductFile $files)
	{
		$files->setProduct($this);
		$this->files[] = $files;

		return $this;
	}

	/**
	 * Remove files
	 *
	 * @param \CI\InventoryBundle\Entity\ProductFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\ProductFile $files)
	{
		$this->files->removeElement($files);
	}

	/**
	 * Get files
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * Set storageTemp
	 *
	 * @param string $storageTemp
	 * @return Product
	 */
	public function setStorageTemp($storageTemp)
	{
		$this->storageTemp = $storageTemp;

		return $this;
	}

	/**
	 * Get storageTemp
	 *
	 * @return string
	 */
	public function getStorageTemp()
	{
		return $this->storageTemp;
	}

	/**
	 * Set shelfLife
	 *
	 * @param string $shelfLife
	 * @return Product
	 */
	public function setShelfLife($shelfLife)
	{
		$this->shelfLife = $shelfLife;

		return $this;
	}

	/**
	 * Get shelfLife
	 *
	 * @return string
	 */
	public function getShelfLife()
	{
		return $this->shelfLife;
	}

	/**
	 * Set otherProductInfo
	 *
	 * @param string $otherProductInfo
	 * @return Product
	 */
	public function setOtherProductInfo($otherProductInfo)
	{
		$this->otherProductInfo = $otherProductInfo;

		return $this;
	}

	/**
	 * Get otherProductInfo
	 *
	 * @return string
	 */
	public function getOtherProductInfo()
	{
		return $this->otherProductInfo;
	}

	/**
	 * Set isHalal
	 *
	 * @param boolean $isHalal
	 * @return Product
	 */
	public function setIsHalal($isHalal)
	{
		$this->isHalal = $isHalal;

		return $this;
	}

	/**
	 * Get isHalal
	 *
	 * @return boolean
	 */
	public function getIsHalal()
	{
		return $this->isHalal;
	}

	/**
	 * Set supplierProductName
	 *
	 * @param string $supplierProductName
	 * @return Product
	 */
	public function setSupplierProductName($supplierProductName)
	{
		$this->supplierProductName = $supplierProductName;

		return $this;
	}

	/**
	 * Get supplierProductName
	 *
	 * @return string
	 */
	public function getSupplierProductName()
	{
		return $this->supplierProductName;
	}

	/**
	 * Set supplierProductCode
	 *
	 * @param string $supplierProductCode
	 * @return Product
	 */
	public function setSupplierProductCode($supplierProductCode)
	{
		$this->supplierProductCode = $supplierProductCode;

		return $this;
	}

	/**
	 * Get supplierProductCode
	 *
	 * @return string
	 */
	public function getSupplierProductCode()
	{
		return $this->supplierProductCode;
	}


	/**
	 * Set packaging
	 *
	 * @param \CI\InventoryBundle\Entity\Packaging $packaging
	 * @return Product
	 */
	public function setPackaging(\CI\InventoryBundle\Entity\Packaging $packaging = null)
	{
		$this->packaging = $packaging;

		return $this;
	}

	/**
	 * Get packaging
	 *
	 * @return \CI\InventoryBundle\Entity\Packaging
	 */
	public function getPackaging()
	{
		return $this->packaging;
	}

	/**
	 * Add warehouseInventories
	 *
	 * @param \CI\InventoryBundle\Entity\WarehouseInventory $warehouseInventories
	 *
	 * @return Product
	 */
	public function addWarehouseInventory(\CI\InventoryBundle\Entity\WarehouseInventory $warehouseInventories)
	{
		$warehouseInventories->setProduct($this);
		$this->warehouseInventories[] = $warehouseInventories;

		return $this;
	}

	/**
	 * Remove warehouseInventories
	 *
	 * @param \CI\InventoryBundle\Entity\WarehouseInventory $warehouseInventories
	 */
	public function removeWarehouseInventory(\CI\InventoryBundle\Entity\WarehouseInventory $warehouseInventories)
	{
		$this->warehouseInventories->removeElement($warehouseInventories);
	}

	/**
	 * Get warehouseInventories
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getWarehouseInventories()
	{
		return $this->warehouseInventories;
	}

	/**
	 * Add inventory
	 *
	 * @param \CI\InventoryBundle\Entity\Inventory $inventory
	 *
	 * @return Product
	 */
	public function addInventory(\CI\InventoryBundle\Entity\Inventory $inventory)
	{
		$inventory->setProduct($this);
		$this->inventories[] = $inventory;

		return $this;
	}

	/**
	 * Remove inventory
	 *
	 * @param \CI\InventoryBundle\Entity\Inventory $inventory
	 */
	public function removeInventory(\CI\InventoryBundle\Entity\Inventory $inventory)
	{
		$this->inventories->removeElement($inventory);
	}

	/**
	 * Get inventories
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getInventories()
	{
		return $this->inventories;
	}

	/**
	 * Set totalAllocated
	 *
	 * @param float $totalAllocated
	 * @return Product
	 */
	public function setTotalAllocated($totalAllocated)
	{
		$this->totalAllocated = $totalAllocated;

		return $this;
	}

	/**
	 * Get totalAllocated
	 *
	 * @return float
	 */
	public function getTotalAllocated()
	{
		return $this->totalAllocated;
	}

	/**
	 * Set totalStaged
	 *
	 * @param float $totalStaged
	 * @return Product
	 */
	public function setTotalStaged($totalStaged)
	{
		$this->totalStaged = $totalStaged;

		return $this;
	}

	/**
	 * Get totalStaged
	 *
	 * @return float
	 */
	public function getTotalStaged()
	{
		return $this->totalStaged;
	}

	/**
	 * Set totalOnHand
	 *
	 * @param float $totalOnHand
	 * @return Product
	 */
	public function setTotalOnHand($totalOnHand)
	{
		$this->totalOnHand = $totalOnHand;

		return $this;
	}

	/**
	 * Get totalOnHand
	 *
	 * @return float
	 */
	public function getTotalOnHand()
	{
		return $this->totalOnHand;
	}

	/**
	 * Set reorderThreshold
	 *
	 * @param float $reorderThreshold
	 * @return Product
	 */
	public function setReorderThreshold($reorderThreshold)
	{
		$this->reorderThreshold = $reorderThreshold;

		return $this;
	}

	/**
	 * Get reorderThreshold
	 *
	 * @return float
	 */
	public function getReorderThreshold()
	{
		return $this->reorderThreshold;
	}

	/**
	 * Set reorderQuantity
	 *
	 * @param float $reorderQuantity
	 * @return Product
	 */
	public function setReorderQuantity($reorderQuantity)
	{
		$this->reorderQuantity = $reorderQuantity;

		return $this;
	}

	/**
	 * Get reorderQuantity
	 *
	 * @return float
	 */
	public function getReorderQuantity()
	{
		return $this->reorderQuantity;
	}

	/**
	 * Set isTracked
	 *
	 * @param boolean $isTracked
	 * @return Product
	 */
	public function setIsTracked($isTracked)
	{
		$this->isTracked = $isTracked;

		return $this;
	}

	/**
	 * Get isTracked
	 *
	 * @return boolean
	 */
	public function getIsTracked()
	{
		return $this->isTracked;
	}

	/**
	 * Set designatedWarehouseNumber
	 *
	 * @param string $designatedWarehouseNumber
	 * @return Product
	 */
	public function setDesignatedWarehouseNumber($designatedWarehouseNumber)
	{
		$this->designatedWarehouseNumber = $designatedWarehouseNumber;

		return $this;
	}

	/**
	 * Get designatedWarehouseNumber
	 *
	 * @return string
	 */
	public function getDesignatedWarehouseNumber()
	{
		return $this->designatedWarehouseNumber;
	}

	/**
	 * Set industry
	 *
	 * @param \CI\InventoryBundle\Entity\Industry $industry
	 * @return Product
	 */
	public function setIndustry(\CI\InventoryBundle\Entity\Industry $industry = null)
	{
		$this->industry = $industry;

		return $this;
	}

	/**
	 * Get industry
	 *
	 * @return \CI\InventoryBundle\Entity\Industry
	 */
	public function getIndustry()
	{
		return $this->industry;
	}

	/**
	 * Add salesForecasts
	 *
	 * @param \CI\InventoryBundle\Entity\SalesForecast $salesForecasts
	 * @return Product
	 */
	public function addSalesForecast(\CI\InventoryBundle\Entity\SalesForecast $salesForecasts)
	{
		$this->salesForecasts[] = $salesForecasts;

		return $this;
	}

	/**
	 * Remove salesForecasts
	 *
	 * @param \CI\InventoryBundle\Entity\SalesForecast $salesForecasts
	 */
	public function removeSalesForecast(\CI\InventoryBundle\Entity\SalesForecast $salesForecasts)
	{
		$this->salesForecasts->removeElement($salesForecasts);
	}

	/**
	 * Get salesForecasts
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getSalesForecasts()
	{
		return $this->salesForecasts;
	}

	/**
	 * Get qtyOnHand
	 *
	 * @return string
	 */
	public function updateTotalOnHand()
	{
		$qtyOnHand = '0.00';
		foreach ($this->getInventories() as $inventory) {
			$qtyOnHand = bcadd($qtyOnHand, $inventory->getQtyOnHand(), 2);
		}

		$this->setTotalOnHand($qtyOnHand);
	}

	/**
	 * Get qtyAllocated
	 *
	 * @return string
	 */
	public function updateTotalAllocated()
	{
		$qtyAllocated = '0.00';
		foreach ($this->getWarehouseInventories() as $warehouseInventory) {
			$qtyAllocated = bcadd($qtyAllocated, $warehouseInventory->getQtyAllocated(), 2);
		}

		$this->setTotalAllocated($qtyAllocated);
	}

	/**
	 * Get qtyStaged
	 *
	 * @return string
	 */
	public function updateTotalStaged()
	{
		$qtyStaged = '0.00';
		foreach ($this->getWarehouseInventories() as $warehouseInventory) {
			$qtyStaged = bcadd($qtyStaged, $warehouseInventory->getQtyStaged(), 2);
		}

		$this->setTotalStaged($qtyStaged);
	}


	public function isDeletable()
	{
		if (count($this->getPurchaseOrderItems()) > 0) {
			return false;
		}
		return true;
	}

	public function getDisplayName()
	{
		$productName = '';
		if ($this->getSupplierProductName() && $this->getSupplierProductCode()) {
			$productName = $this->getSupplierProductCode() . ' ' . $this->getSupplierProductName();
		} else {
			$productName = $this->getSku() . ' ' . $this->getName();
		}

		return $productName;
	}

	public function getCustomerProductName($customer)
	{
		$codes = $this->getCodes();
		$criteria = Criteria::create()->where(Criteria::expr()->eq('customer', $customer))->setMaxResults(1);

		return $codes->matching($criteria);
	}

	public function getQtyOnHandPackage()
	{
		return $this->getTotalOnHand() / $this->getPackaging()->getKgsUnit();
	}

	public function getQtyAllocatedPackage()
	{
		return $this->getTotalAllocated() / $this->getPackaging()->getKgsUnit();
	}

	public function getQtyStagedPackage()
	{
		return $this->getTotalStaged() / $this->getPackaging()->getKgsUnit();
	}

    public function getLog()
    {
    	return array(
    		'Sku' => $this->getSku(),
    		'Name' => $this->getName(),
    		'Description' => $this->getDescription(),
    		'Barcode' => $this->getBarcode(),
    		'Category' => $this->getCategory()->getName(),
    		'Supplier' => $this->getSupplier()->getShortName(),
    		'Supplier Product Name' => $this->getSupplierProductName(),
    		'Supplier Product Code' => $this->getSupplierProductCode(),
    		'Packaging' => $this->getPackaging()->getName(),
    		'Shelf Life' => $this->getShelfLife(),
    		'Storage Temperature' => $this->getStorageTemp(),
    		'Other Product Info' => $this->getOtherProductInfo(),
    		'Quantity On Hand' => $this->getTotalOnHand(),
    		'Quantity Allocated' => $this->getTotalAllocated(),
    		'Halal' => $this->getIsHalal() ? 'Yes' : 'No',
    		'Reorder Threshold' => $this->getReorderThreshold(),
    		'Reorder Quantity' => $this->getReorderQuantity(),
    		'Active' => $this->getActive() ? 'Yes' : 'No',
    		'Lot Tracked' => $this->getIsTracked() ? 'Yes' : 'No',
    		'Designated Warehouse Number' => $this->getDesignatedWarehouseNumber(),
    		'Total Staged (kg)' => $this->getTotalStaged(),
    		'Total Staged (Package)' => $this->getQtyStagedPackage(),
    		'Industry' => $this->getIndustry() ? $this->getIndustry()->getName() : null
    	);
    }
}