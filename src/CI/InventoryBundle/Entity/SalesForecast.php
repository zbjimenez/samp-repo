<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * SalesForecast
 *
 * @ORM\Table(name="sales_forecast")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\SalesForecastRepository")
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"process"})
 * @UniqueEntity(fields={"year", "customer", "product"}, errorPath="year", message="Sales forecast for this year already exists.")
 */
class SalesForecast extends BaseEntity
{
	/**
	 * @var string
	 *
	 * @ORM\Column(name="year", type="string", length=255)
	 * @Assert\NotBlank(message="Please enter a year.")
	 */
	private $year;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="jan_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the January quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $janQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="jan_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the January amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $janAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="feb_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the February quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $febQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="feb_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the February amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $febAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="mar_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the March quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $marQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="mar_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the March amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $marAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="apr_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the April quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $aprQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="apr_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the April amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $aprAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="may_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the May quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $mayQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="may_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the May amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $mayAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="jun_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the June quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $junQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="jun_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the June amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $junAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="jul_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the July quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $julQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="jul_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the July amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $julAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="aug_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the August quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $augQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="aug_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the August amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $augAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="sep_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the September quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $sepQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="sep_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the September amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $sepAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="oct_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the October quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $octQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="oct_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the October amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $octAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="nov_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the November quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $novQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="nov_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the November amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $novAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="dec_qty", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter the December quantity.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $decQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="dec_amt", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Please enter the December amount.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $decAmt;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="total_qty", type="decimal", scale=2, precision=13)
	 */
	private $totalQty;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="total_amt", type="decimal", scale=2, precision=15)
	 */
	private $totalAmt;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Customer", inversedBy="salesForecasts")
	 * @Assert\NotBlank(message="Please enter a customer.")
	 */
	private $customer;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="salesForecasts")
	 * @Assert\NotBlank(message="Please enter a product.")
	 */
	private $product;
	
	/**
	 * @ORM\OneToMany(targetEntity="SalesForecastFile", mappedBy="salesForecast", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->totalQty = 0;
		$this->totalAmt = 0;
		$this->files = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set year
	 *
	 * @param string $year
	 * @return SalesForecast
	 */
	public function setYear($year)
	{
		$this->year = $year;
	
		return $this;
	}

	/**
	 * Get year
	 *
	 * @return string 
	 */
	public function getYear()
	{
		return $this->year;
	}

	/**
	 * Set janQty
	 *
	 * @param string $janQty
	 * @return SalesForecast
	 */
	public function setJanQty($janQty)
	{
		$this->janQty = $janQty;
	
		return $this;
	}

	/**
	 * Get janQty
	 *
	 * @return string 
	 */
	public function getJanQty()
	{
		return $this->janQty;
	}

	/**
	 * Set janAmt
	 *
	 * @param string $janAmt
	 * @return SalesForecast
	 */
	public function setJanAmt($janAmt)
	{
		$this->janAmt = $janAmt;
	
		return $this;
	}

	/**
	 * Get janAmt
	 *
	 * @return string 
	 */
	public function getJanAmt()
	{
		return $this->janAmt;
	}

	/**
	 * Set febQty
	 *
	 * @param string $febQty
	 * @return SalesForecast
	 */
	public function setFebQty($febQty)
	{
		$this->febQty = $febQty;
	
		return $this;
	}

	/**
	 * Get febQty
	 *
	 * @return string 
	 */
	public function getFebQty()
	{
		return $this->febQty;
	}

	/**
	 * Set febAmt
	 *
	 * @param string $febAmt
	 * @return SalesForecast
	 */
	public function setFebAmt($febAmt)
	{
		$this->febAmt = $febAmt;
	
		return $this;
	}

	/**
	 * Get febAmt
	 *
	 * @return string 
	 */
	public function getFebAmt()
	{
		return $this->febAmt;
	}

	/**
	 * Set marQty
	 *
	 * @param string $marQty
	 * @return SalesForecast
	 */
	public function setMarQty($marQty)
	{
		$this->marQty = $marQty;
	
		return $this;
	}

	/**
	 * Get marQty
	 *
	 * @return string 
	 */
	public function getMarQty()
	{
		return $this->marQty;
	}

	/**
	 * Set marAmt
	 *
	 * @param string $marAmt
	 * @return SalesForecast
	 */
	public function setMarAmt($marAmt)
	{
		$this->marAmt = $marAmt;
	
		return $this;
	}

	/**
	 * Get marAmt
	 *
	 * @return string 
	 */
	public function getMarAmt()
	{
		return $this->marAmt;
	}

	/**
	 * Set aprQty
	 *
	 * @param string $aprQty
	 * @return SalesForecast
	 */
	public function setAprQty($aprQty)
	{
		$this->aprQty = $aprQty;
	
		return $this;
	}

	/**
	 * Get aprQty
	 *
	 * @return string 
	 */
	public function getAprQty()
	{
		return $this->aprQty;
	}

	/**
	 * Set aprAmt
	 *
	 * @param string $aprAmt
	 * @return SalesForecast
	 */
	public function setAprAmt($aprAmt)
	{
		$this->aprAmt = $aprAmt;
	
		return $this;
	}

	/**
	 * Get aprAmt
	 *
	 * @return string 
	 */
	public function getAprAmt()
	{
		return $this->aprAmt;
	}

	/**
	 * Set mayQty
	 *
	 * @param string $mayQty
	 * @return SalesForecast
	 */
	public function setMayQty($mayQty)
	{
		$this->mayQty = $mayQty;
	
		return $this;
	}

	/**
	 * Get mayQty
	 *
	 * @return string 
	 */
	public function getMayQty()
	{
		return $this->mayQty;
	}

	/**
	 * Set mayAmt
	 *
	 * @param string $mayAmt
	 * @return SalesForecast
	 */
	public function setMayAmt($mayAmt)
	{
		$this->mayAmt = $mayAmt;
	
		return $this;
	}

	/**
	 * Get mayAmt
	 *
	 * @return string 
	 */
	public function getMayAmt()
	{
		return $this->mayAmt;
	}

	/**
	 * Set junQty
	 *
	 * @param string $junQty
	 * @return SalesForecast
	 */
	public function setJunQty($junQty)
	{
		$this->junQty = $junQty;
	
		return $this;
	}

	/**
	 * Get junQty
	 *
	 * @return string 
	 */
	public function getJunQty()
	{
		return $this->junQty;
	}

	/**
	 * Set junAmt
	 *
	 * @param string $junAmt
	 * @return SalesForecast
	 */
	public function setJunAmt($junAmt)
	{
		$this->junAmt = $junAmt;
	
		return $this;
	}

	/**
	 * Get junAmt
	 *
	 * @return string 
	 */
	public function getJunAmt()
	{
		return $this->junAmt;
	}

	/**
	 * Set julQty
	 *
	 * @param string $julQty
	 * @return SalesForecast
	 */
	public function setJulQty($julQty)
	{
		$this->julQty = $julQty;
	
		return $this;
	}

	/**
	 * Get julQty
	 *
	 * @return string 
	 */
	public function getJulQty()
	{
		return $this->julQty;
	}

	/**
	 * Set julAmt
	 *
	 * @param string $julAmt
	 * @return SalesForecast
	 */
	public function setJulAmt($julAmt)
	{
		$this->julAmt = $julAmt;
	
		return $this;
	}

	/**
	 * Get julAmt
	 *
	 * @return string 
	 */
	public function getJulAmt()
	{
		return $this->julAmt;
	}

	/**
	 * Set augQty
	 *
	 * @param string $augQty
	 * @return SalesForecast
	 */
	public function setAugQty($augQty)
	{
		$this->augQty = $augQty;
	
		return $this;
	}

	/**
	 * Get augQty
	 *
	 * @return string 
	 */
	public function getAugQty()
	{
		return $this->augQty;
	}

	/**
	 * Set augAmt
	 *
	 * @param string $augAmt
	 * @return SalesForecast
	 */
	public function setAugAmt($augAmt)
	{
		$this->augAmt = $augAmt;
	
		return $this;
	}

	/**
	 * Get augAmt
	 *
	 * @return string 
	 */
	public function getAugAmt()
	{
		return $this->augAmt;
	}

	/**
	 * Set sepQty
	 *
	 * @param string $sepQty
	 * @return SalesForecast
	 */
	public function setSepQty($sepQty)
	{
		$this->sepQty = $sepQty;
	
		return $this;
	}

	/**
	 * Get sepQty
	 *
	 * @return string 
	 */
	public function getSepQty()
	{
		return $this->sepQty;
	}

	/**
	 * Set sepAmt
	 *
	 * @param string $sepAmt
	 * @return SalesForecast
	 */
	public function setSepAmt($sepAmt)
	{
		$this->sepAmt = $sepAmt;
	
		return $this;
	}

	/**
	 * Get sepAmt
	 *
	 * @return string 
	 */
	public function getSepAmt()
	{
		return $this->sepAmt;
	}

	/**
	 * Set octQty
	 *
	 * @param string $octQty
	 * @return SalesForecast
	 */
	public function setOctQty($octQty)
	{
		$this->octQty = $octQty;
	
		return $this;
	}

	/**
	 * Get octQty
	 *
	 * @return string 
	 */
	public function getOctQty()
	{
		return $this->octQty;
	}

	/**
	 * Set octAmt
	 *
	 * @param string $octAmt
	 * @return SalesForecast
	 */
	public function setOctAmt($octAmt)
	{
		$this->octAmt = $octAmt;
	
		return $this;
	}

	/**
	 * Get octAmt
	 *
	 * @return string 
	 */
	public function getOctAmt()
	{
		return $this->octAmt;
	}

	/**
	 * Set novQty
	 *
	 * @param string $novQty
	 * @return SalesForecast
	 */
	public function setNovQty($novQty)
	{
		$this->novQty = $novQty;
	
		return $this;
	}

	/**
	 * Get novQty
	 *
	 * @return string 
	 */
	public function getNovQty()
	{
		return $this->novQty;
	}

	/**
	 * Set novAmt
	 *
	 * @param string $novAmt
	 * @return SalesForecast
	 */
	public function setNovAmt($novAmt)
	{
		$this->novAmt = $novAmt;
	
		return $this;
	}

	/**
	 * Get novAmt
	 *
	 * @return string 
	 */
	public function getNovAmt()
	{
		return $this->novAmt;
	}

	/**
	 * Set decQty
	 *
	 * @param string $decQty
	 * @return SalesForecast
	 */
	public function setDecQty($decQty)
	{
		$this->decQty = $decQty;
	
		return $this;
	}

	/**
	 * Get decQty
	 *
	 * @return string 
	 */
	public function getDecQty()
	{
		return $this->decQty;
	}

	/**
	 * Set decAmt
	 *
	 * @param string $decAmt
	 * @return SalesForecast
	 */
	public function setDecAmt($decAmt)
	{
		$this->decAmt = $decAmt;
	
		return $this;
	}

	/**
	 * Get decAmt
	 *
	 * @return string 
	 */
	public function getDecAmt()
	{
		return $this->decAmt;
	}

	/**
	 * Set totalQty
	 *
	 * @param string $totalQty
	 * @return SalesForecast
	 */
	public function setTotalQty($totalQty)
	{
		$this->totalQty = $totalQty;
	
		return $this;
	}

	/**
	 * Get totalQty
	 *
	 * @return string 
	 */
	public function getTotalQty()
	{
		return $this->totalQty;
	}

	/**
	 * Set totalAmt
	 *
	 * @param string $totalAmt
	 * @return SalesForecast
	 */
	public function setTotalAmt($totalAmt)
	{
		$this->totalAmt = $totalAmt;
	
		return $this;
	}

	/**
	 * Get totalAmt
	 *
	 * @return string 
	 */
	public function getTotalAmt()
	{
		return $this->totalAmt;
	}

	/**
	 * Set customer
	 *
	 * @param \CI\InventoryBundle\Entity\Customer $customer
	 * @return SalesForecast
	 */
	public function setCustomer(\CI\InventoryBundle\Entity\Customer $customer = null)
	{
		$this->customer = $customer;
	
		return $this;
	}

	/**
	 * Get customer
	 *
	 * @return \CI\InventoryBundle\Entity\Customer 
	 */
	public function getCustomer()
	{
		return $this->customer;
	}

	/**
	 * Set product
	 *
	 * @param \CI\InventoryBundle\Entity\Product $product
	 * @return SalesForecast
	 */
	public function setProduct(\CI\InventoryBundle\Entity\Product $product = null)
	{
		$this->product = $product;
	
		return $this;
	}

	/**
	 * Get product
	 *
	 * @return \CI\InventoryBundle\Entity\Product 
	 */
	public function getProduct()
	{
		return $this->product;
	}

	/**
	 * Add files
	 *
	 * @param \CI\InventoryBundle\Entity\SalesForecastFile $files
	 * @return SalesForecast
	 */
	public function addFile(\CI\InventoryBundle\Entity\SalesForecastFile $files)
	{
		$files->setSalesForecast($this);
		$this->files[] = $files;
	
		return $this;
	}
	
	/**
	 * Remove files
	 *
	 * @param \CI\InventoryBundle\Entity\SalesForecastFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\SalesForecastFile $files)
	{
		$this->files->removeElement($files);
	}
	
	/**
	 * Get files
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}
	
	public static function getMonths()
	{
		return array(
			'January' => 'January',
			'February' => 'February',
			'March' => 'March',
			'April' => 'April',
			'May' => 'May',
			'June' => 'June',
			'July' => 'July',
			'August' => 'August',
			'September' => 'September',
			'October' => 'October',
			'November' => 'November',
			'December' => 'December',
		);
	}
	
	public function getMonthValue($month)
	{
		switch ($month) {
			case 'January': return array('qty' => $this->getJanQty(), 'amt' => $this->getJanAmt());
			case 'February': return array('qty' => $this->getFebQty(), 'amt' => $this->getFebAmt());
			case 'March': return array('qty' => $this->getMarQty(), 'amt' => $this->getMarAmt());
			case 'April': return array('qty' => $this->getAprQty(), 'amt' => $this->getAprAmt());
			case 'May': return array('qty' => $this->getMayQty(), 'amt' => $this->getMayAmt());
			case 'June': return array('qty' => $this->getJunQty(), 'amt' => $this->getJunAmt());
			case 'July': return array('qty' => $this->getJulQty(), 'amt' => $this->getJulAmt());
			case 'August': return array('qty' => $this->getAugQty(), 'amt' => $this->getAugAmt());
			case 'September': return array('qty' => $this->getSepQty(), 'amt' => $this->getSepAmt());
			case 'October': return array('qty' => $this->getOctQty(), 'amt' => $this->getOctAmt());
			case 'November': return array('qty' => $this->getNovQty(), 'amt' => $this->getNovAmt());
			case 'December': return array('qty' => $this->getDecQty(), 'amt' => $this->getDecAmt());
			default: throw new \Exception('Invalid month.');
		}
	}
	
	public function process(ExecutionContextInterface $context)
	{
		$this->setTotalQty(
			$this->getJanQty() + $this->getFebQty() + $this->getMarQty() + $this->getAprQty() + $this->getMayQty() + $this->getJunQty() +
			$this->getJulQty() + $this->getAugQty() + $this->getSepQty() + $this->getOctQty() + $this->getNovQty() + $this->getDecQty()
		);
		
		$this->setTotalAmt(
			$this->getJanAmt() + $this->getFebAmt() + $this->getMarAmt() + $this->getAprAmt() + $this->getMayAmt() + $this->getJunAmt() +
			$this->getJulAmt() + $this->getAugAmt() + $this->getSepAmt() + $this->getOctAmt() + $this->getNovAmt() + $this->getDecAmt()
		);
	}
	
	public function getLog()
	{
		return array(
			'Customer' => $this->getCustomer()->getShortName(),
			'Product' => $this->getProduct()->getDisplayName(),
			'Year' => $this->getYear(),
			'January Qty' => number_format($this->getJanQty(), 2),
			'January Amt' => number_format($this->getJanAmt(), 2),
			'February Qty' => number_format($this->getFebQty(), 2),
			'February Amt' => number_format($this->getFebAmt(), 2),
			'March Qty' => number_format($this->getMarQty(), 2),
			'March Amt' => number_format($this->getMarAmt(), 2),
			'April Qty' => number_format($this->getAprQty(), 2),
			'April Amt' => number_format($this->getAprAmt(), 2),
			'May Qty' => number_format($this->getMayQty(), 2),
			'May Amt' => number_format($this->getMayAmt(), 2),
			'June Qty' => number_format($this->getJunQty(), 2),
			'June Amt' => number_format($this->getJunAmt(), 2),
			'July Qty' => number_format($this->getJulQty(), 2),
			'July Amt' => number_format($this->getJulAmt(), 2),
			'August Qty' => number_format($this->getAugQty(), 2),
			'August Amt' => number_format($this->getAugAmt(), 2),
			'September Qty' => number_format($this->getSepQty(), 2),
			'September Amt' => number_format($this->getSepAmt(), 2),
			'October Qty' => number_format($this->getOctQty(), 2),
			'October Amt' => number_format($this->getOctAmt(), 2),
			'November Qty' => number_format($this->getNovQty(), 2),
			'November Amt' => number_format($this->getNovAmt(), 2),
			'December Qty' => number_format($this->getDecQty(), 2),
			'December Amt' => number_format($this->getDecAmt(), 2),
			'Total Qty' => number_format($this->getTotalQty(), 2),
			'Total Amt' => number_format($this->getTotalAmt(), 2),
		);
	}
}