<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Criteria;

/**
 * Branch
 *
 * @ORM\Table(name="branch")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\BranchRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Branch extends BaseEntity
{
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_main", type="boolean")
	 */
	private $isMain;
	
	/**
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Assert\NotBlank(message="Name must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $name;
	
	/**
	 * @ORM\Column(name="address", type="text")
	 * @Assert\NotBlank(message="Address must not be blank.")
	 * @Assert\Type(type="string")
	 */
	private $address;
	
	/**
	 * @ORM\Column(name="area", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $area;
	
	/**
	 * @ORM\Column(name="contact_details", type="string", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $contactDetails;

	/**
	 * @ORM\ManyToOne(targetEntity="Customer", inversedBy="branches")
	 */
	private $customer;
	
	/**
	 * @ORM\OneToMany(targetEntity="Shipping", mappedBy="branch", cascade={"persist", "remove"})
	 */
	private $shippings;
	
    /**
     * Constructor
     */
    public function __construct()
    {
    	$this->isMain = false;
    	$this->shippings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Branch
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set area
     *
     * @param string $area
     * @return Branch
     */
    public function setArea($area)
    {
    	$this->area = $area;
    
    	return $this;
    }
    
    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
    	return $this->area;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Branch
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set contactDetails
     *
     * @param string $contactDetails
     * @return Branch
     */
    public function setContactDetails($contactDetails)
    {
        $this->contactDetails = $contactDetails;
    
        return $this;
    }

    /**
     * Get contactDetails
     *
     * @return string 
     */
    public function getContactDetails()
    {
        return $this->contactDetails;
    }

    /**
     * Set customer
     *
     * @param \CI\InventoryBundle\Entity\Customer $customer
     * @return Branch
     */
    public function setCustomer(\CI\InventoryBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;
    
        return $this;
    }

    /**
     * Get customer
     *
     * @return \CI\InventoryBundle\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }
    
    /**
     * Set isMain
     *
     * @param boolean $isMain
     * @return Branch
     */
    public function setIsMain($isMain)
    {
    	$this->isMain = $isMain;
    
    	return $this;
    }
    
    /**
     * Get isMain
     *
     * @return boolean
     */
    public function getIsMain()
    {
    	return $this->isMain;
    }
    
    /**
     * Add shippings
     *
     * @param \CI\InventoryBundle\Entity\Shipping $shippings
     * @return Branch
     */
    public function addShipping(\CI\InventoryBundle\Entity\Shipping $shippings)
    {
    	$this->shippings[] = $shippings;
    
    	return $this;
    }
    
    /**
     * Remove shippings
     *
     * @param \CI\InventoryBundle\Entity\Shipping $shippings
     */
    public function removeShipping(\CI\InventoryBundle\Entity\Shipping $shippings)
    {
    	$this->shippings->removeElement($shippings);
    }
    
    /**
     * Get shippings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShippings()
    {
    	return $this->shippings;
    }
    
    public function isDeletable()
    {
    	if ($this->getIsMain() || count($this->getCustomer()->getBranches()) == 1) {
    		return false;
    	}
    	return true;
    }
    
    public function validate()
    {
    	$count = count($this->getCustomer()->getMain());
    	
    	if ($count > 1) {
    		throw new \Exception('Only one branch can be selected as main.');
    	}
    }
    
    public function getLog()
    {
    	return array(
    		'Main' => $this->getIsMain() ? 'Yes' : 'No', 
    		'Name' => $this->getName(),
    		'Address' => $this->getAddress(),
    		'Contact Details' => $this->getContactDetails(),
    		'Area' => $this->getArea()
    	);
    }
}