<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

use CI\InventoryBundle\DependencyInjection\InventoryServiceItemInterface;

/**
 * PurchaseReturnItem
 *
 * @ORM\Table(name="purchase_return_item")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"process"})
 */
class PurchaseReturnItem implements InventoryServiceItemInterface
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="quantity", type="decimal", scale=2, precision=9)
	 * @Assert\NotBlank(message="Please enter a quantity for this item.")
	 * @Assert\Range(min=0.01, max=9999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $quantity;
	
	/**
	 * @ORM\ManyToOne(targetEntity="PurchaseReturn", inversedBy="items")
	 */
	private $purchaseReturn;
	
	/**
	 * @ORM\ManyToOne(targetEntity="ReceiveOrderItem", inversedBy="purchaseReturnItems")
	 */
	private $receiveOrderItem;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="remarks", type="text", nullable=true)
	 * @Assert\Type("string")
	 */
	private $remarks;

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set quantity
	 *
	 * @param string $quantity
	 * @return PurchaseReturnItem
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	
		return $this;
	}

	/**
	 * Get quantity
	 *
	 * @return string 
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * Set purchaseReturn
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseReturn $purchaseReturn
	 * @return PurchaseReturnItem
	 */
	public function setPurchaseReturn(\CI\InventoryBundle\Entity\PurchaseReturn $purchaseReturn = null)
	{
		$this->purchaseReturn = $purchaseReturn;
	
		return $this;
	}

	/**
	 * Get purchaseReturn
	 *
	 * @return \CI\InventoryBundle\Entity\PurchaseReturn 
	 */
	public function getPurchaseReturn()
	{
		return $this->purchaseReturn;
	}

	/**
	 * Set receiveOrderItem
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrderItem $receiveOrderItem
	 * @return PurchaseReturnItem
	 */
	public function setReceiveOrderItem(\CI\InventoryBundle\Entity\ReceiveOrderItem $receiveOrderItem = null)
	{
		$this->receiveOrderItem = $receiveOrderItem;
	
		return $this;
	}

	/**
	 * Get receiveOrderItem
	 *
	 * @return \CI\InventoryBundle\Entity\ReceiveOrderItem 
	 */
	public function getReceiveOrderItem()
	{
		return $this->receiveOrderItem;
	}

	/**
	 * Set remarks
	 *
	 * @param string $remarks
	 * @return PurchaseReturn
	 */
	public function setRemarks($remarks)
	{
		$this->remarks = $remarks;
	
		return $this;
	}
	
	/**
	 * Get remarks
	 *
	 * @return string 
	 */
	public function getRemarks()
	{
		return $this->remarks;
	}
	
	public function process(ExecutionContextInterface $context)
	{
		$this->getPurchaseReturn()->setTotal($this->getPurchaseReturn()->getTotal() + round($this->getQuantity() * $this->getReceiveOrderItem()->getPurchaseOrderItem()->getUnitCost(), 2));
		
		if ($this->getQuantity() > $this->getReceiveOrderItem()->getQuantity() - $this->getReceiveOrderItem()->getTotalReturned($this)) {
			$context->addViolationAt('quantity', 'Limit reached');
		}
	}
	
	public function getLog()
	{
		return array(
			'Product' => $this->getISProduct()->getSku() . ' ' . $this->getISProduct()->getName(),
			'Lot Number' => $this->getReceiveOrderItem()->getLotNumber(),
			'Quantity' => number_format($this->getQuantity(), 2) . ' kg',
		);
	}

	public function getPalletId()
	{
		return $this->getReceiveOrderItem()->getPalletId();
	}

	public function getLotNumber()
	{
		return $this->getReceiveOrderItem()->getLotNumber();
	}
	
	public function getISProduct()
	{
		return $this->getReceiveOrderItem()->getProduct();
	}
	
	public function getISQuantity()
	{
		return $this->getQuantity();
	}
}