<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * SalesReturnFile
 *
 * @ORM\Table(name="sales_return_file")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SalesReturnFile
{
	/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="SalesReturn", inversedBy="files")
	 */
	private $salesReturn;
	
	/**
	 * @ORM\Column(name="file", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 * @Assert\Length(max=255)
	 */
	private $file;
	
	/**
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;
	
	/**
	 * @Assert\File(maxSize="10M")
	 */
	private $file_actual;
	
	private $prevId;
	
	public function getAbsoluteFilePath()
	{
		return null === $this->file ? null : $this->getUploadRootDir() . '/' . $this->file . '.' . $this->getExtension();
	}
	
	public function getWebFilePath()
	{
		return null === $this->file ? null : $this->getUploadDir() . '/' . $this->file . '.' . $this->getExtension();
	}
	
	protected function getUploadRootDir()
	{
		return __DIR__.'/../../../../web' . $this->getUploadDir();
	}
	
	protected function getUploadDir()
	{
		$id = $this->prevId;
		if ($this->getSalesReturn()) {
			$id = $this->getSalesReturn()->getId();
		}
		
		return '/uploads/salesReturn/' . $id;
	}
	
	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * Set salesReturn
	 *
	 * @param \CI\InventoryBundle\Entity\SalesReturn $salesReturn
	 * @return SalesReturnFile
	 */
	public function setSalesReturn(\CI\InventoryBundle\Entity\SalesReturn $salesReturn = null)
	{
		$this->salesReturn = $salesReturn;
	
		return $this;
	}
	
	/**
	 * Get salesReturn
	 *
	 * @return \CI\InventoryBundle\Entity\SalesReturn
	 */
	public function getSalesReturn()
	{
		return $this->salesReturn;
	}

	/**
	 * Set file
	 *
	 * @param string $file
	 * @return salesReturnFile
	 */
	public function setFile($file)
	{
		$this->file = $file;
	
		return $this;
	}

	/**
	 * Get file
	 *
	 * @return string 
	 */
	public function getFileActual()
	{
		return $this->file_actual;
	}
	
	/**
	 * Set file
	 *
	 * @param string $file
	 * @return salesReturnFile
	 */
	public function setFileActual($file)
	{
		$this->file_actual = $file;
	
		return $this;
	}
	
	/**
	 * Get file
	 *
	 * @return string
	 */
	public function getFile()
	{
		return $this->file;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return salesReturnFile
	 */
	public function setName($name)
	{
		$this->name = $name;
	
		return $this;
	}
	
	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Set prevId
	 *
	 * @param string $prevId
	 * @return salesReturnFile
	 */
	public function setPrevId($prevId)
	{
		$this->prevId = $prevId;
	
		return $this;
	}
	
	/**
	 * Get prevId
	 *
	 * @return integer
	 */
	public function getPrevId()
	{
		return $this->prevId;
	}
	
	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->file_actual) {
			$name = uniqid($this->getSalesReturn()->getId(), true);
			$this->name = $this->file_actual->getClientOriginalName();
			$this->file = $name;
		}
	}
	
	public function getExtension()
	{
		$ext = explode('.', $this->getName());
		$ext = $ext[count($ext)-1];
		 
		return $ext;
	}
	
	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function upload()
	{
		// the file property can be empty if the field is not required
		if (!is_null($this->file_actual)) {
			$this->file_actual->move($this->getUploadRootDir(), $this->file . '.' . $this->getExtension());
			$this->file_actual = null;
		}
	}
	
	/**
	 * @ORM\PreRemove()
	 */
	public function removeUpload()
	{
		if ($file = $this->getAbsoluteFilePath()) {
			unlink($file);
		}
	}
	       
    public function getLog()
    {
        return array(
           'File' => $this->getName()
        );
   }
}