<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

use CI\InventoryBundle\Entity\SalesActivity;

/**
 * Sales Activity Item
 *
 * @ORM\Table(name="sales_activity_item")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\SalesActivityItemRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 */
class SalesActivityItem extends BaseEntity
{
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="in_office", type="boolean")
	 */
	private $inOffice;
	
	/**
	 * @var timeIn
	 *
	 * @ORM\Column(name="start_time", type="time", nullable=true)
	 * @Assert\Time()
	 */
	private $startTime;
	
	/**
	 * @var timeOut
	 *
	 * @ORM\Column(name="end_time", type="time", nullable=true)
	 * @Assert\Time()
	 */
	private $endTime;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="contact", type="string", length=100, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $contact;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $description;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="comments", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $comments;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Customer", inversedBy="salesActivityItems")
	 */
	private $customer;
	
	/**
	 * @ORM\ManyToOne(targetEntity="SalesActivity", inversedBy="items")
	 */
	private $salesActivity;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->inOffice = false;
	}
	
	/**
	 * Set inOffice
	 *
	 * @param boolean $inOffice
	 * @return SalesActivityItem
	 */
	public function setInOffice($inOffice)
	{
		$this->inOffice = $inOffice;
	
		return $this;
	}
	
	/**
	 * Get inOffice
	 *
	 * @return boolean
	 */
	public function getInOffice()
	{
		return $this->inOffice;
	}

	/**
	 * Set startTime
	 *
	 * @param \DateTime $startTime
	 * @return SalesActivityItem
	 */
	public function setStartTime($startTime)
	{
		$this->startTime = $startTime;
	
		return $this;
	}
	
	/**
	 * Get startTime
	 *
	 * @return \DateTime
	 */
	public function getStartTime()
	{
		return $this->startTime;
	}
	
	/**
	 * Set endTime
	 *
	 * @param \DateTime $endTime
	 * @return SalesActivityItem
	 */
	public function setEndTime($endTime)
	{
		$this->endTime = $endTime;
	
		return $this;
	}
	
	/**
	 * Get endTime
	 *
	 * @return \DateTime
	 */
	public function getEndTime()
	{
		return $this->endTime;
	}

    /**
     * Set contact
     *
     * @param string $contact
     * @return SalesActivityItem
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return SalesActivityItem
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return SalesActivityItem
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    
        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set customer
     *
     * @param \CI\InventoryBundle\Entity\Customer $customer
     * @return SalesActivityItem
     */
    public function setCustomer(\CI\InventoryBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;
    
        return $this;
    }

    /**
     * Get customer
     *
     * @return \CI\InventoryBundle\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set salesActivity
     *
     * @param \CI\InventoryBundle\Entity\SalesActivity $salesActivity
     * @return SalesActivityItem
     */
    public function setSalesActivity(\CI\InventoryBundle\Entity\SalesActivity $salesActivity = null)
    {
        $this->salesActivity = $salesActivity;
    
        return $this;
    }

    /**
     * Get salesActivity
     *
     * @return \CI\InventoryBundle\Entity\SalesActivity 
     */
    public function getSalesActivity()
    {
        return $this->salesActivity;
    }
    
	public function getLog()
	{
		$log = array(
			'In Office?' => $this->getInOffice() ? 'Yes' : 'No',
			'Start Time' => $this->getStartTime(),
			'End Time' => $this->getEndTime(),
			'Contact Person' => $this->getContact(),
			'Description' => $this->getDescription()
		);

		if ($this->getCustomer()) {
			$log['Customer'] = $this->getCustomer()->getName();
		}
		
		if ($this->getComments()) {
			$log['Comments'] = $this->getComments();
		}
		
		return $log;
	}
	
	public function process(ExecutionContextInterface $context)
	{
		if (in_array($this->getSalesActivity()->getDayType(), array(SalesActivity::WORK_DAY))) {
			if (is_null($this->getStartTime())) {
				$context->addViolationAt('startTime', 'Start Time should not be blank.');
			}
			
			if (is_null($this->getEndTime())) {
				$context->addViolationAt('endTime', 'End Time should not be blank.');
			}
			
			if ($this->getInOffice()) {
				$this->setCustomer(null);
			} else {
				if (is_null($this->getCustomer())) {
					$context->addViolationAt('customer', 'Customer should not be blank.');
				}
			}
			
			if (is_null($this->getContact())) {
				$context->addViolationAt('contact', 'Contact should not be blank.');
			}
			
			if (is_null($this->getDescription())) {
				$context->addViolationAt('description', 'Description should not be blank.');
			}
		}
	}
}
