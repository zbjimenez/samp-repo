<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Inventory
 *
 * @ORM\Table(name="inventory_log")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\InventoryLogRepository")
 */
class InventoryLog
{
    const TRANS_ADJUST = 10;
    const TRANS_RECEIVE = 20;
    const TRANS_PUTAWAY = 25;
    const TRANS_DELIVER = 30;
    const TRANS_PURCHASE_RETURN = 40;
    const TRANS_SALES_RETURN = 50;
    const TRANS_TRANSFER = 60;
    
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="trans_type", type="integer")
	 * @Assert\Type(type="int")
	 * @Assert\Choice(choices = {"10", "20", "30", "40", "50"})
	 */
	private $transType;
	
	/**
     * @ORM\ManyToOne(targetEntity="Inventory", inversedBy="inventoryLogs")
     */
    private $inventory;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="quantity", type="decimal", scale=2, precision=9)
	 * @Assert\Type(type="numeric", message="The value {{ value }} is not a valid number.")
	 * @Assert\Range(min=0, max=99999999999.999)
	 */
	private $quantity;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="balance", type="decimal", scale=2, precision=9)
	 * @Assert\Type(type="numeric", message="The value {{ value }} is not a valid number.")
	 * @Assert\Range(min=0, max=99999999999.999)
	 */
	private $balance;
	
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 * @Gedmo\Timestampable(on="create")
	 */
	private $createdAt;
	
	/*
	 * @ORM\ManyToOne(targetEntity="CI\CoreBundle\Entity\User", inversedBy="inventoryLog")
	 */
	private $createdBy;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="memo", type="string", length=100, nullable=true)
	 * @Assert\Type(type="string")
	 * @Assert\Length(max=100)
	 */
	private $memo;	
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     * @return InventoryLog
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return float 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
    
    /**
     * Set balance
     *
     * @param float $balance
     * @return InventoryLog
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    
        return $this;
    }

    /**
     * Get balance
     *
     * @return float 
     */
    public function getBalance()
    {
        return $this->balance;
    }
    
    /**
     * Set transId
     *
     * @param integer $transId
     * @return InventoryLog
     */
    public function setTransId($id)
    {
        $this->transId = $id;
    
        return $this;
    }
    
    /**
     * Get transId
     *
     * @return integer
     */
    public function getTransId()
    {
        return $this->transId;
    }

    /**
     * Set transType
     *
     * @param integer $transType
     * @return InventoryLog
     */
    public function setTransType($type)
    {
        $this->transType = $type;
    
        return $this;
    }

    /**
     * Get transType
     *
     * @return integer 
     */
    public function getTransType()
    {
        return $this->transType;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return InventoryLog
     */
    public function setCreatedAt($timeStamp)
    {
        $this->createdAt = $timeStamp;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * Set createdBy
     *
     * @param \User $user
     * @return InventoryLog
     */
    public function setCreatedBy($user)
    {
        $this->createdBy = $user;
    
        return $this;
    }
    
    /**
     * Get createdBy
     *
     * @return \User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set memo
     *
     * @param string $memo
     * @return InventoryLog
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
    
        return $this;
    }

    /**
     * Get memo
     *
     * @return string 
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Set inventory
     *
     * @param \CI\InventoryBundle\Entity\Inventory $inventory
     * @return InventoryLog
     */
    public function setInventory(\CI\InventoryBundle\Entity\Inventory $inventory)
    {
        $this->inventory = $inventory;
    
        return $this;
    }

    /**
     * Get inventory
     *
     * @return \CI\InventoryBundle\Entity\Inventory 
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * Get product
     *
     * @return \CI\InventoryBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->inventory->getProduct();
    }
    
    public function getTransTypeLabel()
    {
        switch ($this->getTransType()) {
            case self::TRANS_ADJUST: return array('type' => 'Manual Adjustment', 'class' => 'primary');
            case self::TRANS_RECEIVE: return array('type' => 'Receive', 'class' => 'primary');
            case self::TRANS_PUTAWAY: return array('type' => 'Put Away', 'class' => 'primary');
            case self::TRANS_DELIVER: return array('type' => 'Deliver/Order Picking', 'class' => 'primary');
            case self::TRANS_PURCHASE_RETURN: return array('type' => 'Shipment Discrepancy Notice', 'class' => 'primary');
            case self::TRANS_SALES_RETURN: return array('type' => 'Sales Return', 'class' => 'primary');
            case self::TRANS_TRANSFER: return array('type' => 'Transfer', 'class' => 'primary');
            default:
                return array('type' => 'Invalid', 'class' => 'default');
        }
    }
}