<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

use CI\InventoryBundle\DependencyInjection\InventoryServiceEntityInterface;

/**
 * PurchaseReturn
 *
 * @ORM\Table(name="purchase_return")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\PurchaseReturnRepository")
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"process"})
 */
class PurchaseReturn extends BaseEntity implements InventoryServiceEntityInterface
{
	const STATUS_DRAFT = 10;
	const STATUS_RETURNED = 23;
	const STATUS_VOID = 30;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="status", type="integer", length=2)
	 * @Assert\Choice(choices = {"10", "23", "30"})
	 * @Assert\Type(type="int")
	 * @Assert\Range(min=10, max=30)
	 */
	private $status;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="return_date", type="date")
	 * @Assert\NotBlank(message="Please enter the return date.")
	 * @Assert\Date()
	 */
	private $returnDate;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="actionTaken", type="text", nullable=true)
	 * @Assert\Type("string")
	 */
	private $actionTaken;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="total", type="decimal", scale=2, precision=13)
	 */
	private $total;
	
	/**
	 * @ORM\OneToMany(targetEntity="PurchaseReturnItem", mappedBy="purchaseReturn", cascade={"persist", "remove"})
	 * @Assert\Count(min="1", minMessage="Please enter at least one (1) item.")
	 * @Assert\Valid
	 */
	private $items;
	
	/**
	 * @ORM\ManyToOne(targetEntity="ReceiveOrder", inversedBy="purchaseReturns")
	 * @ASsert\NotBlank(message="Please enter an RO# for this shipment discrepancy notice.")
	 */
	private $receiveOrder;

	/**
	 * @ORM\OneToMany(targetEntity="PurchaseReturnFile", mappedBy="purchaseReturn", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;

	/**
	 * @ORM\ManyToOne(targetEntity="Supplier", inversedBy="purchaseReturns")
	 * @Assert\NotBlank(message="Supplier must not be blank.")
	 */
	private $supplier;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="invoice_number", type="string", length=255)
	 * @Assert\NotBlank(message="Invoice Number must not be blank.")
	 * @Assert\Type(type="string")
	 */
	private $invoiceNumber;

	/**
	 * @var date
	 *
	 * @ORM\Column(name="date_prepared", type="date")
	 * @Assert\NotBlank(message="Please enter the date prepared.")
	 * @Assert\Date()
	 */
	private $datePrepared;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="nature_is_quantity", type="boolean")
	 */
	private $natureIsQuantity;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="nature_is_quality", type="boolean")
	 */
	private $natureIsQuality;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="nature_is_other", type="boolean")
	 */
	private $natureIsOther;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="particular", type="text", nullable=true)
	 * @Assert\Type("string")
	 */
	private $particular;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
		$this->returnDate = new \DateTime();
		$this->files = new \Doctrine\Common\Collections\ArrayCollection();
		$this->datePrepared = new \DateTime();
		$this->dateReceived = new \DateTime();
		$this->natureIsQuantity = false;
		$this->natureIsQuality = false;
		$this->natureIsOther = false;
	}
	
	/**
	 * Set status
	 *
	 * @param integer $status
	 * @return PurchaseReturn
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	
		return $this;
	}
	
	/**
	 * Get status
	 *
	 * @return integer 
	 */
	public function getStatus()
	{
		return $this->status;
	}
	
	/**
	 * Set returnDate
	 *
	 * @param \DateTime $returnDate
	 * @return PurchaseReturn
	 */
	public function setReturnDate($returnDate)
	{
		$this->returnDate = $returnDate;
	
		return $this;
	}
	
	/**
	 * Get returnDate
	 *
	 * @return \DateTime 
	 */
	public function getReturnDate()
	{
		return $this->returnDate;
	}
	
	/**
	 * Set actionTaken
	 *
	 * @param string $actionTaken
	 * @return PurchaseReturn
	 */
	public function setActionTaken($actionTaken)
	{
		$this->actionTaken = $actionTaken;
	
		return $this;
	}
	
	/**
	 * Get actionTaken
	 *
	 * @return string 
	 */
	public function getActionTaken()
	{
		return $this->actionTaken;
	}
	
	/**
	 * Set total
	 *
	 * @param string $total
	 * @return PurchaseReturn
	 */
	public function setTotal($total)
	{
		$this->total = $total;
	
		return $this;
	}
	
	/**
	 * Get total
	 *
	 * @return string 
	 */
	public function getTotal()
	{
		return $this->total;
	}
	
	/**
	 * Set items
	 *
	 * @param \Doctrine\Common\Collections\Collection $items
	 * @return PurchaseReturn
	 */
	public function setItems(\Doctrine\Common\Collections\Collection $items)
	{
		foreach ($items as $item) {
			$item->setPurchaseReturn($this);
		}
		$this->items = $items;
		
		return $this;
	}
	
	/**
	 * Get items
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getItems()
	{
		return $this->items;
	}
	
	/**
	 * Set receiveOrder
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrder $receiveOrder
	 * @return PurchaseReturn
	 */
	public function setReceiveOrder(\CI\InventoryBundle\Entity\ReceiveOrder $receiveOrder = null)
	{
		$this->receiveOrder = $receiveOrder;
	
		return $this;
	}
	
	/**
	 * Get receiveOrder
	 *
	 * @return \CI\InventoryBundle\Entity\ReceiveOrder 
	 */
	public function getReceiveOrder()
	{
		return $this->receiveOrder;
	}

	/**
	 * Add files
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseReturnFile $files
	 * @return PurchaseReturn
	 */
	public function addFile(\CI\InventoryBundle\Entity\PurchaseReturnFile $files)
	{
		$files->setPurchaseReturn($this);
		$this->files[] = $files;
	
		return $this;
	}
	
	/**
	 * Remove files
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseReturnFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\PurchaseReturnFile $files)
	{
		$this->files->removeElement($files);
	}
	
	/**
	 * Get files
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * Set supplier
	 *
	 * @param \CI\InventoryBundle\Entity\Supplier $supplier
	 * @return PurchaseReturn
	 */
	public function setSupplier(\CI\InventoryBundle\Entity\Supplier $supplier = null)
	{
		$this->supplier = $supplier;
	
		return $this;
	}

	/**
	 * Get supplier
	 *
	 * @return \CI\InventoryBundle\Entity\Supplier 
	 */
	public function getSupplier()
	{
		return $this->supplier;
	}

	/**
	 * Set invoiceNumber
	 *
	 * @param string $invoiceNumber
	 * @return PurchaseReturn
	 */
	public function setInvoiceNumber($invoiceNumber)
	{
		$this->invoiceNumber = $invoiceNumber;
	
		return $this;
	}

	/**
	 * Get invoiceNumber
	 *
	 * @return string 
	 */
	public function getInvoiceNumber()
	{
		return $this->invoiceNumber;
	}

	/**
	 * Set datePrepared
	 *
	 * @param \DateTime $datePrepared
	 * @return PurchaseReturn
	 */
	public function setDatePrepared($datePrepared)
	{
		$this->datePrepared = $datePrepared;
	
		return $this;
	}
	
	/**
	 * Get datePrepared
	 *
	 * @return \DateTime 
	 */
	public function getDatePrepared()
	{
		return $this->datePrepared;
	}

	/**
	 * Set natureIsQuantity
	 *
	 * @param boolean $natureIsQuantity
	 * @return PurchaseReturn
	 */
	public function setNatureIsQuantity($natureIsQuantity)
	{
		$this->natureIsQuantity = $natureIsQuantity;
	
		return $this;
	}
	
	/**
	 * Get natureIsQuantity
	 *
	 * @return boolean
	 */
	public function getNatureIsQuantity()
	{
		return $this->natureIsQuantity;
	}

	/**
	 * Set natureIsQuality
	 *
	 * @param boolean $natureIsQuality
	 * @return PurchaseReturn
	 */
	public function setNatureIsQuality($natureIsQuality)
	{
		$this->natureIsQuality = $natureIsQuality;
	
		return $this;
	}
	
	/**
	 * Get natureIsQuality
	 *
	 * @return boolean
	 */
	public function getNatureIsQuality()
	{
		return $this->natureIsQuality;
	}

	/**
	 * Set natureIsOther
	 *
	 * @param boolean $natureIsOther
	 * @return PurchaseReturn
	 */
	public function setNatureIsOther($natureIsOther)
	{
		$this->natureIsOther = $natureIsOther;
	
		return $this;
	}
	
	/**
	 * Get natureIsOther
	 *
	 * @return boolean
	 */
	public function getNatureIsOther()
	{
		return $this->natureIsOther;
	}

	/**
	 * Set particular
	 *
	 * @param string $particular
	 * @return PurchaseReturn
	 */
	public function setParticular($particular)
	{
		$this->particular = $particular;
	
		return $this;
	}
	
	/**
	 * Get particular
	 *
	 * @return string 
	 */
	public function getParticular()
	{
		return $this->particular;
	}
	
	public function translateStatus()
	{
		switch ($this->getStatus()) {
			case self::STATUS_DRAFT: return 'Draft';
			case self::STATUS_RETURNED: return 'Returned';
			case self::STATUS_VOID: return 'Void';
			default: return 'Error';
		}
	}
	
	public function isEditable()
	{
		if ($this->getStatus() > self::STATUS_DRAFT) {
			return false;
		}
		
		return true;
	}
	
	public function isDeletable()
	{
		if ($this->getStatus() > self::STATUS_DRAFT) {
			return false;
		}
	
		return true;
	}

	public function translateNature() {
		$arr = array();
		if ($this->getNatureIsQuantity() == true) {
			$arr[] = "Quantity";
		}
		if ($this->getNatureIsQuality() == true) {
			$arr[] = "Quality";
		}
		if ($this->getNatureIsOther() == true) {
			$arr[] = "Other";
		}

		return implode(", ", $arr);
	}
	
	public function process(ExecutionContextInterface $context)
	{
		$this->setTotal(0);
	}
	
	public function getLog()
	{
		return array(
			'PO#' => $this->getReceiveOrder()->getPurchaseOrder()->getId(),
			'RO#' => $this->getReceiveOrder()->getReferenceNumber(),
			'Status' => $this->translateStatus(),
			'Return Date' => $this->getReturnDate()->format('M d, Y'),
			'Total' => number_format($this->getTotal(), 2),
			'ActionTaken' => nl2br($this->getActionTaken()),
		);
	}
	
	public function getISItems()
	{
		return $this->getItems();
	}

	public function getWarehouse()
	{
		return $this->getReceiveOrder()->getWarehouse();
	}
}