<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use CI\InventoryBundle\DependencyInjection\InventoryServiceItemInterface;

/**
 * ReceiveOrderItem
 *
 * @ORM\Table(name="receive_order_item")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\ReceiveOrderItemRepository")
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"validate"})
 */
class ReceiveOrderItem implements InventoryServiceItemInterface
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id; 
	
	/**
	 * @var boolean
	 */
	private $isIncluded = true;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="quantity", type="decimal", scale=2, precision=9)
	 */
	private $quantity;
	
	private $previousQuantity = 0;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="lot_number", type="string", length=255)
	 * @Assert\NotBlank(message="Lot Number must not be blank.")
	 * @Assert\Length(max=255)
	 * @Assert\Type(type="string")
	 */
	private $lotNumber;
	
	/**
	 * @ORM\ManyToOne(targetEntity="ReceiveOrder", inversedBy="items")
	 */
	private $receiveOrder;
	
	/**
	 * @ORM\ManyToOne(targetEntity="PurchaseOrderItem", inversedBy="receiveOrderItems")
	 * @Assert\NotBlank(message="Product must not be blank.")
	 */
	private $purchaseOrderItem;
	
	/**
	 * @ORM\OneToMany(targetEntity="PurchaseReturnItem", mappedBy="receiveOrderItem", cascade={"persist"})
	 */
	private $purchaseReturnItems;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mode", type="string", length=255)
	 * @Assert\NotBlank(message="Mode must not be blank.")
	 * @Assert\Length(max=255)
	 * @Assert\Type(type="string")
	 */
	private $mode;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length=255)
	 * @Assert\NotBlank(message="Type must not be blank.")
	 * @Assert\Length(max=255)
	 * @Assert\Type(type="string")
	 */
	private $type;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="barcode", type="string", length=255)
	 * @Assert\NotBlank(message="Barcode must not be blank.")
	 * @Assert\Length(max=255)
	 * @Assert\Type(type="string")
	 */
	private $barcode;

	/**
	 * @ORM\OneToMany(targetEntity="PutAwayItem", mappedBy="receiveOrderItem", cascade={"persist", "remove"})
	 */
	private $putAwayItems;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="palletId", type="string", length=255)
	 * @Assert\NotBlank(message="Pallet ID must not be blank.")
	 * @Assert\Length(max=255)
	 * @Assert\Type(type="string")
	 */
	private $palletId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="productionDateFromScan", type="string", length=255)
	 * @Assert\NotBlank(message="Production Date must not be blank.")
	 * @Assert\Regex(pattern="/\d{2}\/\d{2}\/\d{4}/", message="Incorrect date format, please use correct date format MM/DD/YYYY.")
	 * @Assert\Length(max=255)
	 * @Assert\Type(type="string")
	 */
	private $productionDateFromScan;

	/**
	 * @var date
	 *
	 * @ORM\Column(name="production_date", type="date")
	 * @Assert\Date()
	 */
	private $productionDate;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->quantity = 0;
		$this->purchaseReturnItems = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * Set isIncluded
	 *
	 * @param boolean $isIncluded
	 * @return ReceiveOrderItem
	 */
	public function setIsIncluded($isIncluded)
	{
		$this->isIncluded = $isIncluded;
	
		return $this;
	}
	
	/**
	 * Get isIncluded
	 *
	 * @return boolean
	 */
	public function getIsIncluded()
	{
		return $this->isIncluded;
	}
	
	/**
	 * Set previousQuantity
	 *
	 * @param float $previousQuantity
	 * @return ReceiveOrderItem
	 */
	public function setPreviousQuantity($previousQuantity)
	{
		$this->previousQuantity = $previousQuantity;
	
		return $this;
	}
	
	/**
	 * Get previousQuantity
	 *
	 * @return float
	 */
	public function getPreviousQuantity()
	{
		return $this->previousQuantity;
	}

	/**
	 * Set quantity
	 *
	 * @param string $quantity
	 * @return ReceiveOrderItem
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	
		return $this;
	}

	/**
	 * Get quantity
	 *
	 * @return string 
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * Set lotNumber
	 *
	 * @param string $lotNumber
	 * @return ReceiveOrderItem
	 */
	public function setLotNumber($lotNumber)
	{
		$this->lotNumber = $lotNumber;
	
		return $this;
	}

	/**
	 * Get lotNumber
	 *
	 * @return string 
	 */
	public function getLotNumber()
	{
		return $this->lotNumber;
	}

	/**
	 * Set receiveOrder
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrder $receiveOrder
	 * @return ReceiveOrderItem
	 */
	public function setReceiveOrder(\CI\InventoryBundle\Entity\ReceiveOrder $receiveOrder = null)
	{
		$this->receiveOrder = $receiveOrder;
	
		return $this;
	}

	/**
	 * Get receiveOrder
	 *
	 * @return \CI\InventoryBundle\Entity\ReceiveOrder 
	 */
	public function getReceiveOrder()
	{
		return $this->receiveOrder;
	}

	/**
	 * Set purchaseOrderItem
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrderItem $purchaseOrderItem
	 * @return ReceiveOrderItem
	 */
	public function setPurchaseOrderItem(\CI\InventoryBundle\Entity\PurchaseOrderItem $purchaseOrderItem = null)
	{
		$this->purchaseOrderItem = $purchaseOrderItem;
	
		return $this;
	}

	/**
	 * Get purchaseOrderItem
	 *
	 * @return \CI\InventoryBundle\Entity\PurchaseOrderItem 
	 */
	public function getPurchaseOrderItem()
	{
		return $this->purchaseOrderItem;
	}
	
	public function getProduct()
	{
		return $this->getPurchaseOrderItem()->getProduct();
	}
	
	public function getPurchaseReturnItems()
	{
		return $this->purchaseReturnItems;
	}

	/**
	 * Set mode
	 *
	 * @param string $mode
	 * @return ReceiveOrderItem
	 */
	public function setMode($mode)
	{
		$this->mode = $mode;
	
		return $this;
	}

	/**
	 * Get mode
	 *
	 * @return string 
	 */
	public function getMode()
	{
		return $this->mode;
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 * @return ReceiveOrderItem
	 */
	public function setType($type)
	{
		$this->type = $type;
	
		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string 
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * Set barcode
	 *
	 * @param string $barcode
	 * @return ReceiveOrderItem
	 */
	public function setBarcode($barcode)
	{
		$this->barcode = $barcode;
	
		return $this;
	}

	/**
	 * Get barcode
	 *
	 * @return string 
	 */
	public function getBarcode()
	{
		return $this->barcode;
	}

	/**
	 * Set palletId
	 *
	 * @param string $palletId
	 * @return ReceiveOrderItem
	 */
	public function setPalletId($palletId)
	{
		$this->palletId = $palletId;
	
		return $this;
	}

	/**
	 * Get palletId
	 *
	 * @return string 
	 */
	public function getPalletId()
	{
		return $this->palletId;
	}

	/**
	 * Set productionDateFromScan
	 *
	 * @param string $productionDateFromScan
	 * @return ReceiveOrderItem
	 */
	public function setProductionDateFromScan($productionDateFromScan)
	{
		$this->productionDateFromScan = $productionDateFromScan;
	
		return $this;
	}

	/**
	 * Get productionDateFromScan
	 *
	 * @return string 
	 */
	public function getProductionDateFromScan()
	{
		return $this->productionDateFromScan;
	}

	/**
	 * Set productionDate
	 *
	 * @param \DateTime $productionDate
	 * @return ReceiveOrderItem
	 */
	public function setProductionDate($productionDate)
	{
		$this->productionDate = $productionDate;
	
		return $this;
	}

	/**
	 * Get productionDate
	 *
	 * @return \DateTime 
	 */
	public function getProductionDate()
	{
		return $this->productionDate;
	}
	
	/**
	 * Add putAwayItems
	 *
	 * @param \CI\InventoryBundle\Entity\PutAwayItem $putAwayItems
	 * @return PurchaseOrderItem
	 */
	public function addPutAwayItem(\CI\InventoryBundle\Entity\PutAwayItem $putAwayItems)
	{
		$this->putAwayItems[] = $putAwayItems;
	
		return $this;
	}
	
	/**
	 * Remove putAwayItems
	 *
	 * @param \CI\InventoryBundle\Entity\PutAwayItem $putAwayItems
	 */
	public function removePutAwayItem(\CI\InventoryBundle\Entity\PutAwayItem $putAwayItems)
	{
		$this->putAwayItems->removeElement($putAwayItems);
	}
	
	/**
	 * Get putAwayItems
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getPutAwayItems()
	{
		return $this->putAwayItems;
	}

	public function getTotalPutAwayFromPW($excludePutAway = null)
	{
		$totalPutAway = 0;
	
		foreach ($this->getPutAwayItems() as $item) {
			if ($excludePutAway && $excludePutAway->getId() == $item->getPutAway()->getId()) {
				continue;
			}
	
			if ($item->getPutAway()->getActive() == true) {
				$totalPutAway += $item->getQuantity();
			}
		}
	
		return $totalPutAway;
	}

	public function getMaxQtyAllowedToPutAwayFromPW(PutAway $pw = null) 
	{
		return $this->getQuantity() - $this->getTotalPutAwayFromPW($pw);
	}

	public function validate(ExecutionContextInterface $context)
	{
		if ($this->getIsIncluded() == true) {
			if (is_null($this->getQuantity())) {
				$context->addViolationAt('quantity', 'Quantity must not be blank.');
			}
	
			if (!is_numeric($this->getQuantity())) {
				$context->addViolationAt('quantity', 'Quantity is not a valid number.');
			} else {
				if ($this->getQuantity() < 0.01) {
					$context->addViolationAt('quantity', 'Minimum value is 0.01.');
				}
				 
				if ($this->getQuantity() > 9999999.99) {
					$context->addViolationAt('quantity', 'Maximum value is 9999999.99.');
				}
				
				if ($this->getQuantity() < $this->getTotalReturned()) {
					$context->addViolationAt('quantity', 'Value cannot be below quantity returned.');
				}
			}
		}
	}
	
	public function getTotalReturned(PurchaseReturnItem $excluded = null)
	{
		$totalReturned = 0;
		
		foreach ($this->getPurchaseReturnItems() as $item) {
			if ($excluded && $excluded->getId() == $item->getId()) {
				continue;
			}
	
			if (PurchaseReturn::STATUS_RETURNED == $item->getPurchaseReturn()->getStatus()) {
				$totalReturned += $item->getQuantity();
			}
		}
	
		return $totalReturned;
	}

	public function getTotalPutAway($excludePutAwayItem = null)
	{
		$totalPutAway = 0;
	
		foreach ($this->getPutAwayItems() as $item) {
			if ($excludePutAwayItem && $excludePutAwayItem->getId() == $item->getId()) {
				continue;
			}
	
			if ($item->getPutAway()->getActive() == true) {
				$totalPutAway += $item->getQuantity();
			}
		}
	
		return $totalPutAway;
	}
	
	public function getISProduct()
	{
		return $this->getProduct();
	}
	
	public function getISQuantity()
	{
		return $this->getQuantity() - $this->getPreviousQuantity();
	}

	public function getWarehouse()
	{
		return $this->getPurchaseOrderItem()->getPurchaseOrder()->getWarehouse();
	}
	
	public function getLog()
	{
		return array(
			'Quantity' => number_format($this->getQuantity(), 2) . ' kg',
			'Product' => $this->getProduct()->getSku() . ' ' . $this->getProduct()->getName(),
			'Lot No.' => $this->getLotNumber(),
			'Mode' => $this->getMode(),
			'Type' => $this->getType(),
			'Production Date' => $this->getProductionDate(),
			'Pallet ID' => $this->getPalletId()
		);
	}
}