<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Sales Activity
 *
 * @ORM\Table(name="sales_activity")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\SalesActivityRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class SalesActivity extends BaseEntity
{
	const VL = 'Vacation Leave';
	const SL = 'Sick Leave';
	const HOLIDAY = 'Holiday';
	const WORK_DAY = 'Work Day';
	
	const STATUS_DRAFT = 10;
	const STATUS_SUBMITTED = 21;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="status", type="integer", length=2)
	 * @Assert\Choice(choices = {"10", "21"})
	 * @Assert\Type(type="int")
	 * @Assert\Range(max=21)
	 */
	private $status;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="day_type", type="string", length=15)
	 * @Assert\Choice(choices = {"Sick Leave", "Vacation Leave", "Holiday", "Work Day"})
	 * @Assert\NotBlank(message="Day type must not be blank.")
	 * @Assert\Type(type="string")
	 */
	private $dayType;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="date", type="date")
	 * @Assert\NotBlank(message="Please enter the date.")
	 * @Assert\Date()
	 */
	private $date;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="read_flag", type="boolean")
	 */
	private $readFlag;
	/**
	 * @ORM\ManyToOne(targetEntity="CI\CoreBundle\Entity\User", inversedBy="salesActivities")
	 */
	private $agent;
	
	/**
	 * @ORM\OneToMany(targetEntity="SalesActivityItem", mappedBy="salesActivity", cascade={"persist", "remove"})
	 * @Assert\Count(min="1", minMessage="Please enter at least one activity.")
	 * @Assert\Valid
	 */
	private $items;
	
	/**
	 * @ORM\OneToMany(targetEntity="SalesActivityFile", mappedBy="salesActivity", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->date = new \DateTime('now');
		$this->dayType = self::WORK_DAY;
		$this->readFlag = false;
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
		$this->files = new \Doctrine\Common\Collections\ArrayCollection();
	}

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return SalesActivity
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set agent
     *
     * @param \CI\CoreBundle\Entity\User $agent
     * @return SalesActivity
     */
    public function setAgent(\CI\CoreBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;
    
        return $this;
    }

    /**
     * Get agent
     *
     * @return \CI\CoreBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Add items
     *
     * @param \CI\InventoryBundle\Entity\SalesActivityItem $items
     * @return SalesActivity
     */
    public function addItem(\CI\InventoryBundle\Entity\SalesActivityItem $items)
    {
    	$items->setSalesActivity($this);
        $this->items[] = $items;
    
        return $this;
    }

    /**
     * Remove items
     *
     * @param \CI\InventoryBundle\Entity\SalesActivityItem $items
     */
    public function removeItem(\CI\InventoryBundle\Entity\SalesActivityItem $items)
    {
        $this->items->removeElement($items);
    }
	
	/**
	 * Get items
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getItems()
	{
		return $this->items;
	}
	
	/**
	 * Add files
	 *
	 * @param \CI\InventoryBundle\Entity\SalesActivityFile $files
	 * @return SalesActivity
	 */
	public function addFile(\CI\InventoryBundle\Entity\SalesActivityFile $files)
	{
		$files->setSalesActivity($this);
		$this->files[] = $files;
	
		return $this;
	}
	
	/**
	 * Remove files
	 *
	 * @param \CI\InventoryBundle\Entity\SalesActivityFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\SalesActivityFile $files)
	{
		$this->files->removeElement($files);
	}
	
	/**
	 * Get files
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}
	
	/**
	 * Set dayType
	 *
	 * @param string $dayType
	 * @return SalesActivity
	 */
	public function setDayType($dayType)
	{
		$this->dayType = $dayType;
	
		return $this;
	}
	
	/**
	 * Get dayType
	 *
	 * @return string
	 */
	public function getDayType()
	{
		return $this->dayType;
	}

    /**
     * Set readFlag
     *
     * @param boolean $readFlag
     * @return SalesActivity
     */
    public function setReadFlag($readFlag)
    {
        $this->readFlag = $readFlag;
    
        return $this;
    }

    /**
     * Get readFlag
     *
     * @return boolean 
     */
    public function getReadFlag()
    {
        return $this->readFlag;
    }

	/**
	 * Set status
	 *
	 * @param integer $status
	 * @return SalesActivity
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	
		return $this;
	}
	
	/**
	 * Get status
	 *
	 * @return integer
	 */
	public function getStatus()
	{
		return $this->status;
	}

	public function translateStatus()
	{
		switch ($this->getStatus()) {
			case self::STATUS_DRAFT: return 'Draft';
			case self::STATUS_SUBMITTED: return 'Submitted';
			default: return 'Error';
		}
	}
	
	public function isEditable()
    {
    	if ($this->getStatus() == self::STATUS_DRAFT) {
    		return true;
    	}
    	
    	return false;
    }
    
    public function isDeletable()
    {
    	if ($this->getStatus() == self::STATUS_DRAFT) {
    		return true;
    	}
    	
    	return false;
    }
	
	public function hasComments()
    {
    	foreach ($this->getItems() as $item) {
    		if ($item->getComments()) {
    			return true;
    		}
    	}

    	return false;
    }

	public function getLog()
	{
		return array(
			'Day Type' => $this->getDayType(),
			'Date' => $this->getDate(),
			'Agent' => $this->getAgent()->getName(),
			'Status' => $this->translateStatus(),
			'Read?'  => $this->getReadFlag()? 'Yes': 'No'
		);
	}
}
