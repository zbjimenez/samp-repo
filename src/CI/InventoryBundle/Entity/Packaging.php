<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Packaging
 *
 * @ORM\Table(name="packaging")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\PackagingRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="name", message="This Packaging name is already in use.")
 */
class Packaging extends BaseEntity
{
	/**
	 * @ORM\Column(name="name", type="string", length=100)
	 * @Assert\NotBlank(message="Name must not be blank.")
	 * @Assert\Type(type="string")
	 */
	private $name;
	
	/**
	 * @ORM\Column(name="description", type="text")
	 * @Assert\NotBlank(message="Description must not be blank.")
	 * @Assert\Type(type="string")
	 */
	private $description;
	
	/**
	 * @ORM\Column(name="kgsUnit", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Kgs/Unit must not be blank.")
	 * @Assert\Range(min=0.01, max=99999999999)
	 * @Assert\Type(type="numeric", message="The value '{{ value }}' is not a valid number.")
	 */
	private $kgsUnit;
	
	/**
	 * @ORM\OneToMany(targetEntity="Product", mappedBy="packaging", cascade={"persist"})
	 */
	private $products;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->products = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Packaging
	 */
	public function setName($name)
	{
		$this->name = $name;
	
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string 
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Packaging
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	
		return $this;
	}
	
	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}
	
	
	/**
	 * Set kgsUnit
	 *
	 * @param string $kgsUnit
	 * @return Packaging
	 */
	public function setKgsUnit($kgsUnit)
	{
		$this->kgsUnit = $kgsUnit;
	
		return $this;
	}
	
	/**
	 * Get kgsUnit
	 *
	 * @return string
	 */
	public function getKgsUnit()
	{
		return $this->kgsUnit;
	}
	
	/**
	 * Add products
	 *
	 * @param \CI\InventoryBundle\Entity\Product $products
	 * @return Packaging
	 */
	public function addProduct(\CI\InventoryBundle\Entity\Product $products)
	{
		$this->products[] = $products;
	
		return $this;
	}
	
	/**
	 * Remove products
	 *
	 * @param \CI\InventoryBundle\Entity\Product $products
	 */
	public function removeProduct(\CI\InventoryBundle\Entity\Product $products)
	{
		$this->products->removeElement($products);
	}
	
	/**
	 * Get products
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getProducts()
	{
		return $this->products;
	}
	
	public function isDeletable()
	{
		return false;
	}
	
	public function getLog()
	{
		return array(
			'Name' => $this->getName(),
			'Description' => $this->getDescription(),
			'Kgs/Unit' => $this->getKgsUnit(),
			'Status' => $this->getActive()? 'Active' : 'Inactive',
		);
	} 
}