<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Supplier.
 *
 * @ORM\Table(name="supplier")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\SupplierRepository")
 * @ORM\HasLifeCycleCallbacks
 * @UniqueEntity(fields="name", message="This name is already in use.")
 * @UniqueEntity(fields="shortName", message="This short name is already in use.")
 */
class Supplier extends BaseEntity
{
	const EWT_NONE = '0.00';
	const EWT_GOODS = '0.01';
	const EWT_SERVICES = '0.02';

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Assert\NotBlank(message="Name must not be blank.")
	 * @Assert\Length(max=255, maxMessage="Input must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="short_name", type="string", length=255)
	 * @Assert\NotBlank(message="Short Name must not be blank.")
	 * @Assert\Length(max=255, maxMessage="Input must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 */
	private $shortName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="address", type="text")
	 * @Assert\NotBlank(message="Address must not be blank.")
	 * @Assert\Type(type="string")
	 */
	private $address;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="contact_details", type="text")
	 * @Assert\NotBlank(message="Contact details must not be blank.")
	 * @Assert\Type(type="string")
	 */
	private $contactDetails;

	/**
	 * @var decimal
	 *
	 * @ORM\Column(name="ewt", type="decimal", precision=3, scale=2)
	 * @Assert\Choice(choices={0, 0.01, 0.02}, message="Invalid EWT selected.")
	 * @Assert\NotBlank(message="EWT must not be blank.")
	 * @Assert\Type(type="numeric", message="The value '{{ value }}' is not a valid number.")
	 */
	private $ewt;

	/**
	 * @ORM\Column(name="terms", type="integer", nullable=true)
	 * @Assert\Type(type="integer")
	 * @Assert\Range(max=999)
	 */
	private $terms;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="memo", type="string", length=255, nullable=true)
	 * @Assert\Length(max=255, maxMessage="Input must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 */
	private $memo;

	/**
	 * @ORM\OneToMany(targetEntity="Product", mappedBy="supplier", cascade={"persist", "remove"})
	 */
	private $products;

	/**
	 * @ORM\OneToMany(targetEntity="PurchaseOrder", mappedBy="supplier", cascade={"persist", "remove"})
	 */
	private $purchaseOrders;

	/**
	 * @ORM\OneToMany(targetEntity="PurchaseReturn", mappedBy="supplier", cascade={"persist", "remove"})
	 */
	private $purchaseReturns;

	/**
	 * @ORM\OneToMany(targetEntity="SupplierFile", mappedBy="supplier", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		$this->ewt = self::EWT_NONE;
		$this->terms = 0;
		$this->products = new \Doctrine\Common\Collections\ArrayCollection();
		$this->purchaseOrders = new \Doctrine\Common\Collections\ArrayCollection();
		$this->files = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set name.
	 *
	 * @param string $name
	 *
	 * @return Supplier
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set shortName.
	 *
	 * @param string $shortName
	 *
	 * @return Supplier
	 */
	public function setShortName($shortName)
	{
		$this->shortName = $shortName;

		return $this;
	}

	/**
	 * Get shortName.
	 *
	 * @return string
	 */
	public function getShortName()
	{
		return $this->shortName;
	}

	/**
	 * Set address.
	 *
	 * @param string $address
	 *
	 * @return Supplier
	 */
	public function setAddress($address)
	{
		$this->address = $address;

		return $this;
	}

	/**
	 * Get address.
	 *
	 * @return string
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * Set contactDetails.
	 *
	 * @param string $contactDetails
	 *
	 * @return Supplier
	 */
	public function setContactDetails($contactDetails)
	{
		$this->contactDetails = $contactDetails;

		return $this;
	}

	/**
	 * Get contactDetails.
	 *
	 * @return string
	 */
	public function getContactDetails()
	{
		return $this->contactDetails;
	}

	/**
	 * Set ewt.
	 *
	 * @param string $ewt
	 *
	 * @return Supplier
	 */
	public function setEwt($ewt)
	{
		$this->ewt = $ewt;

		return $this;
	}

	/**
	 * Get ewt.
	 *
	 * @return string
	 */
	public function getEwt()
	{
		return $this->ewt;
	}

	/**
	 * Set memo.
	 *
	 * @param string $memo
	 *
	 * @return Supplier
	 */
	public function setMemo($memo)
	{
		$this->memo = $memo;

		return $this;
	}

	/**
	 * Get memo.
	 *
	 * @return string
	 */
	public function getMemo()
	{
		return $this->memo;
	}

	/**
	 * Add products.
	 *
	 * @param \CI\InventoryBundle\Entity\Product $products
	 *
	 * @return Supplier
	 */
	public function addProduct(\CI\InventoryBundle\Entity\Product $products)
	{
		$this->products[] = $products;

		return $this;
	}

	/**
	 * Remove products.
	 *
	 * @param \CI\InventoryBundle\Entity\Product $products
	 */
	public function removeProduct(\CI\InventoryBundle\Entity\Product $products)
	{
		$this->products->removeElement($products);
	}

	/**
	 * Get products.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getProducts()
	{
		return $this->products;
	}

	/**
	 * Add purchaseOrders.
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders
	 *
	 * @return Supplier
	 */
	public function addPurchaseOrder(\CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders)
	{
		$this->purchaseOrders[] = $purchaseOrders;

		return $this;
	}

	/**
	 * Remove purchaseOrders.
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders
	 */
	public function removePurchaseOrder(\CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders)
	{
		$this->purchaseOrders->removeElement($purchaseOrders);
	}

	/**
	 * Get purchaseOrders.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getPurchaseOrders()
	{
		return $this->purchaseOrders;
	}

	/**
	 * Add purchaseReturns.
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseReturn $purchaseReturns
	 *
	 * @return Supplier
	 */
	public function addPurchaseReturn(\CI\InventoryBundle\Entity\PurchaseReturn $purchaseReturns)
	{
		$this->purchaseReturns[] = $purchaseReturns;

		return $this;
	}

	/**
	 * Remove purchaseReturns.
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseReturn $purchaseReturns
	 */
	public function removePurchaseReturn(\CI\InventoryBundle\Entity\PurchaseReturn $purchaseReturns)
	{
		$this->purchaseReturns->removeElement($purchaseReturns);
	}

	/**
	 * Get purchaseReturns.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getPurchaseReturns()
	{
		return $this->purchaseReturns;
	}

	/**
	 * Add files.
	 *
	 * @param \CI\InventoryBundle\Entity\SupplierFile $files
	 *
	 * @return Supplier
	 */
	public function addFile(\CI\InventoryBundle\Entity\SupplierFile $files)
	{
		$files->setSupplier($this);
		$this->files[] = $files;

		return $this;
	}

	/**
	 * Remove files.
	 *
	 * @param \CI\InventoryBundle\Entity\SupplierFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\SupplierFile $files)
	{
		$this->files->removeElement($files);
	}

	/**
	 * Get files.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * Set terms.
	 *
	 * @param int $terms
	 *
	 * @return Supplier
	 */
	public function setTerms($terms)
	{
		$this->terms = $terms;

		return $this;
	}

	/**
	 * Get terms.
	 *
	 * @return int
	 */
	public function getTerms()
	{
		return $this->terms;
	}

	public function translateEwt()
	{
		switch ($this->getEwt()) {
			case self::EWT_GOODS:
				return '1% (Goods)';
				break;
			case self::EWT_SERVICES:
				return '2% (Services)';
				break;
			case self::EWT_NONE:
				return 'None';
				break;
			default:
				return 'Invalid value.';
		}
	}

	public function getLog()
	{
		$logs = [
			'Active' => $this->getActive() ? 'Yes' : 'No',
			'Name' => $this->getName(),
			'Short Name' => $this->getShortName(),
			'Address' => $this->getAddress(),
			'EWT' => $this->translateEwt(),
			'Contact Details' => $this->getContactDetails(),
			'Terms' => $this->getTerms().' days',
		];

		return $logs;
	}

	public function isDeletable()
	{
		if (count($this->getPurchaseOrders()) > 0) {
			return false;
		}

		return true;
	}
}
