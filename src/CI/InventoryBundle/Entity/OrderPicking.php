<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use CI\InventoryBundle\DependencyInjection\InventoryServiceEntityInterface;

/**
 * OrderPicking
 *
 * @ORM\Table(name="order_picking")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\OrderPickingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrderPicking extends BaseEntity implements InventoryServiceEntityInterface
{
	/**
	 * @var date
	 *
	 * @ORM\Column(name="date", type="date")
	 * @Assert\NotBlank(message="Date must not be blank.")
	 * @Assert\Date()
	 */
	private $date;

	/**
	 * @ORM\ManyToOne(targetEntity="Shipping", inversedBy="orderPickings")
	 */
	private $shipping;

	/**
	 * @ORM\OneToMany(targetEntity="OrderPickingItem", mappedBy="orderPicking", cascade={"persist", "remove"})
	 * @Assert\Count(min="1", minMessage="Must provide at least one item.")
	 * @Assert\Valid
	 */
	private $items;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 * @return OrderPicking
	 */
	public function setDate($date)
	{
		$this->date = $date;
	
		return $this;
	}

	/**
	 * Get date
	 *
	 * @return \DateTime 
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * Set shipping
	 *
	 * @param \CI\InventoryBundle\Entity\Shipping $shipping
	 * @return OrderPicking
	 */
	public function setShipping(\CI\InventoryBundle\Entity\Shipping $shipping = null)
	{
		$this->shipping = $shipping;
	
		return $this;
	}

	/**
	 * Get shipping
	 *
	 * @return \CI\InventoryBundle\Entity\Shipping 
	 */
	public function getShipping()
	{
		return $this->shipping;
	}
	
	public function setItems(\Doctrine\Common\Collections\Collection $items)
	{
		foreach ($items as $item) {
			$item->getShippingItem()->getOrderPickingItems()->add($item);
			$item->setOrderPicking($this);
		}
		
		$this->items = $items;
	}

	/**
	 * Get items
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getItems()
	{
		return $this->items;
	}

	public function getISItems()
	{
		return $this->getItems();
	}

	public function getWarehouse()
	{
		return $this->getShipping()->getSalesOrder()->getWarehouse();
	}

	public function updatePickedQuantity()
	{
		foreach ($this->getItems() as $item) {
			$shippingItem = $item->getShippingItem();
			$shippingItem->setPickedQuantity($shippingItem->getTotalPicked());
		}
	}

	public function isEditable()
	{
		foreach ($this->getItems() as $item) {
			if ($item->getIsPicked() === false) {
				return true;
			}
		}

		return false;
	}

	public function isDeletable()
	{
		foreach ($this->getItems() as $item) {
			if ($item->getIsPicked() === false) {
				return true;
			}
		}

		return false;
	}

	public function getLog()
	{
		return array(
			'Delivery' => $this->getShipping()->getId(),
			'Date' => $this->getDate(),
		);
	}
}