<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * CI\InventoryBundle\Entity\File
 *
 * @ORM\Table(name="file")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\FileRepository")
 * @ORM\HasLifecycleCallbacks
 */
class File
{
	const BASE_UPLOAD_DIR = 'uploads';
	
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="hash_name", type="string", length=255)
     * @Assert\Type(type="string")
     * @Assert\Length(max=255)
     */
    private $hashName;
    
    /**
     * @ORM\Column(name="file_name", type="string", length=255)
     * @Assert\Type(type="string")
     * @Assert\Length(max=255)
     */
    private $fileName;
    
    /**
     * @ORM\Column(name="rel_path", type="string", length=255)
     * @Assert\Type(type="string")
     * @Assert\Length(max=255)
     */
    private $relPath;
    
    /**
     * @ORM\Column(name="class", type="string", length=255)
     * @Assert\Type(type="string")
     * @Assert\Length(max=255)
     */
    private $class;
    
    /**
     * @ORM\Column(name="class_id", type="string", length=255)
     * @Assert\Type(type="string")
     * @Assert\Length(max=255)
     */
    private $classID;
    
    /**
     * @Assert\File(maxSize="10M")
     */
    private $file;
    
    //private $prevAbsoluteFilePath;
    
 	public function getAbsoluteFilePath()
    {
    	return empty($this->hashName) ? null : $this->getUploadRootDir() . $this->hashName . '.' . $this->getExtension();
    }
    
    public function getWebFilePath()
    {
    	return empty($this->hashName) ? null : $this->getUploadDir() . $this->hashName . '.' . $this->getExtension();
    }
    
    public function getUploadRootDir()
    {
    	return __DIR__.'/../../../../web/' . $this->getUploadDir();
    }
    
    protected function getUploadDir()
    {
    	return self::BASE_UPLOAD_DIR . '/' . $this->relPath . '/';
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set hashName
     *
     * @param string $hashName
     * @return File
     */
    public function setHashName($hashName)
    {
    	$this->hashName = $hashName;
    
    	return $this;
    }
    
    /**
     * Get hashName
     *
     * @return string
     */
    public function getHashName()
    {
    	return $this->hashName;
    }
    
    /**
     * Set fileName
     *
     * @param string $fileName
     * @return File
     */
    public function setFileName($fileName)
    {
    	$this->fileName = $fileName;
    
    	return $this;
    }
    
    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
    	return $this->fileName;
    }
    
    /**
     * Set relPath
     *
     * @param string $relPath
     * @return File
     */
    public function setRelPath($relPath)
    {
    	$this->relPath = $relPath;
    
    	return $this;
    }
    
    /**
     * Get relPath
     *
     * @return string
     */
    public function getRelPath()
    {
    	return $this->relPath;
    }
    
    public function setFile($file)
    {
    	$this->file = $file;
    
    	return $this;
    }
    
    public function getFile()
    {
    	return $this->file;
    }
    
    /**
     * Set class
     *
     * @param string $class
     * @return File
     */
    public function setClass($class)
    {
    	$this->class = $class;
    
    	return $this;
    }
    
    /**
     * Get class
     *
     * @return string
     */
    public function getClass()
    {
    	return $this->class;
    }
    
    /**
     * Set classID
     *
     * @param string $classID
     * @return File
     */
    public function setClassID($classID)
    {
    	$this->classID = $classID;
    
    	return $this;
    }
    
    /**
     * Get classID
     *
     * @return string
     */
    public function getClassID()
    {
    	return $this->classID;
    }
    
    public function getExtension()
    {
    	$ext = explode('.', $this->getFileName());
    	$ext = $ext[count($ext)-1];
    
    	return $ext;
    }
    
//     public function preUpload($deleteImage)
//     {
//     	if (!is_null($this->file)) {
//     		$this->prevAbsoluteFilePath = $this->getAbsoluteFilePath();
    		
//     		$hash = uniqid("", true);
//     		$this->fileName = $this->file->getClientOriginalName();
//     		$this->hashName = $hash;
//     	} elseif ($deleteImage) {
// 			// store the old absolute image path to delete after the update
// 			$this->prevAbsoluteFilePath = $this->getAbsoluteFilePath();
// 			// clear the image
// 			$this->fileName = null;
// 			$this->hashName = null;
// 		}
//     }
    
//     public function upload($deleteImage)
//     {
//     	// the file property can be empty if the field is not required
//     	if (!is_null($this->file)) {
//     		$this->file->move($this->getUploadRootDir(), $this->hashName . '.' . $this->getExtension());
//     		if (isset($this->prevAbsoluteFilePath) && file_exists($this->prevAbsoluteFilePath)) {
//     			unlink($this->prevAbsoluteFilePath);
//     		}
//     		$this->file = null;
//     	} else if ($deleteImage) {
// 			// delete previously uploaded image if exists
// 			if (isset($this->prevAbsoluteFilePath) && file_exists($this->prevAbsoluteFilePath)) {
// 				unlink($this->prevAbsoluteFilePath);
// 			}
// 		} 
//     }
    
    /**
     * @ORM\PostRemove
     */
    public function removeUpload()
    {
    	$file = $this->getAbsoluteFilePath();
    	
    	if (file_exists($file)) {
    		unlink($file);
    	}
    }
    
//     public function clearData()
//     {
//     	$this->fileName = null;
//     	$this->hashName = null;
//     }
    
//     /**
//      * Set class
//      *
//      * @param string $class
//      * @return File
//      */
//     public function setPrev($class)
//     {
//     	$this->prevAbsoluteFilePath = $class;
    
//     	return $this;
//     }
    
//     /**
//      * Get class
//      *
//      * @return string
//      */
//     public function getPrev()
//     {
//     	return $this->prevAbsoluteFilePath;
//     }
}