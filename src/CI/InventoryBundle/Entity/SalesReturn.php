<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

use CI\InventoryBundle\DependencyInjection\InventoryServiceEntityInterface;

/**
 * SalesReturn
 *
 * @ORM\Table(name="sales_return")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\SalesReturnRepository")
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"process"})
 */
class SalesReturn extends BaseEntity implements InventoryServiceEntityInterface
{
	const STATUS_DRAFT = 10;
	const STATUS_RETURNED = 23;
	const STATUS_VOID = 30;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="status", type="integer", length=2)
	 * @Assert\Choice(choices = {"10", "23", "30"})
	 * @Assert\Type(type="int")
	 * @Assert\Range(min=10, max=30)
	 */
	private $status;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="return_date", type="date")
	 * @Assert\NotBlank(message="Please enter the return date.")
	 * @Assert\Date()
	 */
	private $returnDate;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="memo", type="text", nullable=true)
	 * @Assert\Type("string")
	 */
	private $memo;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="total", type="decimal", scale=2, precision=13)
	 */
	private $total;
	
	/**
	 * @ORM\OneToMany(targetEntity="SalesReturnItem", mappedBy="salesReturn", cascade={"persist", "remove"})
	 * @Assert\Count(min="1", minMessage="Please enter at least one (1) item.")
	 * @Assert\Valid
	 */
	private $items;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Shipping", inversedBy="salesReturns")
	 * @ASsert\NotBlank(message="Please enter a DR# for this sales return.")
	 */
	private $shipping;

	/**
	 * @ORM\OneToMany(targetEntity="SalesReturnFile", mappedBy="salesReturn", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->returnDate = new \DateTime();
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
    	$this->files = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
	/**
	 * Set status
	 *
	 * @param integer $status
	 * @return PurchaseReturn
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	
		return $this;
	}
	
	/**
	 * Get status
	 *
	 * @return integer 
	 */
	public function getStatus()
	{
		return $this->status;
	}
	
	/**
	 * Set returnDate
	 *
	 * @param \DateTime $returnDate
	 * @return PurchaseReturn
	 */
	public function setReturnDate($returnDate)
	{
		$this->returnDate = $returnDate;
	
		return $this;
	}
	
	/**
	 * Get returnDate
	 *
	 * @return \DateTime 
	 */
	public function getReturnDate()
	{
		return $this->returnDate;
	}
	
	/**
	 * Set memo
	 *
	 * @param string $memo
	 * @return PurchaseReturn
	 */
	public function setMemo($memo)
	{
		$this->memo = $memo;
	
		return $this;
	}
	
	/**
	 * Get memo
	 *
	 * @return string 
	 */
	public function getMemo()
	{
		return $this->memo;
	}
	
	/**
	 * Set total
	 *
	 * @param string $total
	 * @return PurchaseReturn
	 */
	public function setTotal($total)
	{
		$this->total = $total;
	
		return $this;
	}
	
	/**
	 * Get total
	 *
	 * @return string 
	 */
	public function getTotal()
	{
		return $this->total;
	}
	
	/**
	 * Set items
	 *
	 * @param \Doctrine\Common\Collections\Collection $items
	 * @return SalesReturn
	 */
	public function setItems(\Doctrine\Common\Collections\Collection $items)
	{
		foreach ($items as $item) {
			$item->setSalesReturn($this);
		}
		$this->items = $items;
		
		return $this;
	}
	
	/**
	 * Get items
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getItems()
	{
		return $this->items;
	}
	
	/**
	 * Set shipping
	 *
	 * @param \CI\InventoryBundle\Entity\Shipping $shipping
	 * @return SalesReturn
	 */
	public function setShipping(\CI\InventoryBundle\Entity\Shipping $shipping = null)
	{
		$this->shipping = $shipping;
	
		return $this;
	}
	
	/**
	 * Get shipping
	 *
	 * @return \CI\InventoryBundle\Entity\Shipping 
	 */
	public function getShipping()
	{
		return $this->shipping;
	}

	/**
	 * Add files
	 *
	 * @param \CI\InventoryBundle\Entity\SalesReturnFile $files
	 * @return SalesReturn
	 */
	public function addFile(\CI\InventoryBundle\Entity\SalesReturnFile $files)
	{
		$files->setSalesReturn($this);
		$this->files[] = $files;
	
		return $this;
	}
	
	/**
	 * Remove files
	 *
	 * @param \CI\InventoryBundle\Entity\SalesReturnFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\SalesReturnFile $files)
	{
		$this->files->removeElement($files);
	}
	
	/**
	 * Get files
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}
	
	public function translateStatus()
	{
		switch ($this->getStatus()) {
			case self::STATUS_DRAFT: return 'Draft';
			case self::STATUS_RETURNED: return 'Returned';
			case self::STATUS_VOID: return 'Void';
			default: return 'Error';
		}
	}
	
	public function isEditable()
	{
		if ($this->getStatus() > self::STATUS_DRAFT) {
			return false;
		}
		
		return true;
	}
	
	public function isDeletable()
	{
		if ($this->getStatus() > self::STATUS_DRAFT) {
			return false;
		}
	
		return true;
	}
	
	public function process(ExecutionContextInterface $context)
	{
		$this->setTotal(0);
	}
	
	public function getLog()
	{
		return array(
			'SO#' => $this->getShipping()->getSalesOrder()->getRefCode(),
			'DR#' => $this->getShipping()->getDrId(),
			'Status' => $this->translateStatus(),
			'Return Date' => $this->getReturnDate()->format('M d, Y'),
			'Total' => number_format($this->getTotal(), 2),
			'Memo' => nl2br($this->getMemo()),
		);
	}
	
	public function getISItems()
	{
		return $this->getItems();
	}
}