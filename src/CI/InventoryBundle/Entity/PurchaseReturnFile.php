<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * PurchaseReturnFile
 *
 * @ORM\Table(name="purchase_return_file")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class PurchaseReturnFile
{
	/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="PurchaseReturn", inversedBy="files")
	 */
	private $purchaseReturn;
	
	/**
	 * @ORM\Column(name="file", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 * @Assert\Length(max=255)
	 */
	private $file;
	
	/**
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;
	
	/**
	 * @Assert\File(maxSize="10M")
	 */
	private $file_actual;
	
	private $prevId;
	
	public function getAbsoluteFilePath()
	{
		return null === $this->file ? null : $this->getUploadRootDir() . '/' . $this->file . '.' . $this->getExtension();
	}
	
	public function getWebFilePath()
	{
		return null === $this->file ? null : $this->getUploadDir() . '/' . $this->file . '.' . $this->getExtension();
	}
	
	protected function getUploadRootDir()
	{
		return __DIR__.'/../../../../web' . $this->getUploadDir();
	}
	
	protected function getUploadDir()
	{
		$id = $this->prevId;
		if ($this->getPurchaseReturn()) {
			$id = $this->getPurchaseReturn()->getId();
		}
		
		return '/uploads/purchaseReturn/' . $id;
	}
	
	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * Set purchaseReturn
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseReturn $purchaseReturn
	 * @return PurchaseReturnFile
	 */
	public function setPurchaseReturn(\CI\InventoryBundle\Entity\PurchaseReturn $purchaseReturn = null)
	{
		$this->purchaseReturn = $purchaseReturn;
	
		return $this;
	}
	
	/**
	 * Get purchaseReturn
	 *
	 * @return \CI\InventoryBundle\Entity\PurchaseReturn
	 */
	public function getPurchaseReturn()
	{
		return $this->purchaseReturn;
	}

	/**
	 * Set file
	 *
	 * @param string $file
	 * @return purchaseReturnFile
	 */
	public function setFile($file)
	{
		$this->file = $file;
	
		return $this;
	}

	/**
	 * Get file
	 *
	 * @return string 
	 */
	public function getFileActual()
	{
		return $this->file_actual;
	}
	
	/**
	 * Set file
	 *
	 * @param string $file
	 * @return purchaseReturnFile
	 */
	public function setFileActual($file)
	{
		$this->file_actual = $file;
	
		return $this;
	}
	
	/**
	 * Get file
	 *
	 * @return string
	 */
	public function getFile()
	{
		return $this->file;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return purchaseReturnFile
	 */
	public function setName($name)
	{
		$this->name = $name;
	
		return $this;
	}
	
	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Set prevId
	 *
	 * @param string $prevId
	 * @return purchaseReturnFile
	 */
	public function setPrevId($prevId)
	{
		$this->prevId = $prevId;
	
		return $this;
	}
	
	/**
	 * Get prevId
	 *
	 * @return integer
	 */
	public function getPrevId()
	{
		return $this->prevId;
	}
	
	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->file_actual) {
			$name = uniqid($this->getPurchaseReturn()->getId(), true);
			$this->name = $this->file_actual->getClientOriginalName();
			$this->file = $name;
		}
	}
	
	public function getExtension()
	{
		$ext = explode('.', $this->getName());
		$ext = $ext[count($ext)-1];
		 
		return $ext;
	}
	
	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function upload()
	{
		// the file property can be empty if the field is not required
		if (!is_null($this->file_actual)) {
			$this->file_actual->move($this->getUploadRootDir(), $this->file . '.' . $this->getExtension());
			$this->file_actual = null;
		}
	}
	
	/**
	 * @ORM\PreRemove()
	 */
	public function removeUpload()
	{
		if ($file = $this->getAbsoluteFilePath()) {
			unlink($file);
		}
	}

	public function getLog()
	{	
		return array(
			'File' => $this->getName()

		);
	}
}