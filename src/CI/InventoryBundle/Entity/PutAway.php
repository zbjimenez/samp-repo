<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use CI\InventoryBundle\DependencyInjection\InventoryServiceEntityInterface;
use Symfony\Component\Validator\ExecutionContextInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * PutAway
 *
 * @ORM\Table(name="put_away")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\PutAwayRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"validateItems"})
 */
class PutAway extends BaseEntity implements InventoryServiceEntityInterface
{
	const TYPE_RECEIVE_ORDER = 'RO';
	const TYPE_BACKLOAD = 'BL';

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="put_away_type", type="string", length=2)
	 * @Assert\Choice(choices = {"RO", "BL"})
	 */
	private $putAwayType;

	/**
	 * @ORM\ManyToOne(targetEntity="ReceiveOrder", inversedBy="putAways")
	 */
	private $receiveOrder;

	/**
	 * @ORM\ManyToOne(targetEntity="Backload", inversedBy="putAways")
	 */
	private $backload;

	/**
	 * @var date
	 *
	 * @ORM\Column(name="date", type="date")
	 * @Assert\NotBlank(message="Put Away Date must not be blank.")
	 * @Assert\Date()
	 */
	private $date;
	
	/**
	 * @ORM\OneToMany(targetEntity="PutAwayItem", mappedBy="putAway", cascade={"persist", "remove"})
	 * @Assert\Count(min="1", minMessage="You must check at least one item.")
	 * @Assert\Valid
	 */
	private $items;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get items
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getItems()
	{
		return $this->items;
	}
	
	/**
	 * Add items
	 *
	 * @param \CI\InventoryBundle\Entity\PutAwayItem $items
	 * @return PutAway
	 */
	public function addItem(\CI\InventoryBundle\Entity\PutAwayItem $items)
	{
		$items->setPutAway($this);
		$this->items[] = $items;
	
		return $this;
	}
	
	/**
	 * Remove items
	 *
	 * @param \CI\InventoryBundle\Entity\PutAwayItem $items
	 */
	public function removeItem(\CI\InventoryBundle\Entity\PutAwayItem $items)
	{
		$this->items->removeElement($items);
	}

	/**
	 * Set receiveOrder
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrder $receiveOrder
	 * @return PutAway
	 */
	public function setReceiveOrder(\CI\InventoryBundle\Entity\ReceiveOrder $receiveOrder = null)
	{
		$this->receiveOrder = $receiveOrder;
	
		return $this;
	}

	/**
	 * Get receiveOrder
	 *
	 * @return \CI\InventoryBundle\Entity\ReceiveOrder 
	 */
	public function getReceiveOrder()
	{
		return $this->receiveOrder;
	}
	
	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 * @return PutAway
	 */
	public function setDate($date)
	{
		$this->date = $date;
	
		return $this;
	}

	/**
	 * Get date
	 *
	 * @return \DateTime 
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * Set putAwayType
	 *
	 * @param string $putAwayType
	 * @return PutAway
	 */
	public function setPutAwayType($putAwayType)
	{
		$this->putAwayType = $putAwayType;
	
		return $this;
	}

	/**
	 * Get putAwayType
	 *
	 * @return string 
	 */
	public function getPutAwayType()
	{
		return $this->putAwayType;
	}

	/**
	 * Set backload
	 *
	 * @param \CI\InventoryBundle\Entity\Backload $backload
	 * @return PutAway
	 */
	public function setBackload(\CI\InventoryBundle\Entity\Backload $backload = null)
	{
		$this->backload = $backload;
	
		return $this;
	}

	/**
	 * Get backload
	 *
	 * @return \CI\InventoryBundle\Entity\Backload 
	 */
	public function getBackload()
	{
		return $this->backload;
	}
	
	public function isEditable()
	{
		if ($this->getPutAwayType() === self::TYPE_RECEIVE_ORDER) {
			if ($this->getReceiveOrder()->getActive() && $this->getActive()) {
				return true;
			}
		} else {
			if ($this->getBackload()->getActive() && $this->getActive()) {
				return true;
			}
		}

		return false;
	}
	
	
	public function translateStatus()
	{
		return $this->getActive() ? 'Successfully Put Away' : 'Void';
	}

	public function getStatus()
	{
		return $this->translateStatus();
	}
	
	public function getISItems()
	{
		return $this->getItems();
	}

	public function getPurchaseOrder()
	{
		return $this->getReceiveOrder()->getPurchaseOrder();
	}

	public function getWarehouse()
	{
		if ($this->getPutAwayType() === self::TYPE_RECEIVE_ORDER) {
			return $this->getPurchaseOrder()->getWarehouse();
		} else {
			return $this->getBackload()->getShipping()->getSalesOrder()->getWarehouse();
		}
	}

	public function getQtyPutAway()
	{
		$quantity = 0;
		foreach($this->getItems() as $item) {
			$quantity += $item->getQuantity();
		}
	
		return $quantity;
	}

	public function getProductsPreview() {
		$arr = array();
		foreach($this->getItems() as $item) {
			$exp = '['.$item->getProduct()->getSku() . '] ' . $item->getProduct()->getName();
			if (!in_array($exp, $arr)) {
				$arr[] = $exp;
			}
		}

		return implode("\n", $arr);
	}
	
	public function validateItems(ExecutionContextInterface $context)
	{
		foreach ($this->getItems() as $item) {
			if ($item->getIsIncluded()) {
				return true;
			}
		}
		
		// validate if at least one item was added
		$context->addViolationAt('items', 'At least one item must be put away.');
	}

	public function getLog()
	{
		return array(
			'RO #' => $this->getReceiveOrder()->getId(),
			'Date' => $this->getDate(),
			'Status' => $this->translateStatus()
		);
	}
}