<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Quotation
 *
 * @ORM\Table(name="quotation")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\QuotationRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 */
class Quotation extends BaseEntity
{
	const STATUS_PENDING = 10;
	const STATUS_APPROVED = 20;
	const STATUS_VOID = 30;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="status", type="integer", length=2)
	 * @Assert\Choice(choices = {"10", "20", "30"})
	 * @Assert\Type(type="int")
	 * @Assert\Length(max=2)
	 */
	private $status;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="reference_number", type="string", length=255, nullable=true)
	 */
	private $referenceNumber;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="date", type="date")
	 * @Assert\NotBlank(message="Date must not be blank.")
	 * @Assert\Date()
	 */
	private $date;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="contact_person", type="string", length=255)
	 * @Assert\NotBlank(message="Contact person must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $contactPerson;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="position", type="string", length=255, nullable=true)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $position;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="origin", type="string", length=255, nullable=true)
	 * @Assert\Length(max=255)
	 * @Assert\Type(type="string")
	 */
	private $origin;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="quantity", type="decimal", scale=2, precision=9, nullable=true)
	 * @Assert\Range(min=0, max=9999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $quantity;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="price", type="decimal", scale=2, precision=10)
	 * @Assert\NotBlank(message="Price must not be blank.")
	 * @Assert\Range(min=0.01, max=99999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $price;
	
	/**
	 * @ORM\Column(name="terms", type="integer")
	 * @Assert\NotBlank(message="Terms must not be blank.")
	 * @Assert\Type(type="integer")
	 * @Assert\Range(min=0, max=999)
	 */
	private $terms;
	
	/**
	 * @ORM\Column(name="delivery_notes", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $deliveryNotes;

	/**
	 * @ORM\Column(name="memo", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $memo;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="validity_start_date", type="date")
	 * @Assert\NotBlank(message="Start date must not be blank.")
	 * @Assert\Date()
	 */
	private $validityStartDate;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="validity_end_date", type="date")
	 * @Assert\NotBlank(message="End date must not be blank.")
	 * @Assert\Date()
	 */
	private $validityEndDate;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="read_name", type="string", length=255, nullable=true)
	 */
	private $readName;

	/**
	 * @var date
	 *
	 * @ORM\Column(name="read_datetime", type="datetime", nullable=true)
	 */
	private $readDateTime;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Customer", inversedBy="quotations")
	 * @ASsert\NotBlank(message="Customer must not be blank.")
	 */
	private $customer;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="quotations")
	 * @ASsert\NotBlank(message="Product must not be blank.")
	 */
	private $product;
	
	/**
	 * @ORM\ManyToOne(targetEntity="CI\CoreBundle\Entity\User", inversedBy="quotations")
	 */
	private $salesAgent;
	
	/**
	 * @ORM\OneToMany(targetEntity="SalesOrderItem", mappedBy="quotation", cascade={"persist"})
	 */
	private $salesOrderItems;
	
	/**
	 * @ORM\OneToMany(targetEntity="QuotationFile", mappedBy="quotation", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;

	/**
	 * @ORM\ManyToOne(targetEntity="Currency", inversedBy="quotations")
	 */
	private $currency;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="exchange_rate", type="decimal", scale=2, precision=13, nullable=true)
	 *
	 */
	private $exchangeRate;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->date = new \DateTime();
		$this->status = self::STATUS_PENDING;
		$this->quantity = 0;
		$this->salesOrderItems = new \Doctrine\Common\Collections\ArrayCollection();
		$this->files = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set status
	 *
	 * @param integer $status
	 * @return Quotation
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	
		return $this;
	}

	/**
	 * Get status
	 *
	 * @return integer 
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 * @return Quotation
	 */
	public function setDate($date)
	{
		$this->date = $date;
	
		return $this;
	}

	/**
	 * Get date
	 *
	 * @return \DateTime 
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * Set contactPerson
	 *
	 * @param string $contactPerson
	 * @return Quotation
	 */
	public function setContactPerson($contactPerson)
	{
		$this->contactPerson = $contactPerson;
	
		return $this;
	}

	/**
	 * Get contactPerson
	 *
	 * @return string 
	 */
	public function getContactPerson()
	{
		return $this->contactPerson;
	}

	/**
	 * Set position
	 *
	 * @param string $position
	 * @return Quotation
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	
		return $this;
	}

	/**
	 * Get position
	 *
	 * @return string 
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * Set origin
	 *
	 * @param string $origin
	 * @return Quotation
	 */
	public function setOrigin($origin)
	{
		$this->origin = $origin;
	
		return $this;
	}

	/**
	 * Get origin
	 *
	 * @return string 
	 */
	public function getOrigin()
	{
		return $this->origin;
	}

	/**
	 * Set quantity
	 *
	 * @param string $quantity
	 * @return Quotation
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	
		return $this;
	}

	/**
	 * Get quantity
	 *
	 * @return string 
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * Set price
	 *
	 * @param string $price
	 * @return Quotation
	 */
	public function setPrice($price)
	{
		$this->price = $price;
	
		return $this;
	}

	/**
	 * Get price
	 *
	 * @return string 
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * Set terms
	 *
	 * @param integer $terms
	 * @return Quotation
	 */
	public function setTerms($terms)
	{
		$this->terms = $terms;
	
		return $this;
	}

	/**
	 * Get terms
	 *
	 * @return integer 
	 */
	public function getTerms()
	{
		return $this->terms;
	}

	/**
	 * Set deliveryNotes
	 *
	 * @param string $deliveryNotes
	 * @return Quotation
	 */
	public function setDeliveryNotes($deliveryNotes)
	{
		$this->deliveryNotes = $deliveryNotes;
	
		return $this;
	}

	/**
	 * Get deliveryNotes
	 *
	 * @return string 
	 */
	public function getDeliveryNotes()
	{
		return $this->deliveryNotes;
	}

	/**
	 * Set memo
	 *
	 * @param string $memo
	 * @return Quotation
	 */
	public function setMemo($memo)
	{
		$this->memo = $memo;
	
		return $this;
	}

	/**
	 * Get memo
	 *
	 * @return string 
	 */
	public function getMemo()
	{
		return $this->memo;
	}

	/**
	 * Set validityStartDate
	 *
	 * @param \DateTime $validityStartDate
	 * @return Quotation
	 */
	public function setValidityStartDate($validityStartDate)
	{
		$this->validityStartDate = $validityStartDate;
	
		return $this;
	}

	/**
	 * Get validityStartDate
	 *
	 * @return \DateTime 
	 */
	public function getValidityStartDate()
	{
		return $this->validityStartDate;
	}

	/**
	 * Set validityEndDate
	 *
	 * @param \DateTime $validityEndDate
	 * @return Quotation
	 */
	public function setValidityEndDate($validityEndDate)
	{
		$this->validityEndDate = $validityEndDate;
	
		return $this;
	}

	/**
	 * Get validityEndDate
	 *
	 * @return \DateTime 
	 */
	public function getValidityEndDate()
	{
		return $this->validityEndDate;
	}

	/**
	 * Set customer
	 *
	 * @param \CI\InventoryBundle\Entity\Customer $customer
	 * @return Quotation
	 */
	public function setCustomer(\CI\InventoryBundle\Entity\Customer $customer = null)
	{
		$this->customer = $customer;
	
		return $this;
	}

	/**
	 * Get customer
	 *
	 * @return \CI\InventoryBundle\Entity\Customer 
	 */
	public function getCustomer()
	{
		return $this->customer;
	}

	/**
	 * Set product
	 *
	 * @param \CI\InventoryBundle\Entity\Product $product
	 * @return Quotation
	 */
	public function setProduct(\CI\InventoryBundle\Entity\Product $product = null)
	{
		$this->product = $product;
	
		return $this;
	}

	/**
	 * Get product
	 *
	 * @return \CI\InventoryBundle\Entity\Product 
	 */
	public function getProduct()
	{
		return $this->product;
	}

	/**
	 * Set salesAgent
	 *
	 * @param \CI\CoreBundle\Entity\User $salesAgent
	 * @return Quotation
	 */
	public function setSalesAgent(\CI\CoreBundle\Entity\User $salesAgent = null)
	{
		$this->salesAgent = $salesAgent;
	
		return $this;
	}

	/**
	 * Get salesAgent
	 *
	 * @return \CI\CoreBundle\Entity\User 
	 */
	public function getSalesAgent()
	{
		return $this->salesAgent;
	}
	
	/**
	 * Set referenceNumber
	 *
	 * @param string $referenceNumber
	 * @return Quotation
	 */
	public function setReferenceNumber($referenceNumber)
	{
		$this->referenceNumber = $referenceNumber;
	
		return $this;
	}
	
	/**
	 * Get referenceNumber
	 *
	 * @return string
	 */
	public function getReferenceNumber()
	{
		return $this->referenceNumber;
	}
	
	/**
	 * Add files
	 *
	 * @param \CI\InventoryBundle\Entity\QuotationFile $files
	 * @return Quotation
	 */
	public function addFile(\CI\InventoryBundle\Entity\QuotationFile $files)
	{
		$files->setQuotation($this);
		$this->files[] = $files;
	
		return $this;
	}
	
	/**
	 * Remove files
	 *
	 * @param \CI\InventoryBundle\Entity\QuotationFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\QuotationFile $files)
	{
		$this->files->removeElement($files);
	}
	
	/**
	 * Get files
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * Set currency
	 *
	 * @param \CI\InventoryBundle\Entity\Currency $currency
	 * @return Quotation
	 */
	public function setCurrency(\CI\InventoryBundle\Entity\Currency $currency = null)
	{
		$this->currency = $currency;
	
		return $this;
	}
	
	/**
	 * Get currency
	 *
	 * @return \CI\InventoryBundle\Entity\Currency
	 */
	public function getCurrency()
	{
		return $this->currency;
	}

	/**
	 * Set exchangeRate
	 *
	 * @param string $exchangeRate
	 * @return Quotation
	 */
	public function setExchangeRate($exchangeRate)
	{
		$this->exchangeRate = $exchangeRate;
	
		return $this;
	}

	/**
	 * Get exchangeRate
	 *
	 * @return string 
	 */
	public function getExchangeRate()
	{
		return $this->exchangeRate;
	}
	
	public function getSalesOrderItems()
	{
		return $this->salesOrderItems;
	}

	/**
	 * Set readName
	 *
	 * @param string $readName
	 * @return Quotation
	 */
	public function setReadName($readName)
	{
		$this->readName = $readName;
	
		return $this;
	}

	/**
	 * Get readName
	 *
	 * @return string 
	 */
	public function getReadName()
	{
		return $this->readName;
	}

	/**
	 * Set readDateTime
	 *
	 * @param \DateTime $readDateTime
	 * @return Quotation
	 */
	public function setReadDateTime($readDateTime)
	{
		$this->readDateTime = $readDateTime;
	
		return $this;
	}

	/**
	 * Get readDateTime
	 *
	 * @return \DateTime 
	 */
	public function getReadDateTime()
	{
		return $this->readDateTime;
	}

	public function isEditable()
	{
		if ($this->getStatus() == self::STATUS_PENDING) {
			return true;
		}
		return false;
	}
	
	public function isDeletable()
	{
		if ($this->getStatus() == self::STATUS_PENDING) {
			return true;
		}
		return false;
	}
	
	public function translateStatus()
	{
		switch ($this->getStatus()) {
			case self::STATUS_PENDING:
				return 'Pending for Approval';
			case self::STATUS_APPROVED:
				return 'Approved';
			case self::STATUS_VOID:
				return 'Void';
			default:
				return 'Error';
		}
	}
	
	public function getProductName()
	{
		$product = $this->getProduct();
		$productCode = $product->getCustomerProductName($this->getCustomer());
		
		return count($productCode) > 0 ? $productCode[0]->getAlias() . ' (' . $product->getName() . ')' : $product->getName();
	}
	
	public function getPricePerBag()
	{
		$bag = $this->getQuantity() / $this->getPackaging()->getKgsUnit();
		$pricePerBag = $this->getPrice() / $bag;
		
		return $pricePerBag;
	}
	
	public function isValid($dateToCompare = null)
	{
		$now = new \DateTime('now');
		
		if (!empty($dateToCompare) && $dateToCompare instanceof \DateTime) {
			$now = $dateToCompare;
		}
		
		if ($now >= $this->getValidityStartDate() && $now <= $this->getValidityEndDate()) {
			return true;
		} else {
			return false;
		}
	}
	
	public function getActiveSalesOrder()
	{
		foreach($this->getSalesOrderItems() as $soItem) {
			$so = $soItem->getSalesOrder();
			if ($so->getStatus() === SalesOrder::STATUS_APPROVED || $so->getStatus() === SalesOrder::STATUS_CLOSED) {
				return $so;
			}
		}
		
		return null;
	}

	/*
	* Using the exchange rate, this returns the price in the user's local currency.
	* This is just exchange rate multiplied by the price.
	*
	* If the exchange rate is 0 or the currency isn't foreign, there will be no conversion.
	*
	* Note: In the UI, PHP is always presumed to be the local currency regardless if whatever
	* currency the user has to set to be local.
	*/
	public function getConvertedPrice()
	{
		return (($this->getCurrency()->getIsForeign() == true) && ($this->getExchangeRate() > 0)) ? bcmul($this->getExchangeRate(), $this->getPrice(), 2) : $this->getPrice() ;
	}

	public function process(ExecutionContextInterface $context)
	{
		if ($this->getValidityStartDate()->diff($this->getValidityEndDate())->d > 7) {
			$context->addViolationAt('validityEndDate', 'Validity end date should not exceed 7 days from the start date.');
		}

		if (($this->getCurrency() && $this->getCurrency()->getIsForeign() == true) && ($this->getExchangeRate() == 0)) {
			$context->addViolationAt('exchangeRate', 'The currency that you have selected is Foreign, you must provide an Exchange Rate.');
		}
	}
	
	public function getLog()
	{
		return array(
			'Quotation #' => $this->getReferenceNumber(),
			'Prepared By' => $this->getSalesAgent()->getName(),
			'Date' => $this->getDate(),
			'Contact Person' => $this->getContactPerson(),
			'Position' => $this->getPosition(),
			'Customer' => $this->getCustomer()->getName(),
			'Product' => $this->getProductName(),
			'Origin' => $this->getOrigin(),
			'Quantity' => number_format($this->getQuantity(), 2) . ' kg',
			'Price' => number_format($this->getPrice(), 2),
			'Packaging Size' => $this->getProduct()->getPackaging()->getDescription(),
			'Terms' => $this->getTerms(),
			'Delivery Notes' => $this->getDeliveryNotes(),
			'Memo' => $this->getMemo(),
			'Price Validity Start Date' => $this->getValidityStartDate(),
			'Price Validity End Date' => $this->getValidityEndDate(),
			'Status' => $this->translateStatus(),
			'Read by' => $this->getReadName(),
		);
	}
}