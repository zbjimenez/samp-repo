<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

use CI\InventoryBundle\DependencyInjection\InventoryServiceItemInterface;

/**
 * SalesReturnItem
 *
 * @ORM\Table(name="sales_return_item")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"process"})
 */
class SalesReturnItem implements InventoryServiceItemInterface
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="quantity", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Please enter a quantity for this item.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $quantity;
	
	/**
	 * @ORM\ManyToOne(targetEntity="SalesReturn", inversedBy="items")
	 */
	private $salesReturn;
	
	/**
	 * @ORM\ManyToOne(targetEntity="ShippingItem", inversedBy="salesReturnItems")
	 */
	private $shippingItem;

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set quantity
	 *
	 * @param string $quantity
	 * @return PurchaseReturnItem
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	
		return $this;
	}

	/**
	 * Get quantity
	 *
	 * @return string 
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * Set salesReturn
	 *
	 * @param \CI\InventoryBundle\Entity\SalesReturn $salesReturn
	 * @return SalesReturnItem
	 */
	public function setSalesReturn(\CI\InventoryBundle\Entity\SalesReturn $salesReturn = null)
	{
		$this->salesReturn = $salesReturn;
	
		return $this;
	}

	/**
	 * Get salesReturn
	 *
	 * @return \CI\InventoryBundle\Entity\SalesReturn 
	 */
	public function getSalesReturn()
	{
		return $this->salesReturn;
	}

	/**
	 * Set shippingItem
	 *
	 * @param \CI\InventoryBundle\Entity\ShippingItem $shippingItem
	 * @return SalesReturnItem
	 */
	public function setShippingItem(\CI\InventoryBundle\Entity\ShippingItem $shippingItem = null)
	{
		$this->shippingItem = $shippingItem;
	
		return $this;
	}

	/**
	 * Get shippingItem
	 *
	 * @return \CI\InventoryBundle\Entity\ShippingItem 
	 */
	public function getShippingItem()
	{
		return $this->shippingItem;
	}
	
	public function process(ExecutionContextInterface $context)
	{
		$this->getSalesReturn()->setTotal($this->getSalesReturn()->getTotal() + round($this->getQuantity() * $this->getShippingItem()->getSalesOrderItem()->getUnitPrice(), 2));
		
		if ($this->getQuantity() > $this->getShippingItem()->getQuantity() - $this->getShippingItem()->getTotalReturned($this)) {
			$context->addViolationAt('quantity', 'Limit reached');
		}
	}
	
	public function getLog()
	{
		return array(
			'Product' => $this->getISProduct()->getSku() . ' ' . $this->getISProduct()->getName(),
			'Quantity' => number_format($this->getQuantity(), 2) . ' kg',
		);
	}
	
	public function getISProduct()
	{
		return $this->getShippingItem()->getISProduct();
	}
	
	public function getISQuantity()
	{
		return $this->getQuantity();
	}

	public function getStorageLocation()
	{
		return $this->getShippingItem()->getStorageLocation();
	}
}