<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * BackloadItem
 *
 * @ORM\Table(name="backload_item")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\BackloadItemRepository")
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"validate"})
 */
class BackloadItem
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var boolean
	 */
	private $isIncluded = true;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="quantity", type="decimal", scale=2, precision=11)
	 * @Assert\NotBlank(message="Quantity must not be blank.", groups={"Included"})
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $quantity;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lot_number", type="string", length=255)
	 * @Assert\NotBlank(message="Lot number must not be blank.", groups={"Included"})
	 */
	private $lotNumber;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="production_date", type="date")
	 * @Assert\Date()
	 * @Assert\NotBlank(message="Production date must not be blank.", groups={"Included"})
	 */
	private $productionDate;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="pallet_id", type="string", length=255, nullable=true)
	 */
	private $palletId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="remarks", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $remarks;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="corrective_action", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $correctiveAction;

	/**
	 * @ORM\ManyToOne(targetEntity="ShippingItem", inversedBy="backloadItems")
	 */
	private $shippingItem;

	/**
	 * @ORM\ManyToOne(targetEntity="Backload", inversedBy="items")
	 */
	private $backload;

	/**
	 * @ORM\OneToMany(targetEntity="PutAwayItem", mappedBy="backloadItem", cascade={"persist", "remove"})
	 */
	private $putAwayItems;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->putAwayItems = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set isIncluded
	 *
	 * @param boolean $isIncluded
	 * @return BackloadItem
	 */
	public function setIsIncluded($isIncluded)
	{
		$this->isIncluded = $isIncluded;

		return $this;
	}

	/**
	 * Get isIncluded
	 *
	 * @return boolean
	 */
	public function getIsIncluded()
	{
		return $this->isIncluded;
	}

	/**
	 * Set quantity
	 *
	 * @param string $quantity
	 * @return BackloadItem
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	
		return $this;
	}

	/**
	 * Get quantity
	 *
	 * @return string 
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * Set lotNumber
	 *
	 * @param string $lotNumber
	 * @return BackloadItem
	 */
	public function setLotNumber($lotNumber)
	{
		$this->lotNumber = $lotNumber;
	
		return $this;
	}

	/**
	 * Get lotNumber
	 *
	 * @return string 
	 */
	public function getLotNumber()
	{
		return $this->lotNumber;
	}

	/**
	 * Set productionDate
	 *
	 * @param \DateTime $productionDate
	 * @return BackloadItem
	 */
	public function setProductionDate($productionDate)
	{
		$this->productionDate = $productionDate;
	
		return $this;
	}

	/**
	 * Get productionDate
	 *
	 * @return \DateTime 
	 */
	public function getProductionDate()
	{
		return $this->productionDate;
	}

	/**
	 * Set palletId
	 *
	 * @param string $palletId
	 * @return BackloadItem
	 */
	public function setPalletId($palletId)
	{
		$this->palletId = $palletId;
	
		return $this;
	}

	/**
	 * Get palletId
	 *
	 * @return string 
	 */
	public function getPalletId()
	{
		return $this->palletId;
	}

	/**
	 * Set remarks
	 *
	 * @param string $remarks
	 * @return BackloadItem
	 */
	public function setRemarks($remarks)
	{
		$this->remarks = $remarks;
	
		return $this;
	}

	/**
	 * Get remarks
	 *
	 * @return string 
	 */
	public function getRemarks()
	{
		return $this->remarks;
	}

	/**
	 * Set correctiveAction
	 *
	 * @param string $correctiveAction
	 * @return BackloadItem
	 */
	public function setCorrectiveAction($correctiveAction)
	{
		$this->correctiveAction = $correctiveAction;
	
		return $this;
	}

	/**
	 * Get correctiveAction
	 *
	 * @return string 
	 */
	public function getCorrectiveAction()
	{
		return $this->correctiveAction;
	}

	/**
	 * Set shippingItem
	 *
	 * @param \CI\InventoryBundle\Entity\ShippingItem $shippingItem
	 * @return BackloadItem
	 */
	public function setShippingItem(\CI\InventoryBundle\Entity\ShippingItem $shippingItem = null)
	{
		$this->shippingItem = $shippingItem;
	
		return $this;
	}

	/**
	 * Get shippingItem
	 *
	 * @return \CI\InventoryBundle\Entity\ShippingItem 
	 */
	public function getShippingItem()
	{
		return $this->shippingItem;
	}

	/**
	 * Set backload
	 *
	 * @param \CI\InventoryBundle\Entity\Backload $backload
	 * @return BackloadItem
	 */
	public function setBackload(\CI\InventoryBundle\Entity\Backload $backload = null)
	{
		$this->backload = $backload;
	
		return $this;
	}

	/**
	 * Get backload
	 *
	 * @return \CI\InventoryBundle\Entity\Backload 
	 */
	public function getBackload()
	{
		return $this->backload;
	}

	/**
	 * Get putAwayItems
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getPutAwayItems()
	{
		return $this->putAwayItems;
	}

	public function validate(ExecutionContextInterface $context)
	{
		if ($this->getIsIncluded()) {
			$limit = $this->getShippingItem()->getMaxQtyAllowedToBackload($this);

			if ($this->getQuantity() > $limit) {
				$context->addViolationAt('quantity', 'Limit reached.');
			}
		}
	}

	public function getLog()
	{
		return array(
			'Quantity' => $this->getQuantity(),
			'Product' => $this->getShippingItem()->getProductName(),
			'Lot Number' => $this->getLotNumber(),
			'Pallet ID' => $this->getPalletId(),
			'Production Date' => $this->getProductionDate(),
			'Remarks' => $this->getRemarks(),
			'Corrective Action' => $this->getCorrectiveAction(),
		);
	}
}