<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PurchaseOrderItem
 *
 * @ORM\Table(name="purchase_order_item")				  
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\PurchaseOrderItemRepository")
 */
class PurchaseOrderItem
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id; 
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="quantity", type="decimal", scale=2, precision=9)
	 * @Assert\NotBlank(message="Quantity must not be blank.")
	 * @Assert\Range(min=0.01, max=9999999.99, invalidMessage="The value is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $quantity;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="line_total", type="decimal", scale=2, precision=13)
	 */
	private $lineTotal;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="unit_cost", type="decimal", scale=2, precision=10)
	 * @Assert\NotBlank(message="Unit cost must not be blank.")
	 * @Assert\Range(min=0.01, max=99999999.99, invalidMessage="The value is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $unitCost;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="contract_number", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $contractNumber;
	
	/**
	 * @ORM\ManyToOne(targetEntity="PurchaseOrder", inversedBy="items")
	 */
	private $purchaseOrder;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="purchaseOrderItems")
	 * @ASsert\NotBlank(message="Product must not be blank.")
	 */
	private $product;
	
	/**
	 * @ORM\OneToMany(targetEntity="ReceiveOrderItem", mappedBy="purchaseOrderItem", cascade={"persist", "remove"})
	 */
	private $receiveOrderItems;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->lineTotal = 0;
		$this->quantity = 0;
		$this->unitCost = 0;
		$this->receiveOrderItems = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set quantity
	 *
	 * @param string $quantity
	 * @return PurchaseOrderItem
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	
		return $this;
	}

	/**
	 * Get quantity
	 *
	 * @return string 
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * Set lineTotal
	 *
	 * @param string $lineTotal
	 * @return PurchaseOrderItem
	 */
	public function setLineTotal($lineTotal)
	{
		$this->lineTotal = $lineTotal;
	
		return $this;
	}

	/**
	 * Get lineTotal
	 *
	 * @return string 
	 */
	public function getLineTotal()
	{
		return $this->lineTotal;
	}

	/**
	 * Set unitCost
	 *
	 * @param string $unitCost
	 * @return PurchaseOrderItem
	 */
	public function setUnitCost($unitCost)
	{
		$this->unitCost = $unitCost;
	
		return $this;
	}

	/**
	 * Get unitCost
	 *
	 * @return string 
	 */
	public function getUnitCost()
	{
		return $this->unitCost;
	}

	/**
	 * Set contractNumber
	 *
	 * @param string $contractNumber
	 * @return PurchaseOrderItem
	 */
	public function setContractNumber($contractNumber)
	{
		$this->contractNumber = $contractNumber;
	
		return $this;
	}

	/**
	 * Get contractNumber
	 *
	 * @return string 
	 */
	public function getContractNumber()
	{
		return $this->contractNumber;
	}

	/**
	 * Set purchaseOrder
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrder
	 * @return PurchaseOrderItem
	 */
	public function setPurchaseOrder(\CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrder = null)
	{
		$this->purchaseOrder = $purchaseOrder;
	
		return $this;
	}

	/**
	 * Get purchaseOrder
	 *
	 * @return \CI\InventoryBundle\Entity\PurchaseOrder 
	 */
	public function getPurchaseOrder()
	{
		return $this->purchaseOrder;
	}

	/**
	 * Set product
	 *
	 * @param \CI\InventoryBundle\Entity\Product $product
	 * @return PurchaseOrderItem
	 */
	public function setProduct(\CI\InventoryBundle\Entity\Product $product = null)
	{
		$this->product = $product;
	
		return $this;
	}

	/**
	 * Get product
	 *
	 * @return \CI\InventoryBundle\Entity\Product 
	 */
	public function getProduct()
	{
		return $this->product;
	}
	
	/**
	 * Add receiveOrderItems
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrderItem $receiveOrderItems
	 * @return PurchaseOrderItem
	 */
	public function addReceiveOrderItem(\CI\InventoryBundle\Entity\ReceiveOrderItem $receiveOrderItems)
	{
		$this->receiveOrderItems[] = $receiveOrderItems;
	
		return $this;
	}
	
	/**
	 * Remove receiveOrderItems
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrderItem $receiveOrderItems
	 */
	public function removeReceiveOrderItem(\CI\InventoryBundle\Entity\ReceiveOrderItem $receiveOrderItems)
	{
		$this->receiveOrderItems->removeElement($receiveOrderItems);
	}
	
	/**
	 * Get receiveOrderItems
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getReceiveOrderItems()
	{
		return $this->receiveOrderItems;
	}
	
	public function getTotalReceived($excludeReceiveItem = null)
	{
		$totalReceived = 0;
	
		foreach ($this->getReceiveOrderItems() as $item) {
			if ($excludeReceiveItem && $excludeReceiveItem->getId() == $item->getId()) {
				continue;
			}
	
			if ($item->getReceiveOrder()->getActive() == true) {
				$totalReceived += $item->getQuantity();
			}
		}
	
		return $totalReceived;
	}

	public function getTotalReceivedFromRO($excludeReceiveOrder = null)
	{
		$totalReceived = 0;
	
		foreach ($this->getReceiveOrderItems() as $item) {
			if ($excludeReceiveOrder && $excludeReceiveOrder->getId() == $item->getReceiveOrder()->getId()) {
				continue;
			}
	
			if ($item->getReceiveOrder()->getActive() == true) {
				$totalReceived += $item->getQuantity();
			}
		}
	
		return $totalReceived;
	}
	
	public function getMaxQtyAllowedToReceive(ReceiveOrderItem $roItem = null) 
	{
		return $this->getQuantity() - $this->getTotalReceived($roItem);
	}

	public function getMaxQtyAllowedToReceiveFromRO(ReceiveOrder $ro = null) 
	{
		return $this->getQuantity() - $this->getTotalReceivedFromRO($ro);
	}
	
	public function getLog()
	{
		return array(
			'Quantity' => number_format($this->getQuantity(), 2) . ' kg',
			'Product' => $this->getProduct()->getSku() . ' ' . $this->getProduct()->getName(),
			'Contract No.' => $this->getContractNumber(),
			'Unit Cost' => number_format($this->getUnitCost(), 2),
			'Line Total' => number_format($this->getLineTotal(), 2)
		);
	}
}