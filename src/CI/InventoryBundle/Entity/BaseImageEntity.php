<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * BaseImageEntity
 * 
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
abstract class BaseImageEntity extends BaseEntity
{	
	const BASE_UPLOAD_DIR = 'uploads';
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $image;
	
	/**
	 * @Assert\File(maxSize="10000000")
	 *
	 */
	private $file;
	
	private $prevAbsoluteImagePath;
	
	private $deleteImage;
	
	public function getImage()
	{
		return $this->image;
	}
	
	/**
	 * Set image
	 *
	 * @param string $image
	 * @return BaseImageEntity
	 */
	public function setImage($image)
	{
		$this->image = $image;
	
		return $this;
	}
	
	/**
	 * Sets file.
	 *
	 * @param UploadedFile $file
	 */
	public function setFile(UploadedFile $file = null)
	{
		$this->file = $file;
	}
	
	/**
	 * Get file.
	 *
	 * @return UploadedFile
	 */
	public function getFile()
	{
		return $this->file;
	}
	
	/**
	 * Sets deleteImage
	 * 
	 * @param boolean $deleteImage
	 */
	public function setDeleteImage($deleteImage)
	{
		$this->deleteImage = $deleteImage;
		
		return $this;
	}
	
	public function getDeleteImage()
	{
		return $this->deleteImage;
	}
	
	abstract public function getBaseEntityImageDir();
	
	protected function getUploadDir()
	{
		return self::BASE_UPLOAD_DIR . '/' . $this->getBaseEntityImageDir() . '/';
	}
	
	protected function getUploadRootDir()
	{
		return __DIR__ . '/../../../../web/' . $this->getUploadDir();
	}
	
	public function getAbsoluteImagePath()
	{
		return null === $this->image ? null : $this->getUploadRootDir() . $this->image;
	}
	
	public function getWebImagePath()
	{
		return null === $this->image ? null : $this->getUploadDir() . $this->image;
	}
	
	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function preUpload()
	{
		if (!is_null($this->file)) {
			// store the old absolute image path to delete after the update
			$this->prevAbsoluteImagePath = $this->getAbsoluteImagePath();
			
			// generate image filename
			$ext = explode('.', $this->file->getClientOriginalName());
			$ext = $ext[count($ext)-1];
			$this->image = uniqid() . '.' . $ext;
			
		} else if ($this->deleteImage) {
			// store the old absolute image path to delete after the update
			$this->prevAbsoluteImagePath = $this->getAbsoluteImagePath();
			// clear the image
			$this->image = null;
		}
	}
	
	/**
	 * @ORM\PostPersist
	 * @ORM\PostUpdate
	 */
	public function upload()
	{	
		// the file property can be empty if the field is not required
		if (!is_null($this->file)) {
			$this->file->move($this->getUploadRootDir(), $this->image);
			
			// delete previously uploaded image if exists
			if (isset($this->prevAbsoluteImagePath)) {
				unlink($this->prevAbsoluteImagePath);
			}
			
			$this->file = null;
		} else if ($this->deleteImage) {
			// delete previously uploaded image if exists
			if (isset($this->prevAbsoluteImagePath)) {
				unlink($this->prevAbsoluteImagePath);
			}
		} 
	}
	
	/**
	 * @ORM\PostRemove
	 * @ORM\PreRemove
	 */
	public function removeUpload()
	{
		if (!is_null($this->getId())) {
			if ($file = $this->getAbsoluteImagePath()) {
				unlink($file);
			}
		}
	}
}