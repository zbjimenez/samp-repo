<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * OrderPickingItemRepository
 */
class OrderPickingItemRepository extends EntityRepository
{
	
	public function getOrderPickingItemReports($params = null)
	{
		
		$qb = $this->createQueryBuilder('opi')
			->select('PARTIAL opi.{id}', 'op.id AS opNumber', 's.id', 'sl.warehouseNumber', 'c.shortName', 'p.name', 'opi.lotNumber', 
				'opi.productionDate', 'opi.palletId', 'sl.fullLocation', 'opi.quantity', 'op.date')
			->join('opi.orderPicking', 'op')
			->join('op.shipping', 's')
			->join('s.salesOrder', 'so')
			->join('so.customer', 'c')
			->join('opi.shippingItem', 'si')
			->join('si.salesOrderItem', 'soi')
			->join('soi.quotation', 'q')
			->join('q.product', 'p')
			->join('opi.storageLocation', 'sl')
		;

		if (!empty($params)) {
			if ($params['warehouse']) {
				$qb->andWhere('sl.warehouseNumber = :warehouse')
				->setParameter('warehouse', $params['warehouse']);
			}

			if ($params['customer']) {
				$qb->andWhere('c.id = :customer')
				->setParameter('customer', $params['customer']->getId());
			}

			if ($params['date']) {
				$qb->andWhere('op.date = :date')
				->setParameter('date', $params['date']->format('Y-m-d'));
			}
		}
		
		$qb->addOrderBy('opi.id', 'ASC');
		
		return $qb->getQuery();
	}

	public function findLotNumbers($productId, $shippingId)
	{
		$qb = $this->createQueryBuilder('opi')
			->select('opi.id', 'opi.lotNumber AS label')
			->join('opi.shippingItem', 'si')
			->join('si.shipping', 's', 'WITH', 's.id = :shippingId')
			->join('si.salesOrderItem', 'soi')
			->join('soi.quotation', 'q')
			->join('q.product', 'p', 'WITH', 'p.id = :productId')
			->setParameter('productId', $productId)
			->setParameter('shippingId', $shippingId);

		return $qb->getQuery()->getScalarResult();
	}
}