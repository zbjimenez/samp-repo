<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * IndustryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class IndustryRepository extends EntityRepository
{
	public function find($id)
	{
		try {
			return $this->createQueryBuilder('i')
				->select('i')
				->where('i.id = :id')
				->setParameter('id', $id)
				->getQuery()
				->getSingleResult();
		} catch (NoResultException $e) {
			throw new NotFoundHttpException('Unable to find Industry #' . $id . '.');
		}
	}

	public function findAll($params = null)
	{
		$qb = $this->createQueryBuilder('i')
			->select('i');
	
		if (!empty($params)) {
			if ($params['query']) {
				$qb->where('REGEXP(:regexp, i.name) = 1')
					->setParameter('regexp', '[[:<:]](' . preg_quote($params['query']) . ')');
			}
				
			if (!is_null($params['status'])) {
				$qb->andWhere('i.active = :active')
					->setParameter('active', $params['status']);
			}
		} else {
			$qb->where('i.active = 1');
		}
		
		$qb->orderBy('i.name');
		
		return $qb->getQuery();
	}

	public function findAllQb()
	{
		return $this->createQueryBuilder('i')
		->select('i')
		->where('i.active = 1')
		->orderBy('i.name');
	}
}
