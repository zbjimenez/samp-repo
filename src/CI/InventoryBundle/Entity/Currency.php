<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Currency
 *
 * @ORM\Table(name="currency")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\CurrencyRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="currencyName", message="This Currency name is already in use.")
 */
class Currency extends BaseEntity
{
	/**
	 * @var string
	 *
	 * @ORM\Column(name="currency_name", type="string", length=255)
	 * @Assert\NotBlank(message="Currency Name must not be blank.")
	 * @Assert\Length(max=255, maxMessage="Input must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 */
	private $currencyName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="currency_code", type="string", length=3)
	 * @Assert\NotBlank(message="Currency Code must not be blank.")
	 * @Assert\Length(max=3, maxMessage="Input must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 */
	private $currencyCode;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_foreign", type="boolean")
	 */
	private $isForeign;
	
	/**
	 * @ORM\OneToMany(targetEntity="PurchaseOrder", mappedBy="currency", cascade={"persist", "remove"})
	 */
	private $purchaseOrders;

	/**
	 * @ORM\OneToMany(targetEntity="Quotation", mappedBy="currency", cascade={"persist", "remove"})
	 */
	private $quotations;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->isForeign = false;
		$this->purchaseOrders = new \Doctrine\Common\Collections\ArrayCollection();
		$this->quotations = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set currencyName
	 *
	 * @param string $currencyName
	 * @return Currency
	 */
	public function setCurrencyName($currencyName)
	{
		$this->currencyName = $currencyName;
	
		return $this;
	}

	/**
	 * Get currencyName
	 *
	 * @return string 
	 */
	public function getCurrencyName()
	{
		return $this->currencyName;
	}

	/**
	 * Set currencyCode
	 *
	 * @param string $currencyCode
	 * @return Currency
	 */
	public function setCurrencyCode($currencyCode)
	{
		$this->currencyCode = $currencyCode;
	
		return $this;
	}

	/**
	 * Get currencyCode
	 *
	 * @return string 
	 */
	public function getCurrencyCode()
	{
		return $this->currencyCode;
	}

	/**
     * Set isForeign
     *
     * @param boolean $isForeign
     * @return Currency
     */
    public function setIsForeign($isForeign)
    {
    	$this->isForeign = $isForeign;
    
    	return $this;
    }
    
    /**
     * Get isForeign
     *
     * @return boolean
     */
    public function getIsForeign()
    {
    	return $this->isForeign;
    }
	
	/**
	 * Add purchaseOrders
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders
	 * @return Currency
	 */
	public function addPurchaseOrder(\CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders)
	{
		$this->purchaseOrders[] = $purchaseOrders;
	
		return $this;
	}
	
	/**
	 * Remove purchaseOrders
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders
	 */
	public function removePurchaseOrder(\CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders)
	{
		$this->purchaseOrders->removeElement($purchaseOrders);
	}
	
	/**
	 * Get quotations
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getPurchaseOrders()
	{
		return $this->purchaseOrders;
	}

	/**
	 * Add quotations
	 *
	 * @param \CI\InventoryBundle\Entity\Quotation $quotations
	 * @return Currency
	 */
	public function addQuotation(\CI\InventoryBundle\Entity\Quotation $quotations)
	{
		$this->quotations[] = $quotations;
	
		return $this;
	}
	
	/**
	 * Remove quotations
	 *
	 * @param \CI\InventoryBundle\Entity\Quotation $quotations
	 */
	public function removeQuotation(\CI\InventoryBundle\Entity\Quotation $quotations)
	{
		$this->quotations->removeElement($quotations);
	}
	
	/**
	 * Get quotations
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getQuotations()
	{
		return $this->quotations;
	}

	public function isDeletable()
	{
		if ($this->getPurchaseOrders()->count() > 0) {
			return false;
		}

		if ($this->getQuotations()->count() > 0) {
			return false;
		}
		
		return true;
	}

	public function translateLocality()
	{
		if ($this->getIsForeign()) {
			return 'Foreign';
		} else {
			return 'Local';
		}
	}
	
	public function getLog()
	{
		return array(
			'Currency Name' => $this->getCurrencyName(),
			'Currency Code' => $this->getCurrencyCode(),
			'Status' => $this->getActive()? 'Active': 'Inactive',
			'Locality' => $this->translateLocality()
		);
	}
}