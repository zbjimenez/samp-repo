<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Industry
 *
 * @ORM\Table(name="industry")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\IndustryRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="name", message="This Industry name is already in use.")
 */
class Industry extends BaseEntity
{
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Assert\NotBlank(message="Name must not be blank.")
	 * @Assert\Length(max=255, maxMessage="Input must be less than {{ limit }} characters long.")
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @ORM\OneToMany(targetEntity="Product", mappedBy="industry", cascade={"persist", "remove"})
	 */
	private $products;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->products = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Industry
	 */
	public function setName($name)
	{
		$this->name = $name;
	
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string 
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Industry
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	
		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string 
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Add products
	 *
	 * @param \CI\InventoryBundle\Entity\Product $products
	 * @return Industry
	 */
	public function addProduct(\CI\InventoryBundle\Entity\Product $products)
	{
		$this->products[] = $products;
	
		return $this;
	}

	/**
	 * Remove products
	 *
	 * @param \CI\InventoryBundle\Entity\Product $products
	 */
	public function removeProduct(\CI\InventoryBundle\Entity\Product $products)
	{
		$this->products->removeElement($products);
	}

	/**
	 * Get products
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getProducts()
	{
		return $this->products;
	}

	public function isDeletable()
	{
		if ($this->getProducts()->count() > 0) {
			return false;
		}
		
		return true;
	}

	public function getLog()
	{
		return array(
			'Name' => $this->getName(),
			'Description' => $this->getDescription(),
			'Status' => $this->isActive() ? 'Active' : 'Inacticve',
		);
	}
}
