<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PurchaseOrder
 *
 * @ORM\Table(name="purchase_order")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\PurchaseOrderRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 * @UniqueEntity(fields="poId", message="This PO # is already in use.")
 */
class PurchaseOrder extends BaseEntity
{
	const STATUS_DRAFT = 10; 
	const STATUS_APPROVED = 20;
	const STATUS_VOID = 30;
	const STATUS_CLOSED = 40;
	
	const SHIPPING_STATUS_PENDING = 15;
	const SHIPPING_STATUS_ON_PROCESS = 17;
	const SHIPPING_STATUS_RECEIVING = 19;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="status", type="integer", length=2)
	 * @Assert\Choice(choices = {"10", "20", "30", "40"})
	 * @Assert\Type(type="int")
	 * @Assert\Range(max=40)
	 */
	private $status;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="shipping_status", type="integer", length=2)
	 * @Assert\Choice(choices = {"15", "17", "19"})
	 * @Assert\Type(type="int")
	 * @Assert\Range(max=24)
	 */
	private $shippingStatus;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="po_id", type="string", length=255, unique=true)
	 * @Assert\NotBlank(message="PO # must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $poId;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="cn_pf", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $cnPf;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="invoice_number", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $invoiceNumber;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="container_number", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $containerNumber;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="seal_number", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $sealNumber;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="po_date", type="date")
	 * @Assert\NotBlank(message="PO date must not be blank.")
	 * @Assert\Date()
	 */
	private $poDate;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="delivery_date", type="date", nullable=true)
	 * @Assert\Date()
	 */
	private $deliveryDate;
	
	/**
	 * @ORM\Column(name="terms", type="integer")
	 * @Assert\NotBlank(message="Terms must not be blank.")
	 * @Assert\Type(type="integer")
	 * @Assert\Range(min=0, max=999)
	 */
	private $terms;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="shipment_advice", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $shipmentAdvice;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="memo", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $memo;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="total", type="decimal", scale=2, precision=13)
	 */
	private $total;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="adjustment_total", type="decimal", scale=2, precision=13)
	 */
	private $adjustmentTotal;

	/**
	 * @ORM\ManyToOne(targetEntity="Supplier", inversedBy="purchaseOrders")
	 * @Assert\NotBlank(message="Supplier must not be blank.")
	 */
	private $supplier;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Currency", inversedBy="purchaseOrders")
	 * @Assert\NotBlank(message="Currency must not be blank.")
	 */
	private $currency;
	
	/**
	 * @ORM\OneToMany(targetEntity="PurchaseOrderItem", mappedBy="purchaseOrder", cascade={"persist", "remove"})
	 * @Assert\Count(min="1", minMessage="You must add at least one item.")
	 * @Assert\Valid
	 */
	private $items;
	
	/**
	 * @ORM\OneToMany(targetEntity="Adjustment", mappedBy="purchaseOrder", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $adjustments;
	
	/**
	 * @ORM\OneToMany(targetEntity="ReceiveOrder", mappedBy="purchaseOrder", cascade={"persist", "remove"})
	 */
	private $receiveOrders;

	/**
	 * @ORM\OneToMany(targetEntity="PurchaseOrderFile", mappedBy="purchaseOrder", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;

	/**
	 * @ORM\ManyToOne(targetEntity="Warehouse", inversedBy="purchaseOrders")
	 * @Assert\NotBlank(message="Warehouse must not be blank.")
	 */
	private $warehouse;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->poDate = new \DateTime();
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
		$this->adjustments = new \Doctrine\Common\Collections\ArrayCollection();
		$this->receiveOrders = new \Doctrine\Common\Collections\ArrayCollection();
		$this->files = new \Doctrine\Common\Collections\ArrayCollection();
		$this->total = 0;
		$this->adjustmentTotal = 0;
		$this->terms = 0;
		$this->shippingStatus = self::SHIPPING_STATUS_PENDING;
	}

	/**
	 * Set status
	 *
	 * @param integer $status
	 * @return PurchaseOrder
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	
		return $this;
	}

	/**
	 * Get status
	 *
	 * @return integer 
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set poId
	 *
	 * @param string $poId
	 * @return PurchaseOrder
	 */
	public function setPoId($poId)
	{
		$this->poId = $poId;
	
		return $this;
	}

	/**
	 * Get poId
	 *
	 * @return string 
	 */
	public function getPoId()
	{
		return $this->poId;
	}

	/**
	 * Set cnPf
	 *
	 * @param string $cnPf
	 * @return PurchaseOrder
	 */
	public function setCnPf($cnPf)
	{
		$this->cnPf = $cnPf;
	
		return $this;
	}

	/**
	 * Get cnPf
	 *
	 * @return string 
	 */
	public function getCnPf()
	{
		return $this->cnPf;
	}

	/**
	 * Set invoiceNumber
	 *
	 * @param string $invoiceNumber
	 * @return PurchaseOrder
	 */
	public function setInvoiceNumber($invoiceNumber)
	{
		$this->invoiceNumber = $invoiceNumber;
	
		return $this;
	}

	/**
	 * Get invoiceNumber
	 *
	 * @return string 
	 */
	public function getInvoiceNumber()
	{
		return $this->invoiceNumber;
	}

	/**
	 * Set containerNumber
	 *
	 * @param string $containerNumber
	 * @return PurchaseOrder
	 */
	public function setContainerNumber($containerNumber)
	{
		$this->containerNumber = $containerNumber;
	
		return $this;
	}

	/**
	 * Get containerNumber
	 *
	 * @return string 
	 */
	public function getContainerNumber()
	{
		return $this->containerNumber;
	}

	/**
	 * Set sealNumber
	 *
	 * @param string $sealNumber
	 * @return PurchaseOrder
	 */
	public function setSealNumber($sealNumber)
	{
		$this->sealNumber = $sealNumber;
	
		return $this;
	}

	/**
	 * Get sealNumber
	 *
	 * @return string 
	 */
	public function getSealNumber()
	{
		return $this->sealNumber;
	}

	/**
	 * Set poDate
	 *
	 * @param \DateTime $poDate
	 * @return PurchaseOrder
	 */
	public function setPoDate($poDate)
	{
		$this->poDate = $poDate;
	
		return $this;
	}

	/**
	 * Get poDate
	 *
	 * @return \DateTime 
	 */
	public function getPoDate()
	{
		return $this->poDate;
	}

	/**
	 * Set deliveryDate
	 *
	 * @param \DateTime $deliveryDate
	 * @return PurchaseOrder
	 */
	public function setDeliveryDate($deliveryDate)
	{
		$this->deliveryDate = $deliveryDate;
	
		return $this;
	}

	/**
	 * Get deliveryDate
	 *
	 * @return \DateTime 
	 */
	public function getDeliveryDate()
	{
		return $this->deliveryDate;
	}

	/**
	 * Set terms
	 *
	 * @param integer $terms
	 * @return PurchaseOrder
	 */
	public function setTerms($terms)
	{
		$this->terms = $terms;
	
		return $this;
	}

	/**
	 * Get terms
	 *
	 * @return integer 
	 */
	public function getTerms()
	{
		return $this->terms;
	}

	/**
	 * Set shipmentAdvice
	 *
	 * @param string $shipmentAdvice
	 * @return PurchaseOrder
	 */
	public function setShipmentAdvice($shipmentAdvice)
	{
		$this->shipmentAdvice = $shipmentAdvice;
	
		return $this;
	}

	/**
	 * Get shipmentAdvice
	 *
	 * @return string 
	 */
	public function getShipmentAdvice()
	{
		return $this->shipmentAdvice;
	}

	/**
	 * Set memo
	 *
	 * @param string $memo
	 * @return PurchaseOrder
	 */
	public function setMemo($memo)
	{
		$this->memo = $memo;
	
		return $this;
	}

	/**
	 * Get memo
	 *
	 * @return string 
	 */
	public function getMemo()
	{
		return $this->memo;
	}

	/**
	 * Set total
	 *
	 * @param string $total
	 * @return PurchaseOrder
	 */
	public function setTotal($total)
	{
		$this->total = $total;
	
		return $this;
	}

	/**
	 * Get total
	 *
	 * @return string 
	 */
	public function getTotal()
	{
		return $this->total;
	}

	/**
	 * Set adjustmentTotal
	 *
	 * @param string $adjustmentTotal
	 * @return PurchaseOrder
	 */
	public function setAdjustmentTotal($adjustmentTotal)
	{
		$this->adjustmentTotal = $adjustmentTotal;
	
		return $this;
	}

	/**
	 * Get adjustmentTotal
	 *
	 * @return string 
	 */
	public function getAdjustmentTotal()
	{
		return $this->adjustmentTotal;
	}

	/**
	 * Set supplier
	 *
	 * @param \CI\InventoryBundle\Entity\Supplier $supplier
	 * @return PurchaseOrder
	 */
	public function setSupplier(\CI\InventoryBundle\Entity\Supplier $supplier = null)
	{
		$this->supplier = $supplier;
	
		return $this;
	}

	/**
	 * Get supplier
	 *
	 * @return \CI\InventoryBundle\Entity\Supplier 
	 */
	public function getSupplier()
	{
		return $this->supplier;
	}

	public function setItems(\Doctrine\Common\Collections\Collection $items)
	{
		foreach($items as $item) {
			$item->setPurchaseOrder($this);
		}
		$this->items = $items;
	}

	/**
	 * Get items
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getItems()
	{
		return $this->items;
	}
	
	public function setAdjustments(\Doctrine\Common\Collections\Collection $adjustments)
	{
		foreach($adjustments as $adjustment) {
			$adjustment->setPurchaseOrder($this);
		}
		$this->adjustments = $adjustments;
	}

	/**
	 * Get adjustments
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getAdjustments()
	{
		return $this->adjustments;
	}
	

	/**
	 * Add receiveOrders
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrder $receiveOrders
	 * @return PurchaseOrder
	 */
	public function addReceiveOrder(\CI\InventoryBundle\Entity\ReceiveOrder $receiveOrders)
	{
		$this->receiveOrders[] = $receiveOrders;
	
		return $this;
	}
	
	/**
	 * Remove receiveOrders
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrder $receiveOrders
	 */
	public function removeReceiveOrder(\CI\InventoryBundle\Entity\ReceiveOrder $receiveOrders)
	{
		$this->receiveOrders->removeElement($receiveOrders);
	}
	
	/**
	 * Get receiveOrders
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getReceiveOrders()
	{
		return $this->receiveOrders;
	}

	/**
	 * Add files
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrderFile $files
	 * @return PurchaseOrder
	 */
	public function addFile(\CI\InventoryBundle\Entity\PurchaseOrderFile $files)
	{
		$files->setPurchaseOrder($this);
		$this->files[] = $files;
	
		return $this;
	}
	
	/**
	 * Remove files
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrderFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\PurchaseOrderFile $files)
	{
		$this->files->removeElement($files);
	}
	
	/**
	 * Get files
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * Set warehouse
	 *
	 * @param \CI\InventoryBundle\Entity\Warehouse $warehouse
	 * @return PurchaseOrder
	 */
	public function setWarehouse(\CI\InventoryBundle\Entity\Warehouse $warehouse = null)
	{
		$this->warehouse = $warehouse;
	
		return $this;
	}
	
	/**
	 * Get warehouse
	 *
	 * @return \CI\InventoryBundle\Entity\Warehouse
	 */
	public function getWarehouse()
	{
		return $this->warehouse;
	}
	
	/**
	 * Set shippingStatus
	 *
	 * @param integer $shippingStatus
	 * @return PurchaseOrder
	 */
	public function setShippingStatus($shippingStatus)
	{
		$this->shippingStatus = $shippingStatus;
	
		return $this;
	}
	
	/**
	 * Get shippingStatus
	 *
	 * @return integer
	 */
	public function getShippingStatus()
	{
		return $this->shippingStatus;
	}
	
	/**
	 * Set currency
	 *
	 * @param \CI\InventoryBundle\Entity\Currency $currency
	 * @return PurchaseOrder
	 */
	public function setCurrency(\CI\InventoryBundle\Entity\Currency $currency = null)
	{
		$this->currency = $currency;
	
		return $this;
	}
	
	/**
	 * Get currency
	 *
	 * @return \CI\InventoryBundle\Entity\Currency
	 */
	public function getCurrency()
	{
		return $this->currency;
	}

	public function isEditable()
	{
		if ($this->getStatus() < self::STATUS_VOID && count($this->getActiveReceiveOrders()) == 0) {
			return true;
		}
		return false;
	}
	
	public function isDeletable()
	{
		if ($this->getStatus() == self::STATUS_DRAFT) {
			return true;
		}
		return false;
	}
	
	public function translateStatus()
	{
		switch ($this->getStatus()) {
			case self::STATUS_DRAFT:
				return 'Draft';
			case self::STATUS_APPROVED:
				return 'Approved';
			case self::STATUS_VOID:
				return 'Void';
			case self::STATUS_CLOSED:
				return 'Closed';
			default:
				return 'Error';
		}
	}
	
	public function translateShippingStatus()
	{
		switch ($this->getShippingStatus()) {
			case self::SHIPPING_STATUS_PENDING:
				return 'Pending';
			case self::SHIPPING_STATUS_ON_PROCESS:
				return 'On Process';
			case self::SHIPPING_STATUS_RECEIVING:
				return 'Receiving';
			default:
				return 'Error';
		}
	}
	
	/**
	 * Returns the quantity ordered for all PO items
	 */
	public function getQtyOrdered()
	{
		$quantity = 0;
		foreach($this->getItems() as $item) {
			$quantity += $item->getQuantity();
		}
	
		return $quantity;
	}
	
	/**
	 * Returns the quantity received for all PO items
	 */
	public function getQtyReceived()
	{
		$quantity = 0;
		foreach($this->getItems() as $item) {
			$quantity += $item->getTotalReceived();
		}
	
		return $quantity;
	}
	
	public function process(ExecutionContextInterface $context)
	{
		$grandTotal = $total = $adjustmentTotal = 0;
		
		foreach ($this->getItems() as $item) {
			$lineTotal = round($item->getQuantity() * $item->getUnitCost(), 2, PHP_ROUND_HALF_UP);
			$item->setLineTotal($lineTotal);
			$total += $lineTotal;
		}
		
		foreach ($this->getAdjustments() as $adj) {
			$adjustmentTotal += $adj->getAmount();
		}
		
		$this->setAdjustmentTotal($adjustmentTotal);
		
		$grandTotal = $total - $adjustmentTotal;
		$this->setTotal($grandTotal);
	}
	
	public function getSubtotal()
	{
		return $this->getTotal() + $this->getAdjustmentTotal();
	}
	
	public function getActiveReceiveOrders()
	{
		$receiveOrders = $this->getReceiveOrders();
		$criteria = Criteria::create()->where(Criteria::expr()->eq('active', true));
	
		return $receiveOrders->matching($criteria);;
	}
	
	public function getLog()
	{
		return array(
			'PO #' => $this->getPoId(),
			'Supplier' => $this->getSupplier()->getName(),
			'Currency' => $this->getCurrency()->getCurrencyName(),
			'CN/PF #' => $this->getCnPf(),
			'Invoice #' => $this->getInvoiceNumber(),
			'Container #' => $this->getContainerNumber(),
			'Seal #' => $this->getSealNumber(),
			'PO Date' => $this->getPoDate(),
			'Delivery Date' => $this->getDeliveryDate(),
			'Terms' => $this->getTerms() . ' days',
			'Shipment Advice' => $this->getShipmentAdvice(),
			'Memo' => $this->getMemo(),
			'Status' => $this->translateStatus(),
			'Shipping Status' => $this->translateShippingStatus(),
			'Total' => number_format($this->getTotal(), 2)
		);
	}
}