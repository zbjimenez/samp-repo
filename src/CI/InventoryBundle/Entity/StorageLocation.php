<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * StorageLocation
 *
 * @ORM\Table(name="storage_location")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\StorageLocationRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 * @UniqueEntity(fields="fullLocation", message="Duplicate storage location. Please make sure that the combination of warehouse number, lane, row and high level number is unique")
 */
class StorageLocation extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="Warehouse", inversedBy="storageLocations")
	 * @Assert\NotBlank(message="Warehouse must not be blank.")
	 */
	private $warehouse;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="warehouse_number", type="string", length=255)
	 * @Assert\NotBlank(message="Warehouse Number must not be blank.")
	 * @Assert\Length(max=255, maxMessage="Input must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 */
	private $warehouseNumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lane", type="string", length=255)
	 * @Assert\NotBlank(message="Lane must not be blank.")
	 * @Assert\Length(max=255, maxMessage="Input must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 */
	private $lane;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="row", type="string", length=255)
	 * @Assert\NotBlank(message="Row must not be blank.")
	 * @Assert\Length(max=255, maxMessage="Input must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 */
	private $row;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="high_level_number", type="string", length=255)
	 * @Assert\NotBlank(message="High Level Number must not be blank.")
	 * @Assert\Length(max=255, maxMessage="Input must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 */
	private $highLevelNumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="full_location", type="string", length=255)
	 * @Assert\Type(type="string")
	 */
	private $fullLocation;

	/**
	 * @ORM\OneToMany(targetEntity="Inventory", mappedBy="storageLocation", cascade={"persist", "remove"})
	 */
	private $inventories;

	/**
	 * @ORM\OneToMany(targetEntity="OrderPickingItem", mappedBy="storageLocation")
	 */
	private $orderPickingItems;

	/**
	 * @ORM\OneToMany(targetEntity="PutAwayItem", mappedBy="storageLocation")
	 */
	private $putAwayItems;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->inventories = new \Doctrine\Common\Collections\ArrayCollection();
		$this->orderPickingItems = new \Doctrine\Common\Collections\ArrayCollection();
		$this->putAwayItems = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set warehouse
	 *
	 * @param \CI\InventoryBundle\Entity\Warehouse $warehouse
	 * @return StorageLocation
	 */
	public function setWarehouse(\CI\InventoryBundle\Entity\Warehouse $warehouse = null)
	{
		$warehouse->addStorageLocation($this);
		$this->warehouse = $warehouse;
	
		return $this;
	}

	/**
	 * Get warehouse
	 *
	 * @return \CI\InventoryBundle\Entity\Warehouse 
	 */
	public function getWarehouse()
	{
		return $this->warehouse;
	}

	/**
	 * Set warehouseNumber
	 *
	 * @param string $warehouseNumber
	 * @return StorageLocation
	 */
	public function setWarehouseNumber($warehouseNumber)
	{
		$this->warehouseNumber = $warehouseNumber;
	
		return $this;
	}

	/**
	 * Get warehouseNumber
	 *
	 * @return string 
	 */
	public function getWarehouseNumber()
	{
		return $this->warehouseNumber;
	}

	/**
	 * Set lane
	 *
	 * @param string $lane
	 * @return StorageLocation
	 */
	public function setLane($lane)
	{
		$this->lane = $lane;
	
		return $this;
	}

	/**
	 * Get lane
	 *
	 * @return string 
	 */
	public function getLane()
	{
		return $this->lane;
	}

	/**
	 * Set row
	 *
	 * @param string $row
	 * @return StorageLocation
	 */
	public function setRow($row)
	{
		$this->row = $row;
	
		return $this;
	}

	/**
	 * Get row
	 *
	 * @return string 
	 */
	public function getRow()
	{
		return $this->row;
	}

	/**
	 * Set highLevelNumber
	 *
	 * @param string $highLevelNumber
	 * @return StorageLocation
	 */
	public function setHighLevelNumber($highLevelNumber)
	{
		$this->highLevelNumber = $highLevelNumber;
	
		return $this;
	}

	/**
	 * Get highLevelNumber
	 *
	 * @return string 
	 */
	public function getHighLevelNumber()
	{
		return $this->highLevelNumber;
	}

	/**
	 * Set fullLocation
	 *
	 * @param string $fullLocation
	 * @return StorageLocation
	 */
	public function setFullLocation($fullLocation)
	{
		$this->fullLocation = $fullLocation;
	
		return $this;
	}

	/**
	 * Get fullLocation
	 *
	 * @return string 
	 */
	public function getFullLocation()
	{
		return $this->fullLocation;
	}

	/**
	 * Add inventories
	 *
	 * @param \CI\InventoryBundle\Entity\Inventory $inventories
	 * @return Storagelocation
	 */
	public function addInventories(\CI\InventoryBundle\Entity\Inventory $inventories)
	{
		$this->inventories[] = $inventories;
	
		return $this;
	}

	/**
	 * Remove inventories
	 *
	 * @param \CI\InventoryBundle\Entity\Inventory $inventories
	 */
	public function removeInventories(\CI\InventoryBundle\Entity\Inventory $inventories)
	{
		$this->inventories->removeElement($inventories);
	}

	/**
	 * Get inventories
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getInventories()
	{
		return $this->inventories;
	}
	
	/**
	 * Get orderPickingItems
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getOrderPickingItems()
	{
		return $this->orderPickingItems;
	}
	
	/**
	 * Check if orderPickingItems are all picked up
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function checkOrderPickingItems()
	{
		foreach ($this->getOrderPickingItems() as $orderPickingItem) {
			if (!$orderPickingItem->getIsPicked()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Add putAwayItems
	 *
	 * @param \CI\InventoryBundle\Entity\PutAwayItem $putAwayItems
	 * @return Storagelocation
	 */
	public function addPutAwayItems(\CI\InventoryBundle\Entity\PutAwayItem $putAwayItems)
	{
		$this->putAwayItems[] = $putAwayItems;
	
		return $this;
	}

	/**
	 * Remove putAwayItems
	 *
	 * @param \CI\InventoryBundle\Entity\PutAwayItem $putAwayItems
	 */
	public function removePutAwayItems(\CI\InventoryBundle\Entity\PutAwayItem $putAwayItems)
	{
		$this->putAwayItems->removeElement($putAwayItems);
	}

	/**
	 * Get putAwayItems
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getPutAwayItems()
	{
		return $this->putAwayItems;
	}
	
	/**
	 * Check if PutAwayItems are all completed
	 * Assumes that put aways are always either void or 
	 * succesfully put away
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function checkPutAwayItems()
	{
		return true;
	}

	public function process(ExecutionContextInterface $context)
	{
		$this->setFullLocation($this->getWarehouseNumber() . $this->getLane() . $this->getRow() . $this->getHighLevelNumber());
	}

	public function isDeletable()
	{
		return true;//for now
	}
	
	public function getLog()
	{
		return array(
			'Warehouse Number' => $this->getWarehouseNumber(),
			'Lane' => $this->getLane(),
			'Row' => $this->getRow(),
			'High Level Number' => $this->getHighLevelNumber(),
			'Status' => $this->getActive() ? 'Active' : 'Inactive',
		);
	}
}
