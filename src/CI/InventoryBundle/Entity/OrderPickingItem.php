<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

use CI\InventoryBundle\DependencyInjection\InventoryServiceItemInterface;

/**
 * OrderPickingItem
 *
 * @ORM\Table(name="order_picking_item")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\OrderPickingItemRepository")
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"validate"})
 */
class OrderPickingItem implements InventoryServiceItemInterface
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="quantity", type="decimal", precision=11, scale=2)
	 * @Assert\NotBlank(message="Quantity must not be blank.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $quantity;

	/**
	 * @ORM\Column(name="lot_number", type="string", length=255)
	 * @Assert\NotBlank(message="Lot number must not be blank.")
	 */
	private $lotNumber;

	/**
	 * @ORM\Column(name="pallet_id", type="string", length=255)
	 * @Assert\NotBlank(message="Pallet ID must not be blank.")
	 */
	private $palletId;

	/**
	 * @var date
	 *
	 * @ORM\Column(name="production_date", type="date")
	 * @Assert\Date()
	 * @Assert\NotBlank(message="Production date must not be blank.")
	 */
	private $productionDate;

	/**
	 * @ORM\Column(name="is_picked", type="boolean")
	 */
	private $isPicked;

	/**
	 * @ORM\ManyToOne(targetEntity="StorageLocation", inversedBy="orderPickingItems")
	 * @Assert\NotBlank(message="Storage Location must not be blank.")
	 */
	private $storageLocation;

	/**
	 * @ORM\ManyToOne(targetEntity="OrderPicking", inversedBy="items")
	 */
	private $orderPicking;

	/**
	 * @ORM\ManyToOne(targetEntity="ShippingItem", inversedBy="orderPickingItems")
	 * @Assert\NotBlank(message="Product must not be blank.")
	 */
	private $shippingItem;

	public function __construct()
	{
		$this->isPicked = false;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set quantity
	 *
	 * @param string $quantity
	 * @return OrderPickingItem
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	
		return $this;
	}

	/**
	 * Get quantity
	 *
	 * @return string 
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * Set orderPicking
	 *
	 * @param \CI\InventoryBundle\Entity\OrderPicking $orderPicking
	 * @return OrderPickingItem
	 */
	public function setOrderPicking(\CI\InventoryBundle\Entity\OrderPicking $orderPicking = null)
	{
		$this->orderPicking = $orderPicking;
	
		return $this;
	}

	/**
	 * Get orderPicking
	 *
	 * @return \CI\InventoryBundle\Entity\OrderPicking 
	 */
	public function getOrderPicking()
	{
		return $this->orderPicking;
	}

	/**
	 * Set shippingItem
	 *
	 * @param \CI\InventoryBundle\Entity\ShippingItem $shippingItem
	 * @return OrderPickingItem
	 */
	public function setShippingItem(\CI\InventoryBundle\Entity\ShippingItem $shippingItem = null)
	{
		$this->shippingItem = $shippingItem;
	
		return $this;
	}

	/**
	 * Get shippingItem
	 *
	 * @return \CI\InventoryBundle\Entity\ShippingItem 
	 */
	public function getShippingItem()
	{
		return $this->shippingItem;
	}

	/**
	 * Set lotNumber
	 *
	 * @param string $lotNumber
	 * @return OrderPickingItem
	 */
	public function setLotNumber($lotNumber)
	{
		$this->lotNumber = $lotNumber;
	
		return $this;
	}

	/**
	 * Get lotNumber
	 *
	 * @return string 
	 */
	public function getLotNumber()
	{
		return $this->lotNumber;
	}

	/**
	 * Set palletId
	 *
	 * @param string $palletId
	 * @return OrderPickingItem
	 */
	public function setPalletId($palletId)
	{
		$this->palletId = $palletId;
	
		return $this;
	}

	/**
	 * Get palletId
	 *
	 * @return string 
	 */
	public function getPalletId()
	{
		return $this->palletId;
	}

	/**
	 * Set storageLocation
	 *
	 * @param \CI\InventoryBundle\Entity\StorageLocation $storageLocation
	 * @return OrderPickingItem
	 */
	public function setStorageLocation(\CI\InventoryBundle\Entity\StorageLocation $storageLocation = null)
	{
		$this->storageLocation = $storageLocation;
	
		return $this;
	}

	/**
	 * Get storageLocation
	 *
	 * @return \CI\InventoryBundle\Entity\StorageLocation 
	 */
	public function getStorageLocation()
	{
		return $this->storageLocation;
	}

	/**
	 * Set productionDate
	 *
	 * @param \DateTime $productionDate
	 * @return OrderPickingItem
	 */
	public function setProductionDate($productionDate)
	{
		$this->productionDate = $productionDate;
	
		return $this;
	}

	/**
	 * Get productionDate
	 *
	 * @return \DateTime 
	 */
	public function getProductionDate()
	{
		return $this->productionDate;
	}

	/**
	 * Set isPicked
	 *
	 * @param boolean $isPicked
	 * @return OrderPickingItem
	 */
	public function setIsPicked($isPicked)
	{
		$this->isPicked = $isPicked;
	
		return $this;
	}

	/**
	 * Get isPicked
	 *
	 * @return boolean 
	 */
	public function getIsPicked()
	{
		return $this->isPicked;
	}

	public function getISProduct() 
	{
		return $this->getShippingItem()->getSalesOrderItem()->getQuotation()->getProduct();
	}

	public function getISQuantity()
	{
		return $this->getQuantity();
	}

	public function validate(ExecutionContextInterface $context)
	{
		if (!$this->getId() || $this->getIsPicked() === false) {
			$limit = $this->getShippingItem()->getMaxQtyAllowedToPick($this);

			if ($this->getQuantity() > $limit) {
				$context->addViolationAt('quantity', 'Limit reached.');
			}
		}
	}

	public function getLog()
	{
		return array(
			'Quantity' => $this->getQuantity(),
			'Lot Number' => $this->getLotNumber(),
			'Pallet ID' => $this->getPalletId(),
			'Production Date' => $this->getProductionDate(),
			'Storage Location' => $this->getStorageLocation()->getFullLocation(),
			'Is Picked' => $this->getIsPicked() ? 'Yes' : 'No',
		);
	}
}