<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * ReceiveOrderRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ReceiveOrderRepository extends EntityRepository
{
	public function find($id)
	{
		try {
			return $this->createQueryBuilder('ro')
				->select('ro')
				->where('ro.id = :id')
				->setParameter('id', $id)
				->getQuery()
				->getSingleResult();
		} catch (NoResultException $e) {
			throw new NotFoundHttpException('Unable to find Receive Order #' . $id . '.');
		}
	}

	public function load($id)
	{
		try {
			return $this->createQueryBuilder('ro')
				->select('ro', 'roi', 'po', 'poi', 'p', 'pr')
				->join('ro.purchaseOrder', 'po')
				->join('ro.items', 'roi')
				->join('roi.purchaseOrderItem', 'poi')
				->join('poi.product', 'p')
				->leftJoin('ro.purchaseReturns', 'pr')
				->where('ro.id = :id')
				->setParameter('id', $id)
				->getQuery()
				->getSingleResult();
		} catch (NoResultException $e) {
			throw new NotFoundHttpException('Unable to find Receive Order #' . $id . '.');
		}
	}

	public function loadAll($params = null)
	{
		$qb = $this->createQueryBuilder('ro')
			->select('ro', 'po')
			->join('ro.purchaseOrder', 'po');
	
		if (!empty($params)) {
			if ($params['roId']) {
				$qb->andWhere("REGEXP(:roId, ro.id) = 1")
					->setParameter('roId', '[[:<:]](' . preg_quote($params['roId']) . ')');
			}

			if ($params['poId']) {
				$qb->andWhere("REGEXP(:poId, po.poId) = 1")
					->setParameter('poId', '[[:<:]](' . preg_quote($params['poId']) . ')');
			}
			
			if ($params['referenceNumber']) {
				$qb->andWhere("REGEXP(:referenceNumber, ro.referenceNumber) = 1")
					->setParameter('referenceNumber', '[[:<:]](' . preg_quote($params['referenceNumber']) . ')');
			}
	
			if (isset($params['status'])) {
				$qb->andWhere('ro.active = :status')
					->setParameter('status', $params['status']);
			}
	
			if ($params['dateFrom']) {
				$qb->andWhere('ro.dateReceived >= :dateFrom')
					->setParameter('dateFrom', $params['dateFrom']->format('Y-m-d'));
			}
	
			if ($params['dateTo']) {
				$qb->andWhere('ro.dateReceived <= :dateTo')
					->setParameter('dateTo', $params['dateTo']->format('Y-m-d'));
			}
		}
	
		$qb->orderBy('ro.dateReceived');
	
		return $qb->getQuery();
	}
}
