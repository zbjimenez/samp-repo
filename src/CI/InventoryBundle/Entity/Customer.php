<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Customer.
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\CustomerRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"validate"})
 * @UniqueEntity(fields="name", message="This name is already in use.")
 * @UniqueEntity(fields="shortName", message="This short name is already in use.")
 */
class Customer extends BaseEntity
{
	const EWT_NONE = '0';
	const EWT_GOODS = '0.01';
	const EWT_SERVICES = '0.02';

	/**
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Name must not be blank.")
	 */
	private $name;

	/**
	 * @ORM\Column(name="short_name", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Short name must not be blank.")
	 */
	private $shortName;

	/**
	 * @ORM\Column(name="contact_details", type="text")
	 * @Assert\NotBlank(message="Contact details must not be blank.")
	 * @Assert\Type(type="string")
	 */
	private $contactDetails;

	/**
	 * @ORM\Column(name="terms", type="integer")
	 * @Assert\NotBlank(message="Terms must not be blank.")
	 * @Assert\Type(type="integer")
	 * @Assert\Range(min=0, max=999)
	 */
	private $terms;

	/**
	 * @ORM\Column(name="tin", type="string", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $tin;

	/**
	 * @ORM\Column(name="memo", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $memo;

	/**
	 * @ORM\Column(name="ewt", type="decimal", precision=3, scale=2)
	 * @Assert\NotBlank(message="Must select an EWT.")
	 * @Assert\Choice(choices={0, 0.01, 0.02}, message="Invalid EWT selected.")
	 */
	private $ewt;

	/**
	 * @ORM\ManyToOne(targetEntity="CI\CoreBundle\Entity\User", inversedBy="customers")
	 * @ORM\JoinColumn(name="salesAgent_id", referencedColumnName="id", nullable=false)
	 * @Assert\NotBlank(message="Sales agent must not be blank.")
	 */
	private $salesAgent;

	/**
	 * @ORM\OneToMany(targetEntity="Branch", mappedBy="customer", cascade={"persist", "remove"})
	 * @Assert\Count(min="1", minMessage="You must add at least one branch.")
	 * @ORM\OrderBy({"name"="ASC"})
	 * @Assert\Valid
	 */
	private $branches;

	/**
	 * @ORM\OneToMany(targetEntity="SalesActivityItem", mappedBy="customer", cascade={"persist", "remove"})
	 */
	private $salesActivityItems;

	/**
	 * @ORM\OneToMany(targetEntity="ProductCode", mappedBy="customer", cascade={"persist", "remove"})
	 */
	private $codes;

	/**
	 * @ORM\OneToMany(targetEntity="Quotation", mappedBy="customer", cascade={"persist", "remove"})
	 */
	private $quotations;

	/**
	 * @ORM\OneToMany(targetEntity="SalesOrder", mappedBy="customer", cascade={"persist", "remove"})
	 */
	private $salesOrders;

	/**
	 * @ORM\OneToMany(targetEntity="SalesForecast", mappedBy="customer", cascade={"persist"})
	 */
	private $salesForecasts;

	/**
	 * @ORM\OneToMany(targetEntity="CustomerFile", mappedBy="customer", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		$this->terms = 0;
		$this->ewt = self::EWT_NONE;
		$this->branches = new \Doctrine\Common\Collections\ArrayCollection();
		$this->salesActivityItems = new \Doctrine\Common\Collections\ArrayCollection();
		$this->codes = new \Doctrine\Common\Collections\ArrayCollection();
		$this->quotations = new \Doctrine\Common\Collections\ArrayCollection();
		$this->salesOrders = new \Doctrine\Common\Collections\ArrayCollection();
		$this->salesForecasts = new \Doctrine\Common\Collections\ArrayCollection();
		$this->files = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set name.
	 *
	 * @param string $name
	 *
	 * @return Customer
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set shortName.
	 *
	 * @param string $shortName
	 *
	 * @return Customer
	 */
	public function setShortName($shortName)
	{
		$this->shortName = $shortName;

		return $this;
	}

	/**
	 * Get shortName.
	 *
	 * @return string
	 */
	public function getShortName()
	{
		return $this->shortName;
	}

	/**
	 * Set contactDetails.
	 *
	 * @param string $contactDetails
	 *
	 * @return Customer
	 */
	public function setContactDetails($contactDetails)
	{
		$this->contactDetails = $contactDetails;

		return $this;
	}

	/**
	 * Get contactDetails.
	 *
	 * @return string
	 */
	public function getContactDetails()
	{
		return $this->contactDetails;
	}

	/**
	 * Set terms.
	 *
	 * @param int $terms
	 *
	 * @return Customer
	 */
	public function setTerms($terms)
	{
		$this->terms = $terms;

		return $this;
	}

	/**
	 * Get terms.
	 *
	 * @return int
	 */
	public function getTerms()
	{
		return $this->terms;
	}

	/**
	 * Set tin.
	 *
	 * @param string $tin
	 *
	 * @return Customer
	 */
	public function setTin($tin)
	{
		$this->tin = $tin;

		return $this;
	}

	/**
	 * Get tin.
	 *
	 * @return string
	 */
	public function getTin()
	{
		return $this->tin;
	}

	/**
	 * Set memo.
	 *
	 * @param string $memo
	 *
	 * @return Customer
	 */
	public function setMemo($memo)
	{
		$this->memo = $memo;

		return $this;
	}

	/**
	 * Get memo.
	 *
	 * @return string
	 */
	public function getMemo()
	{
		return $this->memo;
	}

	/**
	 * Set salesAgent.
	 *
	 * @param \CI\CoreBundle\Entity\User $salesAgent
	 *
	 * @return Customer
	 */
	public function setSalesAgent(\CI\CoreBundle\Entity\User $salesAgent = null)
	{
		$this->salesAgent = $salesAgent;

		return $this;
	}

	/**
	 * Get salesAgent.
	 *
	 * @return \CI\CoreBundle\Entity\User
	 */
	public function getSalesAgent()
	{
		return $this->salesAgent;
	}

	/**
	 * Add branches.
	 *
	 * @param \CI\InventoryBundle\Entity\Branch $branches
	 *
	 * @return Customer
	 */
	public function addBranch(\CI\InventoryBundle\Entity\Branch $branches)
	{
		$branches->setCustomer($this);
		$this->branches[] = $branches;

		return $this;
	}

	/**
	 * Remove branches.
	 *
	 * @param \CI\InventoryBundle\Entity\Branch $branches
	 */
	public function removeBranch(\CI\InventoryBundle\Entity\Branch $branches)
	{
		$this->branches->removeElement($branches);
	}

	/**
	 * Get branches.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getBranches()
	{
		return $this->branches;
	}

	/**
	 * Get salesOrders.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getSalesOrders()
	{
		return $this->salesOrders;
	}

	/**
	 * Set ewt.
	 *
	 * @param string $ewt
	 *
	 * @return Customer
	 */
	public function setEwt($ewt)
	{
		$this->ewt = $ewt;

		return $this;
	}

	/**
	 * Get ewt.
	 *
	 * @return string
	 */
	public function getEwt()
	{
		return $this->ewt;
	}

	/**
	 * Add codes.
	 *
	 * @param \CI\InventoryBundle\Entity\ProductCode $codes
	 *
	 * @return Customer
	 */
	public function addCode(\CI\InventoryBundle\Entity\ProductCode $codes)
	{
		$this->codes[] = $codes;

		return $this;
	}

	/**
	 * Remove codes.
	 *
	 * @param \CI\InventoryBundle\Entity\ProductCode $codes
	 */
	public function removeCode(\CI\InventoryBundle\Entity\ProductCode $codes)
	{
		$this->codes->removeElement($codes);
	}

	/**
	 * Get codes.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getCodes()
	{
		return $this->codes;
	}

	/**
	 * Add quotations.
	 *
	 * @param \CI\InventoryBundle\Entity\Quotation $quotations
	 *
	 * @return Customer
	 */
	public function addQuotation(\CI\InventoryBundle\Entity\Quotation $quotations)
	{
		$this->quotations[] = $quotations;

		return $this;
	}

	/**
	 * Remove quotations.
	 *
	 * @param \CI\InventoryBundle\Entity\Quotation $quotations
	 */
	public function removeQuotation(\CI\InventoryBundle\Entity\Quotation $quotations)
	{
		$this->quotations->removeElement($quotations);
	}

	/**
	 * Get quotations.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getQuotations()
	{
		return $this->quotations;
	}

	/**
	 * Add files.
	 *
	 * @param \CI\InventoryBundle\Entity\CustomerFile $files
	 *
	 * @return Customer
	 */
	public function addFile(\CI\InventoryBundle\Entity\CustomerFile $files)
	{
		$files->setCustomer($this);
		$this->files[] = $files;

		return $this;
	}

	/**
	 * Remove files.
	 *
	 * @param \CI\InventoryBundle\Entity\CustomerFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\CustomerFile $files)
	{
		$this->files->removeElement($files);
	}

	/**
	 * Get files.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}

	public function isEditable()
	{
		return true;
	}

	//for future modification
	public function isDeletable()
	{
		return true;
	}

	public function translateEwt($toTranslate = null)
	{
		$ewt = !empty($toTranslate) ? $toTranslate : $this->getEwt();

		switch ($ewt) {
			case self::EWT_NONE:
				return 'None';
			case self::EWT_GOODS:
				return 'Goods (1%)';
			case self::EWT_SERVICES:
				return 'Services (2%)';
		}
	}

	public function validate(ExecutionContextInterface $context)
	{
		$count = count($this->getMain());

		if ($count > 1) {
			$context->addViolation('Only one branch can be selected as main.');
		} elseif ($count == 0) {
			$context->addViolation('Please select one main branch.');
		}
	}

	public function getFirstBranch()
	{
		$branches = $this->getBranches();
		$criteria = Criteria::create()->where(Criteria::expr()->eq('isMain', false))->setMaxResults(1);

		return $branches->matching($criteria);
	}

	public function getMain()
	{
		$branches = $this->getBranches();
		$criteria = Criteria::create()->where(Criteria::expr()->eq('isMain', true))->setMaxResults(1);

		return $branches->matching($criteria);
	}

	public function getLog()
	{
		return [
			'Name' => $this->getName(),
			'Short Name' => $this->getShortName(),
			'Contact Details' => $this->getContactDetails(),
			'Sales Agent' => $this->getSalesAgent()->getName(),
			'Terms' => $this->getTerms(),
			'Tin' => $this->getTin(),
			'EWT' => $this->translateEwt(),
			'Memo' => $this->getMemo(),
			'Active' => $this->getActive() ? 'Yes' : 'No',
		];
	}
}
