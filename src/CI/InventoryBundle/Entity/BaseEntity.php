<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
//use CI\InventoryBundle\DependencyInjection\FileInterface;

/**
 * BaseEntity
 * 
 * @ORM\MappedSuperclass
 * 
 */
//abstract class BaseEntity implements FileInterface
abstract class BaseEntity
{
	const STATUS_ACTIVE = 'active';
	const STATUS_INACTIVE = 'inactive';
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;
    
    /**
     * @ORM\Column(name="created_by", type="string", length=255)
     * @Gedmo\Blameable(on="create")
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @ORM\Column(name="updated_by", type="string", length=255, nullable=true)
     * @Gedmo\Blameable(on="update")
     */
    private $updatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = 1;

    /**
     * Constructor
     */
    public function __construct()
    {
    	$this->active = 1;
    }
    
    /**
     * Set id
     *
     * @param integer $id
     * @return BaseEntity
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return BaseEntity
     */
    public function setCreatedAt($createdAt)
    {
    	$this->createdAt = $createdAt;
    
    	return $this;
    }
    
    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

	/**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return BaseEntity
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }
    
    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set as active or inactive
     * active = true and inactive = false
     *
     * @param integer $active
     * @return BaseEntity
     */
    public function setActive($status)
    {
    	if ($this::STATUS_INACTIVE === $status or false == $status) 
        	$this->active = false;
    	else
    		$this->active = true;
    		
        return $this;
    }

    /**
     * Is active or inactive
     *
     * @return integer
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return SalesOrder
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param string $updatedBy
     * @return SalesOrder
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return string 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}