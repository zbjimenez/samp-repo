<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use CI\InventoryBundle\DependencyInjection\InventoryServiceItemInterface;

/**
 * SalesOrderItem
 *
 * @ORM\Table(name="sales_order_item")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\SalesOrderItemRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 */
class SalesOrderItem extends BaseEntity implements InventoryServiceItemInterface
{
	/**
	 * @var float
	 *
	 * @ORM\Column(name="quantity", type="decimal", precision=11, scale=2)
	 * @Assert\NotBlank(message="Quantity must not be blank.")
	 * @Assert\Range(min=0.01, max=999999999.99, invalidMessage="The value is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $quantity;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="unit_price", type="decimal", precision=13, scale=2)
	 * @Assert\NotBlank(message="Unit Price must not be blank.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $unitPrice;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="line_total", type="decimal", scale=2, precision=13)
	 */
	private $lineTotal;
	
	/**
	 * @ORM\ManyToOne(targetEntity="SalesOrder", inversedBy="items")
	 */
	private $salesOrder;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Quotation", inversedBy="salesOrderItems")
	 * @Assert\NotBlank()
	 */
	private $quotation;
	
	/**
	 * @ORM\OneToMany(targetEntity="ShippingItem", mappedBy="salesOrderItem", cascade={"persist", "remove"})
	 */
	private $shippingItems;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->shippingItems = new \Doctrine\Common\Collections\ArrayCollection();
	}

    /**
     * Set quantity
     *
     * @param string $quantity
     * @return SalesOrderItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return string 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set unitPrice
     *
     * @param string $unitPrice
     * @return SalesOrderItem
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    
        return $this;
    }

    /**
     * Get unitPrice
     *
     * @return string 
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * Set lineTotal
     *
     * @param string $lineTotal
     * @return SalesOrderItem
     */
    public function setLineTotal($lineTotal)
    {
        $this->lineTotal = $lineTotal;
    
        return $this;
    }

    /**
     * Get lineTotal
     *
     * @return string 
     */
    public function getLineTotal()
    {
        return $this->lineTotal;
    }

    /**
     * Set quotation
     *
     * @param \CI\InventoryBundle\Entity\Quotation $quotation
     * @return SalesOrderItem
     */
    public function setQuotation(\CI\InventoryBundle\Entity\Quotation $quotation = null)
    {
        $this->quotation = $quotation;
    
        return $this;
    }

    /**
     * Get quotation
     *
     * @return \CI\InventoryBundle\Entity\Quotation 
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

    /**
     * Set salesOrder
     *
     * @param \CI\InventoryBundle\Entity\SalesOrder $salesOrder
     * @return SalesOrderItem
     */
    public function setSalesOrder(\CI\InventoryBundle\Entity\SalesOrder $salesOrder = null)
    {
        $this->salesOrder = $salesOrder;
    
        return $this;
    }

    /**
     * Get salesOrder
     *
     * @return \CI\InventoryBundle\Entity\SalesOrder 
     */
    public function getSalesOrder()
    {
        return $this->salesOrder;
    }
    
    /**
     * Add shippingItems
     *
     * @param \CI\InventoryBundle\Entity\ShippingItem $shippingItems
     * @return Packaging
     */
    public function addShippingItem(\CI\InventoryBundle\Entity\ShippingItem $shippingItems)
    {
    	$this->shippingItems[] = $shippingItems;
    
    	return $this;
    }
    
    /**
     * Remove shippingItems
     *
     * @param \CI\InventoryBundle\Entity\ShippingItem $shippingItems
     */
    public function removeShippingItem(\CI\InventoryBundle\Entity\ShippingItem $shippingItems)
    {
    	$this->shippingItems->removeElement($shippingItems);
    }
    
    /**
     * Get shippingItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShippingItems()
    {
    	return $this->shippingItems;
    }
    
    public function getTotalDelivered($excludeDeliverItem = null)
    {
    	$totalDelivered = 0;
    
    	foreach ($this->getShippingItems() as $item) {
    		if ($excludeDeliverItem && $excludeDeliverItem->getId() == $item->getId()) {
    			continue;
    		}
    		
    		if ($item->getShipping()->getStatus() == Shipping::STATUS_DELIVERED || $item->getShipping()->getStatus() == Shipping::STATUS_FULFILLED) {
    			$totalDelivered += $item->getQuantity();
    		}
    	}
    
    	return $totalDelivered;
    }
    
    public function getMaxQtyAllowedToDeliver(ShippingItem $drItem = null)
    {
    	return $this->getQuantity() - $this->getTotalDelivered($drItem);
    }
    
    //commented out other restrictions for potential future use
    public function process(ExecutionContextInterface $context)
    {
		$this->setLineTotal($this->getQuantity() * $this->getUnitPrice());

		$quotation = $this->getQuotation();
		if (!empty($quotation)) {
			if ($this->getUnitPrice() >= 0.01 && $this->getUnitPrice() <= 99999999.99 && $this->getUnitPrice() < $quotation->getPrice()) {
				$context->addViolationAt('unitPrice', 'Please make sure item price is higher than or equal to quotation price.');	
			}
		}
    }
    
    public function getLog()
    {
    	return array(
    		'Quotation #' => $this->getQuotation()->getReferenceNumber(),
    		'Product' => $this->getISProduct()->getName(),
    		'Quantity' => $this->getQuantity() . ' kg',
    		'Unit Price' => number_format($this->getUnitPrice(), 2),
    		'Line Total' => number_format($this->getLineTotal(), 2),
    	);
    }
    
    public function getISProduct()
    {
    	return $this->getQuotation()->getProduct();
    }
    
    public function getISQuantity()
    {
    	return $this->getQuantity();
    }
}