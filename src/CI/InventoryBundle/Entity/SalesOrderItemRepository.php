<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * SalesOrderItemRepository
 */
class SalesOrderItemRepository extends EntityRepository
{

	public function getSalesOrderReports($params = null)
	{
		$qb = $this->createQueryBuilder('soi')
		->select('PARTIAL soi.{id}',
			'so.refCode as refCode',
			'so.date as date',
			'c.name as name',
			'so.poRefCode as poRefCode',
			'MAX(s.deliveryDate) as deliveryDate',
			'p.name as productName',
			'(soi.quantity - SUM(IF(s.id IS NULL, 0, IF(s.status = :delivered OR s.status = :fulfilled, si.quantity, 0))) as balance',
			'soi.unitPrice as unitPrice',
			'soi.lineTotal as lineTotal')
		->join('soi.salesOrder', 'so')
		->join('so.customer', 'c')
		->join('soi.quotation', 'q')
		->join('q.product', 'p')
		->leftJoin('soi.shippingItems', 'si')
		->leftJoin('si.shipping', 's')
		->setParameter('delivered', Shipping::STATUS_DELIVERED)
		->setParameter('fulfilled', Shipping::STATUS_FULFILLED)
		->where('p.active = 1')
		->andWhere('so.status = :status')
		->groupBy('soi.id')
		->setParameter('status', SalesOrder::STATUS_APPROVED);

		if (!empty($params)) {
			if ($params['soId']) {
				$qb->andWhere('REGEXP(:soId, so.refCode) = 1')
				->setParameter('soId', '[[:<:]](' . preg_quote($params['soId']) . ')');
			}

			if ($params['poId']) {
				$qb->andWhere('REGEXP(:poId, so.poRefCode) = 1')
				->setParameter('poId', '[[:<:]](' . preg_quote($params['poId']) . ')');
			}

			if ($params['dateFrom']) {
				$qb->andWhere('so.date >= :dateFrom')
				->setParameter('dateFrom', $params['dateFrom']->format('Y-m-d'));
			}

			if ($params['dateTo']) {
				$qb->andWhere('so.date <= :dateTo')
				->setParameter('dateTo', $params['dateTo']->format('Y-m-d'));
			}

			if ($params['customer']) {
				$qb->andWhere('c.id = :customer')
				->setParameter('customer', $params['customer']);
			}

		}

		$qb->orderBy('soi.id');

		return $qb->getQuery();
	}

	public function getSalesByCustomerAndIndustry($params = null)
	{
		$qb = $this->createQueryBuilder('soi')
		->select('PARTIAL soi.{id}', 'c.shortName AS customerShortName',
				'i.name AS industryName',
				"COALESCE(SUM(soi.quantity * soi.unitPrice), 0) AS soTotal")
		->join('soi.salesOrder', 'so')
		->join('so.customer', 'c')
		->join('soi.quotation', 'q')
		->join('q.product', 'p')
		->join('p.industry', 'i')
		->andWhere('so.status = :status')
		->setParameter('status', SalesOrder::STATUS_APPROVED);

		if (!empty($params)) {
			if($params['customer']) {
				$qb->andWhere('c.id = :customerId')
				->setParameter('customerId', $params['customer']->getId());
			}

			if($params['industry']) {
				$qb->andWhere('i.id = :industryId')
				->setParameter('industryId', $params['industry']->getId());
			}
		}

		$qb->groupBy('c.id')
		->addGroupBy('i.id')
		->orderBy('c.shortName')
		->addOrderBy('i.name');

		return $qb->getQuery();
	}

	public function getSalesByCustomerAndIndustryTotal($params = null)
	{
		$qb = $this->createQueryBuilder('soi')
		->select("COALESCE(SUM(soi.quantity * soi.unitPrice), 0) AS grandTotal")
		->join('soi.salesOrder', 'so')
		->join('so.customer', 'c')
		->join('soi.quotation', 'q')
		->join('q.product', 'p')
		->join('p.industry', 'i')
		->andWhere('so.status = :status')
		->setParameter('status', SalesOrder::STATUS_APPROVED);

		if (!empty($params)) {
			if($params['customer']) {
				$qb->andWhere('c.id = :customerId')
				->setParameter('customerId', $params['customer']->getId());
			}

			if($params['industry']) {
				$qb->andWhere('i.id = :industryId')
				->setParameter('industryId', $params['industry']->getId());
			}
		}

		return $qb->getQuery()->getScalarResult()[0];
	}
}
