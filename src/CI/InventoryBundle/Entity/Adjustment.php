<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Adjustment
 *
 * @ORM\Table(name="adjustment")
 * @ORM\Entity()
 */
class Adjustment
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="amount", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="Amount must not be blank.")
	 * @Assert\Range(min=0.01, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $amount;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="contract_number", type="text")
	 * @Assert\NotBlank(message="Description must not be blank.")
	 * @Assert\Type(type="string")
	 */
	private $description;
	
	/**
	 * @ORM\ManyToOne(targetEntity="PurchaseOrder", inversedBy="adjustments")
	 */
	private $purchaseOrder;
	
    /**
     * Constructor
     */
    public function __construct()
    {
    	//empty construct
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return Adjustment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Adjustment
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set purchaseOrder
     *
     * @param \CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrder
     * @return Adjustment
     */
    public function setPurchaseOrder(\CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrder = null)
    {
        $this->purchaseOrder = $purchaseOrder;
    
        return $this;
    }

    /**
     * Get purchaseOrder
     *
     * @return \CI\InventoryBundle\Entity\PurchaseOrder 
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }
    
    public function getLog()
    {
    	return array(
    		'Description' => $this->getDescription(),
    		'Amount' => number_format($this->getAmount(), 2)
    	);
    }
}