<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use CI\InventoryBundle\DependencyInjection\InventoryServiceEntityInterface;

/**
 * Shipping.
 *
 * @ORM\Table(name="shipping")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\ShippingRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields="drId", message="This DR # is already in use.")
 */
class Shipping extends BaseEntity implements InventoryServiceEntityInterface
{
	const STATUS_DRAFT = 10;
	const STATUS_PENDING = 15;
	const STATUS_DELIVERED = 25;
	const STATUS_FULFILLED = 26;

	const TYPE_DR = 1;
	const TYPE_SI = 2;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="status", type="integer", length=2)
	 * @Assert\Type(type="integer")
	 * @Assert\Choice(choices = {"10", "15", "25", "26", "30"})
	 */
	private $status;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dr_id", type="string", length=255)
	 * @Assert\NotBlank(message="DR # must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $drId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="si_ref_code", type="string", length=255, nullable=true)
	 * @Assert\Length(min=0, max=255)
	 * @Assert\Type(type="string")
	 */
	private $siRefCode;

	/**
	 * @var date
	 *
	 * @ORM\Column(name="delivery_date", type="date")
	 * @Assert\NotBlank(message="Delivery date must not be blank.")
	 * @Assert\Date()
	 */
	private $deliveryDate;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="truck", type="string", length=255, nullable=true)
	 */
	private $truck;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="driver", type="string", length=255, nullable=true)
	 */
	private $driver;

	/**
	 * @var bool
	 *
	 * @ORM\Column(name="attention", type="boolean")
	 */
	private $attention;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="memo", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $memo;

	/**
	 * @ORM\OneToMany(targetEntity="ShippingItem", mappedBy="shipping", cascade={"persist", "remove"})
	 * @Assert\Count(min="1", minMessage="Must provide at least one item.")
	 * @Assert\Valid
	 */
	private $items;

	/**
	 * @ORM\ManyToOne(targetEntity="SalesOrder", inversedBy="shippings")
	 */
	private $salesOrder;

	/**
	 * @ORM\ManyToOne(targetEntity="Branch", inversedBy="shippings")
	 * @Assert\NotBlank(message="Branch must not be blank.")
	 */
	private $branch;

	/**
	 * @ORM\OneToMany(targetEntity="OrderPicking", mappedBy="shipping", cascade={"persist"})
	 */
	private $orderPickings;

	/**
	 * @ORM\OneToMany(targetEntity="SalesReturn", mappedBy="shipping", cascade={"persist"})
	 */
	private $salesReturns;

	/**
	 * @ORM\OneToMany(targetEntity="Backload", mappedBy="shipping", cascade={"persist"})
	 */
	private $backloads;

	/**
	 * @ORM\OneToMany(targetEntity="ShippingFile", mappedBy="shipping", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		$this->deliveryDate = new \DateTime();
		$this->status = self::STATUS_DRAFT;
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
		$this->salesReturns = new \Doctrine\Common\Collections\ArrayCollection();
		$this->files = new \Doctrine\Common\Collections\ArrayCollection();
		$this->orderPickings = new \Doctrine\Common\Collections\ArrayCollection();
		$this->backloads = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set status.
	 *
	 * @param int $status
	 *
	 * @return Shipping
	 */
	public function setStatus($status)
	{
		$this->status = $status;

		return $this;
	}

	/**
	 * Get status.
	 *
	 * @return int
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set drId.
	 *
	 * @param int $drId
	 *
	 * @return Shipping
	 */
	public function setDrId($drId)
	{
		$this->drId = $drId;

		return $this;
	}

	/**
	 * Get drId.
	 *
	 * @return int
	 */
	public function getDrId()
	{
		return $this->drId;
	}

	/**
	 * Set siRefCode.
	 *
	 * @param string $siRefCode
	 *
	 * @return Shipping
	 */
	public function setSiRefCode($siRefCode)
	{
		$this->siRefCode = $siRefCode;

		return $this;
	}

	/**
	 * Get siRefCode.
	 *
	 * @return string
	 */
	public function getSiRefCode()
	{
		return $this->siRefCode;
	}

	/**
	 * Set deliveryDate.
	 *
	 * @param \DateTime $deliveryDate
	 *
	 * @return Shipping
	 */
	public function setDeliveryDate($deliveryDate)
	{
		$this->deliveryDate = $deliveryDate;

		return $this;
	}

	/**
	 * Get deliveryDate.
	 *
	 * @return \DateTime
	 */
	public function getDeliveryDate()
	{
		return $this->deliveryDate;
	}

	/**
	 * Set truck.
	 *
	 * @param string $truck
	 *
	 * @return Shipping
	 */
	public function setTruck($truck)
	{
		$this->truck = $truck;

		return $this;
	}

	/**
	 * Get truck.
	 *
	 * @return string
	 */
	public function getTruck()
	{
		return $this->truck;
	}

	/**
	 * Set attention.
	 *
	 * @param string $attention
	 *
	 * @return Shipping
	 */
	public function setAttention($attention)
	{
		$this->attention = $attention;

		return $this;
	}

	/**
	 * Get attention.
	 *
	 * @return string
	 */
	public function getAttention()
	{
		return $this->attention;
	}

	/**
	 * Set driver.
	 *
	 * @param string $driver
	 *
	 * @return Shipping
	 */
	public function setDriver($driver)
	{
		$this->driver = $driver;

		return $this;
	}

	/**
	 * Get driver.
	 *
	 * @return string
	 */
	public function getDriver()
	{
		return $this->driver;
	}

	/**
	 * Set memo.
	 *
	 * @param string $memo
	 *
	 * @return Shipping
	 */
	public function setMemo($memo)
	{
		$this->memo = $memo;

		return $this;
	}

	/**
	 * Get memo.
	 *
	 * @return string
	 */
	public function getMemo()
	{
		return $this->memo;
	}

	/**
	 * Add items.
	 *
	 * @param \CI\InventoryBundle\Entity\ShippingItem $items
	 *
	 * @return
	 */
	public function addItem(\CI\InventoryBundle\Entity\ShippingItem $items)
	{
		$items->setShipping($this);
		$this->items[] = $items;

		return $this;
	}

	/**
	 * Remove items.
	 *
	 * @param \CI\InventoryBundle\Entity\ShippingItem $items
	 */
	public function removeItem(\CI\InventoryBundle\Entity\ShippingItem $items)
	{
		$this->items->removeElement($items);
	}

	/**
	 * Get items.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * Set salesOrder.
	 *
	 * @param \CI\InventoryBundle\Entity\SalesOrder $salesOrder
	 *
	 * @return Shipping
	 */
	public function setSalesOrder(\CI\InventoryBundle\Entity\SalesOrder $salesOrder = null)
	{
		$this->salesOrder = $salesOrder;

		return $this;
	}

	/**
	 * Get salesOrder.
	 *
	 * @return \CI\InventoryBundle\Entity\SalesOrder
	 */
	public function getSalesOrder()
	{
		return $this->salesOrder;
	}

	/**
	 * Set branch.
	 *
	 * @param \CI\InventoryBundle\Entity\Branch $branch
	 *
	 * @return Shipping
	 */
	public function setBranch(\CI\InventoryBundle\Entity\Branch $branch = null)
	{
		$this->branch = $branch;

		return $this;
	}

	/**
	 * Get branch.
	 *
	 * @return \CI\InventoryBundle\Entity\Branch
	 */
	public function getBranch()
	{
		return $this->branch;
	}

	public function getSalesReturns()
	{
		return $this->salesReturns;
	}

	public function getActiveSalesReturns()
	{
		$criteria = Criteria::create()->where(Criteria::expr()->lt('status', SalesReturn::STATUS_VOID));

		return $this->getSalesReturns()->matching($criteria);
	}

	/**
	 * Add files.
	 *
	 * @param \CI\InventoryBundle\Entity\ShippingFile $files
	 *
	 * @return Shipping
	 */
	public function addFile(\CI\InventoryBundle\Entity\ShippingFile $files)
	{
		$files->setShipping($this);
		$this->files[] = $files;

		return $this;
	}

	/**
	 * Remove files.
	 *
	 * @param \CI\InventoryBundle\Entity\ShippingFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\ShippingFile $files)
	{
		$this->files->removeElement($files);
	}

	/**
	 * Get files.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * Get orderPickings.
	 *
	 * @return \CI\InventoryBundle\Entity\OrderPicking
	 */
	public function getOrderPickings()
	{
		return $this->orderPickings;
	}

	/**
	 * Get backloads.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getBackloads()
	{
		return $this->backloads;
	}

	public function translateStatus()
	{
		switch ($this->getStatus()) {
			case self::STATUS_DRAFT:
				return array('status' => 'Draft', 'class' => 'default');
			case self::STATUS_PENDING:
				return array('status' => 'For Delivery', 'class' => 'default');
			case self::STATUS_DELIVERED:
				return array('status' => 'Delivered', 'class' => 'success');
			case self::STATUS_FULFILLED:
				return array('status' => 'Fulfilled', 'class' => 'info');
			default:
				return array('status' => 'Error', 'class' => 'warning');
		}
	}

	public function isEditable()
	{
		if (($this->getStatus() == self::STATUS_PENDING || $this->getStatus() == self::STATUS_DRAFT) && $this->getOrderPickings()->count() === 0) {
			return true;
		} else {
			return false;
		}
	}

	public function isDeletable()
	{
		return $this->getStatus() == self::STATUS_DRAFT;
	}

	public function isFullyPicked()
	{
		$fullyPicked = true;

		foreach ($this->getItems() as $item) {
			if ($item->getMaxQtyAllowedToPick() > 0) {
				return false;
			}
		}

		return $fullyPicked;
	}

	public function getLog()
	{
		return array(
			'DR #' => $this->getDrId(),
			'SI #' => $this->getSiRefCode(),
			'SO #' => $this->getSalesOrder()->getRefCode(),
			'Branch' => $this->getBranch()->getName(),
			'Delivery Date' => $this->getDeliveryDate(),
			'Attention' => $this->getAttention() ? 'Yes' : 'No',
			'Truck' => $this->getTruck(),
			'Driver' => $this->getDriver(),
			'Memo' => $this->getMemo(),
			'Status' => $this->translateStatus()['status'],
		);
	}

	public function getISItems()
	{
		return $this->getItems();
	}

	public function getWarehouse()
	{
		return $this->getSalesOrder()->getWarehouse();
	}
}
