<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use CI\InventoryBundle\DependencyInjection\InventoryServiceEntityInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * SalesOrder.
 *
 * @ORM\Table(name="sales_order")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\SalesOrderRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 * @UniqueEntity(fields="refCode", message="This SO # is already in use.")
 */
class SalesOrder extends BaseEntity implements InventoryServiceEntityInterface
{
	const STATUS_DRAFT = 10;
	const STATUS_APPROVED = 20;
	const STATUS_VOID = 30;
	const STATUS_CLOSED = 40;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="status", type="integer", length=2)
	 * @Assert\Type(type="integer")
	 * @Assert\Choice(choices = {"10", "20", "30", "40"})
	 */
	private $status;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ref_code", type="string", length=25)
	 * @Assert\Length(max=25, maxMessage="SO # must be less than {{ limit }} characters long.")
	 * @Assert\NotBlank(message="SO # must not be blank.")
	 * @Assert\Type(type="string")
	 * @Assert\Regex(pattern="/^[a-zA-Z0-9\-]*$/", match=true, message="SO # can only contain numbers, letters and hyphen.")
	 */
	private $refCode;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="po_ref_code", type="string", length=25, nullable=true)
	 * @Assert\Length(max=25, maxMessage="PO # must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 * @Assert\Regex(pattern="/^[a-zA-Z0-9\-]*$/", match=true, message="PO # can only contain numbers, letters and hyphen.")
	 */
	private $poRefCode;

	/**
	 * @var date
	 *
	 * @ORM\Column(name="date", type="date")
	 * @Assert\NotBlank(message="Please enter the date.")
	 * @Assert\Date()
	 */
	private $date;

	/**
	 * @ORM\Column(name="terms", type="integer")
	 * @Assert\NotBlank(message="Terms must not be blank.")
	 * @Assert\Type(type="integer")
	 * @Assert\Range(min=0, max=999)
	 */
	private $terms;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="memo", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $memo;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="subtotal", type="decimal", scale=2, precision=13)
	 */
	private $subtotal;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="total", type="decimal", scale=2, precision=13)
	 */
	private $total;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="ewt", type="decimal", scale=2, precision=3)
	 */
	private $ewt;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="vat_amount", type="decimal", scale=2, precision=13)
	 * @Assert\NotBlank(message="VAT Amount must not be blank.")
	 * @Assert\Range(min=0.00, max=99999999999.99, invalidMessage="The value '{{ value }}' is not a valid number.")
	 * @Assert\Type(type="numeric")
	 */
	private $vatAmount;

	/**
	 * @ORM\ManyToOne(targetEntity="Customer", inversedBy="salesOrders")
	 * @Assert\NotBlank(message="Customer must not be blank.")
	 */
	private $customer;

	/**
	 * @ORM\ManyToOne(targetEntity="CI\CoreBundle\Entity\User", inversedBy="ownedSalesOrders")
	 */
	private $ownedBy;

	/**
	 * @ORM\OneToMany(targetEntity="SalesOrderItem", mappedBy="salesOrder", cascade={"persist", "remove"})
	 * @Assert\Count(min="1", minMessage="Please enter at least one item.")
	 * @Assert\Valid
	 */
	private $items;

	/**
	 * @ORM\OneToMany(targetEntity="Shipping", mappedBy="salesOrder", cascade={"persist", "remove"})
	 */
	private $shippings;

	/**
	 * @ORM\OneToMany(targetEntity="SalesOrderFile", mappedBy="salesOrder", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;

	/**
	 * @ORM\ManyToOne(targetEntity="Warehouse", inversedBy="salesOrders")
	 * @Assert\NotBlank(message="Warehouse must not be blank.")
	 */
	private $warehouse;

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		$this->date = new \DateTime('now');
		$this->terms = 0;
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
		$this->shippings = new \Doctrine\Common\Collections\ArrayCollection();
		$this->files = new \Doctrine\Common\Collections\ArrayCollection();
		$this->total = 0;
		$this->ewt = 0;
		$this->subtotal = 0;
		$this->vatAmount = 0;
	}

	/**
	 * Set status.
	 *
	 * @param int $status
	 *
	 * @return SalesOrder
	 */
	public function setStatus($status)
	{
		$this->status = $status;

		return $this;
	}

	/**
	 * Get status.
	 *
	 * @return int
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set refCode.
	 *
	 * @param string $refCode
	 *
	 * @return SalesOrder
	 */
	public function setRefCode($refCode)
	{
		$this->refCode = $refCode;

		return $this;
	}

	/**
	 * Get refCode.
	 *
	 * @return string
	 */
	public function getRefCode()
	{
		return $this->refCode;
	}

	/**
	 * Set poRefCode.
	 *
	 * @param string $poRefCode
	 *
	 * @return SalesOrder
	 */
	public function setPoRefCode($poRefCode)
	{
		$this->poRefCode = $poRefCode;

		return $this;
	}

	/**
	 * Get poRefCode.
	 *
	 * @return string
	 */
	public function getPoRefCode()
	{
		return $this->poRefCode;
	}

	/**
	 * Set date.
	 *
	 * @param \DateTime $date
	 *
	 * @return SalesOrder
	 */
	public function setDate($date)
	{
		$this->date = $date;

		return $this;
	}

	/**
	 * Get date.
	 *
	 * @return \DateTime
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * Set terms.
	 *
	 * @param int $terms
	 *
	 * @return SalesOrder
	 */
	public function setTerms($terms)
	{
		$this->terms = $terms;

		return $this;
	}

	/**
	 * Get terms.
	 *
	 * @return int
	 */
	public function getTerms()
	{
		return $this->terms;
	}

	/**
	 * Set memo.
	 *
	 * @param string $memo
	 *
	 * @return SalesOrder
	 */
	public function setMemo($memo)
	{
		$this->memo = $memo;

		return $this;
	}

	/**
	 * Get memo.
	 *
	 * @return string
	 */
	public function getMemo()
	{
		return $this->memo;
	}

	/**
	 * Set total.
	 *
	 * @param string $total
	 *
	 * @return SalesOrder
	 */
	public function setSubtotal($total)
	{
		$this->subtotal = $total;

		return $this;
	}

	/**
	 * Get total.
	 *
	 * @return string
	 */
	public function getSubtotal()
	{
		return $this->subtotal;
	}

	/**
	 * Set total.
	 *
	 * @param string $total
	 *
	 * @return SalesOrder
	 */
	public function setTotal($total)
	{
		$this->total = $total;

		return $this;
	}

	/**
	 * Get total.
	 *
	 * @return string
	 */
	public function getTotal()
	{
		return $this->total;
	}

	/**
	 * Set ewt.
	 *
	 * @param string $ewt
	 *
	 * @return SalesOrder
	 */
	public function setEwt($ewt)
	{
		$this->ewt = $ewt;

		return $this;
	}

	/**
	 * Get ewt.
	 *
	 * @return string
	 */
	public function getEwt()
	{
		return $this->ewt;
	}

	/**
	 * Set vatAmount.
	 *
	 * @param string $vatAmount
	 *
	 * @return SalesOrder
	 */
	public function setVatAmount($vatAmount)
	{
		$this->vatAmount = $vatAmount;

		return $this;
	}

	/**
	 * Get vatAmount.
	 *
	 * @return string
	 */
	public function getVatAmount()
	{
		return $this->vatAmount;
	}

	/**
	 * Set customer.
	 *
	 * @param \CI\InventoryBundle\Entity\Customer $customer
	 *
	 * @return SalesOrder
	 */
	public function setCustomer(\CI\InventoryBundle\Entity\Customer $customer = null)
	{
		$this->customer = $customer;

		return $this;
	}

	/**
	 * Get customer.
	 *
	 * @return \CI\InventoryBundle\Entity\Customer
	 */
	public function getCustomer()
	{
		return $this->customer;
	}

	/**
	 * Set ownedBy.
	 *
	 * @param \CI\CoreBundle\Entity\User $ownedBy
	 *
	 * @return SalesOrder
	 */
	public function setOwnedBy(\CI\CoreBundle\Entity\User $ownedBy = null)
	{
		$this->ownedBy = $ownedBy;

		return $this;
	}

	/**
	 * Get ownedBy.
	 *
	 * @return \CI\CoreBundle\Entity\User
	 */
	public function getOwnedBy()
	{
		return $this->ownedBy;
	}

	public function setItems(\Doctrine\Common\Collections\Collection $items)
	{
		foreach ($items as $item) {
			$item->setSalesOrder($this);
		}

		$this->items = $items;
	}

	/**
	 * Get items.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * Add shippings.
	 *
	 * @param \CI\InventoryBundle\Entity\Shipping $shippings
	 *
	 * @return SalesOrder
	 */
	public function addShipping(\CI\InventoryBundle\Entity\Shipping $shippings)
	{
		$shippings->setSalesOrder($this);
		$this->shippings[] = $shippings;

		return $this;
	}

	/**
	 * Remove shippings.
	 *
	 * @param \CI\InventoryBundle\Entity\Shipping $shippings
	 */
	public function removeShipping(\CI\InventoryBundle\Entity\Shipping $shippings)
	{
		$this->shippings->removeElement($shippings);
	}

	/**
	 * Get shipping.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getShippings()
	{
		return $this->shippings;
	}

	/**
	 * Add files.
	 *
	 * @param \CI\InventoryBundle\Entity\SalesOrderFile $files
	 *
	 * @return SalesOrder
	 */
	public function addFile(\CI\InventoryBundle\Entity\SalesOrderFile $files)
	{
		$files->setSalesOrder($this);
		$this->files[] = $files;

		return $this;
	}

	/**
	 * Remove files.
	 *
	 * @param \CI\InventoryBundle\Entity\SalesOrderFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\SalesOrderFile $files)
	{
		$this->files->removeElement($files);
	}

	/**
	 * Get files.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * Set warehouse.
	 *
	 * @param \CI\InventoryBundle\Entity\Warehouse $warehouse
	 *
	 * @return SalesOrder
	 */
	public function setWarehouse(\CI\InventoryBundle\Entity\Warehouse $warehouse = null)
	{
		$this->warehouse = $warehouse;

		return $this;
	}

	/**
	 * Get warehouse.
	 *
	 * @return \CI\InventoryBundle\Entity\Warehouse
	 */
	public function getWarehouse()
	{
		return $this->warehouse;
	}

	public function translateStatus()
	{
		switch ($this->getStatus()) {
			case self::STATUS_DRAFT:
				return 'Draft';
			case self::STATUS_APPROVED:
				return 'Approved';
			case self::STATUS_VOID:
				return 'Void';
			case self::STATUS_CLOSED:
				return 'Closed';
		}
	}

	/**
	 * Returns the quantity ordered for this sales order entity.
	 */
	public function getQtyOrdered()
	{
		$qty = 0;
		foreach ($this->getItems() as $item) {
			$qty += $item->getQuantity();
		}

		return $qty;
	}

	/**
	 * Returns the quantity delivered for all SO items.
	 */
	public function getQtyDelivered()
	{
		$quantity = 0;
		foreach ($this->getItems() as $item) {
			$quantity += $item->getTotalDelivered();
		}

		return $quantity;
	}

	public function process(ExecutionContextInterface $context)
	{
		$total = 0;
		$subtotal = 0;
		$rowAmount = 0;
		$included = array();

		foreach ($this->getItems() as $item) {
			$quotation = $item->getQuotation();
			$lineTotal = round($item->getQuantity() * $item->getUnitPrice(), 2);

			if (!empty($quotation)) {
				if (!in_array($quotation->getId(), $included)) {
					$included[] = $quotation->getId();
				} else {
					$context->addViolation('A quotation cannot be added to the same sales order more than once.');
				}

				$rowAmount = $lineTotal;
			}

			$subtotal += $rowAmount;
		}

		$this->setSubtotal($subtotal);
		$this->setEwt($this->getCustomer() ? $this->getCustomer()->getEwt() : 0);
		$total = $subtotal + $this->getVatAmount();
		$this->setTotal($total - round($subtotal / 1.12 * $this->getEwt(), 2));
	}

	public function isEditable()
	{
		if ($this->getStatus() < self::STATUS_VOID && $this->getShippings()->count() == 0) {
			return true;
		}

		return false;
	}

	public function isDeletable()
	{
		$result = false;

		if ($this->getStatus() < self::STATUS_APPROVED) {
			$result = true;
		}

		return $result;
	}

	public function getActiveShipping()
	{
		return $this->getShippings();
	}

	public function getLog()
	{
		return array(
			'SO #' => $this->getRefCode(),
			'Customer' => $this->getCustomer()->getShortName(),
			'PO #' => $this->getPoRefCode(),
			'Date' => $this->getDate(),
			'Terms' => $this->getTerms(),
			'Memo' => $this->getMemo(),
			'Total' => number_format($this->getTotal(), 2),
			'VAT Amount' => number_format($this->getVatAmount(), 2),
			'Status' => $this->translateStatus(),
		);
	}

	public function getISItems()
	{
		return $this->getItems();
	}
}
