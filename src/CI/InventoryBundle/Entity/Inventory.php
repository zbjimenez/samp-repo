<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * Inventory
 *
 * This is the Storage Location level inventory.
 *
 * @ORM\Table(name="inventory")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\InventoryRepository")
 */
class Inventory extends BaseEntity
{
	const ADJUSTMENT_IN = 'In';
	const ADJUSTMENT_OUT = 'Out';

	/**
	 * @var float
	 *
	 * @ORM\Column(name="qty_on_hand", type="decimal", scale=2, precision=13)
	 */
	private $qtyOnHand;

	/**
	 * @ORM\ManyToOne(targetEntity="StorageLocation", inversedBy="inventories")
	 */
	private $storageLocation;

	/**
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="inventories")
	 */
	private $product;

	/**
	 * @ORM\OneToMany(targetEntity="InventoryLog", mappedBy="inventory", cascade={"persist", "remove"})
	 */
	private $inventoryLogs;

	/**
	 * @ORM\Column(name="lot_number", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $lotNumber;

	/**
	 * @ORM\Column(name="pallet_id", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $palletId;

	/**
	 * @var date
	 *
	 * @ORM\Column(name="production_date", type="date", nullable=true)
	 * @Assert\Date()
	 */
	private $productionDate;

	/**
	 * @var date
	 *
	 * @ORM\Column(name="factory_date", type="date", nullable=true)
	 * @Assert\Date()
	 */
	private $factoryDate;

	private $memo;

	private $qtyChange;
	private $transType;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->qtyOnHand = 0;
		$this->qtyChange = 0;
	}

	/**
	 * Set memo
	 *
	 * @param string $memo
	 * @return Product
	 */
	public function setMemo($memo)
	{
		$this->memo = $memo;
	
		return $this;
	}
	
	/**
	 * Get memo
	 *
	 * @return string
	 */
	public function getMemo()
	{
		return $this->memo;
	}

	/**
	 * Set qtyOnHand
	 *
	 * @param float $qtyOnHand
	 * @return Inventory
	 */
	public function setQtyOnHand($qtyOnHand)
	{
		$this->setQtyChange($qtyOnHand - $this->qtyOnHand);
		$this->qtyOnHand = $qtyOnHand;

		return $this;
	}

	/**
	 * Get qtyOnHand
	 *
	 * @return float
	 */
	public function getQtyOnHand()
	{
		return $this->qtyOnHand;
	}

	/**
	 * Set qtyChange
	 *
	 * @param integer $qtyChange
	 * @return Inventory
	 */
	public function setQtyChange($qtyChange)
	{
		$this->qtyChange += $qtyChange;

		return $this;
	}

	/**
	 * Get qtyChange
	 *
	 * @return integer
	 */
	public function getQtyChange()
	{
		return $this->qtyChange;
	}

	/**
	 * Set storageLocation
	 *
	 * @param \CI\InventoryBundle\Entity\StorageLocation $storageLocation
	 * @return Inventory
	 */
	public function setStorageLocation(\CI\InventoryBundle\Entity\StorageLocation $storageLocation = null)
	{
		$this->storageLocation = $storageLocation;

		return $this;
	}

	/**
	 * Get storageLocation
	 *
	 * @return \CI\InventoryBundle\Entity\StorageLocation
	 */
	public function getStorageLocation()
	{
		return $this->storageLocation;
	}

	/**
	 * Set product
	 *
	 * @param \CI\InventoryBundle\Entity\Product $product
	 * @return Inventory
	 */
	public function setProduct(\CI\InventoryBundle\Entity\Product $product = null)
	{
		$this->product = $product;

		return $this;
	}

	/**
	 * Get product
	 *
	 * @return \CI\InventoryBundle\Entity\Product
	 */
	public function getProduct()
	{
		return $this->product;
	}

	/**
	 * Add inventoryLog
	 *
	 * @param \CI\InventoryBundle\Entity\InventoryLog $inventoryLog
	 * @return Inventory
	 */
	public function addInventoryLogs(\CI\InventoryBundle\Entity\InventoryLog $inventoryLog)
	{
		$this->inventoryLog[] = $inventoryLog;
	
		return $this;
	}
	
	/**
	 * Remove inventoryLog
	 *
	 * @param \CI\InventoryBundle\Entity\InventoryLog $inventoryLog
	 */
	public function removeInventoryLogs(\CI\InventoryBundle\Entity\InventoryLog $inventoryLog)
	{
		$this->inventoryLog->removeElement($inventoryLog);
	}
	
	/**
	 * Get inventoryLog
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getInventoryLogs()
	{
		return $this->inventoryLog;
	}

	/**
	 * Set lotNumber
	 *
	 * @param string $lotNumber
	 * @return Product
	 */
	public function setLotNumber($lotNumber)
	{
		$this->lotNumber = $lotNumber;
	
		return $this;
	}

	/**
	 * Get lotNumber
	 *
	 * @return string 
	 */
	public function getLotNumber()
	{
		return $this->lotNumber;
	}

	/**
	 * Set palletId
	 *
	 * @param string $palletId
	 * @return Product
	 */
	public function setPalletId($palletId)
	{
		$this->palletId = $palletId;
	
		return $this;
	}

	/**
	 * Get palletId
	 *
	 * @return string 
	 */
	public function getPalletId()
	{
		return $this->palletId;
	}

	/**
	 * Set productionDate
	 *
	 * @param \DateTime $productionDate
	 * @return ReceiveOrder
	 */
	public function setProductionDate($productionDate)
	{
		$this->productionDate = $productionDate;
	
		return $this;
	}

	/**
	 * Get productionDate
	 *
	 * @return \DateTime 
	 */
	public function getProductionDate()
	{
		return $this->productionDate;
	}

	/**
	 * Set factoryDate
	 *
	 * @param \DateTime $factoryDate
	 * @return ReceiveOrder
	 */
	public function setFactoryDate($factoryDate)
	{
		$this->factoryDate = $factoryDate;

		return $this;
	}

	/**
	 * Get factoryDate
	 *
	 * @return \DateTime 
	 */
	public function getFactoryDate()
	{
		return $this->factoryDate;
	}

	/**
	 * Set transType
	 *
	 * @param string $transType
	 * @return Inventory
	 */
	public function setTransType($transType)
	{
		$this->transType = $transType;
	
		return $this;
	}
	
	
	/**
	 * Get transType
	 *
	 * @return string
	 */
	public function getTransType()
	{
		return $this->transType;
	}

	public function getQtyOnHandPackage()
	{
		return $this->getQtyOnHand() / $this->getProduct()->getPackaging()->getKgsUnit();
	}
	
	public function getQtyAllocatedPackage()
	{
		return $this->getQtyAllocated() / $this->getProduct()->getPackaging()->getKgsUnit();	
	}

	public function isEmpty()
	{
		return ($this->getQtyOnHand() == 0 && $this->getQtyAllocated() == 0);
	}

	public function serializeToArray($isMismatched = false)
	{
		return array(
				'id' => $this->getId(),
				'storageLocation' => array(
					'id' => $this->getStorageLocation()->getId(),
					'fullLocation' => $this->getStorageLocation()->getFullLocation(),
					'warehouseNumber' => $this->getStorageLocation()->getWarehouseNumber(),
					'warehouse' => array(
						'id' => $this->getStorageLocation()->getWarehouse()->getId(),
						'name' => $this->getStorageLocation()->getWarehouse()->getName(),
					)
				),
				'qtyOnHand' => $this->getQtyOnHand(),
				'qtyOnHandPackage' => $this->getQtyOnHandPackage(),
				'lotNumber' => $this->getLotNumber() ? $this->getLotNumber() : '',
				'palletId' => $this->getPalletId() ? $this->getPalletId() : '',
				'productionDate' => $this->getProductionDate() ? date_format($this->getProductionDate(), 'M d, Y') : "",
				'factoryDate' => $this->getFactoryDate() ? date_format($this->getFactoryDate(), 'M d, Y') : "",
				'productionDateForm' => $this->getProductionDate() ? date_format($this->getProductionDate(), 'M/d/Y') : "",
				'factoryDateForm' => $this->getFactoryDate() ? date_format($this->getFactoryDate(), 'M/d/Y') : ""
			);
	}
}
