<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Warehouse
 *
 * @ORM\Table(name="warehouse")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\WarehouseRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="name", message="This Warehouse name is already in use.")
 */
class Warehouse extends BaseEntity
{

	/**
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Assert\NotBlank(message="Name must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $name;

	/**
	 * @ORM\Column(name="address", type="string", length=255)
	 * @Assert\NotBlank(message="Address must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $address;

	/**
	 * @ORM\OneToMany(targetEntity="WarehouseInventory", mappedBy="warehouse", cascade={"persist", "remove"})
	 */
	private $warehouseInventories;

	/**
	 * @ORM\OneToMany(targetEntity="PurchaseOrder", mappedBy="warehouse")
	 */
	private $purchaseOrders;

	/**
	 * @ORM\OneToMany(targetEntity="SalesOrder", mappedBy="warehouse")
	 */
	private $salesOrders;

	/**
	 * @ORM\OneToMany(targetEntity="StorageLocation", mappedBy="warehouse", cascade={"persist", "remove"})
	 */
	private $storageLocations;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->warehouseInventories = new \Doctrine\Common\Collections\ArrayCollection();
		$this->purchaseOrders = new \Doctrine\Common\Collections\ArrayCollection();
		$this->salesOrders = new \Doctrine\Common\Collections\ArrayCollection();
		$this->storageLocations = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Product
	 */
	public function setName($name)
	{
		$this->name = $name;
	
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string 
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set address
	 *
	 * @param string $address
	 * @return Warehouse
	 */
	public function setAddress($address)
	{
		$this->address = $address;
	
		return $this;
	}

	/**
	 * Get address
	 *
	 * @return string 
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * Add warehouseInventories
	 *
	 * @param \CI\InventoryBundle\Entity\WarehouseInventory $warehouseInventories
	 * @return Warehouse
	 */
	public function addWarehouseInventory(\CI\InventoryBundle\Entity\WarehouseInventory $warehouseInventories)
	{
		$this->warehouseInventories[] = $warehouseInventories;
	
		return $this;
	}
	
	/**
	 * Remove warehouseInventories
	 *
	 * @param \CI\InventoryBundle\Entity\WarehouseInventory $warehouseInventories
	 */
	public function removeWarehouseInventory(\CI\InventoryBundle\Entity\WarehouseInventory $warehouseInventories)
	{
		$this->warehouseInventories->removeElement($warehouseInventories);
	}
	
	/**
	 * Get quotations
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getWarehouseInventories()
	{
		return $this->warehouseInventories;
	}

	/**
	 * Add purchaseOrders
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders
	 * @return Warehouse
	 */
	public function addPurchaseOrder(\CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders)
	{
		$this->purchaseOrders[] = $purchaseOrders;
	
		return $this;
	}
	
	/**
	 * Remove purchaseOrders
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders
	 */
	public function removePurchaseOrder(\CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrders)
	{
		$this->purchaseOrders->removeElement($purchaseOrders);
	}
	
	/**
	 * Get purchaseOrders
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getPurchaseOrders()
	{
		return $this->purchaseOrders;
	}

	/**
	 * Add salesOrders
	 *
	 * @param \CI\InventoryBundle\Entity\SalesOrder $salesOrders
	 * @return Warehouse
	 */
	public function addSalesOrder(\CI\InventoryBundle\Entity\SalesOrder $salesOrders)
	{
		$this->salesOrders[] = $salesOrders;
	
		return $this;
	}
	
	/**
	 * Remove salesOrders
	 *
	 * @param \CI\InventoryBundle\Entity\SalesOrder $salesOrders
	 */
	public function removeSalesOrder(\CI\InventoryBundle\Entity\SalesOrder $salesOrders)
	{
		$this->salesOrders->removeElement($salesOrders);
	}
	
	/**
	 * Get salesOrders
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getSalesOrders()
	{
		return $this->salesOrders;
	}

	/**
	 * Add storageLocations
	 *
	 * @param \CI\InventoryBundle\Entity\StorageLocation $storageLocations
	 * @return Warehouse
	 */
	public function addStorageLocation(\CI\InventoryBundle\Entity\StorageLocation $storageLocations)
	{
		$this->storageLocations[] = $storageLocations;
	
		return $this;
	}
	
	/**
	 * Remove storageLocations
	 *
	 * @param \CI\InventoryBundle\Entity\StorageLocation $storageLocations
	 */
	public function removeStorageLocation(\CI\InventoryBundle\Entity\StorageLocation $storageLocations)
	{
		$this->storageLocations->removeElement($storageLocations);
	}
	
	/**
	 * Get quotations
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getStorageLocations()
	{
		return $this->storageLocations;
	}

	public function isDeletable()
	{
		return (count($this->getStorageLocations()) == 0);
	}
	
	public function getLog()
	{
		return array(
			'Name' => $this->getName(),
			'Address' => $this->getAddress(),
			'Active' => $this->getActive()? 'True': 'False'
		);
	}
}