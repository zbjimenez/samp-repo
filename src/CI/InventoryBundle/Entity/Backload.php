<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Backload
 *
 * @ORM\Table(name="backload")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\BackloadRepository")
 * @Assert\Callback(methods={"validate"})
 * @ORM\HasLifecycleCallbacks()
 */
class Backload extends BaseEntity
{
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date", type="date")
	 * @Assert\NotBlank(message="Date must not be blank.")
	 * @Assert\Date()
	 */
	private $date;

	/**
	 * @ORM\Column(name="for_put_away", type="boolean")
	 */
	private $forPutAway;

	/**
	 * @ORM\ManyToOne(targetEntity="Shipping", inversedBy="backloads")
	 */
	private $shipping;

	/**
	 * @ORM\OneToMany(targetEntity="BackloadItem", mappedBy="backload", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $items;

	/**
	 * @ORM\OneToMany(targetEntity="PutAway", mappedBy="backload", cascade={"persist", "remove"})
	 */
	private $putAways;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->date = new \DateTime();
		$this->forPutAway = false;
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
		$this->putAways = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 * @return Backload
	 */
	public function setDate($date)
	{
		$this->date = $date;
	
		return $this;
	}

	/**
	 * Get date
	 *
	 * @return \DateTime 
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * Set forPutAway
	 *
	 * @param boolean $forPutAway
	 * @return Backload
	 */
	public function setForPutAway($forPutAway)
	{
		$this->forPutAway = $forPutAway;
	
		return $this;
	}

	/**
	 * Get forPutAway
	 *
	 * @return boolean 
	 */
	public function getForPutAway()
	{
		return $this->forPutAway;
	}

	/**
	 * Set shipping
	 *
	 * @param \CI\InventoryBundle\Entity\Shipping $shipping
	 * @return Backload
	 */
	public function setShipping(\CI\InventoryBundle\Entity\Shipping $shipping = null)
	{
		$this->shipping = $shipping;
	
		return $this;
	}

	/**
	 * Get shipping
	 *
	 * @return \CI\InventoryBundle\Entity\Shipping 
	 */
	public function getShipping()
	{
		return $this->shipping;
	}

	public function setItems(\Doctrine\Common\Collections\Collection $items)
	{
		foreach ($items as $item) {
			$item->setBackload($this);
		}
		
		$this->items = $items;
	}

	/**
	 * Get items
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * Get putAways
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getPutAways()
	{
		return $this->putAways;
	}

	public function validate(ExecutionContextInterface $context)
	{
		$items = $this->getItems()->matching(Criteria::create()->where(Criteria::expr()->eq('isIncluded', true)));

		if ($items->count() == 0) {
			$context->addViolation('Please select at least one item.');
		}
	}

	public function checkForPutAway()
	{
		if ($this->getForPutAway()) {
			return false;
		} else {
			foreach ($this->getItems() as $item) {
				if (is_null($item->getPalletId())) {
					return false;
				}
			}

			return true;
		}
	}

	public function getActivePutAways()
	{
		$putAways = $this->getPutAways()->filter(function($putAway) { return $putAway->isActive(); });

		return $putAways;
	}

	public function isEditable()
	{
		return !$this->getForPutAway() && $this->getActive();
	}

	public function isDeletable()
	{
		return !$this->getForPutAway();
	}

	public function getLog()
	{
		return array(
			'Delivery #' => $this->getShipping()->getId(),
			'Date' => $this->getDate(),
			'For Put Away' => $this->getForPutAway() ? 'Yes' : 'No',
		);
	}
}