<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * PurchaseOrderItemRepository
 *
 */
class PurchaseOrderItemRepository extends EntityRepository
{
	
	public function getReceivingReports($params = null)
	{
		
		$qb = $this->createQueryBuilder('poi')
		->select('PARTIAL poi.{id}', 
			'po.poId as poId',
			'po.id AS sysId',
			's.name as supplier',
			'p.name as product', 
			'poi.quantity',
			'(poi.quantity - SUM(IFNULL(roi.quantity, 0))) as balance', 
			'po.cnPf as cnPf', 
			'po.invoiceNumber as invoiceNumber', 
			'po.containerNumber as containerNumber', 
			'po.sealNumber as sealNumber', 
			'po.shipmentAdvice as shipmentAdvice',
			'po.memo as memo')
		->join('poi.purchaseOrder', 'po')
		->join('poi.product', 'p')
		->join('p.supplier', 's')
		->join('p.category', 'cat')
		->leftJoin('poi.receiveOrderItems', 'roi')
		->where('p.active = 1')
		->andWhere('po.status = :status')
		->groupBy('poi.id')
		->setParameter('status', PurchaseOrder::STATUS_APPROVED);
		
		if (!empty($params)) {
			if ($params['poId']) {
				$qb->where('REGEXP(:regexp, po.poId) = 1')
				->setParameter('regexp', '[[:<:]](' . preg_quote($params['poId']) . ')');
			}
		}
	
		$qb->orderBy('po.poId');
		return $qb->getQuery();
	}

	public function getExistingItemsQuantityWithProduct($product)
	{
		return intval($this->createQueryBuilder('poi')
		->select('SUM(poi.quantity)')
		->join('poi.product', 'p')
		->join('poi.purchaseOrder', 'po')
		->where('p.id = :productId')
		->setParameter('productId', $product->getId())
		->andWhere('po.status = :draft')
		->setParameter('draft', PurchaseOrder::STATUS_DRAFT)
		->getQuery()
		->getOneOrNullResult()[1]);
	}
}
