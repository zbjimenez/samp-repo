<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use CI\InventoryBundle\DependencyInjection\InventoryServiceEntityInterface;
use Symfony\Component\Validator\ExecutionContextInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * ReceiveOrder
 *
 * @ORM\Table(name="receive_order")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\ReceiveOrderRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"validateItems"})
 */
class ReceiveOrder extends BaseEntity implements InventoryServiceEntityInterface
{
	/**
	 * @var date
	 *
	 * @ORM\Column(name="date_received", type="date")
	 * @Assert\NotBlank(message="Date received must not be blank.")
	 * @Assert\Date()
	 */
	private $dateReceived;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="reference_number", type="string", length=255, nullable=true)
	 * @Assert\Length(max=255)
	 * @Assert\Type(type="string")
	 */
	private $referenceNumber;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="memo", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $memo;

	/**
	 * @ORM\ManyToOne(targetEntity="PurchaseOrder", inversedBy="receiveOrders")
	 * @ASsert\NotBlank(message="PO # must not be blank.")
	 */
	private $purchaseOrder;
	
	/**
	 * @ORM\OneToMany(targetEntity="ReceiveOrderItem", mappedBy="receiveOrder", cascade={"persist", "remove"})
	 * @Assert\Count(min="1", minMessage="You must check at least one item.")
	 * @Assert\Valid
	 */
	private $items;
	
	/**
	 * @ORM\OneToMany(targetEntity="PurchaseReturn", mappedBy="receiveOrder", cascade={"persist"})
	 */
	private $purchaseReturns;

	/**
	 * @ORM\OneToMany(targetEntity="ReceiveOrderFile", mappedBy="receiveOrder", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $files;

	/**
	 * @ORM\OneToMany(targetEntity="PutAway", mappedBy="receiveOrder", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $putAways;

	/**
	 * @ORM\Column(name="received_from", type="string", length=255)
	 * @Assert\NotBlank(message="Received From must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $receivedFrom;

	/**
	 * @ORM\Column(name="dr_no", type="string", length=255)
	 * @Assert\NotBlank(message="DR No. must not be blank")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $drNo;

	/**
	 * @ORM\Column(name="so_no", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $soNo;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="invoice_number", type="string", length=255)
	 * @Assert\NotBlank(message="Invoice Number must not be blank")
	 * @Assert\Type(type="string")
	 */
	private $invoiceNumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="container_number", type="string", length=255)
	 * @Assert\NotBlank(message="Container # must not be blank")
	 * @Assert\Type(type="string")
	 */
	private $containerNumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="seal_number", type="string", length=255)
	 * @Assert\NotBlank(message="Seal # must not be blank")
	 * @Assert\Type(type="string")
	 */
	private $sealNumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="factory_number", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $factoryNumber;

	/**
	 * @ORM\OneToOne(targetEntity="ReceiveOrder", cascade={"persist", "remove"})
	 */
	private $receiveOrder;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Assert\NotBlank(message="Supplier Name must not be blank.")
	 * @Assert\Length(max=255, maxMessage="Input must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 */
	private $supplierName;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->dateReceived = new \DateTime();
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
		$this->purchaseReturns = new \Doctrine\Common\Collections\ArrayCollection();
		$this->files = new \Doctrine\Common\Collections\ArrayCollection();
		$this->putAways = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get items
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getItems()
	{
		return $this->items;
	}
	
	/**
	 * Add items
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrderItem $items
	 * @return ReceiveOrder
	 */
	public function addItem(\CI\InventoryBundle\Entity\ReceiveOrderItem $items)
	{
		$items->setReceiveOrder($this);
		$this->items[] = $items;
	
		return $this;
	}
	
	/**
	 * Remove items
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrderItem $items
	 */
	public function removeItem(\CI\InventoryBundle\Entity\ReceiveOrderItem $items)
	{
		$this->items->removeElement($items);
	}

	/**
	 * Set dateReceived
	 *
	 * @param \DateTime $dateReceived
	 * @return ReceiveOrder
	 */
	public function setDateReceived($dateReceived)
	{
		$this->dateReceived = $dateReceived;
	
		return $this;
	}

	/**
	 * Get dateReceived
	 *
	 * @return \DateTime 
	 */
	public function getDateReceived()
	{
		return $this->dateReceived;
	}

	/**
	 * Set referenceNumber
	 *
	 * @param string $referenceNumber
	 * @return ReceiveOrder
	 */
	public function setReferenceNumber($referenceNumber)
	{
		$this->referenceNumber = $referenceNumber;
	
		return $this;
	}

	/**
	 * Get referenceNumber
	 *
	 * @return string 
	 */
	public function getReferenceNumber()
	{
		return $this->referenceNumber;
	}

	/**
	 * Set memo
	 *
	 * @param string $memo
	 * @return ReceiveOrder
	 */
	public function setMemo($memo)
	{
		$this->memo = $memo;
	
		return $this;
	}

	/**
	 * Get memo
	 *
	 * @return string 
	 */
	public function getMemo()
	{
		return $this->memo;
	}

	/**
	 * Set purchaseOrder
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrder
	 * @return ReceiveOrder
	 */
	public function setPurchaseOrder(\CI\InventoryBundle\Entity\PurchaseOrder $purchaseOrder = null)
	{
		$this->purchaseOrder = $purchaseOrder;
	
		return $this;
	}

	/**
	 * Get purchaseOrder
	 *
	 * @return \CI\InventoryBundle\Entity\PurchaseOrder 
	 */
	public function getPurchaseOrder()
	{
		return $this->purchaseOrder;
	}
	
	public function getPurchaseReturns()
	{
		return $this->purchaseReturns;
	}
	
	/**
	 * Add files
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrderFile $files
	 * @return ReceiveOrder
	 */
	public function addFile(\CI\InventoryBundle\Entity\ReceiveOrderFile $files)
	{
		$files->setReceiveOrder($this);
		$this->files[] = $files;
	
		return $this;
	}
	
	/**
	 * Remove files
	 *
	 * @param \CI\InventoryBundle\Entity\ReceiveOrderFile $files
	 */
	public function removeFile(\CI\InventoryBundle\Entity\ReceiveOrderFile $files)
	{
		$this->files->removeElement($files);
	}
	
	/**
	 * Get files
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * Get putAways
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getPutAways()
	{
		return $this->putAways;
	}
	
	/**
	 * Add putAways
	 *
	 * @param \CI\InventoryBundle\Entity\PutAway $putAways
	 * @return ReceiveOrder
	 */
	public function addPutAway(\CI\InventoryBundle\Entity\PutAway $putAways)
	{
		$putAways->setReceiveOrder($this);
		$this->putAways[] = $putAways;
	
		return $this;
	}
	
	/**
	 * Remove putAways
	 *
	 * @param \CI\InventoryBundle\Entity\PutAway $putAways
	 */
	public function removePutAway(\CI\InventoryBundle\Entity\PutAway $putAways)
	{
		$this->putAways->removeElement($putAways);
	}

	/**
	 * Set receivedFrom
	 *
	 * @param string $receivedFrom
	 * @return ReceiveOrder
	 */
	public function setReceivedFrom($receivedFrom)
	{
		$this->receivedFrom = $receivedFrom;
	
		return $this;
	}

	/**
	 * Get receivedFrom
	 *
	 * @return string 
	 */
	public function getReceivedFrom()
	{
		return $this->receivedFrom;
	}

	/**
	 * Set drNo
	 *
	 * @param string $drNo
	 * @return ReceiveOrder
	 */
	public function setDrNo($drNo)
	{
		$this->drNo = $drNo;
	
		return $this;
	}

	/**
	 * Get drNo
	 *
	 * @return string 
	 */
	public function getDrNo()
	{
		return $this->drNo;
	}

	/**
	 * Set soNo
	 *
	 * @param string $soNo
	 * @return ReceiveOrder
	 */
	public function setSoNo($soNo)
	{
		$this->soNo = $soNo;
	
		return $this;
	}

	/**
	 * Get soNo
	 *
	 * @return string 
	 */
	public function getSoNo()
	{
		return $this->soNo;
	}

	/**
	 * Set invoiceNumber
	 *
	 * @param string $invoiceNumber
	 * @return ReceiveOrder
	 */
	public function setInvoiceNumber($invoiceNumber)
	{
		$this->invoiceNumber = $invoiceNumber;
	
		return $this;
	}

	/**
	 * Get invoiceNumber
	 *
	 * @return string 
	 */
	public function getInvoiceNumber()
	{
		return $this->invoiceNumber;
	}

	/**
	 * Set containerNumber
	 *
	 * @param string $containerNumber
	 * @return ReceiveOrder
	 */
	public function setContainerNumber($containerNumber)
	{
		$this->containerNumber = $containerNumber;
	
		return $this;
	}

	/**
	 * Get containerNumber
	 *
	 * @return string 
	 */
	public function getContainerNumber()
	{
		return $this->containerNumber;
	}

	/**
	 * Set sealNumber
	 *
	 * @param string $sealNumber
	 * @return ReceiveOrder
	 */
	public function setSealNumber($sealNumber)
	{
		$this->sealNumber = $sealNumber;
	
		return $this;
	}

	/**
	 * Get sealNumber
	 *
	 * @return string 
	 */
	public function getSealNumber()
	{
		return $this->sealNumber;
	}

	/**
	 * Set factoryNumber
	 *
	 * @param string $factoryNumber
	 * @return ReceiveOrder
	 */
	public function setFactoryNumber($factoryNumber)
	{
		$this->factoryNumber = $factoryNumber;
	
		return $this;
	}

	/**
	 * Get factoryNumber
	 *
	 * @return string 
	 */
	public function getFactoryNumber()
	{
		return $this->factoryNumber;
	}

	/**
	 * Set supplierName
	 *
	 * @param string $supplierName
	 * @return ReceiveOrder
	 */
	public function setSupplierName($supplierName)
	{
		$this->supplierName = $supplierName;
	
		return $this;
	}

	/**
	 * Get supplierName
	 *
	 * @return string 
	 */
	public function getSupplierName()
	{
		return $this->supplierName;
	}

	public function isEditable()
	{
		if ($this->getPurchaseOrder()->getStatus() == PurchaseOrder::STATUS_APPROVED && $this->getActive() && count($this->getActivePutAways()) == 0) {
			return true;
		}
		return false;
	}
	
	public function translateStatus()
	{
		return $this->getActive() ? 'Received' : 'Void';
	}
	
	public function getISItems()
	{
		return $this->getItems();
	}
	
	public function getStatus()
	{
		return $this->translateStatus();
	}

	public function getWarehouse()
	{
		return $this->getPurchaseOrder()->getWarehouse();
	}
	
	public function validateItems(ExecutionContextInterface $context)
	{
		// validate if at least one item was added
		foreach ($this->getPurchaseOrder()->getItems() as $poItem) {
			$totalQuantity = 0;
			$maxQtyAllowed = $poItem->getMaxQtyAllowedToReceiveFromRO($this);
			foreach ($this->getItems() as $item) {
				if ($item->getIsIncluded() && $item->getPurchaseOrderItem()->getId() == $poItem->getId()) {
					$totalQuantity = $totalQuantity + $item->getQuantity();
				}
			}

			if ($totalQuantity > $maxQtyAllowed) {
				$context->addViolationAt('items', 'Purchase Order Quantity with product ' .
					$poItem->getProduct()->getName() . ' Limit reached.');
			}
		}

		foreach ($this->getItems() as $item) {
			if ($item->getIsIncluded()) {
				return true;
			}
		}
		
		$context->addViolationAt('items', 'At least one item must be received.');
	}
	
	public function getActivePurchaseReturns()
	{
		$criteria = Criteria::create()->where(Criteria::expr()->lt('status', PurchaseReturn::STATUS_VOID));
	
		return $this->getPurchaseReturns()->matching($criteria);
	}

	public function getActivePutAways()
	{
		$criteria = Criteria::create()->where(Criteria::expr()->eq('active', true));
	
		return $this->getPutAways()->matching($criteria);
	}

	/**
	 * Returns the quantity received for all RO items
	 */
	public function getQtyReceived()
	{
		$quantity = 0;
		foreach($this->getItems() as $item) {
			$quantity += $item->getQuantity();
		}
	
		return $quantity;
	}
	
	/**
	 * Returns the quantity put away for all PW items
	 */
	public function getQtyPutAway()
	{
		$quantity = 0;
		foreach($this->getItems() as $item) {
			$quantity += $item->getTotalPutAway();
		}
	
		return $quantity;
	}
	
	public function getLog()
	{
		$log = array(
			'PO #' => $this->getPurchaseOrder()->getPoId(),
			'Date Received' => $this->getDateReceived(),
			'Status' => $this->translateStatus(),
			'Received From' => $this->getReceivedFrom(),
			'DR #' => $this->getDrNo(),
			'Invoice #' => $this->getInvoiceNumber(),
			'Container #' => $this->getContainerNumber(),
			'Seal #' => $this->getSealNumber()
		);

		if ($this->getReferenceNumber()) {
			$log['Reference #'] = $this->getReferenceNumber();
		}

		if ($this->getMemo()) {
			$log['Memo #'] = $this->getMemo();
		}

		if ($this->getSoNo()) {
			$log['SO #'] = $this->getSoNo();
		}

		if ($this->getFactoryNumber()) {
			$log['Factory #'] = $this->getFactoryNumber();
		}

		return $log;
	}
}