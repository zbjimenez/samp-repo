<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * WarehouseInventory
 *
 * @ORM\Table(name="warehouse_inventory")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\WarehouseInventoryRepository")
 */
class WarehouseInventory extends BaseEntity
{
	const ADJUSTMENT_IN = 'In';
	const ADJUSTMENT_OUT = 'Out';
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="qty_allocated", type="decimal", scale=2, precision=13)
	 */
	private $qtyAllocated;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="qty_staged", type="decimal", scale=2, precision=13)
	 */
	private $qtyStaged;

	/**
	 * @ORM\ManyToOne(targetEntity="Warehouse", inversedBy="warehouseInventories")
	 */
	private $warehouse;

	/**
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="warehouseInventories")
	 */
	private $product;

	private $qtyChange;
	private $transType;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->qtyAllocated = 0;
		$this->qtyStaged = 0;
		$this->qtyChange = 0;
	}

	/**
	 * Set qtyAllocated
	 *
	 * @param float $qtyAllocated
	 * @return WarehouseInventory
	 */
	public function setQtyAllocated($qtyAllocated)
	{
		$this->qtyAllocated = $qtyAllocated;

		return $this;
	}

	/**
	 * Set qtyChange
	 *
	 * @param integer $qtyChange
	 * @return WarehouseInventory
	 */
	public function setQtyChange($qtyChange)
	{
		$this->qtyChange += $qtyChange;

		return $this;
	}

	/**
	 * Get qtyChange
	 *
	 * @return integer
	 */
	public function getQtyChange()
	{
		return $this->qtyChange;
	}


	/**
	 * Get qtyAllocated
	 *
	 * @return float
	 */
	public function getQtyAllocated()
	{
		return $this->qtyAllocated;
	}

	/**
	 * Set qtyStaged
	 *
	 * @param float $qtyStaged
	 * @return WarehouseInventory
	 */
	public function setQtyStaged($qtyStaged)
	{
		$this->qtyStaged = $qtyStaged;

		return $this;
	}


	/**
	 * Get qtyStaged
	 *
	 * @return float
	 */
	public function getQtyStaged()
	{
		return $this->qtyStaged;
	}

	/**
	 * Set warehouse
	 *
	 * @param \CI\InventoryBundle\Entity\StorageLocation $warehouse
	 * @return WarehouseInventory
	 */
	public function setWarehouse(\CI\InventoryBundle\Entity\Warehouse $warehouse = null)
	{
		$this->warehouse = $warehouse;

		return $this;
	}

	/**
	 * Get warehouse
	 *
	 * @return \CI\InventoryBundle\Entity\Warehouse
	 */
	public function getWarehouse()
	{
		return $this->warehouse;
	}

	/**
	 * Set product
	 *
	 * @param \CI\InventoryBundle\Entity\Product $product
	 * @return WarehouseInventory
	 */
	public function setProduct(\CI\InventoryBundle\Entity\Product $product = null)
	{
		$this->product = $product;

		return $this;
	}

	/**
	 * Get product
	 *
	 * @return \CI\InventoryBundle\Entity\Product
	 */
	public function getProduct()
	{
		return $this->product;
	}

	/**
	 * Set transType
	 *
	 * @param string $transType
	 * @return WarehouseInventory
	 */
	public function setTransType($transType)
	{
		$this->transType = $transType;
	
		return $this;
	}
	
	
	/**
	 * Get transType
	 *
	 * @return string
	 */
	public function getTransType()
	{
		return $this->transType;
	}

	public function getQtyAllocatedPackage()
	{
		return $this->getQtyAllocated() / $this->getProduct()->getPackaging()->getKgsUnit();	
	}

	public function getQtyStagedPackage()
	{
		return $this->getQtyStaged() / $this->getProduct()->getPackaging()->getKgsUnit();	
	}

	public function isEmpty()
	{
		return (($this->getQtyAllocated() + $this->getQtyStaged()) == 0);
	}
}
