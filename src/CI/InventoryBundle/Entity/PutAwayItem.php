<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use CI\InventoryBundle\DependencyInjection\InventoryServiceItemInterface;

/**
 * PutAwayItem
 *
 * @ORM\Table(name="put_away_item")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\PutAwayItemRepository")
 * @ORM\HasLifecycleCallbacks
 */
class PutAwayItem implements InventoryServiceItemInterface
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id; 

	/**
	 * @var boolean
	 */
	private $isIncluded = true;

	/**
	 * @ORM\ManyToOne(targetEntity="PutAway", inversedBy="items")
	 */
	private $putAway;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="storage_location_from_scan", type="string", length=255)
	 * @Assert\NotBlank(message="Storage Location must not be blank.")
	 * @Assert\Length(max=255)
	 * @Assert\Type(type="string")
	 */
	private $storageLocationFromScan;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="pallet_id", type="string", length=255)
	 * @Assert\NotBlank(message="Pallet ID must not be blank.")
	 * @Assert\Length(max=255)
	 * @Assert\Type(type="string")
	 */
	private $palletId;

	private $previousPalletId;

	/**
	 * @ORM\ManyToOne(targetEntity="StorageLocation", inversedBy="putAwayItems")
	 */
	private $storageLocation;

	private $previousStorageLocation;

	/**
	 * @ORM\ManyToOne(targetEntity="ReceiveOrderItem", inversedBy="putAwayItems")
	 */
	private $receiveOrderItem;

	/**
	 * @ORM\ManyToOne(targetEntity="BackloadItem", inversedBy="putAwayItems")
	 */
	private $backloadItem;

	
	/**
	 * Constructor
	 */
	public function __construct()
	{
	}
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set isIncluded
	 *
	 * @param boolean $isIncluded
	 * @return PutAwayItem
	 */
	public function setIsIncluded($isIncluded)
	{
		$this->isIncluded = $isIncluded;
	
		return $this;
	}
	
	/**
	 * Get isIncluded
	 *
	 * @return boolean
	 */
	public function getIsIncluded()
	{
		return $this->isIncluded;
	}

	/**
	 * Set previousStorageLocation
	 *
	 * @param StorageLocation $previousStorageLocation
	 * @return PutAwayItem
	 */
	public function setPreviousStorageLocation($previousStorageLocation)
	{
		$this->previousStorageLocation = $previousStorageLocation;
	
		return $this;
	}
	
	/**
	 * Get previousStorageLocation
	 *
	 * @return StorageLocation
	 */
	public function getPreviousStorageLocation()
	{
		return $this->previousStorageLocation;
	}

	/**
	 * Set previousPalletId
	 *
	 * @param PalletId $previousPalletId
	 * @return PutAwayItem
	 */
	public function setPreviousPalletId($previousPalletId)
	{
		$this->previousPalletId = $previousPalletId;
	
		return $this;
	}
	
	/**
	 * Get previousPalletId
	 *
	 * @return PalletId
	 */
	public function getPreviousPalletId()
	{
		return $this->previousPalletId;
	}
	

	/**
	 * Set putAway
	 *
	 * @param \CI\InventoryBundle\Entity\PutAway $putAway
	 * @return PutAwayItem
	 */
	public function setPutAway(\CI\InventoryBundle\Entity\PutAway $putAway = null)
	{
		$this->putAway = $putAway;
	
		return $this;
	}

	/**
	 * Get putAway
	 *
	 * @return \CI\InventoryBundle\Entity\PutAway 
	 */
	public function getPutAway()
	{
		return $this->putAway;
	}

	/**
	 * Set storageLocationFromScan
	 *
	 * @param string $storageLocationFromScan
	 * @return PutAwayItem
	 */
	public function setStorageLocationFromScan($storageLocationFromScan)
	{
		$this->storageLocationFromScan = $storageLocationFromScan;
	
		return $this;
	}

	/**
	 * Get storageLocationFromScan
	 *
	 * @return string 
	 */
	public function getStorageLocationFromScan()
	{
		return $this->storageLocationFromScan;
	}

	/**
	 * Set palletId
	 *
	 * @param string $palletId
	 * @return PutAwayItem
	 */
	public function setPalletId($palletId)
	{
		$this->palletId = $palletId;
	
		return $this;
	}

	/**
	 * Get palletId
	 *
	 * @return string 
	 */
	public function getPalletId()
	{
		return $this->palletId;
	}

	/**
	 * Set storageLocation
	 *
	 * @param \CI\InventoryBundle\Entity\StorageLocation $storageLocation
	 * @return PutAwayItem
	 */
	public function setStorageLocation(\CI\InventoryBundle\Entity\StorageLocation $storageLocation = null)
	{
		$this->storageLocation = $storageLocation;
	
		return $this;
	}

	/**
	 * Get storageLocation
	 *
	 * @return \CI\InventoryBundle\Entity\StorageLocation 
	 */
	public function getStorageLocation()
	{
		return $this->storageLocation;
	}

	/**
	 * Set receiveOrderItem
	 *
	 * @param \CI\InventoryBundle\Entity\PurchaseOrderItem $receiveOrderItem
	 * @return ReceiveOrderItem
	 */
	public function setReceiveOrderItem(\CI\InventoryBundle\Entity\ReceiveOrderItem $receiveOrderItem = null)
	{
		$this->receiveOrderItem = $receiveOrderItem;
	
		return $this;
	}

	/**
	 * Get receiveOrderItem
	 *
	 * @return \CI\InventoryBundle\Entity\ReceiveOrderItem 
	 */
	public function getReceiveOrderItem()
	{
		return $this->receiveOrderItem;
	}

	/**
	 * Set backloadItem
	 *
	 * @param \CI\InventoryBundle\Entity\BackloadItem $backloadItem
	 * @return PutAwayItem
	 */
	public function setBackloadItem(\CI\InventoryBundle\Entity\BackloadItem $backloadItem = null)
	{
		$this->backloadItem = $backloadItem;
	
		return $this;
	}

	/**
	 * Get backloadItem
	 *
	 * @return \CI\InventoryBundle\Entity\BackloadItem 
	 */
	public function getBackloadItem()
	{
		return $this->backloadItem;
	}

	public function getProduct()
	{
		if ($this->getPutAway()->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
			return $this->getReceiveOrderItem()->getProduct();
		} else {
			return $this->getBackloadItem()->getShippingItem()->getSalesOrderItem()->getQuotation()->getProduct();
		}
	}
	
	public function getISProduct()
	{
		return $this->getProduct();
	}

	public function getQuantity()
	{
		if ($this->getPutAway()->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
			return $this->getReceiveOrderItem()->getQuantity();
		} else {
			return $this->getBackloadItem()->getQuantity();
		}
	}
	
	public function getISQuantity()
	{
		return $this->getQuantity();
	}

	public function getWarehouse()
	{
		return $this->getStorageLocation()->getWarehouse();
	}

	public function getLotNumber()
	{
		if ($this->getPutAway()->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
			return $this->getReceiveOrderItem()->getLotNumber();
		} else {
			return $this->getBackloadItem()->getLotNumber();
		}
	}

	public function getBarcode()
	{
		if ($this->getPutAway()->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
			return $this->getReceiveOrderItem()->getBarcode();
		} else {
			return '';
		}
	}

	public function getProductionDate()
	{
		if ($this->getPutAway()->getPutAwayType() == PutAway::TYPE_RECEIVE_ORDER) {
			return $this->getReceiveOrderItem()->getProductionDate();
		} else {
			return $this->getBackloadItem()->getProductionDate();
		}
	}
	
	public function getLog()
	{
		return array(
			'Lot No.' => $this->getLotNumber(),
			'Barcode' => $this->getBarcode(), 
			'Quantity' => number_format($this->getQuantity(), 2) . ' kg',
			'Production Date' => $this->getProductionDate(),
			'Pallet ID' => $this->getPalletId(),
			'Product' => $this->getProduct()->getSku() . ' ' . $this->getProduct()->getName(),
			'Storage Location' => $this->getStorageLocation()->getFullLocation()
		);
	}
}