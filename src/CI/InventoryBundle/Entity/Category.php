<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\CategoryRepository")
 * @ORM\HasLifeCycleCallbacks
 * @UniqueEntity(fields="name", message="This Category name is already in use.")
 */
class Category extends BaseEntity
{
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=100)
	 * @Assert\NotBlank(message="Name must not be blank.")
	 * @Assert\Length(max=100, maxMessage="Input must be less than {{ limit }} characters long.")
	 * @Assert\Type(type="string")
	 */
	private $name;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 * @Assert\Type("string")
	 */
	private $description;
	
	/**
	 * @ORM\OneToMany(targetEntity="Category", mappedBy="parent", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $children;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
	 */
	private $parent;
	
	/**
	 * @ORM\OneToMany(targetEntity="Product", mappedBy="category", cascade={"persist", "remove"})
	 */
	private $products;
	
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add children
     *
     * @param \CI\InventoryBundle\Entity\Category $children
     * @return Category
     */
    public function addChildren(\CI\InventoryBundle\Entity\Category $children)
    {
    	$children->setParent($this);
        $this->children[] = $children;
    
        return $this;
    }

    /**
     * Remove children
     *
     * @param \CI\InventoryBundle\Entity\Category $children
     */
    public function removeChildren(\CI\InventoryBundle\Entity\Category $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \CI\InventoryBundle\Entity\Category $parent
     * @return Category
     */
    public function setParent(\CI\InventoryBundle\Entity\Category $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \CI\InventoryBundle\Entity\Category 
     */
    public function getParent()
    {
        return $this->parent;
    }
    
    /**
     * Add products
     *
     * @param \CI\InventoryBundle\Entity\Product $products
     * @return Category
     */
    public function addProduct(\CI\InventoryBundle\Entity\Product $products)
    {
    	$this->products[] = $products;
    
    	return $this;
    }
    
    /**
     * Remove products
     *
     * @param \CI\InventoryBundle\Entity\Product $products
     */
    public function removeProduct(\CI\InventoryBundle\Entity\Product $products)
    {
    	$this->products->removeElement($products);
    }
    
    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
    	return $this->products;
    }

    public function getLog()
    {
    	return array(
    		'Active' => $this->getActive() ? 'Yes' : 'No',
    		'Parent Category' => $this->getParent() ? $this->getParent()->getName() : 'None',
    		'Name' => $this->getName(),
    		'Description' => $this->getDescription()
    	);
    }
}