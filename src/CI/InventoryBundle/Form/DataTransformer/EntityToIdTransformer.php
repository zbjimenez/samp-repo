<?php

namespace CI\InventoryBundle\Form\DataTransformer;
 
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
 
class EntityToIdTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;
 
    /**
     * @var string
     */
    protected $class;
    
    protected $handleNan;
 
    public function __construct(ObjectManager $objectManager, $class, $handleNan = false)
    {
        $this->objectManager = $objectManager;
        $this->class = $class;
        $this->handleNan = $handleNan;
    }
 
    public function transform($entity)
    {
        if (null === $entity) {
            return;
        }
 
        return $entity->getId();
    }
 
    public function reverseTransform($id)
    {
    	if ($this->handleNan) {
    		$expression = !$id || !is_numeric($id);
    	} else {
    		$expression = !$id;
    	}
    	 
    	
        if ($expression) {
            return null;
        }
 
        $entity = $this->objectManager
                      ->getRepository($this->class)
                      ->find($id);
 
        if (null === $entity) {
            throw new TransformationFailedException();
        }
 
        return $entity;
    }
}
