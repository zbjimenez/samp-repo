<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder
    		->add('name', 'text', array(
    			'required' => true,
    			'attr' => array(
    				'widget_col' => 4
    			)
    		))
    		->add('description', 'textarea', array(
	    		'required' => false,
				'attr'=> array('rows' => 4)
    		))
    		->add('save', 'submit', array(
    			'label' => 'Save',
    			'attr' => array(
    				'class' => 'btn btn-success submit-button',
    				'data-loading-text' => "Saving..."
    			)
    		))
    		;
    	
		 	$factory = $builder->getFormFactory();
	        $builder->addEventListener(
	            FormEvents::PRE_SET_DATA,
	            function(FormEvent $event) use($factory) {
	                $form = $event->getForm();
	                $data = $event->getData();
	                
	                $formOptions = array(
	                    'class' => 'CIInventoryBundle:Category',
	                	'required' => false,
	                	'empty_value' => 'Choose a parent category',
	                    'property' => 'name',
	                	'label' => 'Parent Category',
	                	'attr' => array(
	                		'widget_col' => 5,
	                		'class' => 'parent-category select2'
	                	),
	                	'auto_initialize' => false,
	                    'query_builder' => function($repository) use ($data) {
	                        $qb = $repository->createQueryBuilder('c')
	                        	->where('c.active = 1')
	                        	->andWhere('c.parent IS NULL');
	                        
	                        if ($data->getId()) {
	                        	$qb->andWhere('c.id != :id')
	                        		->setParameter('id', $data->getId());
	                        }
	                        return $qb;
	                    },
	                );
	
	                $form->add($factory->createNamed('parent', 'entity', null, $formOptions));
	            }
	        );
    	;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    	$resolver->setDefaults(array(
    		'data_class' => 'CI\InventoryBundle\Entity\Category'
    	));
    }
    
    public function getName()
    {
    	return 'ci_inventorybundle_category';
    }
}