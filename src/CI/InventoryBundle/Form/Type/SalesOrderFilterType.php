<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use CI\InventoryBundle\Entity\SalesOrder;
use Doctrine\ORM\EntityRepository;

class SalesOrderFilterType extends AbstractType
{
	private $sc;

	public function __construct(SecurityContextInterface $sc)
	{
		$this->sc = $sc;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$sc = $this->sc;

		$builder
		->add('refCode', 'text', array(
			'label' => 'SO #',
			'required' => false,
			'attr' => array('widget_col' => 4),
		))
		->add('customer', 'entity', array(
			'class' => 'CIInventoryBundle:Customer',
			'property' => 'shortName',
			'empty_value' => 'Choose a customer',
			'required' => false,
			'attr' => array('class' => 'select2 customer'),
			'query_builder' => function (EntityRepository $repository) use ($sc) {
				if ($sc->isGranted('ROLE_SALESORDER_VIEW_FROM_ALL_USERS')) {
					return $repository->findAllQb();
				} else {
					return $repository->createQueryBuilder('c')
						->select('c')
						->join('c.salesAgent', 'agent')
						->where('agent.id = :agentId')
						->setParameter('agentId', $sc->getToken()->getUser()->getId());
				}
			},
		))
		->add('poRefCode', 'text', array(
			'label' => 'PO #',
			'required' => false,
			'attr' => array('widget_col' => 4),
		))
		->add('status', 'choice', array(
			'required' => false,
			'empty_value' => 'All status',
			'attr' => array('widget_col' => 4),
			'choices' => array(
				SalesOrder::STATUS_DRAFT => 'Draft',
				SalesOrder::STATUS_APPROVED => 'Approved',
				SalesOrder::STATUS_VOID => 'Void',
				SalesOrder::STATUS_CLOSED => 'Closed',
			),
		))
		->add('dateFrom', 'date', array(
			'label' => 'Date From',
			'widget' => 'single_text',
			'format' => 'MM/dd/y',
			'required' => false,
			'attr' => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			),
		))
		->add('dateTo', 'date', array(
			'label' => 'Date To',
			'widget' => 'single_text',
			'format' => 'MM/dd/y',
			'required' => false,
			'attr' => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			),
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching...",
			),
		))
		;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesorderfilter';
	}
}
