<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use CI\InventoryBundle\Entity\SalesReturn;

class SalesReturnFilterType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->setMethod("GET")
		->add('salesOrder', 'text', array(
			'label' => 'SO#',
			'required' => false,
		))
		->add('shipping', 'text', array(
			'label' => 'DR#',
			'required' => false,
		))
		->add('status', 'choice', array(
			'required' => false,
			'empty_value' => 'All status',
			'attr' => array('widget_col' => 4),
			'choices' => array(
				SalesReturn::STATUS_DRAFT => 'Draft',
				SalesReturn::STATUS_RETURNED => 'Returned',
				SalesReturn::STATUS_VOID => 'Void',
			),
		))
		->add('dateFrom', 'date', array(
			'label'    => 'Return Date From',
			'required' => false,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
				'widget_col' => 5,
			),
		))
		->add('dateTo', 'date', array(
			'label'    => 'Return Date To',
			'required' => false,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
				'widget_col' => 5,
			),
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesreturnfilter';
	}
}