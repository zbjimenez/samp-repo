<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CI\InventoryBundle\Entity\InventoryLog;

class InventoryLogFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->setMethod('GET')
			->add('product', 'entity', array(
				'class' => 'CIInventoryBundle:Product',
				'property' => 'sku',
				'label' => 'Product',
				'required' => false,
				'empty_value' => 'All products',
				'attr' => array('select2' => 'select2'),
				'query_builder' => function($repository) {
					return $repository->findAllQb();
				}
			))
			->add('dateFrom', 'date', array(
				'attr' => array(
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
					'widget_col' => 5
				),
				'label' => 'Date From',
				'required' => false,
				'widget' => 'single_text',
				'format' => 'MM/dd/y'
			))
			->add('dateTo', 'date', array(
				'attr'	   => array(
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
					'widget_col' => 5
				),
				'label' => 'Date To',
				'required' => false,
				'widget' => 'single_text',
				'format' => 'MM/dd/y'
			))
			->add('type', 'choice', array(
				'required' => false,
				'label' => 'Transaction',
				'empty_value' => 'All types',
				'attr' => array(
					'select2' => 'select2',
					'class' => 'product',
					'widget_col' => 6
				),
				'choices' => array(
					InventoryLog::TRANS_PUTAWAY => 'Put Away',
					InventoryLog::TRANS_DELIVER => 'Deliver/Order Picking',
					InventoryLog::TRANS_ADJUST =>'Manual Adjustment'
				)
			))
			->add('search', 'submit', array(
					'attr' => array(
						'class' => 'btn btn-outline submit-button',
						'data-loading-text' => "Searching..."
					)
			))
		;
	}

	public function getName()
	{
		return 'ci_inventorybundle_inventorylogfilter';
	}
}