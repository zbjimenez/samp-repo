<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CI\InventoryBundle\Entity\Supplier;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class SupplierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder
    	->add('name', 'text', array(
    		'attr' => array('widget_col' => 7)
    	))
    	->add('shortName', 'text', array(
    			'attr' => array('widget_col' => 7)
    	))
    	->add('address', 'textarea', array(
    			'attr' => array('rows' => 4)
    		))
    	->add('contactDetails', 'textarea', array(
    			'label' => 'Contact Details',
    			'attr' => array('rows' => 4)
    		))
    	->add('terms', 'integer', array(
    			'attr' => array(
    				'widget_col' => 4,
    				'input_group' => array('append' => 'days')
    			)
    		))
    	->add('ewt','choice', array(
    			'label' => 'EWT',
    			'attr' => array('widget_col' => 4),
    			'empty_value' => 'Choose an EWT',
    			'choices'  => array(
    				Supplier::EWT_NONE => 'None',
    				Supplier::EWT_GOODS => '1% (Goods)',
    				Supplier::EWT_SERVICES => '2% (Services)'
    			),
    		))
    	->add('memo', 'textarea', array(
    			'required' => false,
    				'attr' => array('rows' => 4)
    		))
    	->add('save', 'submit', array(
    			'attr' => array(
    				'class' => 'btn btn-success submit-button',
    				'data-loading-text' => 'Saving...'
    		)
    	))
    	;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    	$resolver->setDefaults(array(
    		'data_class' => 'CI\InventoryBundle\Entity\Supplier'
    	));
    }
    
    public function getName()
    {
    	return 'ci_inventorybundle_supplier';
    }
}