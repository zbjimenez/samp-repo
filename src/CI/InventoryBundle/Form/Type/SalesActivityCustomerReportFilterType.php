<?php

namespace CI\InventoryBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ExecutionContextInterface;

class SalesActivityCustomerReportFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('inOffice', 'checkbox', array(
			'required' => false,
			'label' => 'Show only in office activities?',
			'attr'=> array(
				'class' => 'in-office px',
				'align_with_widget' => true,
			),
		))
		->add('dateFrom', 'date', array(
			'label'    => 'Date From',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'constraints' => array(new NotBlank()),
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('dateTo', 'date', array(
			'label'    => 'Date To',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'constraints' => array(new NotBlank()),
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
		
		$formMod = function($form, $constraints) {
			$form
			->add('customer', 'entity', array(
				'class' => 'CIInventoryBundle:Customer',
				'property' => 'name',
				'empty_value' => 'Choose a customer',
				'constraints' => $constraints,
				'attr' => array('class' => 'select2 customer'),
				'query_builder' => function(EntityRepository $repository) {
					return $repository->findAllQb();
				}
			))
			;
		};
		
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($formMod) {
			$form = $event->getForm();
			$formMod($form, []);
		});
		
		$builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($formMod) {
			$form = $event->getForm();
			$data = $event->getData();
			
			$constraints = [];
			
			if (!isset($data['inOffice'])) {
				$constraints[] = new NotBlank();
			}
			
			$formMod($form, $constraints);
		});
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesactivitycustomerreportfilter';
	}
}
