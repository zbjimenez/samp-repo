<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PurchaseReturnItemType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('receiveOrderItem', 'entity_hidden', array(
			'class' => 'CIInventoryBundle:ReceiveOrderItem',
			'required' => true,
			'attr'=> array(
				'ng-model' => 'form.items[key].roItem.id',
				'ng-value' => 'form.items[key].roItem.id'
			)
		))
		->add('quantity', 'custom_number', array(
			'attr' => array(
				'class' => 'amount',
				'ng-model' => 'form.items[key].quantity',
				'ng-change' => 'computeTotal()'
			)
		))
		->add('remarks', 'textarea', array(
			'required' => false,
			'attr'=> array(
				'rows' => 4,
				'ng-model' => 'form.items[key].remarks',
			)
		))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\PurchaseReturnItem'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_purchasereturnitem';
	}
}