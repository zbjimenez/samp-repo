<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductCodeType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('customer', 'entity', array(
        		'class' => 'CIInventoryBundle:Customer',
        		'required' => true,
        		'empty_value' => 'Choose a customer',
        		'property' => 'name',
        		'attr' => array('select2' => 'select2'),
        		'query_builder' => function($er) {
        			return $er->findAllQb();
        		}
	        ))
        	->add('alias')
        	->add('code', 'text', array(
        		'attr' => array('widget_col' => 5), 
        		'required' => false,
        	))
        	->add('product', 'entity_hidden', array(
        		'class' => 'CIInventoryBundle:Product'
        	))
            ->add('save', 'submit', array(
            	'label' => 'Save',
            	'attr' => array(
            		'ng-disabled' => 'form.$invalid || eform.$invalid',
            		'class' => 'btn btn-success submit-button',
            		'data-loading-text' => "Saving..."
            	)
            ))
		;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CI\InventoryBundle\Entity\ProductCode'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ci_inventorybundle_productcode';
    }
}