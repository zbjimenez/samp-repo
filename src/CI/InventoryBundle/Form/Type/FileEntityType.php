<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FileEntityType extends AbstractType
{
	protected $info;
	
	public function __construct($info)
	{
		$this->info = $info;
	}
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add($this->info["propertyName"], 'collection', array(
		     	'type' => new FileItemType(
		     		array(
		     			"class" => $this->info["childClass"],
		     			"formName" => $this->info["childFormName"],
		     		)
		     	),
		     	'allow_add' => true,
		     	'allow_delete' => true,
		     	'by_reference' => false,
			))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->info["class"] 
        ));
    }

    public function getName()
    {
        return $this->info["formName"];
    }
}