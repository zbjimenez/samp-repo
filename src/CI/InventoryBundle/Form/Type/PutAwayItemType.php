<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use CI\InventoryBundle\Entity\PutAway;

class PutAwayItemType extends AbstractType
{
	protected $type;
	
	public function __construct($type)
	{
		$this->type = $type;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$type = $this->type;

		if ($type === PutAway::TYPE_RECEIVE_ORDER) {
			$builder
				->add('receiveOrderItem', 'entity_hidden', array(
					'class' => 'CIInventoryBundle:ReceiveOrderItem',
					'attr' => array(
						'ng-model' => 'form.items[key].roItem.id',
						'ng-readonly' => '!item.isIncluded',
						'ng-value' => 'form.items[key].roItem.id'
					)
				))
			;
		} else {
			$builder
				->add('backloadItem', 'entity_hidden', array(
					'class' => 'CIInventoryBundle:BackloadItem',
					'attr' => array(
						'ng-model' => 'form.items[key].backloadItem.id',
						'ng-readonly' => '!item.isIncluded',
						'ng-value' => 'form.items[key].backloadItem.id'
					)
				))
			;
		}

		$builder
			->add('isIncluded', 'checkbox', array(
				'required' => false,
				'attr' => array(
					'class' => 'px',
					'align_with_widget' => true,
					'ng-model' => 'form.items[key].isIncluded'
				)
			))
			->add('storageLocationFromScan', 'text', array(
				'required' => false,
				'attr' => array(
					'ng-model' => 'form.items[key].storageLocationFromScan',
					'ng-readonly' => '!item.isIncluded'
				)
			))
			->add('palletId', 'text', array(
				'required' => false,
				'attr' => array(
					'ng-model' => 'form.items[key].palletId',
					'ng-readonly' => '!item.isIncluded'
				)
			))
		;
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\PutAwayItem',
		));
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_putawayitem';
	}
}