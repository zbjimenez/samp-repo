<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\NotBlank;

class SalesActivityReportFilterType extends AbstractType
{
	private $securityContext;

	public function __construct($securityContext = null)
	{
		$this->securityContext = $securityContext;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('dateFrom', 'date', array(
			'label' => 'Date From',
			'widget' => 'single_text',
			'format' => 'MM/dd/y',
			'constraints' => array(new NotBlank()),
			'attr' => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			),
		))
		->add('dateTo', 'date', array(
			'label' => 'Date To',
			'widget' => 'single_text',
			'format' => 'MM/dd/y',
			'constraints' => array(new NotBlank()),
			'attr' => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			),
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching...",
			),
		))
		;

		if ($this->securityContext->isGranted('ROLE_SALES_ACTIVITY_REPORT_VIEW_FROM_ALL_USERS')) {
			$builder->add('agent', 'entity', array(
				'class' => 'CICoreBundle:User',
				'property' => 'name',
				'empty_value' => 'Choose an agent',
				'constraints' => array(new NotBlank()),
				'attr' => array('class' => 'select2 agent'),
				'query_builder' => function (EntityRepository $repository) {
					return $repository->createQueryBuilder('a')
					->select('a')
					->leftJoin('a.groups', 'g')
					->where("g.name = 'Sales Agent' OR g.name = 'Sales Manager' OR a.roles LIKE :superAdmin OR a.roles LIKE :admin")
					->andWhere('a.expired = 0')
					->setParameter('superAdmin', '%"ROLE_SUPER_ADMIN"%')
					->setParameter('admin', '%"ROLE_ADMIN"%')
					->orderBy('a.lastName', 'ASC')
					;
				},
			));
		}
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesactivityreportfilter';
	}
}
