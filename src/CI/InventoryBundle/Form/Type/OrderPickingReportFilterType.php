<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class OrderPickingReportFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		
		$builder
		->add('warehouse', 'text', array(
        		'label' => 'Warehouse Number',
	        	'required' => false,
        		'attr' => array('widget_col' => 5)
        	))
        	->add('customer', 'entity', array(
                        'class' => 'CIInventoryBundle:Customer',
                        'property' => 'shortName',
                        'empty_value' => 'All customers',
                        'required' => false,
                        'attr' => array('class' => 'select2 customer'),
                        'query_builder' => function(EntityRepository $repository) {
                                return $repository->findAllQb();
                        }
                ))
        	->add('date', 'date', array(
        		'label'    => 'Date',
        		'widget'   => 'single_text',
                        'required' => false,
        		'format'   => 'MM/dd/y',
        		'attr'	   => array(
        			'widget_col' => 5,
        			'datepicker' => true,
        			'input_group' => array('append' => 'calendar')
        		)
        	))
		->add('search', 'submit', array(
                        'label' => 'Search',
                        'attr' => array(
                                'class' => 'btn btn-default btn-outline submit-button',
                                'data-loading-text' => "Search..."
                        )
                ))
		;
	}

	public function getName()
	{
		return 'ci_inventorybundle_orderpickingreportfiltertype';
	}
}