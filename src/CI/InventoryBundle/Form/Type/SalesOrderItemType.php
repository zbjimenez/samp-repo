<?php

namespace CI\InventoryBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use CI\CoreBundle\Entity\User;
use CI\InventoryBundle\Entity\Customer;
use CI\InventoryBundle\Entity\Product;
use CI\InventoryBundle\Entity\Quotation;
use CI\InventoryBundle\Entity\SalesOrderItem;

class SalesOrderItemType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('quotation', 'entity_autocomplete', array(
			'class' => 'CIInventoryBundle:Quotation',
			'required' => true,
			'attr'=> array(
				'placeholder' => 'search by reference number',
				'ng-model' => 'form.items[key].quotation.id',
				'ng-change' => 'clearResults(item)',
				'ng-blur' => 'clearResults(item)',
				'typeahead' => 'quotation as quotation.htmlLabel for quotation in getQuotations($viewValue, item)',
				'typeahead-template-url' => 'typeahead.template.html.twig',
				'typeahead-on-select' => 'onSelectItem($item, $model, $label, key)',
				'tooltip' => 'No results found.',
				'tooltip-placement' => 'right',
				'tooltip-toggle' => 'form.items[key].quotation.noResults'
			)
		))
		->add('quantity', 'custom_number', array(
			'attr' => array(
				'class' => 'quantity',
				'ng-change' => 'computeSubtotal()',
				'ng-model' => 'form.items[key].quantity'
			)
		))
		->add('unitPrice', 'custom_number', array(
			'attr' => array(
				'class' => 'unit-price',
				'ng-change' => 'computeSubtotal()',
				'ng-model' => 'form.items[key].unitPrice'
			)
		))
		;
	}
	
	public function getCustomerId()
	{
		if ($this->customer instanceof Customer) {
			return $this->customer->getId();
		} else {
			return $this->customer;
		}
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\SalesOrderItem'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesorderitem';
	}
}