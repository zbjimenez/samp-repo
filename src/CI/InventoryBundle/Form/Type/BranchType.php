<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BranchType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        	->add('isMain', 'checkbox', array(
        		'required' => false,
        		'label' => 'Main',
        		'attr'=> array(
        			'class' => 'is-main px',
        			'align_with_widget' => true
        		)
        	))
        	->add('name', 'text', array(
        		'label' => 'Name',
        		'attr' => array('widget_col' => 4)
        	))
        	->add('address', 'textarea', array(
        		'label' => 'Address',
        		'attr'	=> array(
        			'rows' => 4
        		)
        	))
        	->add('area', 'text', array(
        		'required' => false,
        		'attr' => array(
        			'widget_col' => 4
        		)
        	))
        	->add('customer', 'entity_hidden', array(
        		'class' => 'CIInventoryBundle:Customer'
        	))
        	->add('contactDetails', 'text', array(
        		'label' => 'Contact Details',
        		'required' => false
        	))
            ->add('save', 'submit', array(
            	'label' => 'Save',
            	'attr' => array(
            		'ng-disabled' => 'form.$invalid || eform.$invalid',
            		'class' => 'btn btn-success submit-button',
            		'data-loading-text' => "Saving..."
            	)
            ))
		;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CI\InventoryBundle\Entity\Branch'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ci_inventorybundle_branch';
    }
}