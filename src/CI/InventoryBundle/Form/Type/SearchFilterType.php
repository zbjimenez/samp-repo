<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchFilterType extends AbstractType
{
    protected $placeholder;
    protected $label;
    
    public function __construct($placeholder = null, $label = null)
    {
        $this->placeholder = $placeholder;
        $this->label = $label;
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $placeholder = $this->placeholder;
        $label = $this->label;
        
        $builder
            ->add('status', 'choice', array(
                'required' => false,
        		'empty_value' => 'All',
        		'data' => true,
        		'choices' => array(
        		    false => 'Inactive',
        			true => 'Active')))
        	->add('query', 'text', array(
        	    'label' => ucwords($label ? $label : $placeholder),
        		'required' => false,
        		'attr' => array(
	        		'placeholder' => 'Search by ' . $placeholder,
        		)	
        	));
    }

    public function getName()
    {
        return 'ci_inventorybundle_searchfilter';
    }
}
