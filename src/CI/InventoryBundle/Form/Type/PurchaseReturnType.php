<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PurchaseReturnType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('returnDate', 'date', array(
			'label' => 'Return Date',
			'widget' => 'single_text',
			'format' => 'MM/dd/y',
			'attr' => array(
				'widget_col' => 6,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			),
		))
		->add('actionTaken', 'textarea', array(
			'required' => false,
			'attr' => array('rows' => 4),
		))
		->add('items', 'collection', array(
			'type' => new PurchaseReturnItemType(),
			'allow_add' => true,
			'allow_delete' => true,
			'by_reference' => false,
			'error_bubbling' => false,
			'prototype_name' => 'ANGLR_key_ANGLR'
		))
		->add('invoiceNumber', 'text', array(
			'label' => 'Invoice Number',
			'attr' => array('widget_col' => 4),
			'required' => true
		))
		->add('datePrepared', 'date', array(
			'label' => 'Date Prepared',
			'widget' => 'single_text',
			'format' => 'MM/dd/y',
			'attr' => array(
				'widget_col' => 6,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			),
		))
		->add('natureIsQuantity', 'checkbox', array(
			'label' => 'Quantity',
			'attr'=> array(
				'class' => 'px',
				'align_with_widget' => true
			)
		))
		->add('natureIsQuality', 'checkbox', array(
			'label' => 'Quality',
			'attr'=> array(
				'class' => 'px',
				'align_with_widget' => true
			)
		))
		->add('natureIsOther', 'checkbox', array(
			'label' => 'Others',
			'attr'=> array(
				'class' => 'px',
				'align_with_widget' => true
			)
		))
		->add('particular', 'text', array(
			'required' => false,
			'label' => 'Particular',
			'attr' => array('widget_col' => 8)
		))
		->add('saveDraft', 'submit', array(
			'label' => 'Save as Draft',
			'attr' => array(
				'class' => 'btn btn-info submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		->add('saveReturned', 'submit', array(
			'label' => 'Save as Returned',
			'attr' => array(
				'class' => 'btn btn-success submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
		
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
			$form = $event->getForm();
			$data = $event->getData();
				
			if (!$data || $data->getId() === null) {
				$form
				->add('receiveOrder', 'entity_hidden', array(
					'class' => 'CIInventoryBundle:ReceiveOrder',
					'label' => 'RO #',
					'required' => true,
				))
				->add('supplier', 'entity_hidden', array(
					'class' => 'CIInventoryBundle:Supplier',
					'label' => 'Supplier',
					'required' => true,
				));
			}
		});

		$builder->addEventListener(FormEvents::POST_BIND, function (FormEvent $event) {
			$form = $event->getForm();
			$data = $event->getData();

			if (!$data->getNatureIsQuantity() && !$data->getNatureIsQuality() && !$data->getNatureIsOther()) {
				$form['natureIsQuantity']->addError(new FormError('Please check at least one.'));
			}
		});
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\PurchaseReturn'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_purchasereturn';
	}
}