<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use CI\InventoryBundle\Entity\Quotation;
use CI\CoreBundle\Entity\User;
use Symfony\Component\Security\Core\SecurityContext;

class QuotationFilterType extends AbstractType
{	
	protected $sc;
	
	public function __construct(SecurityContext $sc)
	{
		$this->sc = $sc;
	}
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$sc = $this->sc;
    	
        $builder
	        ->add('referenceNumber', 'text', array(
	        	'required' => false,
	        	'label' => 'Quotation #',
	        	'attr' => array('widget_col' => 4)
	        ))
        	->add('status', 'choice', array(
        		'required' => false,
        		'empty_value' => 'All status',
        		'attr' => array('widget_col' => 5),
        		'choices' => array(
        			Quotation::STATUS_PENDING => 'Pending for Approval',
        			Quotation::STATUS_APPROVED => 'Approved',
        			Quotation::STATUS_VOID => 'Void'
        	)))
        	->add('dateFrom', 'date', array(
        		'label'    => 'Date From',
        		'required' => false,
        		'widget'   => 'single_text',
        		'format'   => 'MM/dd/y',
        		'attr'	   => array(
        			'widget_col' => 4,
        			'datepicker' => true,
        			'input_group' => array('append' => 'calendar'),
        	)))
        	->add('dateTo', 'date', array(
        		'label'    => 'Date To',
        		'required' => false,
        		'widget'   => 'single_text',
        		'format'   => 'MM/dd/y',
        		'attr'	   => array(
        			'widget_col' => 4,
        			'datepicker' => true,
        			'input_group' => array('append' => 'calendar'),
        	)))
        	->add('search', 'submit', array(
        			'attr' => array(
        					'class' => 'btn btn-outline submit-button',
        					'data-loading-text' => "Searching..."
        			)
        	))
        ;
        	
        $builder->addEventListener(FormEvents::PRE_SET_DATA,
        	function (FormEvent $event) use ($sc) {
        		$product = $event->getData();
        		$form = $event->getForm();
        					
        		if ($sc->isGranted('ROLE_QUOTATION_VIEW_ALL')) {
        			$form->add('salesAgent', 'entity', array(
		        		'class' => 'CICoreBundle:User',
        				'label' => 'Sales Agent',
		        		'property' => 'name',
						'required' => false,
		        		'attr' => array('select2' => 'select2'),
		        		'empty_value' => 'All sales agents',
		        		'query_builder' => function($repository) {
		        			return $repository->findAllQb('Sales Agent');
		        		}
		        	));
        		}
        });
    }

    public function getName()
    {
        return 'ci_inventorybundle_salesorderfilter';
    }
}