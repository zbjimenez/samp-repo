<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use CI\InventoryBundle\Entity\Product;

class ProductType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('sku', 'text', array(
				'attr' => array('widget_col' => 4)
			))
			->add('name', 'text', array(
				'attr' => array('widget_col' => 6)
			))
			->add('description', 'textarea', array(
				'attr' => array('rows' => 4)
			))
			->add('barcode', 'text', array(
				'required' => false,
				'attr' => array('widget_col' => 5)
			))
			->add('storageTemp', 'text', array(
				'label' => 'Storage Temperature',
				'required' => false,
				'attr' => array('widget_col' => 4)
			))
			->add('shelfLife', 'text', array(
					'label' => 'Shelf Life',
					'required' => false,
					'attr' => array('widget_col' => 4)
			))
			->add('otherProductInfo', 'textarea', array(
					'label' => 'Other Product Info',
					'required' => false,
					'attr' => array('rows' => 4)
			))
			->add('isHalal', 'checkbox', array(
					'label' => 'Halal',
					'required' => false,
					'attr' => array(
						'align_with_widget' => true
					)
			))
			->add('supplierProductName', 'text', array(
					'label' => 'Supplier Product Name',
					'required' => false,
					'attr' => array('widget_col' => 4)
			))
			->add('supplierProductCode', 'text', array(
					'label' => 'Supplier Product Code',
					'required' => false,
					'attr' => array('widget_col' => 4)
			))
			->add('category', 'entity', array(
				'class' => 'CIInventoryBundle:Category',
				'required' => true,
				'empty_value' => 'Choose a category',
				'property' => 'name',
				'attr' => array('select2' => 'select2'),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('supplier', 'entity', array(
				'class' => 'CIInventoryBundle:Supplier',
				'required' => true,
				'empty_value' => 'Choose a supplier',
				'property' => 'name',
				'attr' => array('select2' => 'select2'),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('packaging', 'entity', array(
					'class' => 'CIInventoryBundle:Packaging',
					'required' => true,
					'empty_value' => 'Choose a packaging',
					'property' => 'name',
					'attr' => array('select2' => 'select2'),
					'query_builder' => function($er) {
						return $er->findAllQb();
					}
			))
			->add('reorderThreshold', 'custom_number', array(
					'label' => 'Reorder Threshold*',
					'required' => false,
					'attr' => array('widget_col' => 4)
			))
			->add('reorderQuantity', 'custom_number', array(
					'label' => 'Reorder Quantity*',
					'required' => false,
					'attr' => array('widget_col' => 4)
			))
			->add('industry', 'entity', array(
					'class' => 'CIInventoryBundle:Industry',
					'required' => true,
					'empty_value' => 'Choose an industry',
					'property' => 'name',
					'attr' => array('select2' => 'select2'),
					'query_builder' => function($er) {
						return $er->findAllQb();
					}
			))
			->add('isTracked', 'checkbox', array(
				'label' => 'Lot Tracked**',
				'attr' => array(
					'align_with_widget' => true
				)
			))
			->add('designatedWarehouseNumber', 'text', array(
				'required' => false,
				'label' => 'Designated Warehouse Number***',
				'attr' => array('widget_col' => 6)
			))
			->add('save', 'submit', array(
				'label' => 'Save',
				'attr' => array(
					'class' => 'btn btn-success submit-button',
					'data-loading-text' => "Saving..."
				)
			))
		;
			
		$builder->addEventListener(FormEvents::PRE_SET_DATA, 
				function (FormEvent $event) {
			$product = $event->getData();
			$form = $event->getForm();
			
			if (!$product || null === $product->getId()) {
				$form->add('codes', 'collection', array(
					'type' => new ProductCodeType(),
					'allow_add' => true,
					'allow_delete' => true,
					'by_reference' => false
				));
			}
		});
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\Product'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_product';
	}
}