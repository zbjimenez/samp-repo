<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use CI\InventoryBundle\Validator\Constraints\SalesActivityDateConstraint;
use Symfony\Component\Form\FormInterface;
use CI\CoreBundle\Entity\User;
use CI\InventoryBundle\Entity\SalesActivity;

class SalesActivityType extends AbstractType
{
	private $securityContext;
	private $agent;
	
	public function __construct(SecurityContextInterface $sc, $agent = null)
	{
		$this->securityContext = $sc;
		$this->agent = $agent;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('dayType', 'choice', array(
				'label' => 'Day Type',
				'expanded' => true,
				'choices' => array(
					SalesActivity::WORK_DAY => 'Work Day',
					SalesActivity::SL => 'Sick Leave',
					SalesActivity::VL => 'Vacation Leave',
					SalesActivity::HOLIDAY => 'Holiday'
				)
			))
			->add('items', 'collection', array(
				'type' => new SalesActivityItemType($this->securityContext, $this->agent),
				'label' => false,
				'allow_add'=> true,
				'allow_delete'=> true,
				'by_reference' => false,
				'error_bubbling' => false
			))
			->add('saveAsDraft', 'submit', array(
				'label' => 'Save as Draft',
				'attr' => array(
					'class' => 'btn btn-primary submit-button',
					'data-loading-text' => "Saving..."
				)
			))
			->add('saveAsApproved', 'submit', array(
				'label' => 'Save and Submit',
				'attr' => array(
					'class' => 'btn btn-success submit-button',
					'data-loading-text' => "Saving..."
				)
			))
		;
		
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
			$form = $event->getForm();
			$data = $event->getData();
			
			$id = isset($data) ? $data->getId() : null;
			$agentId = $this->agent instanceof User ? $this->agent->getId() : null;
			$userId = $this->securityContext->getToken()->getUser()->getId();
			
			if ($data === null || $data->getId() === null || $userId == $agentId) {
				$form->add('date', 'date', array(
					'label'    => 'Date',
					'widget'   => 'single_text',
					'format'   => 'MM/dd/y',
					'constraints' => array(new SalesActivityDateConstraint(array('agentId' => $userId, 'currentId' => $id))),
					'attr'	   => array(
						'widget_col' => 5,
						'datepicker' => true,
						'input_group' => array('append' => 'calendar'),
						'dpoption-end_date' => strtotime("now")
					)
				));
			}
		});
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\SalesActivity'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesactivity';
	}
}
