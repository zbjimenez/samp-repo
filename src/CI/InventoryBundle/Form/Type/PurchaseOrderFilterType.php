<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CI\InventoryBundle\Entity\PurchaseOrder;

class PurchaseOrderFilterType extends AbstractType
{	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('poId', 'text', array(
				'required' => false,
				'label' => 'PO #',
				'attr' => array('widget_col' => 4)
			))
			->add('supplier', 'entity', array(
				'class' => 'CIInventoryBundle:Supplier',
				'required' => false,
				'empty_value' => 'All suppliers',
				'property' => 'shortName',
				'attr' => array('select2' => 'select2'),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('status', 'choice', array(
				'required' => false,
				'empty_value' => 'All status',
				'attr' => array('widget_col' => 5),
				'choices' => array(
					PurchaseOrder::STATUS_DRAFT => 'Draft',
					PurchaseOrder::STATUS_APPROVED => 'Approved',
					PurchaseOrder::STATUS_VOID => 'Void',
					PurchaseOrder::STATUS_CLOSED => 'Closed'
			)))
			->add('dateFrom', 'date', array(
				'label'    => 'PO Date From',
				'required' => false,
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'widget_col' => 4,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
			)))
			->add('dateTo', 'date', array(
				'label'    => 'PO Date To',
				'required' => false,
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'widget_col' => 4,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
			)))
			->add('isOpen', 'checkbox', array(
				'required' => false,
				'data' => true,
				'attr' => array(
					'widget_col' => 4,
					'class' => '',
				)
			))
			->add('search', 'submit', array(
				'attr' => array(
					'class' => 'btn btn-outline submit-button',
					'data-loading-text' => "Searching..."
				)
			))
		;
	}

	public function getName()
	{
		return 'ci_inventorybundle_purchaseorderfilter';
	}
}