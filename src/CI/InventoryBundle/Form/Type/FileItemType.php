<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FileItemType extends AbstractType
{
	protected $info;
	
	public function __construct($info)
	{
		$this->info = $info;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('file', "file", array(
			'required' => false,
			'mapped' => false,
			'attr' => array(
				'class' => 'file-input'
			)
		))
		->add('changeImage', 'hidden', array(
			'attr' => array('class' => 'change-image'),
			'mapped' => false
		))
		;
	}
	
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    	$resolver->setDefaults(array(
    		'data_class' => $this->info["class"]
    	));
    }

    public function getName()
    {
        return $this->info["formName"];
    }
}