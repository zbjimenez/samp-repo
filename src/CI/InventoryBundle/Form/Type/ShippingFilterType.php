<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CI\InventoryBundle\Entity\Shipping;

class ShippingFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('status', 'choice', array(
			'required' => false,
			'empty_value' => 'All status',
			'attr' => array('widget_col' => 4),
			'choices' => array(
				Shipping::STATUS_DRAFT => 'Draft',
				Shipping::STATUS_PENDING => 'For Delivery',
				Shipping::STATUS_DELIVERED => 'Delivered',
				Shipping::STATUS_FULFILLED => 'Fulfilled',
			)
		))
		->add('drId', 'text', array(
			'required' => false,
			'attr' => array('widget_col' => 6),
			'label' => 'DR #'
		))
		->add('siRefCode', 'text', array(
			'required' => false,
			'attr' => array('widget_col' => 6),
			'label' => 'SI #'
		))
		->add('soId', 'text', array(
			'required' => false,
			'attr' => array('widget_col' => 6),
			'label' => 'SO #'
		))
		->add('dateFrom', 'date', array(
			'label'    => 'Delivery Date From',
			'required' => false,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'datepicker' => true,
				'widget_col' => 4,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('dateTo', 'date', array(
			'label'    => 'Delivery Date To',
			'required' => false,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'datepicker' => true,
				'widget_col' => 4,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('customer', 'entity', array(
			'class' => 'CIInventoryBundle:Customer',
			'required' => false,
			'empty_value' => 'All customers',
			'property' => 'shortName',
			'attr' => array(
				'select2' => 'select2',
				'widget_col' => 6,
			),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('ownedBy', 'entity', array(
			'label' => 'SO Created By',
			'class' => 'CICoreBundle:User',
			'required' => false,
			'empty_value' => 'All users',
			'property' => 'name',
			'attr' => array(
				'select2' => 'select2',
				'widget_col' => 6,
			),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('search', 'submit', array(
			'label' => 'Search',
			'attr' => array(
				'class' => 'btn btn-default btn-outline submit-button',
				'data-loading-text' => "Search..."
			)
		))
		;
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_shippingfilter';
	}
}