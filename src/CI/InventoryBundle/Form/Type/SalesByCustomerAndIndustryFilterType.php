<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class SalesByCustomerAndIndustryFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('customer', 'entity', array(
			'class' => 'CIInventoryBundle:Customer',
			'property' => 'shortName',
			'empty_value' => 'All customers',
			'required' => false,
			'attr' => array('class' => 'select2 customer'),
			'query_builder' => function(EntityRepository $repository) {
				return $repository->findAllQb();
			}
		))
		->add('industry', 'entity', array(
			'class' => 'CIInventoryBundle:Industry',
			'property' => 'name',
			'empty_value' => 'All industries',
			'required' => false,
			'attr' => array('class' => 'select2 industry'),
			'query_builder' => function(EntityRepository $repository) {
				return $repository->findAllQb();
			}
		))
		->add('search', 'submit', array(
			'label' => 'Search',
			'attr' => array(
				'class' => 'btn btn-default btn-outline submit-button',
				'data-loading-text' => "Search..."
			)
		))
		;
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_salesbycustomerandindustryreportfilter';
	}
}