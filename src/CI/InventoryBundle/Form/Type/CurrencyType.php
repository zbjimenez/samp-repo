<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CurrencyType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('currencyName', 'text', array(
				'label' => 'Currency Name',
				'attr' => array(
					'widget_col' => 6,
				)
			))
			->add('currencyCode', 'text', array(
				'label' => 'Currency Code',
				'attr' => array(
					'widget_col' => 3,
				)
			))
			->add('isForeign', 'checkbox', array(
				'label' => 'Foreign Currency',
				'attr' => array(
					'align_with_widget' => true
				)
			))
			->add('save', 'submit', array(
				'label' => 'Save',
				'attr' => array(
					'class' => 'btn btn-success submit-button',
					'data-loading-text' => "Saving..."
				)
			));
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	 public function setDefaultOptions(OptionsResolverInterface $resolver)
	 {
	 	$resolver->setDefaults(array(
	 		'data_class' => 'CI\InventoryBundle\Entity\Currency'
	 	));
	 }
	 
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_currency';
	}
}
