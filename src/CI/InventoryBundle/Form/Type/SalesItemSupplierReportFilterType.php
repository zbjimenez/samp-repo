<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SalesItemSupplierReportFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('product', 'entity', array(
				'class' => 'CIInventoryBundle:Product',
				'required' => false,
				'empty_value' => 'All products',
				'property' => 'sku',
				'attr' => array('select2' => 'select2'),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('supplier', 'entity', array(
				'class' => 'CIInventoryBundle:Supplier',
				'required' => false,
				'empty_value' => 'All suppliers',
				'property' => 'shortName',
				'attr' => array('select2' => 'select2'),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('search', 'submit', array(
				'label' => 'Search',
				'attr' => array(
					'class' => 'btn btn-default btn-outline submit-button',
					'data-loading-text' => "Searching..."
				)
			))
		;
	}

	public function getName()
	{
		return 'ci_inventorybundle_salesitemsupplierreportfiltertype';
	}
}