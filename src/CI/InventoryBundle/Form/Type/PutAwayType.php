<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use CI\InventoryBundle\Entity\PutAway;

class PutAwayType extends AbstractType
{
	protected $type;
	
	public function __construct($type)
	{
		$this->type = $type;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$type = $this->type;

		if ($type === PutAway::TYPE_RECEIVE_ORDER) {
			$builder
				->add('receiveOrder', 'entity_hidden', array(
					'class' => 'CIInventoryBundle:ReceiveOrder',
					'label' => 'RO #',
					'required' => true
				))
			;
		} else {
			$builder
				->add('backload', 'entity_hidden', array(
					'class' => 'CIInventoryBundle:Backload',
					'label' => 'Backload #',
					'required' => true
				))
			;
		}

		$builder
			->add('putAwayType', 'hidden')
			->add('date', 'date', array(
				'label'    => 'Date',
				'required' => true,
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'widget_col' => 4,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
			))
			->add('items', 'collection', array(
				'type' => new PutAwayItemType($type),
				'allow_add' => true,
				'allow_delete' => true,
				'by_reference' => false,
				'error_bubbling' => false,
				'prototype_name' => 'ANGLR_key_ANGLR'
			))
			->add('save', 'submit', array(
				'label' => 'Save',
				'attr' => array(
					'class' => 'btn btn-success submit-button',
					'data-loading-text' => "Saving..."
				)
			))
		;
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\PutAway'
		));
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_putaway';
	}
}