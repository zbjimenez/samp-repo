<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReceiveOrderItemReceivingReportFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('roNo', 'text', array(
				'label' => 'RO #',
				'required' => false,
				'attr' => array('widget_col' => 4)
			))
			->add('lotNumber', 'text', array(
				'label' => 'Lot #',
				'required' => false,
				'attr' => array('widget_col' => 4)
			))
			->add('poId', 'text', array(
				'label' => 'PO #',
				'required' => false,
				'attr' => array('widget_col' => 4)
			))
			->add('product', 'entity', array(
				'class' => 'CIInventoryBundle:Product',
				'required' => false,
				'empty_value' => 'All products',
				'property' => 'displayName',
				'attr' => array(
					'select2' => 'select2',
					'widget_col' => 6,
				),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('dateFrom', 'date', array(
				'label'    => 'Date From',
				'required' => false,
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'widget_col' => 4,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
			)))
			->add('dateTo', 'date', array(
				'label'    => 'Date To',
				'required' => false,
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'widget_col' => 4,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
			)))
			->add('search', 'submit', array(
				'label' => 'Search',
				'attr' => array(
					'class' => 'btn btn-default btn-outline submit-button',
					'data-loading-text' => "Searching..."
				)
			))
		;
	}

	public function getName()
	{
		return 'ci_inventorybundle_receiveorderitemreceivingreportfiltertype';
	}
}