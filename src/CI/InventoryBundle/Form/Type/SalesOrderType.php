<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;

class SalesOrderType extends AbstractType
{
	private $securityContext;

	public function __construct(SecurityContextInterface $sc)
	{
		$this->securityContext = $sc;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$sc = $this->securityContext;

		$builder
		->add('refCode', 'text', array(
			'label' => 'SO #',
		))
		->add('customer', 'entity', array(
			'class' => 'CIInventoryBundle:Customer',
			'property' => 'shortName',
			'empty_value' => 'Choose a customer',
			'attr' => array(
				'class' => 'customer',
				'select2' => true,
				'help_text' => 'Changing the customer will delete previously added items.',
			),
			'query_builder' => function ($repository) use ($sc) {
				if ($sc->isGranted('ROLE_SALESORDER_CREATE_FOR_ALL_CUSTOMERS')) {
					return $repository->findAllQb();
				} else {
					return $repository->createQueryBuilder('c')
					->select('c')
					->join('c.salesAgent', 'a')
					->where('a.id = :agentId')
					->setParameter('agentId', $sc->getToken()->getUser()->getId())
					->orderBy('c.name', 'ASC')
					;
				}
			},
		))
		->add('warehouse', 'entity', array(
			'class' => 'CIInventoryBundle:Warehouse',
			'property' => 'name',
			'empty_value' => 'Choose a warehouse',
			'attr' => array(
				'class' => 'warehouse',
				'select2' => true,
			),
			'query_builder' => function ($repository) use ($sc) {
				return $repository->findAllQb();
			},
		))
		->add('poRefCode', 'text', array(
			'label' => 'PO #',
			'required' => false,
		))
		->add('date', 'date', array(
			'label' => 'Date',
			'widget' => 'single_text',
			'format' => 'MM/dd/y',
			'attr' => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			),
		))
		->add('terms', 'integer', array(
			'attr' => array(
				'widget_col' => 4,
				'min' => 0,
				'max' => 999,
				'input_group' => array('append' => 'days'),
				'ng-model' => 'form.customer.terms',
			),
		))
		->add('memo', 'textarea', array(
			'required' => false,
			'attr' => array('rows' => 4),
		))
		->add('vatAmount', 'custom_number', array(
			'attr' => array(
				'ng-change' => 'computeSubtotal()',
				'ng-model' => 'form.vatAmount',
				'class' => 'text-right',
			),
		))
		->add('items', 'collection', array(
				'type' => new SalesOrderItemType(),
				'label' => false,
				'allow_add' => true,
				'allow_delete' => true,
				'by_reference' => false,
				'error_bubbling' => false,
				'prototype_name' => 'ANGLR_key_ANGLR',
		))
		->add('saveDraft', 'submit', array(
			'label' => 'Save As Draft',
			'attr' => array(
				'class' => 'btn btn-info submit-button',
				'data-loading-text' => "Saving...",
			),
		))
		->add('saveApproved', 'submit', array(
			'label' => 'Save As Approved',
			'attr' => array(
				'class' => 'btn btn-success submit-button',
				'data-loading-text' => "Saving...",
			),
		))
		;
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\SalesOrder',
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesorder';
	}
}
