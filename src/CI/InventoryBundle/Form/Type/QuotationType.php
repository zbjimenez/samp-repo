<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CI\InventoryBundle\Entity\Quotation;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class QuotationType extends AbstractType
{
	protected $sc;
	
	public function __construct($sc)
	{
		$this->sc = $sc;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		if ($this->sc->isGranted('ROLE_QUOTATION_MEMO')) {
			$builder
			->add('memo','textarea', array(
				'required' => false,
				'attr'  => array(
					'rows' => 4,
				)
			));
		}
			
		$builder->addEventListener(FormEvents::PRE_SET_DATA,
			function (FormEvent $event) {
				$quotation = $event->getData();
				$form = $event->getForm();
				$sc = $this->sc;
					
				$label = 'Save';
				if (!$quotation || null === $quotation->getId()) {
					$label = 'Save as Pending for Approval';
				}
				
				$form
				->add('saveAsPendingForApproval', 'submit', array(
					'label' => $label,
					'attr' => array(
						'class' => 'btn btn-primary submit-button',
						'data-loading-text' => "Saving..."
					)
				));

				if ($quotation->isEditable()) {
					$form
					->add('date', 'date', array(
						'widget' => 'single_text',
						'format' => 'MM/dd/y',
						'attr' => array(
							'widget_col' => 4,
							'datepicker' => true,
							'input_group' => array('append' => 'calendar'),
						),
					))
					->add('contactPerson', 'text', array(
						'label' => 'Contact Person',
						'attr' => array('widget_col' => 6)
					))
					->add('position', 'text', array(
		 				'required' => false,
						'attr' => array('widget_col' => 6)
					))
					->add('customer', 'entity', array(
						'class' => 'CIInventoryBundle:Customer',
						'required' => true,
						'empty_value' => 'Choose a customer',
						'property' => 'shortName',
						'attr' => array('select2' => 'select2'),
						'query_builder' => function($er) use ($sc) {
							if (!$sc->isGranted('ROLE_QUOTATION_CREATE_ALL')) {
								return $er->createQueryBuilder('c')
								->select('c')
								->join('c.salesAgent', 'a')
								->where('a.id = :agentId')
								->setParameter('agentId', $sc->getToken()->getUser()->getId())
								->orderBy('c.name', 'ASC')
								;
							} else {
								return $er->findAllQb();
							}
						}
					))
					->add('product', 'entity', array(
						'class' => 'CIInventoryBundle:Product',
						'required' => true,
						'empty_value' => 'Choose a product',
						'property' => 'sku',
						'attr' => array('select2' => 'select2'),
						'query_builder' => function($er) {
							return $er->findAllQb();
						}
					))
					->add('origin', 'text', array(
						'required' => false
					))
					->add('quantity', 'custom_number', array(
						'required' => false,
						'attr' => array(
							'widget_col' => 5,
							'input_group' => array('append' => 'kg')
						)
					))
					->add('price', 'custom_number', array(
						'attr' => array('widget_col' => 5)
					))
					->add('terms', 'integer', array(
						'attr' => array(
							'widget_col' => 4,
							'input_group' => array('append' => 'days')
						)
					))
					->add('deliveryNotes', 'textarea', array(
						'label' => 'Delivery Notes',
						'required' => false,
						'attr' => array('rows' => 4)
					))
					->add('validityStartDate', 'date', array(
						'label' => 'Start Date',
						'widget' => 'single_text',
						'format' => 'MM/dd/y',
						'attr' => array(
							'widget_col' => 4,
							'datepicker' => true,
							'input_group' => array('append' => 'calendar'),
						)
					))
					->add('validityEndDate', 'date', array(
						'label' => 'End Date',
						'widget' => 'single_text',
						'format' => 'MM/dd/y',
						'attr' => array(
							'widget_col' => 4,
							'datepicker' => true,
							'input_group' => array('append' => 'calendar'),
						)
					))
					->add('currency', 'entity', array(
						'class' => 'CIInventoryBundle:Currency',
						'required' => true,
						'empty_value' => 'Choose a currency',
						'property' => 'currencyName',
						'attr' => array('select2' => 'select2'),
						'query_builder' => function($er) {
							return $er->findAllQb();
						}
					))
					->add('exchangeRate', 'custom_number', array(
						'attr' => array('widget_col' => 5),
						'required' => false,
					))
				;
			}
		});
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\Quotation'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_quotation';
	}
}