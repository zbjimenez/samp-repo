<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use CI\InventoryBundle\Entity\ShippingItem;

class OrderPickingItemType extends AbstractType
{
	protected $em;
	protected $shippingId;

	public function __construct($em, $shippingId)
	{
		$this->em = $em;
		$this->shippingId = $shippingId;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$shippingId = $this->shippingId;
		$em = $this->em;

		$builder
			->add('quantity', 'custom_number', array(
				'label' => false,
				'attr' => array(
					'widget_col' => 12,
					'class' => 'text-right',
					'ng-model' => 'form.items[key].quantity',
					'ng-readonly' => 'form.items[key].isPicked',
				),
			))
			->add('shippingItem', 'entity', array(
				'class' => 'CIInventoryBundle:ShippingItem',
				'label' => false,
				'property' => 'productName',
				'empty_value' => 'Choose a product',
				'attr' => array(
					'class' => 'shippingitem-ANGLR_key_ANGLR',
					'select2' => 'select2',
					'widget_col' => 12,
					'ng-model' => 'form.items[key].shippingItem.id',
					'ng-value' => 'form.items[key].shippingItem.id',
					'ng-change' => 'onChangeProduct(item, key)',
				),
				'query_builder' => function($er) use ($shippingId) {
					return $er->getItemsQb($shippingId);
				}
			))
			->add('storageLocation', 'entity_hidden', array(
				'required' => true,
				'class' => 'CIInventoryBundle:StorageLocation',
				'attr' => array(
					'ng-model' => 'form.items[key].storageLocation.id',
					'ng-value' => 'form.items[key].storageLocation.id',
				),
			))
			->add('productionDate', 'date', array(
				'required' => true,
				'widget' => 'single_text',
				'format' => 'y-M-d',
				'attr' => array(
					'class' => 'hide',
					'ng-model' => 'form.items[key].productionDate',
					'ng-value' => 'form.items[key].productionDate',
				),
			))
		;

		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($em, $shippingId) {
			$lotNumbers = array();
			$palletIds = array();

			$form = $event->getForm();
			$data = $event->getData();

			if (!is_null($data)) {
				if ($data->getShippingItem() instanceof ShippingItem) {
					$shippingItemId = $data->getShippingItem()->getId();
					$productId = $em->getRepository('CIInventoryBundle:ShippingItem')->findProductByItemId($shippingItemId)['id'];
					$lotNumberResults = $em->getRepository('CIInventoryBundle:Inventory')->findLotNumbers($productId);

					foreach ($lotNumberResults as $result) {
						$lotNumbers[$result['label']] = $result['label'];
					}

					$lotNumber = $data->getLotNumber();
					$palletIdResults = $em->getRepository('CIInventoryBundle:Inventory')->findPalletIds($productId, $lotNumber);

					foreach ($palletIdResults as $result) {
						$palletIds[$result['label']] = $result['label'];
					}
				}
			}

			$form
				->add('lotNumber', 'choice', array(
					'choices' => $lotNumbers,
					'label' => false,
					'required' => false,
					'attr' => array(
						'class' => 'lotnumber-ANGLR_key_ANGLR',
						'ng-show' => 'form.items[key].shippingItem.id',
						'ng-model' => 'form.items[key].lotNumber.label',
						'ng-value' => 'form.items[key].lotNumber.label',
						'ng-change' => 'onChangeLotNumber(item, key)',
						'select2' => true,
						'ng-options' => 'lotNumber.label as lotNumber.label for lotNumber in form.items[key].lotNumbers track by lotNumber.label',
						'widget_col' => 12,
					),
				))
				->add('palletId', 'choice', array(
					'choices' => $palletIds,
					'label' => false,
					'required' => false,
					'attr' => array(
						'class' => 'pallet-ANGLR_key_ANGLR',
						'ng-show' => 'form.items[key].lotNumber.label',
						'ng-model' => 'form.items[key].palletId.label',
						'ng-value' => 'form.items[key].palletId.label',
						'ng-change' => 'onChangePalletId(item, key)',
						'select2' => true,
						'ng-options' => 'palletId.label as palletId.label for palletId in form.items[key].palletIds track by palletId.label',
						'widget_col' => 12,
					),
				))
			;
		});

		$builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($em) {
			$form = $event->getForm();
			$data = $event->getData();
			$lotNumbers = array();
			$palletIds = array();

			if (!is_null($data)) {
				if (array_key_exists('shippingItem', $data)) {
					$shippingItemId = $data['shippingItem'];
					$productId = $em->getRepository('CIInventoryBundle:ShippingItem')->findProductByItemId($shippingItemId)['id'];
					$lotNumberResults = $em->getRepository('CIInventoryBundle:Inventory')->findLotNumbers($productId);

					foreach ($lotNumberResults as $result) {
						$lotNumbers[$result['label']] = $result['label'];
					}

					if (array_key_exists('lotNumber', $data)) {
						$lotNumber = $data['lotNumber'];
						$palletIdResults = $em->getRepository('CIInventoryBundle:Inventory')->findPalletIds($productId, $lotNumber);

						foreach ($palletIdResults as $result) {
							$palletIds[$result['label']] = $result['label'];
						}
					}
				}

				$form
					->add('lotNumber', 'choice', array(
						'choices' => $lotNumbers,
						'label' => false,
						'attr' => array(
							'class' => 'lotnumber-ANGLR_key_ANGLR',
							'ng-show' => 'form.items[key].shippingItem.id',
							'ng-model' => 'form.items[key].lotNumber.label',
							'ng-value' => 'form.items[key].lotNumber.label',
							'ng-change' => 'onChangeLotNumber(item, key)',
							'select2' => true,
							'ng-options' => 'lotNumber.label as lotNumber.label for lotNumber in form.items[key].lotNumbers track by lotNumber.label',
							'widget_col' => 12,
						),
					))
					->add('palletId', 'choice', array(
						'choices' => $palletIds,
						'label' => false,
						'attr' => array(
							'class' => 'pallet-ANGLR_key_ANGLR',
							'ng-show' => 'form.items[key].lotNumber',
							'ng-model' => 'form.items[key].palletId.label',
							'ng-value' => 'form.items[key].palletId.label',
							'ng-change' => 'onChangePalletId(item, key)',
							'select2' => true,
							'ng-options' => 'palletId.label as palletId.label for palletId in form.items[key].palletIds track by palletId.label',
							'widget_col' => 12,
						),
					))
				;
			}
		});
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\OrderPickingItem'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_orderpickingitem';
	}
}
