<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SalesReturnType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('returnDate', 'date', array(
			'label' => 'Return Date',
			'widget' => 'single_text',
			'format' => 'MM/dd/y',
			'attr' => array(
				'widget_col' => 6,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			),
		))
		->add('memo', 'textarea', array(
			'required' => false,
			'attr' => array('rows' => 4),
		))
		->add('items', 'collection', array(
			'type' => new SalesReturnItemType(),
			'allow_add' => true,
			'allow_delete' => true,
			'by_reference' => false,
			'error_bubbling' => false,
			'prototype_name' => 'ANGLR_key_ANGLR'
		))
		->add('saveDraft', 'submit', array(
			'label' => 'Save as Draft',
			'attr' => array(
				'class' => 'btn btn-info submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		->add('saveReturned', 'submit', array(
			'label' => 'Save as Returned',
			'attr' => array(
				'class' => 'btn btn-success submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
		
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
			$form = $event->getForm();
			$data = $event->getData();
				
			if (!$data || $data->getId() === null) {
				$form->add('shipping', 'entity_hidden', array(
					'class' => 'CIInventoryBundle:Shipping',
					'label' => 'DR #',
					'required' => true,
				));
			}
		});
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\SalesReturn'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesreturn';
	}
}