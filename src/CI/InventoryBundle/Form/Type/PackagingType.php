<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PackagingType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        	->add('name', 'text', array(
        		'label' => 'Name',
        		'attr' => array('widget_col' => 4)
        	))
        	->add('description', 'textarea', array(
        		'label' => 'Printed Description',
        		'attr'	=> array(
        			'rows' => 4
        		)
        	))
        	->add('kgsUnit', 'custom_number', array(
        			'label' => 'Kgs/Unit',
        	))
            ->add('save', 'submit', array(
            	'label' => 'Save',
            	'attr' => array(
            		'class' => 'btn btn-success submit-button',
            		'data-loading-text' => "Saving..."
            	)
            ))
		;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CI\InventoryBundle\Entity\Packaging'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ci_inventorybundle_packaging';
    }
}