<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CI\InventoryBundle\Entity\PutAway;

class PutAwayFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('status', 'choice', array(
			'required' => false,
			'empty_value' => 'All status',
			'attr' => array('widget_col' => 4),
			'choices' => array(
				true => 'Successfully Put Away',
				false => 'Void'
			)
		))
		->add('pwNo', 'text', array(
			'label' => 'PW #',
			'required' => false,
		))
		->add('transactionNo', 'text', array(
			'label' => 'Transaction #',
			'required' => false,
		))
		->add('productSku', 'text', array(
			'label' => 'Product SKU',
			'required' => false,
		))
		->add('type', 'choice', array(
			'label' => 'Type',
			'required' => false,
			'empty_value' => 'All types',
			'choices' => array(
				PutAway::TYPE_RECEIVE_ORDER => 'Receive Order',
				PutAway::TYPE_BACKLOAD => 'Backload',
			),
			'attr' => array(
				'widget_col' => 4,
			),
		))
		->add('dateFrom', 'date', array(
			'label'    => 'Date From',
			'required' => false,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'datepicker' => true,
				'widget_col' => 4,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('dateTo', 'date', array(
			'label'    => 'Date To',
			'required' => false,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'datepicker' => true,
				'widget_col' => 4,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('search', 'submit', array(
			'label' => 'Search',
			'attr' => array(
				'class' => 'btn btn-default btn-outline submit-button',
				'data-loading-text' => "Search..."
			)
		))
		;
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_putawayfilter';
	}
}