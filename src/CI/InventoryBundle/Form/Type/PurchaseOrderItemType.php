<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PurchaseOrderItemType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('quantity', 'custom_number', array(
				'attr' => array(
					'simple_col' => 9,
					'ng-change' => 'computeSubtotal()',
					'ng-model' => 'form.items[key].quantity')
			))
			->add('product', 'entity_autocomplete', array(
				'class' => 'CIInventoryBundle:Product',
				'attr' => array(
					'placeholder' => 'search by product sku',
					'ng-model' => 'form.items[key].productId',
					'ng-change' => 'clearResults(item)',
					'ng-blur' => 'clearResults(item)',
					'typeahead' => 'product as product.label for product in getProducts($viewValue, item)',
					'typeahead-template-url' => 'typeahead.template.html.twig',
					'typeahead-on-select' => 'onSelectItemProduct($item, $model, $label, key)',
					'tooltip' => 'No results found.',
					'tooltip-placement' => 'right',
					'tooltip-toggle' => 'form.items[key].noResults'
				)
			))
			->add('contractNumber', 'text', array(
				'required' => false,
				'attr' => array(
					'ng-model' => 'form.items[key].contractNumber'
				)
			))
			->add('unitCost', 'custom_number', array(
				'attr' => array(
					'class' => 'amount',
					'ng-change' => 'computeSubtotal()',
					'ng-model' => 'form.items[key].unitCost'
				)
			))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\PurchaseOrderItem'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_purchaseorderitem';
	}
}