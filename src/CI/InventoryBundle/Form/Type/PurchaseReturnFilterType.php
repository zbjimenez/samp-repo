<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use CI\InventoryBundle\Entity\PurchaseReturn;

class PurchaseReturnFilterType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->setMethod("GET")
		->add('purchaseOrder', 'text', array(
			'label' => 'PO#',
			'required' => false,
		))
		->add('receiveOrder', 'text', array(
			'label' => 'RO#',
			'required' => false,
		))
		->add('status', 'choice', array(
			'required' => false,
			'empty_value' => 'All status',
			'attr' => array('widget_col' => 4),
			'choices' => array(
				PurchaseReturn::STATUS_DRAFT => 'Draft',
				PurchaseReturn::STATUS_RETURNED => 'Returned',
				PurchaseReturn::STATUS_VOID => 'Void',
			),
		))
		->add('dateFrom', 'date', array(
			'label'    => 'Date From',
			'required' => false,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
				'widget_col' => 5,
			),
		))
		->add('dateTo', 'date', array(
			'label'    => 'Date To',
			'required' => false,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
				'widget_col' => 5,
			),
		))
		->add('supplier', 'entity', array(
			'class' => 'CIInventoryBundle:Supplier',
			'required' => false,
			'empty_value' => 'All suppliers',
			'property' => 'shortName',
			'attr' => array('select2' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_purchasereturnfilter';
	}
}