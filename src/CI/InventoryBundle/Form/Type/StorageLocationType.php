<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StorageLocationType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('warehouse', 'entity', array(
				'class' => 'CIInventoryBundle:Warehouse',
				'required' => true,
				'empty_value' => 'Choose a warehouse',
				'property' => 'name',
				'attr' => array(
					'select2' => 'select2',
				),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('warehouseNumber', 'text', array(
				'label' => 'Warehouse Number',
				'required' => true,
				'attr' => array(
					'widget_col' => 4,
				)
			))
			->add('lane', 'text', array(
				'label' => 'Lane',
				'required' => true,
				'attr' => array(
					'widget_col' => 4,
				)
			))
			->add('row', 'text', array(
				'label' => 'Row',
				'required' => true,
				'attr' => array(
					'widget_col' => 4,
				)
			))
			->add('highLevelNumber', 'text', array(
				'label' => 'High Level Number',
				'required' => true,
				'attr' => array(
					'widget_col' => 4,
				)
			))
			->add('save', 'submit', array(
				'label' => 'Save',
				'attr' => array(
					'class' => 'btn btn-success submit-button',
					'data-loading-text' => "Saving..."
				)
			));
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	 public function setDefaultOptions(OptionsResolverInterface $resolver)
	 {
	 	$resolver->setDefaults(array(
	 		'data_class' => 'CI\InventoryBundle\Entity\StorageLocation'
	 	));
	 }
	 
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_storagelocation';
	}
}
