<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WarehouseInventoryReportFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('warehouse', 'entity', array(
				'class' => 'CIInventoryBundle:Warehouse',
				'required' => false,
				'empty_value' => 'All warehouses',
				'property' => 'name',
				'attr' => array('select2' => 'select2'),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('supplier', 'entity', array(
				'class' => 'CIInventoryBundle:Supplier',
				'required' => false,
				'empty_value' => 'All suppliers',
				'property' => 'name',
				'attr' => array('select2' => 'select2'),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('product', 'entity', array(
				'class' => 'CIInventoryBundle:Product',
				'required' => false,
				'empty_value' => 'All products',
				'property' => 'name',
				'attr' => array('select2' => 'select2'),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('lotNumber', 'text', array(
				'label' => 'Lot Number',
				'required' => false,
				'attr' => array('widget_col' => 4)
			))
			->add('productionDate', 'date', array(
				'label'    => 'Production Date',
				'required' => false,
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'widget_col' => 5,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
				),
			))
			->add('search', 'submit', array(
				'label' => 'Search',
				'attr' => array(
					'class' => 'btn btn-default btn-outline submit-button',
					'data-loading-text' => "Searching..."
				)
			))
		;
	}

	public function getName()
	{
		return 'ci_inventorybundle_warehouseinventoryreportfilter';
	}
}