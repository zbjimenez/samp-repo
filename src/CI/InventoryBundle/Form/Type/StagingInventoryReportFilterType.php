<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StagingInventoryReportFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		
		$builder
			->add('poId', 'text', array(
				'label' => 'PO #',
				'required' => false,
				'attr' => array('widget_col' => 4)
			))
			->add('roId', 'text', array(
				'label' => 'RO #',
				'required' => false,
				'attr' => array('widget_col' => 4)
			))
			->add('dateFrom', 'date', array(
				'label'    => 'Date From',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'required' => false,
				'attr'	   => array(
					'widget_col' => 5,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
			))
			->add('dateTo', 'date', array(
				'label'    => 'Date To',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'required' => false,
				'attr'	   => array(
					'widget_col' => 5,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
			))
			->add('search', 'submit', array(
				'label' => 'Search',
				'attr' => array(
					'class' => 'btn btn-default btn-outline submit-button',
					'data-loading-text' => "Searching..."
				)
			))
		;
	}

	public function getName()
	{
		return 'ci_inventorybundle_staginginventoryreportfiltertype';
	}
}