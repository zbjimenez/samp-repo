<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

use CI\InventoryBundle\Entity\SalesForecast;

class SalesForecastReportFilterType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$arrayYears = array();
		$years = range(date("Y") + 5, date("Y") - 20);
		foreach ($years as $year) {
			$arrayYears[$year] = $year;
		}
		
		$arrayMonths = SalesForecast::getMonths();
		
		$builder->setMethod("GET")
		->add('year', 'choice', array(
			'empty_value' => 'Choose a year',
			'attr' => array('select2' => true),
			'constraints' => new NotBlank(array('message' => 'Please enter a year.')),
			'choices' => $arrayYears,
		))
		->add('month', 'choice', array(
			'required' => false,
			'empty_value' => 'All months',
			'attr' => array('select2' => true),
			'choices' => $arrayMonths,
		))
		->add('customer', 'entity', array(
			'class' => 'CIInventoryBundle:Customer',
			'property' => 'shortName',
			'empty_value' => 'All customers',
			'required' => false,
			'attr' => array('select2' => true),
			'query_builder' => function($repository) {
				return $repository->findAllQb();
			}
		))
		->add('product', 'entity', array(
			'class' => 'CIInventoryBundle:Product',
			'property' => 'displayName',
			'empty_value' => 'All products',
			'required' => false,
			'attr' => array('select2' => true),
			'query_builder' => function($repository) {
				return $repository->findAllQb();
			}
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesforecastreportfilter';
	}
}