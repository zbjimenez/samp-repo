<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SalesForecastType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$arrayYears = array();
		$years = range(date("Y") + 5, date("Y") - 20);
		foreach ($years as $year) {
			$arrayYears[$year] = $year;
		}
		
		$builder
		->add('year', 'choice', array(
			'attr' => array(
				'select2' => true,
				'widget_col' => 5,
				'ng-cloak' => true
			),
			'empty_value' => 'Choose a year',
			'choices' => $arrayYears,
		))
		->add('janQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.janQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('janAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.janAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('febQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.febQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('febAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.febAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('marQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.marQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('marAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.marAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('aprQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.aprQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('aprAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.aprAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('mayQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.mayQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('mayAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.mayAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('junQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.junQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('junAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.junAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('julQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.julQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('julAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.julAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('augQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.augQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('augAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.augAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('sepQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.sepQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('sepAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.sepAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('octQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.octQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('octAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.octAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('novQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.novQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('novAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.novAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('decQty', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.decQty',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('decAmt', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'class' => 'amount',
				'widget_col' => 12,
				'ng-model' => 'form.decAmt',
				'ng-change' => 'computeTotals()',
			),
		))
		->add('save', 'submit', array(
			'label' => 'Save',
			'attr' => array(
				'class' => 'btn btn-success submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
		
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
			$form = $event->getForm();
			$data = $event->getData();
				
			if (!$data || $data->getId() === null) {
				$form->add('customer', 'entity_hidden', array(
					'class' => 'CIInventoryBundle:Customer',
					'label' => 'Customer',
					'required' => true,
				))
				->add('product', 'entity_hidden', array(
					'class' => 'CIInventoryBundle:Product',
					'label' => 'Product',
					'required' => true,
				))
				;
			}
		});
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\SalesForecast'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesforecast';
	}
}