<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CI\InventoryBundle\Entity\Shipping;

class OrderPickingFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->setMethod('GET')
			->add('id', 'text', array(
				'required' => false,
				'attr' => array(
					'widget_col' => 5,
				),
				'label' => 'Order Picking #'
			))
			->add('drId', 'text', array(
				'required' => false,
				'attr' => array(
					'widget_col' => 5,
				),
				'label' => 'DR #'
			))
			->add('dateFrom', 'date', array(
				'label' => 'Date From',
				'required' => false,
				'widget' => 'single_text',
				'format' => 'MM/dd/y',
				'attr' => array(
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
					'widget_col' => 5,
				),
			))
			->add('dateTo', 'date', array(
				'label' => 'Date To',
				'required' => false,
				'widget' => 'single_text',
				'format' => 'MM/dd/y',
				'attr' => array(
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
					'widget_col' => 5,
				),
			))
			->add('search', 'submit', array(
				'label' => 'Search',
				'attr' => array(
					'class' => 'btn btn-default btn-outline submit-button',
					'data-loading-text' => "Search..."
				)
			))
		;
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_orderpickingfilter';
	}
}