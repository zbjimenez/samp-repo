<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use CI\InventoryBundle\Entity\Customer;
use CI\CoreBundle\Entity\User;

class CustomerType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('ewt','choice', array(
				'label' => 'EWT',
				'required' => true,
				'attr' => array('widget_col' => 3),
				'choices'  => array(
					Customer::EWT_NONE => 'None',
					Customer::EWT_GOODS => '1% (Goods)',
					Customer::EWT_SERVICES => '2% (Services)'
				)
			))
			->add('name', 'text', array(
				'attr' => array('widget_col' => 6)
			))
			->add('shortName', 'text', array(
				'label' => 'Short Name',
				'attr' => array('widget_col' => 4)
			))
			->add('salesAgent', 'entity', array(
				'label' => 'Sales Agent',
				'class' => 'CICoreBundle:User',
				'required' => true,
				'empty_value' => 'Choose a sales agent',
				'property' => 'name',
				'attr' => array('select2' => 'select2'),
				'query_builder' => function($er) {
					return $er->findAllQb('Sales Agent');
				}
			))
			->add('contactDetails', 'textarea', array(
				'label' => 'Contact Details',
				'attr' => array('rows' => 4)
			))
			->add('terms', 'integer', array(
				'attr' => array(
					'widget_col' => 4,
					'min' => 0,
					'input_group' => array('append' => 'days')
				)
			))
			->add('tin', 'text', array(
				'label'    => 'TIN',
				'required' => false,
				'attr' => array('widget_col' => 5)
			))
			->add('memo','textarea', array(
				'required' => false,
				'attr'  => array('rows' => 4)
			))
			->add('save', 'submit', array(
				'label' => 'Save',
				'attr' => array(
					'class' => 'btn btn-success submit-button',
					'data-loading-text' => "Saving..."
				)
			))
		;
			
		$builder->addEventListener(FormEvents::PRE_SET_DATA, 
				function (FormEvent $event) {
			$customer = $event->getData();
			$form = $event->getForm();
			
			if (!$customer || null === $customer->getId()) {
				$form->add('branches', 'collection', array(
					'type' => new BranchType(),
					'allow_add' => true,
					'allow_delete' => true,
					'by_reference' => false
				));
			}
		});
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\Customer'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_customer';
	}
}