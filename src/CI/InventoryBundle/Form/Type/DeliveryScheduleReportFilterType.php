<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraints\NotBlank;

class DeliveryScheduleReportFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->setMethod("GET")
		->add('dateFrom', 'date', array(
			'data'     => new \DateTime('now'),
			'label'    => 'Date From',
			'required' => true,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'constraints' => new NotBlank(array('message' => 'Please enter a date.')),
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			),
		))
		->add('dateTo', 'date', array(
			'data'     => new \DateTime('now'),
			'label'    => 'Date To',
			'required' => true,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'constraints' => new NotBlank(array('message' => 'Please enter a date.')),
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			),
		))
		->add('customer', 'entity', array(
			'class' => 'CIInventoryBundle:Customer',
			'required' => false,
			'empty_value' => 'All customers',
			'property' => 'name',
			'attr' => array(
				'select2' => 'select2',
				'widget_col' => 6,
			),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('product', 'entity', array(
			'class' => 'CIInventoryBundle:Product',
			'required' => false,
			'empty_value' => 'All products',
			'property' => 'displayName',
			'attr' => array(
				'select2' => 'select2',
				'widget_col' => 6,
			),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			),
		))
		;
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_deliveryschedulereportfilter';
	}
}