<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PurchaseOrderType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('supplier', 'entity', array(
				'class' => 'CIInventoryBundle:Supplier',
				'required' => true,
				'empty_value' => 'Choose a supplier',
				'property' => 'shortName',
				'attr' => array(
					'select2' => 'select2',
					'ng-model' => 'form.supplier',
					'help_text' => 'Changing the supplier will delete previously added items.',
					'ng-change' => 'onChangeSupplier()'
				),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('currency', 'entity', array(
				'class' => 'CIInventoryBundle:Currency',
				'required' => true,
				'empty_value' => 'Choose a currency',
				'property' => 'currencyName',
				'attr' => array(
					'select2' => 'select2',
					'ng-model' => 'form.currency',
					'ng-change' => 'onChangeCurrency()'
				),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('warehouse', 'entity', array(
				'class' => 'CIInventoryBundle:warehouse',
				'required' => true,
				'empty_value' => 'Choose a warehouse',
				'property' => 'name',
				'attr' => array(
					'select2' => 'select2',
					'ng-model' => 'form.warehouse',
				),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('poId', 'text', array(
				'label' => 'PO #',
				'attr' => array('widget_col' => 4)
			))
			->add('poDate', 'date', array(
				'widget' => 'single_text',
				'label' => 'PO Date',
				'format' => 'MM/dd/y',
				'attr' => array(
					'widget_col' => 5,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
				)
			))
			->add('deliveryDate', 'date', array(
				'widget' => 'single_text',
				'label' => 'Delivery Date',
				'required' => false,
				'format' => 'MM/dd/y',
				'attr' => array(
					'widget_col' => 5,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
				)
			))
			->add('terms', 'integer', array(
				'attr' => array(
					'widget_col' => 4,
					'min' => 0,
					'ng-model' => 'form.terms',
					'input_group' => array('append' => 'days')
				)
			))
			->add('memo', 'textarea', array(
				'required' => false,
				'attr' => array('rows' => 4)
			))
			->add('items', 'collection', array(
				'type' => new PurchaseOrderItemType(),
				'allow_add' => true,
				'allow_delete' => true,
				'by_reference' => false,
				'error_bubbling' => false,
				'prototype_name' => 'ANGLR_key_ANGLR'
			))
			->add('adjustments', 'collection', array(
				'type' => new AdjustmentType(),
				'allow_add' => true,
				'allow_delete' => true,
				'by_reference' => false,
				'error_bubbling' => false,
				'prototype_name' => 'ANGLR_key_ANGLR'
			))
			->add('saveAsDraft', 'submit', array(
				'label' => 'Save as Draft',
				'attr' => array(
					'class' => 'btn btn-primary submit-button',
					'data-loading-text' => "Saving...",
					'ng-disabled' => '!form.supplier'
				)
			))
			->add('saveAsApproved', 'submit', array(
				'label' => 'Save as Approved',
				'attr' => array(
					'class' => 'btn btn-success submit-button',
					'data-loading-text' => "Saving...",
					'ng-disabled' => '!form.supplier'
				)
			))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\PurchaseOrder'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_purchaseorder';
	}
}