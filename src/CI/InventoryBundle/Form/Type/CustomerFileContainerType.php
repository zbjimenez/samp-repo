<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerFileContainerType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('files', 'collection', array(
			'type' => new CustomerFileType(),
			'allow_add' => true,
			'allow_delete' => true,
			'by_reference' => false
		))
		->add('save', 'submit', array(
			'label' => 'Save',
			'attr' => array(
				'class' => 'btn btn-success submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\Customer'
		));
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_customerfilecontainer';
	}
}