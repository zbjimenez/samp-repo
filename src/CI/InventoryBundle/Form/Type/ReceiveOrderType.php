<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReceiveOrderType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('purchaseOrder', 'entity_hidden', array(
			'class' => 'CIInventoryBundle:PurchaseOrder',
			'label' => 'PO #',
			'required' => true
		))
		->add('referenceNumber', 'text', array(
			'label' => 'Reference #',
			'required' => false,
			'attr' => array('widget_col' => 5) 
		))
		->add('dateReceived', 'date', array(
			'label'    => 'Date Received',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('memo', 'textarea', array(
			'required' => false,
			'attr' => array('rows' => 4)
		))
		->add('items', 'collection', array(
			'type' => new ReceiveOrderItemType(),
			'allow_add' => true,
			'allow_delete' => true,
			'by_reference' => false,
			'error_bubbling' => false,
			'prototype_name' => 'ANGLR_key_ANGLR'
		))
		->add('receivedFrom', 'text', array(
			'label' => 'Received From',
			'required' => true,
			'attr' => array('widget_col' => 5) 
		))
		->add('drNo', 'text', array(
			'label' => 'DR #',
			'required' => true,
			'attr' => array('widget_col' => 5) 
		))
		->add('soNo', 'text', array(
			'label' => 'SO #',
			'required' => false,
			'attr' => array('widget_col' => 5) 
		))
		->add('invoiceNumber', 'text', array(
			'label' => 'Invoice #',
			'required' => true,
			'attr' => array('widget_col' => 5) 
		))
		->add('containerNumber', 'text', array(
			'label' => 'Container #',
			'required' => true,
			'attr' => array('widget_col' => 5) 
		))
		->add('sealNumber', 'text', array(
			'label' => 'Seal #',
			'required' => true,
			'attr' => array('widget_col' => 5) 
		))
		->add('factoryNumber', 'text', array(
			'label' => 'Factory #',
			'required' => false,
			'attr' => array('widget_col' => 5) 
		))
		->add('supplierName', 'hidden', array(
			'required' => true,
			'attr' => array('widget_col' => 5) 
		))
		->add('save', 'submit', array(
			'label' => 'Save',
			'attr' => array(
				'class' => 'btn btn-success submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\ReceiveOrder'
		));
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_receiveorder';
	}
}