<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class SalesOrderReportFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('soId', 'text', array(
        		'label' => 'SO #',
	        	'required' => false,
        		'attr' => array('widget_col' => 5)
        	))
        	->add('poId', 'text', array(
				'label' => 'PO #',
				'required' => false,
				'attr' => array('widget_col' => 5)
	       ))
        	->add('dateFrom', 'date', array(
				'label'    => 'Date From',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'widget_col' => 7,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
        	))
        	->add('dateTo', 'date', array(
				'label'    => 'Date To',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'widget_col' => 7,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
        	))
        	->add('customer', 'entity', array(
				'label'    => 'Customer',
				'class' => 'CIInventoryBundle:Customer',
				'property' => 'shortName',
				'empty_value' => 'All customers',
				'required' => false,
				'attr' => array(
					'widget_col' => 7,
					'class' => 'select2 customer'
				),
				'query_builder' => function(EntityRepository $repository) {
	        			return $repository->findAllQb();
				}
        	))
			->add('search', 'submit', array(
				'label' => 'Search',
				'attr' => array(
					'class' => 'btn btn-default btn-outline submit-button',
					'data-loading-text' => "Searching..."
				)
		));
	}

	public function getName()
	{
		return 'ci_inventorybundle_salesorderreportfiltertype';
	}
}