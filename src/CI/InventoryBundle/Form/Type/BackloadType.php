<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use CI\InventoryBundle\Entity\Shipping;

class BackloadType extends AbstractType
{
	protected $em;

	public function __construct($em)
	{
		$this->em = $em;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$em = $this->em;

		$builder
			->add('date', 'date', array(
				'label' => 'Date',
				'widget' => 'single_text',
				'format' => 'MM/dd/y',
				'attr' => array(
					'widget_col' => 4,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
				),
			))
			->add('shipping', 'entity_hidden', array(
				'required' => true,
				'class' => 'CIInventoryBundle:Shipping',
				'label' => 'Delivery #',
			))
			->add('items', 'collection', array(
				'type' => new BackloadItemType($em),
				'allow_add' => true,
				'allow_delete' => true,
				'by_reference' => false,
				'error_bubbling' => true,
				'prototype_name' => 'ANGLR_key_ANGLR',
			))
			->add('save', 'submit', array(
				'label' => 'Save ',
				'attr' => array(
					'class' => 'btn btn-success submit-button',
					'data-loading-text' => "Saving...",
				),
			))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\Backload'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_backload';
	}
}
