<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CI\InventoryBundle\Entity\SalesActivity;

class SalesActivityFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('status', 'choice', array(
				'required' => false,
				'empty_value' => 'All status',
				'attr' => array('widget_col' => 5),
				'choices' => array(
						SalesActivity::STATUS_DRAFT => 'Draft',
						SalesActivity::STATUS_SUBMITTED => 'Submitted'
				)))
			->add('dateFrom', 'date', array(
				'label'    => 'Date From',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'required' => false,
				'attr'	   => array(
					'widget_col' => 5,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
			))
			->add('dateTo', 'date', array(
				'label'    => 'Date To',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'required' => false,
				'attr'	   => array(
					'widget_col' => 5,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
			))
			->add('search', 'submit', array(
				'attr' => array(
					'class' => 'btn btn-outline submit-button',
					'data-loading-text' => "Searching..."
				)
			))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesactivityfilter';
	}
}