<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Doctrine\ORM\EntityRepository;

class BackloadFilterType extends AbstractType
{
	private $sc;
	
	public function __construct(SecurityContextInterface $sc)
	{
		$this->sc = $sc;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$sc = $this->sc;

		$builder
			->setMethod('GET')
			->add('id', 'text', array(
				'required' => false,
				'attr' => array(
					'widget_col' => 5,
				),
				'label' => 'Backload #'
			))
			->add('drId', 'text', array(
				'required' => false,
				'attr' => array(
					'widget_col' => 5,
				),
				'label' => 'DR #'
			))
			->add('dateFrom', 'date', array(
				'label' => 'Date From',
				'required' => false,
				'widget' => 'single_text',
				'format' => 'MM/dd/y',
				'attr' => array(
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
					'widget_col' => 5,
				),
			))
			->add('dateTo', 'date', array(
				'label' => 'Date To',
				'required' => false,
				'widget' => 'single_text',
				'format' => 'MM/dd/y',
				'attr' => array(
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
					'widget_col' => 5,
				),
			))
			->add('customer', 'entity', array(
				'class' => 'CIInventoryBundle:Customer',
				'property' => 'shortName',
				'empty_value' => 'All customers',
				'required' => false,
				'attr' => array(
					'class' => 'select2 customer',
					'widget_col' => 5,
				),
				'query_builder' => function(EntityRepository $repository) use ($sc) {
					if ($sc->isGranted('ROLE_SALES_AGENT') && !$sc->isGranted('ROLE_SUPER_ADMIN')) {
						return $repository->createQueryBuilder('c')
							->select('c')
							->join('c.salesAgent', 'agent')
							->where('agent.id = :agentId')
							->setParameter('agentId', $sc->getToken()->getUser()->getId());
					} else {
						return $repository->findAllQb();
					}
				}
			))
			->add('search', 'submit', array(
				'label' => 'Search',
				'attr' => array(
					'class' => 'btn btn-default btn-outline submit-button',
					'data-loading-text' => "Search..."
				)
			))
		;
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_backloadfilter';
	}
}