<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PurchaseOrderReportFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->setMethod('GET')
		->add('poId', 'text', array(
			'required' => false,
			'label' => 'PO #',
			'attr' => array('widget_col' => 5)
		))
		->add('supplier', 'entity', array(
			'class' => 'CIInventoryBundle:Supplier',
			'property' => 'name',
			'required' => false,
			'empty_value' => 'All suppliers',
			'attr' => array('class' => 'select2'),
			'query_builder' => function($repository) {
				return $repository->findAllQb();
			}
		))
		->add('products', 'entity', array(
			'class' => 'CIInventoryBundle:Product',
			'property' => 'displayName',
			'required' => false,
			'multiple' => true,
			'empty_value' => 'All products',
			'attr' => array('class' => 'select2'),
			'query_builder' => function($repository) {
				return $repository->findAllQb();
			}	
		))
		->add('dateFrom', 'date', array(
			'label'    => 'Date From',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'required' => false,
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('dateTo', 'date', array(
			'label'    => 'Date To',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'required' => false,
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_poreportfilter';
	}
}