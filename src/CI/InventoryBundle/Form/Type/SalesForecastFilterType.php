<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SalesForecastFilterType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$arrayYears = array();
		$years = range(date("Y") + 5, date("Y") - 20);
		foreach ($years as $year) {
			$arrayYears[$year] = $year;
		}
		
		$builder->setMethod("GET")
		->add('customer', 'entity', array(
			'class' => 'CIInventoryBundle:Customer',
			'property' => 'shortName',
			'empty_value' => 'All customers',
			'required' => false,
			'attr' => array('select2' => true),
			'query_builder' => function($repository) {
				return $repository->findAllQb();
			}
		))
		->add('product', 'entity', array(
			'class' => 'CIInventoryBundle:Product',
			'property' => 'displayName',
			'empty_value' => 'All products',
			'required' => false,
			'attr' => array('select2' => true),
			'query_builder' => function($repository) {
				return $repository->findAllQb();
			}
		))
		->add('year', 'choice', array(
			'required' => false,
			'empty_value' => 'All years',
			'attr' => array('select2' => true),
			'choices' => $arrayYears,
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesforecastfilter';
	}
}