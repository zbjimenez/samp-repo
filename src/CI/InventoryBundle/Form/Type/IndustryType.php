<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IndustryType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name', 'text', array(
				'label' => 'Name',
				'attr' => array(
					'widget_col' => 6,
				),
			))
			->add('description', 'textarea', array(
				'required' => false,
				'attr' => array(
					'rows' => 5,
					'widget_col' => 6,
				),
			))
			->add('save', 'submit', array(
				'label' => 'Save',
				'attr' => array(
					'class' => 'btn btn-success submit-button',
					'data-loading-text' => "Saving..."
				)
			))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\Industry'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_industry';
	}
}
