<?php
namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PurchaseOrderFileType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('file_actual', 'file', array(
			'attr' => array(
				'class' => 'file-input'
			)
		))
		;
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\PurchaseOrderFile'
		));
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_purchaseorderfile';
	}
}