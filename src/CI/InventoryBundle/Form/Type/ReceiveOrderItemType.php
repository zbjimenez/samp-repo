<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReceiveOrderItemType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('isIncluded', 'checkbox', array(
			'required' => false,
			'attr' => array(
					'class' => 'px',
					'align_with_widget' => true,
					'ng-model' => 'form.items[key].isIncluded'
			)
		))
		->add('purchaseOrderItem', 'entity_hidden', array(
			'class' => 'CIInventoryBundle:PurchaseOrderItem',
			'attr' => array(
				'ng-model' => 'form.items[key].poItem',
				'ng-readonly' => '!item.isIncluded',
				'ng-value' => 'form.items[key].poItem'
			)
		))
		->add('lotNumber', 'text', array(
			'required' => true,
			'attr' => array(
				'ng-model' => 'form.items[key].lotNumber',
				'ng-readonly' => '!item.isIncluded'
			)
		))
		->add('quantity', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'ng-model' => 'form.items[key].quantity',
				'ng-readonly' => '!item.isIncluded',
				'simple_col' => 9
			)
		))
		->add('mode', 'text', array(
			'required' => true,
			'attr' => array(
				'ng-model' => 'form.items[key].mode',
				'ng-readonly' => '!item.isIncluded',
				'class' => 'small-textbox'
			)
		))
		->add('type', 'text', array(
			'required' => true,
			'attr' => array(
				'ng-model' => 'form.items[key].type',
				'ng-readonly' => '!item.isIncluded',
				'class' => 'small-textbox'
			)
		))
		->add('barcode', 'text', array(
			'required' => true,
			'attr' => array(
				'ng-model' => 'form.items[key].barcode',
				'ng-readonly' => 'true',
				'class' => 'hidden'
			)
		))
		->add('palletId', 'text', array(
			'required' => true,
			'attr' => array(
				'ng-model' => 'form.items[key].palletId',
				'ng-readonly' => '!item.isIncluded'
			)
		))
		->add('productionDateFromScan', 'text', array(
			'required' => true,
			'attr' => array(
				'ng-model' => 'form.items[key].productionDateFromScan',
				'ng-readonly' => '!item.isIncluded'
			)
		))
		;
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\ReceiveOrderItem',
		));
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_receiveorderitem';
	}
}