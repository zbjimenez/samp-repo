<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class PurchaseOrderShippingType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('cnPf', 'text', array(
				'label' => 'CN/PF #',
				'required' => true,
				'constraints' => array(
					new NotBlank(array('message' => 'CN/PF # must not be blank.'))
				),
				'attr' => array(
					'widget_col' => 4,
					'ng-model' => 'form.cnPf')
			))
			->add('invoiceNumber', 'text', array(
				'label' => 'Invoice #',
				'required' => false,
				'attr' => array(
					'widget_col' => 4,
					'ng-model' => 'form.invoiceNumber')
			))
			->add('containerNumber', 'text', array(
				'label' => 'Container #',
				'required' => false,
				'attr' => array(
					'widget_col' => 4,
					'ng-model' => 'form.containerNumber')
			))
			->add('sealNumber', 'text', array(
				'label' => 'Seal #',
				'required' => false,
				'attr' => array(
					'widget_col' => 4,
					'ng-model' => 'form.sealNumber')
			))
			->add('shipmentAdvice', 'textarea', array(
				'label' => 'Shipment Advice',
				'required' => false,
				'attr' => array(
					'rows' => 4,
					'ng-model' => 'form.shipmentAdvice')
			))
			 ->add('save', 'submit', array(
            	'label' => 'Save',
            	'attr' => array(
            		'ng-disabled' => 'sform.$invalid',
            		'class' => 'btn btn-success submit-button',
            		'data-loading-text' => "Saving..."
            	)
            ))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\PurchaseOrder'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_purchaseordershipping';
	}
}