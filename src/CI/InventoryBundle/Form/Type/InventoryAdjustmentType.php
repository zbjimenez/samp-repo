<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use CI\InventoryBundle\Entity\Inventory;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class InventoryAdjustmentType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('type', 'choice', array(
				'required' => true,
				'data' => Inventory::ADJUSTMENT_IN,
				'expanded' => true,
				'choices' => array(
					Inventory::ADJUSTMENT_IN => 'In',
					Inventory::ADJUSTMENT_OUT => 'Out'
				),
				'constraints' => array(
					new NotBlank(array('message' => 'Please select a type.'))
				),
				'attr' => array('ng-model' => 'form.type')
			))
			->add('storageLocation', 'entity', array(
				'class' => 'CIInventoryBundle:StorageLocation',
            	'label' => 'Storage Location',
				'required' => true,
				'empty_value' => 'Choose a Storage Location',
				'property' => 'fullLocation',
				'attr' => array(
					'select2' => 'select2',
					'ng-model' => 'form.storageLocation',
				),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
			))
			->add('quantity', 'custom_number', array(
				'required' => false,
				'attr' => array(
					'widget_col' => 4,
					'ng-model' => 'form.quantity')
			))
			->add('lotNumber', 'text', array(
				'required' => false,
				'label' => 'Lot Number',
				'attr' => array(
					'widget_col' => 6,
					'ng-model' => 'form.lotNumber'
				),
			))
			->add('palletId', 'text', array(
				'required' => true,
				'label' => 'Pallet ID',
				'attr' => array(
					'widget_col' => 6,
					'ng-model' => 'form.palletId'
				),
			))
			->add('productionDate', 'date', array(
				'required' => false,
				'label'    => 'Production Date',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'widget_col' => 4,
					'ng-model' => 'form.productionDateForm',
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
			))
			->add('factoryDate', 'date', array(
				'required' => false,
				'label'    => 'Factory Date',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'widget_col' => 4,
					'ng-model' => 'form.factoryDateForm',
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
			))
			 ->add('save', 'submit', array(
            	'label' => 'Save',
            	'attr' => array(
            		'ng-disabled' => 'aform.$invalid',
            		'class' => 'btn btn-success submit-button',
            		'data-loading-text' => "Saving..."
            	)
            ))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_inventoryadjustment';
	}
}