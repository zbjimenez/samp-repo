<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PriceHistoryReportFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('product', 'entity', array(
				'class' => 'CIInventoryBundle:Product',
				'required' => false,
				'empty_value' => 'All products',
				'property' => 'sku',
				'label' => 'Product Code',
				'attr' => array(
					'select2' => 'select2',
					'widget_col' => 5
				),
				'query_builder' => function($er) {
					return $er->findAllQb();
				}
		))
		->add('search', 'submit', array(
				'attr' => array(
					'class' => 'btn btn-outline submit-button',
					'data-loading-text' => "Searching..."
				)
		))
		;
	}
		
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_pricehistoryreportfilter';
	}
}