<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ShippingItemType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('isIncluded', 'checkbox', array(
			'required' => false,
			'attr' => array(
				'class' => 'px',
				'align_with_widget' => true,
				'ng-model' => 'form.items[key].isIncluded'
			)
		))
		->add('quantity', 'custom_number', array(
			'label' => false,
			'attr' => array(
				'ng-model' => 'form.items[key].quantity',
				'ng-readonly' => '!item.isIncluded',
				'simple_col' => 9
			)
		))
		->add('salesOrderItem', 'entity_hidden', array(
			'label' => false,
			'class' => 'CIInventoryBundle:SalesOrderItem',
			'attr' => array(
				'ng-model' => 'form.items[key].salesOrderItem',
				'ng-readonly' => '!item.isIncluded',
				'ng-value' => 'form.items[key].salesOrderItem',
			)
		))
		->add('remarks', 'text', array(
			'label' => false,
			'attr' => array(
				'ng-model' => 'form.items[key].remarks',
				'ng-readonly' => '!item.isIncluded'
			)
		))
		;
		
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\ShippingItem',
		));
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_shippingitem';
	}
}