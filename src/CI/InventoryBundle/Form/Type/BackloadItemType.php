<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use CI\InventoryBundle\Entity\ShippingItem;

class BackloadItemType extends AbstractType
{
	protected $em;

	public function __construct($em)
	{
		$this->em = $em;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$em = $this->em;

		$builder
			->add('isIncluded', 'checkbox', array(
				'required' => false,
				'attr' => array(
					'class' => 'px',
					'align_with_widget' => true,
					'ng-model' => 'form.items[key].isIncluded',
				)
			))
			->add('shippingItem', 'entity_hidden', array(
				'class' => 'CIInventoryBundle:ShippingItem',
				'attr' => array(
					'ng-model' => 'form.items[key].shippingItem',
					'ng-value' => 'form.items[key].shippingItem'
				)
			))
			->add('palletId', 'text', array(
				'label' => false,
				'required' => false,
				'attr' => array(
					'widget_col' => 12,
				),
			))
			->add('quantity', 'custom_number', array(
				'required' => true,
				'attr' => array(
					'widget_col' => 12,
					'class' => 'text-right',
					'ng-model' => 'form.items[key].quantity',
				),
			))
			->add('remarks', 'textarea', array(
				'required' => false,
				'attr' => array(
					'rows' => 3,
 					'ng-model' => 'form.items[key].remarks',
				)
			))
			->add('correctiveAction', 'textarea', array(
				'required' => false,
				'attr' => array(
					'rows' => 3,
 					'ng-model' => 'form.items[key].correctiveAction',
				)
			))
			->add('productionDate', 'date', array(
				'required' => true,
				'widget' => 'single_text',
				'format' => 'y-M-d',
				'attr' => array(
					'class' => 'hide',
					'ng-model' => 'form.items[key].productionDate',
					'ng-value' => 'form.items[key].productionDate',
				),
			))
		;

		$generateFields = function(FormInterface $form, $lotNumbers) {
			$form
				->add('lotNumber', 'choice', array(
					'choices' => $lotNumbers,
					'required' => false,
					'attr' => array(
						'class' => 'lotnumber-ANGLR_key_ANGLR',
						'ng-model' => 'form.items[key].lotNumber.label',
						'ng-value' => 'form.items[key].lotNumber.label',
						'ng-change' => 'onChangeLotNumber(item)',
						'select2' => true,
						'ng-options' => 'lotNumber.label as lotNumber.label for lotNumber in form.items[key].lotNumbers track by lotNumber.label',
						'widget_col' => 12,
					),
				))
			;
		};

		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($em, $generateFields) {
			$form = $event->getForm();
			$data = $event->getData();

			$lotNumbers = array();

			if (!is_null($data)) {
				if ($data->getShippingItem() instanceof ShippingItem) {
					$shippingItemId = $data->getShippingItem()->getId();
					$shippingId = $data->getShippingItem()->getShipping()->getId();
					$productId = $em->getRepository('CIInventoryBundle:ShippingItem')->findProductByItemId($shippingItemId)['id'];
					$lotNumberResults = $em->getRepository('CIInventoryBundle:OrderPickingItem')->findLotNumbers($productId, $shippingId);

					foreach ($lotNumberResults as $result) {
						$lotNumbers[$result['label']] = $result['label'];
					}
				}
			}

			$generateFields($form, $lotNumbers);
		});

		$builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($em, $generateFields) {
			$form = $event->getForm();
			$data = $event->getData();

			$lotNumbers = array();
			$palletIds = array();

			if (!is_null($data)) {
				if (array_key_exists('shippingItem', $data)) {
					$shippingItemId = $data['shippingItem'];
					$shippingId = $em->getRepository('CIInventoryBundle:ShippingItem')->findShipping($shippingItemId)[0]['shippingId'];
					$productId = $em->getRepository('CIInventoryBundle:ShippingItem')->findProductByItemId($shippingItemId)['id'];
					$lotNumberResults = $em->getRepository('CIInventoryBundle:OrderPickingItem')->findLotNumbers($productId, $shippingId);

					foreach ($lotNumberResults as $result) {
						$lotNumbers[$result['label']] = $result['label'];
					}
				}
			}

			$generateFields($form, $lotNumbers);
		});
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\BackloadItem',
			'validation_groups' => function (FormInterface $form) {
				$data = $form->getData();
				
				if (false === $data->getIsIncluded()) {
					return array('Default');
				}
					
				return array('Default', 'Included');
			},
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_backloaditem';
	}
}
