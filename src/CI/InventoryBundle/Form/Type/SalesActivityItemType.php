<?php

namespace CI\InventoryBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use CI\CoreBundle\Entity\User;
use CI\InventoryBundle\Entity\SalesActivityItem;

class SalesActivityItemType extends AbstractType
{
	private $securityContext;
	private $agent;
	
	public function __construct(SecurityContextInterface $sc, $agent = null)
	{
		$this->securityContext = $sc;
		$this->agent = $agent;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
			$form = $event->getForm();
			$data = $event->getData();
			
			$sc = $this->securityContext;
			$user = $sc->getToken()->getUser();
			$agent = $this->agent;
			
			if (!$data || $data->getId() == null || $agent == $user) {
				$form
				->add('inOffice', 'checkbox', array(
					'required' => false,
					'label' => false,
					'attr'=> array(
						'class' => 'in-office',
					),
				))
				->add('startTime', 'time', array(
					'widget' => 'single_text'
				))
				->add('endTime', 'time', array(
					'widget' => 'single_text'
				))
				->add('customer', 'entity', array(
					'class' => 'CIInventoryBundle:Customer',
					'property' => 'name',
					'required' => false,
					'empty_value' => 'Choose a customer',
					'attr' => array('class' => 'select2 customer'),
					'query_builder' => function(EntityRepository $repository) use ($sc, $user) {
						return $repository->createQueryBuilder('c')
						->select('c')
						->join('c.salesAgent', 'a')
						->where('a.id = :agentId')
						->setParameter('agentId', $user->getId())
						->orderBy('c.name', 'ASC')
						;
					}
				))
				->add('contact', 'text', array(
					'label' => 'Contact Person',
					'required' => true
				))
				->add('description', 'textarea', array(
					'attr'=> array('rows' => 4)
				))
				;
			}
			
			if ($sc->isGranted('ROLE_SALES_MANAGER')) {
				$form->add('comments', 'textarea', array(
					'required' => false,
					'attr'=> array('rows' => 4)
				));
			}
		});
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\SalesActivityItem'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesactivity';
	}
}