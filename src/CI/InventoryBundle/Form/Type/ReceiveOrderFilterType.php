<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ReceiveOrderFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('status', 'choice', array(
			'required' => false,
			'empty_value' => 'All status',
			'attr' => array('widget_col' => 4),
			'choices' => array(
				true => 'Received',
				false => 'Void'
			)
		))
		->add('referenceNumber', 'text', array(
			'required' => false,
			'attr' => array('widget_col' => 6),
			'label' => 'Reference #'
		))
		->add('poId', 'text', array(
			'required' => false,
			'attr' => array('widget_col' => 6),
			'label' => 'PO #'
		))
		->add('roId', 'text', array(
			'required' => false,
			'attr' => array('widget_col' => 6),
			'label' => 'RO #'
		))
		->add('dateFrom', 'date', array(
			'label'    => 'Date Received From',
			'required' => false,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'datepicker' => true,
				'widget_col' => 4,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('dateTo', 'date', array(
			'label'    => 'Date Received To',
			'required' => false,
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'datepicker' => true,
				'widget_col' => 4,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('search', 'submit', array(
			'label' => 'Search',
			'attr' => array(
				'class' => 'btn btn-default btn-outline submit-button',
				'data-loading-text' => "Search..."
			)
		))
		;
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_receiveorderfilter';
	}
}