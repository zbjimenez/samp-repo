<?php
namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use CI\InventoryBundle\Entity\Customer;

class ShippingType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('salesOrder', 'entity_hidden', array(
			'required' => true,
			'class' => 'CIInventoryBundle:SalesOrder',
			'label' => 'SO #',
		))
		->add('drId', 'text', array(
			'label' => 'DR #',
			'attr' => array('widget_col' => 5), 
		))
		->add('siRefCode', 'text', array(
			'label' => 'SI #',
			'required' => false,
			'attr' => array('widget_col' => 5),
		))
		->add('deliveryDate', 'date', array(
			'label'    => 'Delivery Date',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'widget_col' => 4,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			),
		))
		->add('truck', 'text', array(
			'label' => 'Truck',
			'required' => false,
			'attr' => array('widget_col' => 5),
		))
		->add('attention', 'checkbox', array(
			'label' => 'Attention',
			'attr' => array('widget_col' => 5),
		))
		->add('driver', 'text', array(
			'label' => 'Driver',
			'required' => false,
			'attr' => array('widget_col' => 5),
		))
		->add('memo', 'textarea', array(
			'required' => false,
			'attr' => array('rows' => 4),
		))
		->add('items', 'collection', array(
			'type' => new ShippingItemType(),
			'allow_add' => true,
			'allow_delete' => true,
			'by_reference' => false,
			'error_bubbling' => false,
			'prototype_name' => 'ANGLR_key_ANGLR',
		))
		->add('saveAsDraft', 'submit', array(
			'label' => 'Save as Draft',
			'attr' => array(
				'class' => 'btn btn-default submit-button',
				'data-loading-text' => "Saving...",
				'ng-disabled' => '!form.items',
			),
		))
		->add('saveAsPending', 'submit', array(
			'label' => 'Save as For Delivery',
			'attr' => array(
				'class' => 'btn btn-default submit-button',
				'data-loading-text' => "Saving...",
				'ng-disabled' => '!form.items',
			),
		))
		;
		
		$renderBranch = function (FormInterface $form, Customer $customer = null) {
			$form->add('branch', 'entity', array(
				'class' => 'CIInventoryBundle:Branch',
				'property' => 'name',
				'required' => true,
				'empty_value' => 'Choose a branch',
				'attr' => array(
					'select2' => 'select2',
					'widget_col' => 7,
				),
				'query_builder' => function($er) use ($customer) {
					return $er->findAllQb($customer ? $customer->getId() : 0);
				}
			));
		};
		
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) use ($renderBranch) {
			$data = $event->getData();
			$salesOrder = $data->getSalesOrder();
			$renderBranch($event->getForm(), $salesOrder ? $salesOrder->getCustomer() : null);
		});
		
		$builder->get('salesOrder')->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($renderBranch) {
			$salesOrder = $event->getForm()->getData();
			$renderBranch($event->getForm()->getParent(), $salesOrder->getCustomer());
		});
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\Shipping'
		));
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_shipping';
	}
}