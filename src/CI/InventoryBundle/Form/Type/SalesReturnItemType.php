<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SalesReturnItemType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('shippingItem', 'entity_hidden', array(
			'class' => 'CIInventoryBundle:ShippingItem',
			'required' => true,
			'attr'=> array(
				'ng-model' => 'form.items[key].sItem.id',
				'ng-value' => 'form.items[key].sItem.id'
			)
		))
		->add('quantity', 'custom_number', array(
			'attr' => array(
				'class' => 'amount',
				'ng-model' => 'form.items[key].quantity',
				'ng-change' => 'computeTotal()'
			)
		))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\SalesReturnItem'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_salesreturnitem';
	}
}