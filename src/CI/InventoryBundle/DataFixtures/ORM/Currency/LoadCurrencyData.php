<?php
namespace CI\InventoryBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use CI\InventoryBundle\Entity\Currency;

class LoadCurrencyData extends AbstractFixture
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$currency = new Currency();
		$currency->setCurrencyName('Philippine Peso');
		$currency->setCurrencyCode('PHP');
		$currency->setCreatedBy('system');
		$currency->setCreatedAt(new \DateTime('now'));
		
		$manager->persist($currency);
		$manager->flush();
		
		$purchaseOrders = $manager->getRepository('CIInventoryBundle:PurchaseOrder')
		->createQueryBuilder('po')
		->select('po')
		->getQuery()
		->getResult();
		
		foreach ($purchaseOrders as $po) {
			$po->setCurrency($currency);
			$manager->persist($po);
		}
		
		$manager->flush();
	}

}