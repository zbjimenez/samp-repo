<?php
namespace CI\InventoryBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use CI\InventoryBundle\Entity\Warehouse;

class LoadWarehouseData extends AbstractFixture
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$warehouse = new Warehouse();
		$warehouse->setName('Antipolo');
		$warehouse->setAddress('Antipolo');
		$warehouse->setCreatedBy('system');
		$warehouse->setCreatedAt(new \DateTime('now'));
		
		$manager->persist($warehouse);

		$warehouse = new Warehouse();
		$warehouse->setName('Pasig');
		$warehouse->setAddress('Pasig');
		$warehouse->setCreatedBy('system');
		$warehouse->setCreatedAt(new \DateTime('now'));
		
		$manager->persist($warehouse);

		$warehouse = new Warehouse();
        $warehouse->setName('Sucat');
        $warehouse->setAddress('Sucat');
        $warehouse->setCreatedBy('system');
        $warehouse->setCreatedAt(new \DateTime('now'));
        
        $manager->persist($warehouse);
        $manager->flush();
		
	}

}