<?php

namespace CI\InventoryBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;

class LoadDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
// 		$unit = new Unit();
// 		$unit->setName("Pieces");
// 		$unit->setAbbreviation("pc");
// 		$manager->persist($unit);
		
// 		$category = new Category();
// 		$category->setName("Uncategorized");
// 		$manager->persist($category);
// 		$manager->flush();
	}
	
	public function getOrder()
	{
		return 4;
	}
}