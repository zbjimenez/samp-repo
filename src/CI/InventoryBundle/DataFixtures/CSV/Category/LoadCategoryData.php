<?php

namespace CI\InventoryBundle\DataFixtures\CSV\Category;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\ORM\Id\AssignedGenerator;

use CI\InventoryBundle\Entity\Category;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$rows = array();
		$handle = fopen(dirname(__FILE__). '/Category.csv', "r");
		
		if ($handle) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$rows[] = $data;
			}
			fclose($handle);
		}
		
		$i = 0;
		$batchSize = 500; //lower if you get Out of Memory exception
		
		$totalItems = count($rows);
		
		$metadata = $manager->getClassMetaData(Category::class);
		$metadata->setIdGenerator(new AssignedGenerator());
		$metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

		foreach ($rows as $row) {
			if (!empty(trim($row[5]))) {
				$newCategory = new Category();
				$newCategory->setId(trim($row[0]));
				$newCategory->setCreatedAt(new \DateTime(trim($row[2])));
				$newCategory->setUpdatedAt(new \DateTime(trim($row[3])));

				$isActive = trim($row[4]) == '1' ? true : false;
				$newCategory->setActive($isActive);

				$newCategory->setName(trim($row[5]));

				$description = trim($row[6]) == 'NULL' ? null : trim($row[6]);
				$newCategory->setDescription($description);

				$newCategory->setCreatedBy(trim($row[7]));
				$newCategory->setUpdatedBy(trim($row[8]));
				
				$manager->persist($newCategory);
			}
			
			if (($i % $batchSize) === 0) {
				$manager->flush();
				$manager->clear();
			}
			
			$i++;
		
			echo (integer)($i / $totalItems * 100) ."% ($i of $totalItems) \r";
			
			$manager->flush();
		}
		
		$manager->clear();
		
		echo "\n\nDone: $i of $totalItems category records imported.\n\n";
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 7;
	}
}
