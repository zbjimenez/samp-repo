<?php

namespace CI\InventoryBundle\DataFixtures\CSV\ProductCode;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\ORM\Id\AssignedGenerator;

use CI\InventoryBundle\Entity\ProductCode;
use CI\InventoryBundle\Entity\Customer;
use CI\InventoryBundle\Entity\Product;

class LoadProductCodeData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$rows = array();
		$handle = fopen(dirname(__FILE__). '/product_code.csv', "r");
		
		if ($handle) {
			while (($data = fgetcsv($handle, 2000, ",")) !== FALSE) {
				$rows[] = $data;
			}
			fclose($handle);
		}

		$i = 0;
		$batchSize = 1000; //lower if you get Out of Memory exception
		
		$totalItems = count($rows);
		
		$metadata = $manager->getClassMetaData(ProductCode::class);
		$metadata->setIdGenerator(new AssignedGenerator());
		$metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

		foreach ($rows as $key => $row) {
			if (!empty(trim($row[0]))) {

				$customer = $manager->getRepository('CIInventoryBundle:Customer')->find(trim($row[1]));
				$product = $manager->getRepository('CIInventoryBundle:Product')->find(trim($row[2]));

				$newProductCode = new ProductCode();
				$newProductCode->setId(trim($row[0]));

				$newProductCode->setCustomer($customer);
				$newProductCode->setProduct($product);

				$newProductCode->setCreatedAt(new \DateTime(trim($row[3])));
				$newProductCode->setUpdatedAt(new \DateTime(trim($row[4])));

				$isActive = trim($row[5]) == '1' ? true : false;
				$newProductCode->setActive($isActive);

				$code = trim($row[6]) == 'NULL' ? null : trim($row[6]);
				$newProductCode->setCode($code);

				$alias = trim($row[7]) == 'NULL' ? null : trim($row[7]);
				$newProductCode->setAlias($alias);

				$newProductCode->setCreatedBy(trim($row[8]));
				$newProductCode->setUpdatedBy(trim($row[9]));

				$manager->persist($newProductCode);
			}

			if (($i % $batchSize) === 0) {
				$manager->flush();
				$manager->clear();
			}

			$i++;
		
			echo (integer)($i / $totalItems * 100) ."% ($i of $totalItems) \r";

			$manager->flush();
		}
		
		$manager->clear();
		
		echo "\n\nDone: $i of $totalItems product code records imported.\n\n";
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 11;
	}
}
