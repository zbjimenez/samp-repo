<?php

namespace CI\InventoryBundle\DataFixtures\CSV\Supplier;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\ORM\Id\AssignedGenerator;

use CI\CoreBundle\Entity\User;
use CI\InventoryBundle\Entity\Customer;


class LoadCustomerData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$rows = array();
		$handle = fopen(dirname(__FILE__). '/Customer.csv', "r");
		
		if ($handle) {
			while (($data = fgetcsv($handle, 2000, ",")) !== FALSE) {
				$rows[] = $data;
			}
			fclose($handle);
		}
		
		$i = 0;
		$batchSize = 1000; //lower if you get Out of Memory exception

		$salesAgent = $manager->getRepository('CICoreBundle:User')->findOneByUsername('agent1');
		
		$totalItems = count($rows);

		$metadata = $manager->getClassMetaData(Customer::class);
		$metadata->setIdGenerator(new AssignedGenerator());
		$metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

		foreach ($rows as $row) {
			if (!empty(trim($row[4]))) {
				$newCustomer = new Customer();
				$newCustomer->setId(trim($row[0]));
				$newCustomer->setCreatedAt(new \DateTime(trim($row[1])));
				$newCustomer->setUpdatedAt(new \DateTime(trim($row[2])));

				$isActive = trim($row[3]) == '1' ? true : false;
				$newCustomer->setActive($isActive);

				$newCustomer->setName(trim($row[4]));
				$newCustomer->setShortName(trim($row[5]));
				$newCustomer->setContactDetails(trim($row[6]));
				$newCustomer->setTerms(intval(trim($row[7])));
				
				$tin = trim($row[8]) == 'NULL' ? null : trim($row[8]);
				$newCustomer->setTin($tin);

				$memo = trim($row[9]) == 'NULL' ? null : trim($row[9]);
				$newCustomer->setMemo($memo);

				$salesAgent = $manager->merge($salesAgent);

				$newCustomer->setEwt(trim($row[10]));
				$newCustomer->setSalesAgent($salesAgent);
				$newCustomer->setCreatedBy($row[11]);
				$newCustomer->setUpdatedBy($row[12]);

				$manager->persist($newCustomer);
			}
			
			if (($i % $batchSize) === 0) {
				$manager->flush();
				$manager->clear();
			}
			
			$i++;
		
			echo (integer)($i / $totalItems * 100) ."% ($i of $totalItems) \r";
			
			$manager->flush();
		}

		$manager->clear();
	
		echo "\n\nDone: $i of $totalItems customer records imported.\n\n";
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 6;
	}
}
