<?php

namespace CI\InventoryBundle\DataFixtures\CSV\Supplier;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\ORM\Id\AssignedGenerator;

use CI\InventoryBundle\Entity\Supplier;

class LoadSupplierData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$rows = array();
		$handle = fopen(dirname(__FILE__). '/Supplier.csv', "r");
		
		if ($handle) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$rows[] = $data;
			}
			fclose($handle);
		}
		
		$i = 0;
		$batchSize = 500; //lower if you get Out of Memory exception
		
		$totalItems = count($rows);
		
		$metadata = $manager->getClassMetaData(Supplier::class);
		$metadata->setIdGenerator(new AssignedGenerator());
		$metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

		foreach ($rows as $row) {
			if (!empty(trim($row[4]))) {
				$newSupplier = new Supplier();
				$newSupplier->setId(trim($row[0]));
				$newSupplier->setCreatedAt(new \DateTime(trim($row[1])));
				$newSupplier->setUpdatedAt(new \DateTime(trim($row[2])));

				$isActive = trim($row[3]) == '1' ? true : false;
				$newSupplier->setActive($isActive);

				$newSupplier->setName(trim($row[4]));
				$newSupplier->setShortName(trim($row[5]));
				$newSupplier->setAddress(trim($row[6]));
				$newSupplier->setContactDetails(trim($row[7]));
				$newSupplier->setEwt(trim($row[8]));
				$newSupplier->setTerms(intval(trim($row[9])));

				$memo = trim($row[10]) == 'NULL' ? null : trim($row[10]);
				$newSupplier->setMemo($memo);

				$newSupplier->setCreatedBy(trim($row[11]));
				$newSupplier->setUpdatedBy(trim($row[12]));
				
				$manager->persist($newSupplier);
			}
			
			if (($i % $batchSize) === 0) {
				$manager->flush();
				$manager->clear();
			}
			
			$i++;
		
			echo (integer)($i / $totalItems * 100) ."% ($i of $totalItems) \r";
			
			$manager->flush();
		}
		
		$manager->clear();
		
		echo "\n\nDone: $i of $totalItems supplier records imported.\n\n";
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 5;
	}
}
