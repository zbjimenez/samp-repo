<?php

namespace CI\InventoryBundle\DataFixtures\CSV\Supplier;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\ORM\Id\AssignedGenerator;

use CI\InventoryBundle\Entity\Branch;
use CI\InventoryBundle\Entity\Customer;

class LoadBranchData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$rows = array();
		$handle = fopen(dirname(__FILE__). '/Branch.csv', "r");
		
		if ($handle) {
			while (($data = fgetcsv($handle, 2000, ",")) !== FALSE) {
				$rows[] = $data;
			}
			fclose($handle);
		}
		
		$i = 0;
		$batchSize = 1000; //lower if you get Out of Memory exception
		
		$totalItems = count($rows);
		
		$metadata = $manager->getClassMetaData(Branch::class);
		$metadata->setIdGenerator(new AssignedGenerator());
		$metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

		foreach ($rows as $row) {
			if (!empty(trim($row[6]))) {
				$newBranch = new Branch();
				$newBranch->setId(trim($row[0]));

				$customer = $manager->getRepository('CIInventoryBundle:Customer')->find(trim($row[1]));
				$newBranch->setCustomer($customer);
		
				$newBranch->setCreatedAt(new \DateTime(trim($row[2])));
				$newBranch->setUpdatedAt(new \DateTime(trim($row[3])));

				$isActive = trim($row[4]) == '1' ? true : false;
				$newBranch->setActive($isActive);

				$isMain = trim($row[5]) == '1' ? true : false;
				$newBranch->setIsMain($isMain);

				$newBranch->setName(trim($row[6]));
				$newBranch->setAddress(trim($row[7]));

				$area = trim($row[8]) == 'NULL' ? null : trim($row[8]);
				$newBranch->setArea($area);

				$contactDetails = trim($row[9]) == 'NULL' ? null : trim($row[9]);
				$newBranch->setContactDetails($contactDetails);

				$newBranch->setCreatedBy($row[10]);
				$newBranch->setUpdatedBy($row[11]);

				$manager->persist($newBranch);
			}
			
			if (($i % $batchSize) === 0) {
				$manager->flush();
				$manager->clear();
			}
			
			$i++;
		
			echo (integer)($i / $totalItems * 100) ."% ($i of $totalItems) \r";

			$manager->flush();
		}

		
		$manager->clear();
		
		echo "\n\nDone: $i of $totalItems branch records imported.\n\n";
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 9;
	}
}
