<?php

namespace CI\InventoryBundle\DataFixtures\CSV\Product;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\ORM\Id\AssignedGenerator;

use CI\InventoryBundle\Entity\Product;
use CI\InventoryBundle\Entity\Supplier;
use CI\InventoryBundle\Entity\Category;
use CI\InventoryBundle\Entity\Industry;
use CI\InventoryBundle\Entity\Packaging;

class LoadProductData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$rows = array();
		$handle = fopen(dirname(__FILE__). '/Product.csv', "r");
		
		if ($handle) {
			while (($data = fgetcsv($handle, 2000, ",")) !== FALSE) {
				$rows[] = $data;
			}
			fclose($handle);
		}

		$newIndustry = new Industry();
		$newIndustry->setName('Sample Industry');
		$newIndustry->setCreatedAt(new \DateTime());
		$newIndustry->setCreatedBy('admin');
		$newIndustry->setActive(true);
		$manager->persist($newIndustry);
		$manager->flush();

		echo "\n\nSample Industry successfully imported. \n\n";

		$i = 0;
		$batchSize = 1000; //lower if you get Out of Memory exception
		
		$totalItems = count($rows);
		
		$metadata = $manager->getClassMetaData(Product::class);
		$metadata->setIdGenerator(new AssignedGenerator());
		$metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

		foreach ($rows as $key => $row) {
			if (!empty(trim($row[7]))) {

				$category = $manager->getRepository('CIInventoryBundle:Category')->find(trim($row[1]));
				$industry = $manager->getRepository('CIInventoryBundle:Industry')->findOneByName('Sample Industry');
				$packaging = $manager->getRepository('CIInventoryBundle:Packaging')->find(trim($row[14]));

				$newProduct = new Product();
				$newProduct->setId(trim($row[0]));

				$supplier = $manager->getRepository('CIInventoryBundle:Supplier')->find(trim($row[2]));

				if ($supplier instanceof Supplier) {
					$newProduct->setSupplier($supplier);
				} else {
					$newProduct->setSupplier(null);
				}

				$newProduct->setCategory($category);
				$newProduct->setIndustry($industry);
				$newProduct->setPackaging($packaging);

				$newProduct->setCreatedAt(new \DateTime(trim($row[3])));
				$newProduct->setUpdatedAt(new \DateTime(trim($row[4])));

				$isActive = trim($row[5]) == '1' ? true : false;
				$newProduct->setActive($isActive);

				$newProduct->setSku(trim($row[6]));
				$newProduct->setName(trim($row[7]));
				$newProduct->setDescription(trim($row[8]));

				$barcode = trim($row[9]) == 'NULL' ? null : trim($row[9]);
				$newProduct->setBarcode($barcode);

				$newProduct->setCreatedBy(trim($row[12]));
				$newProduct->setUpdatedBy(trim($row[13]));

				$storageTemp = trim($row[15]) == 'NULL' ? null : trim($row[13]);
				$newProduct->setStorageTemp($storageTemp);

				$shelfLife = trim($row[16]) == 'NULL' ? null : trim($row[14]);
				$newProduct->setShelfLife($shelfLife);

				$otherProductInfo = trim($row[17]) == 'NULL' ? null : trim($row[15]);
				$newProduct->setOtherProductInfo($otherProductInfo);

				$isHalal = trim($row[18]) == '1' ? true : false;
				$newProduct->setIsHalal($isHalal);

				$supplierProductName = trim($row[19]) == 'NULL' ? null : trim($row[17]);
				$newProduct->setSupplierProductName($supplierProductName);

				$supplierProductCode = trim($row[20]) == 'NULL' ? null : trim($row[18]);
				$newProduct->setSupplierProductCode($supplierProductCode);

				$newProduct->setTotalAllocated(0);
				$newProduct->setTotalStaged(0);
				$newProduct->setTotalOnHand(0);
				$newProduct->setReorderThreshold(0);
				$newProduct->setReorderQuantity(0);
				$newProduct->setIsTracked(true);
				$newProduct->setDesignatedWarehouseNumber(null);

				$manager->persist($newProduct);
			}

			if (($i % $batchSize) === 0) {
				$manager->flush();
				$manager->clear();
			}

			$i++;
		
			echo (integer)($i / $totalItems * 100) ."% ($i of $totalItems) \r";

			$manager->flush();
		}
		
		$manager->clear();
		
		echo "\n\nDone: $i of $totalItems product records imported.\n\n";
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 10;
	}
}
