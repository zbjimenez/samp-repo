<?php

namespace CI\InventoryBundle\DataFixtures\CSV\Packaging;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\ORM\Id\AssignedGenerator;

use CI\InventoryBundle\Entity\Packaging;

class LoadPackagingData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$rows = array();
		$handle = fopen(dirname(__FILE__). '/Packaging.csv', "r");
		
		if ($handle) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$rows[] = $data;
			}
			fclose($handle);
		}
		
		$i = 0;
		$batchSize = 500; //lower if you get Out of Memory exception
		
		$totalItems = count($rows);
		
		$metadata = $manager->getClassMetaData(Packaging::class);
		$metadata->setIdGenerator(new AssignedGenerator());
		$metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

		foreach ($rows as $row) {
			if (!empty(trim($row[4]))) {
				$newPackaging = new Packaging();
				$newPackaging->setId(trim($row[0]));
				$newPackaging->setCreatedAt(new \DateTime(trim($row[1])));
				$newPackaging->setUpdatedAt(new \DateTime(trim($row[2])));

				$isActive = trim($row[3]) == '1' ? true : false;
				$newPackaging->setActive($isActive);

				$newPackaging->setName(trim($row[4]));
				$newPackaging->setDescription(trim($row[5]));
				$newPackaging->setKgsUnit(floatval(trim($row[6])));
				$newPackaging->setCreatedBy(trim($row[7]));
				$newPackaging->setUpdatedBy(trim($row[8]));
				
				$manager->persist($newPackaging);
			}
			
			if (($i % $batchSize) === 0) {
				$manager->flush();
				$manager->clear();
			}
			
			$i++;
		
			echo (integer)($i / $totalItems * 100) ."% ($i of $totalItems) \r";
			
			$manager->flush();
		}
		
		$manager->clear();
		
		echo "\n\nDone: $i of $totalItems packaging records imported.\n\n";
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 8;
	}
}
