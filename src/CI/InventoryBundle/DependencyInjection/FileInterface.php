<?php

namespace CI\InventoryBundle\DependencyInjection;

/**
 * FileInterface should be implemented by item classes that depend on the FileHandlerService.
 */
interface FileInterface
{
	public function getAllowedFileExt();
}