<?php

namespace CI\InventoryBundle\DependencyInjection;

/**
 * FileEntityInterface should be implemented by entity classes that depend on the FileHandlerService that have
 * multiple files.
 */
interface FileEntityInterface
{
	/**
	 * @return the class of the children files and its ids as an array
	 * array(
	 *   'class' => class_of_children_files,
	 *   'ids' => array_of_children_files
	 * )
	 */
	public function getFilesInfo();
}