<?php

namespace CI\InventoryBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ci_inventory');
        
        $rootNode
            ->children()
                ->scalarNode('base_upload_dir')
                    ->isRequired()
                ->end()
            	->arrayNode('single_upload')
            		->useAttributeAsKey('name', false)
            		->prototype('array')
            			->children()
            				->arrayNode('allowedFileExt')
            					->requiresAtLeastOneElement()
            					->prototype('variable')->end()
            				->end()
            			->end()
            		->end()
            	->end()
            	->arrayNode('multi_upload')
            		->useAttributeAsKey('name', false)
            		->prototype('array')
            			->children()
                			->scalarNode('entityClass')
                			    ->isRequired()
                			->end()
                			->scalarNode('addChildMethod')
                				->isRequired()
                			->end()
            				->arrayNode('allowedFileExt')
    	        				->requiresAtLeastOneElement()
    	        				->prototype('variable')->end()
            				->end()
            				->integerNode('minQuantity')->end()
            				->integerNode('maxQuantity')->end()
            				->arrayNode('forms')
            					->addDefaultsIfNotSet()
            					->children()
            						->scalarNode("formName")->defaultNull()->end()
            						->scalarNode("propertyName")->defaultNull()->end()
            						->scalarNode("childClass")->defaultNull()->end()
            						->scalarNode("childFormName")->defaultNull()->end()
            					->end()
            				->end()
            			->end()
            		->end()
            	->end()
            ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
