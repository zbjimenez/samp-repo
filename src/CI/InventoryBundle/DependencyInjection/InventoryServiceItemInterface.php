<?php

namespace CI\InventoryBundle\DependencyInjection;

/**
 * InventoryServiceItemInterface should be implemented by item classes that depend on the Inventory Service.
 */
interface InventoryServiceItemInterface
{
	public function getISProduct();
	
	public function getISQuantity();
}