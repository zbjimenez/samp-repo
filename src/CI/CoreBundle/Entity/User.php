<?php

namespace CI\CoreBundle\Entity;

use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Scheb\TwoFactorBundle\Model\Google\TwoFactorInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user") 
 * @ORM\Entity(repositoryClass="CI\CoreBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields="email", message="Sorry, this email address is already taken.")
 * @UniqueEntity(fields="username", message="Sorry, this username is already taken.")
 * @Assert\Callback(methods={"validate"})
 */
class User extends BaseUser implements TwoFactorInterface
{
	const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
	const ROLE_ADMIN = 'ROLE_ADMIN';
	const ROLE_SALES_AGENT = 'ROLE_SALES_AGENT';
	const ROLE_SALES_COORDINATOR = 'ROLE_SALES_COORDINATOR';
	const ROLE_SALES_MANAGER = 'ROLE_SALES_MANAGER';
	const ROLE_INVENTORY_CLERK = 'ROLE_INVENTORY_CLERK';
	const ROLE_PURCHASING = 'ROLE_PURCHASING';
	const ROLE_ENCODER = 'ROLE_ENCODER';
	
	const ACCESS_DEACTIVATE = 'deactivate';
	const ACCESS_REACTIVATE = 'reactivate';

	const AUTH_ENABLE = 'enable';
	const AUTH_DISABLE = 'disable';
	
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="first_name", type="string", length=100)
	 * @Assert\NotBlank(message="First Name must not be blank.")
	 * @Assert\Length(min=1, max=100)
	 */
	private $firstName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="last_name", type="string", length=100, nullable=true)
	 * @Assert\Length(min=1, max=100)
	 */
	private $lastName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="gender", type="string", length=1)
	 * @Assert\NotBlank(message="Please select a gender.")
	 */
	private $gender;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="birth_date", type="datetime", nullable=true)
	 */
	private $birthDate;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="contact_number", type="text", length=25, nullable=true)
	 * @Assert\Length(min=1, max=25)
	 */
	private $contactNumber;
	
	/**
	 * @var datetime
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 * @Gedmo\Timestampable(on="create")
	 */
	private $createdAt;
	
	/**
	 * @ORM\Column(name="created_by", type="string", length=255)
	 * @Gedmo\Blameable(on="create")
	 */
	protected $createdBy;
	
	/**
	 * @var datetime
	 *
	 * @ORM\Column(name="updated_at", type="datetime", nullable=true)
	 * @Gedmo\Timestampable(on="update")
	 */
	private $updatedAt;
	
	/**
	 * @ORM\Column(name="updated_by", type="string", length=255, nullable=true)
	 * @Gedmo\Blameable(on="update")
	 */
	protected $updatedBy;
	
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_sidebar_collapsed", type="boolean")
	 */
	private $isSidebarCollapsed;

	/**
	 * @ORM\Column(name="google_authenticator_secret", type="string", nullable=true)
	 */
	private $googleAuthenticatorSecret;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="enable_two_factor", type="boolean")
	 */
	private $enableTwoFactor;
	
	/**
	 * @ORM\OneToMany(targetEntity="UserLog", mappedBy="user", cascade={"persist", "remove"})
	 */
	private $userLogs;
	
	/**
	 * @ORM\OneToMany(targetEntity="CI\InventoryBundle\Entity\SalesActivity", mappedBy="agent", cascade={"persist", "remove"})
	 */
	private $salesActivities;
	
	/*
	 * @ORM\OneToMany(targetEntity="CI\InventoryBundle\Entity\InventoryLog", mappedBy="createdBy")
	 */
	private $inventoryLog;
	
	/**
	 * @ORM\OneToMany(targetEntity="CI\InventoryBundle\Entity\Customer", mappedBy="salesAgent", cascade={"persist", "remove"})
	 */
	private $customers;
	
	/**
	 * @ORM\OneToMany(targetEntity="CI\InventoryBundle\Entity\Quotation", mappedBy="salesAgent", cascade={"persist", "remove"})
	 */
	private $quotations;
	
	/**
	 * @ORM\OneToMany(targetEntity="CI\InventoryBundle\Entity\SalesOrder", mappedBy="ownedBy", cascade={"persist", "remove"})
	 */
	private $ownedSalesOrders;

	/**
	 * @ORM\ManyToMany(targetEntity="Group", inversedBy="users")
	 * @ORM\JoinTable(name="user_user_group")
	 */
	protected $groups;

	public function __construct()
	{
		parent::__construct();
		$this->userLogs = new \Doctrine\Common\Collections\ArrayCollection();
		$this->inventoryLog = new \Doctrine\Common\Collections\ArrayCollection();
		$this->customers = new \Doctrine\Common\Collections\ArrayCollection();
		$this->quotations = new \Doctrine\Common\Collections\ArrayCollection();
		$this->ownedSalesOrders = new \Doctrine\Common\Collections\ArrayCollection();
		$this->groups = new \Doctrine\Common\Collections\ArrayCollection();
		$this->isSidebarCollapsed = false;
		$this->enableTwoFactor = false;
	}

	public function validate(ExecutionContextInterface $context)
	{
		if (count($this->getDisplayRoles()) < 1) {
			$context->addViolation('You must specify at least one Role.');
		}
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set firstName
	 *
	 * @param string $firstName
	 */
	public function setFirstName($firstName)
	{
		$this->firstName = $firstName;
	}

	/**
	 * Get firstName
	 *
	 * @return string
	 */
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
	 * Set lastName
	 *
	 * @param string $lastName
	 */
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}

	/**
	 * Get lastName
	 *
	 * @return string
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * Set gender
	 *
	 * @param string $gender
	 */
	public function setGender($gender)
	{
		$this->gender = $gender;
	}

	/**
	 * Get gender
	 *
	 * @return string
	 */
	public function getGender()
	{
		return $this->gender;
	}

	/**
	 * Set birthDate
	 *
	 * @param date $birthDate
	 */
	public function setBirthDate($birthDate)
	{
		$this->birthDate = $birthDate;
	}

	/**
	 * Get birthDate
	 *
	 * @return date
	 */
	public function getBirthDate()
	{
		return $this->birthDate;
	}

	/**
	 * Set contactNumber
	 *
	 * @param text $contactNumber
	 */
	public function setContactNumber($contactNumber)
	{
		$this->contactNumber = $contactNumber;
	}

	/**
	 * Get contactNumber
	 *
	 * @return text 
	 */
	public function getContactNumber()
	{
		return $this->contactNumber;
	}
	
	/**
	 * Set created_at
	 *
	 * @param datetime $createdAt
	 */
	public function setCreatedAt($createdAt = null)
	{
		$this->createdAt = $createdAt;
	}

	/**
	 * Get createdAt
	 *
	 * @return datetime 
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * Set updated_at
	 *
	 * @param datetime $updatedAt
	 */
	public function setUpdatedAt($updatedAt = null)
	{   
		$this->updatedAt = $updatedAt;
	}

	/**
	 * Get updatedAt
	 *
	 * @return datetime 
	 */
	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}
	
	/**
	 * Set createdBy
	 *
	 * @param string $createdBy
	 * @return User
	 */
	public function setCreatedBy($createdBy)
	{
		$this->createdBy = $createdBy;
	
		return $this;
	}
	
	/**
	 * Get createdBy
	 *
	 * @return string
	 */
	public function getCreatedBy()
	{
		return $this->createdBy;
	}
	
	/**
	 * Set updatedBy
	 *
	 * @param string $updatedBy
	 * @return User
	 */
	public function setUpdatedBy($updatedBy)
	{
		$this->updatedBy = $updatedBy;
	
		return $this;
	}
	
	/**
	 * Get updatedBy
	 *
	 * @return string
	 */
	public function getUpdatedBy()
	{
		return $this->updatedBy;
	}
	
	/**
	 * Get userLogs
	 * 
	 * @return ArrayCollection
	 */
	public function getUserLogs()
	{
		return $this->userLogs;
	}

	/**
	 * Add userLogs
	 *
	 * @param \CI\CoreBundle\Entity\UserLog $userLogs
	 * @return User
	 */
	public function addUserLog(\CI\CoreBundle\Entity\UserLog $userLogs)
	{
		$this->userLogs[] = $userLogs;
	
		return $this;
	}

	/**
	 * Remove userLogs
	 *
	 * @param \CI\CoreBundle\Entity\UserLog $userLogs
	 */
	public function removeUserLog(\CI\CoreBundle\Entity\UserLog $userLogs)
	{
		$this->userLogs->removeElement($userLogs);
	}
	
	/**
	 * Set isSidebarCollapsed
	 *
	 * @param boolean $isSidebarCollapsed
	 * @return User
	 */
	public function setIsSidebarCollapsed($isSidebarCollapsed)
	{
		$this->isSidebarCollapsed = $isSidebarCollapsed;
	
		return $this;
	}
	
	/**
	 * Get isSidebarCollapsed
	 *
	 * @return boolean
	 */
	public function getIsSidebarCollapsed()
	{
		return $this->isSidebarCollapsed;
	}
	
	/**
	 * Add customers
	 *
	 * @param \CI\InventoryBundle\Entity\Customer $customers
	 * @return User
	 */
	public function addCustomer(\CI\InventoryBundle\Entity\Customer $customers)
	{
		$this->customers[] = $customers;
	
		return $this;
	}
	
	/**
	 * Remove customers
	 *
	 * @param \CI\InventoryBundle\Entity\Customer $customers
	 */
	public function removeCustomer(\CI\InventoryBundle\Entity\Customer $customers)
	{
		$this->customers->removeElement($customers);
	}
	
	/**
	 * Get customers
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getCustomers()
	{
		return $this->customers;
	}

	/**
	 * Add quotations
	 *
	 * @param \CI\InventoryBundle\Entity\Quotation $quotations
	 * @return User
	 */
	public function addQuotation(\CI\InventoryBundle\Entity\Quotation $quotations)
	{
		$this->quotations[] = $quotations;
	
		return $this;
	}
	
	/**
	 * Remove quotations
	 *
	 * @param \CI\InventoryBundle\Entity\Quotation $quotations
	 */
	public function removeQuotation(\CI\InventoryBundle\Entity\Quotation $quotations)
	{
		$this->quotations->removeElement($quotations);
	}
	
	/**
	 * Get quotations
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getQuotations()
	{
		return $this->quotations;
	}

	/**
	 * Add inventoryLog
	 *
	 * @param \CI\InventoryBundle\Entity\InventoryLog $inventoryLog
	 * @return User
	 */
	public function addInventoryLog(\CI\InventoryBundle\Entity\InventoryLog $inventoryLog)
	{
		$this->inventoryLog[] = $inventoryLog;
	
		return $this;
	}
	
	/**
	 * Remove inventoryLog
	 *
	 * @param \CI\InventoryBundle\Entity\InventoryLog $inventoryLog
	 */
	public function removeInventoryLog(\CI\InventoryBundle\Entity\InventoryLog $inventoryLog)
	{
		$this->inventoryLog->removeElement($inventoryLog);
	}
	
	/**
	 * Get inventoryLog
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getInventoryLog()
	{
		return $this->inventoryLog;
	}

	/**
	 * Set isDeleted
	 *
	 * @param boolean $isDeleted
	 * @return User
	 */
	public function setIsDeleted($isDeleted)
	{
		$this->isDeleted = $isDeleted;
	
		return $this;
	}

	/**
	 * Add salesActivities
	 *
	 * @param \CI\InventoryBundle\Entity\SalesActivity $salesActivities
	 * @return User
	 */
	public function addSalesActivitie(\CI\InventoryBundle\Entity\SalesActivity $salesActivities)
	{
		$this->salesActivities[] = $salesActivities;
	
		return $this;
	}

	/**
	 * Remove salesActivities
	 *
	 * @param \CI\InventoryBundle\Entity\SalesActivity $salesActivities
	 */
	public function removeSalesActivitie(\CI\InventoryBundle\Entity\SalesActivity $salesActivities)
	{
		$this->salesActivities->removeElement($salesActivities);
	}

	/**
	 * Get salesActivities
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getSalesActivities()
	{
		return $this->salesActivities;
	}

	/**
	 * Add ownedSalesOrders
	 *
	 * @param \CI\InventoryBundle\Entity\SalesOrder $ownedSalesOrders
	 * @return User
	 */
	public function addOwnedSalesOrder(\CI\InventoryBundle\Entity\SalesOrder $ownedSalesOrders)
	{
		$this->ownedSalesOrders[] = $ownedSalesOrders;
	
		return $this;
	}

	/**
	 * Remove ownedSalesOrders
	 *
	 * @param \CI\InventoryBundle\Entity\SalesOrder $ownedSalesOrders
	 */
	public function removeOwnedSalesOrder(\CI\InventoryBundle\Entity\SalesOrder $ownedSalesOrders)
	{
		$this->ownedSalesOrders->removeElement($ownedSalesOrders);
	}

	/**
	 * Get ownedSalesOrders
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getOwnedSalesOrders()
	{
		return $this->ownedSalesOrders;
	}

	/**
	 * Get isDeleted
	 *
	 * @return boolean 
	 */
	public function getIsDeleted()
	{
		return $this->isDeleted;
	}

	public function setGoogleAuthenticatorSecret($googleAuthenticatorSecret) 
	{
		$this->googleAuthenticatorSecret = $googleAuthenticatorSecret;
	}

	public function getGoogleAuthenticatorSecret() 
	{
		if (!$this->enableTwoFactor) {
			return null;
		}
		
		return $this->googleAuthenticatorSecret;
	}

	/**
	 * Set enableTwoFactor
	 *
	 * @param boolean $enableTwoFactor
	 * @return User
	 */
	public function setEnableTwoFactor($enableTwoFactor)
	{
		$this->enableTwoFactor = $enableTwoFactor;
	
		return $this;
	}

	/**
	 * Get enableTwoFactor
	 *
	 * @return boolean 
	 */
	public function getEnableTwoFactor()
	{
		return $this->enableTwoFactor;
	}

	public function getName()
	{
		return $this->getFirstName() . ' ' . $this->getLastName();
	}

	public function getDisplayRoles()
	{
		$roles = array();

		if ($this->hasRole('ROLE_SUPER_ADMIN')) {
			$roles[] = 'Super Admin';
		}

		if ($this->hasRole('ROLE_ADMIN')) {
			$roles[] = 'Admin';
		}

		foreach ($this->getGroups() as $group) {
			$roles[] = $group->getName();
		}

		return $roles;
	}

	public function isDeletable()
	{
		if ($this->getCustomers()->count() > 0 || $this->getQuotations()->count() > 0 || $this->getOwnedSalesOrders()->count() > 0 || $this->getSalesActivities()->count() > 0) {
			return false;
		}

		return true;
	}
}