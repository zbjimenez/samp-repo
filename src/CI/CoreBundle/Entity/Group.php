<?php

namespace CI\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use FOS\UserBundle\Model\Group as BaseGroup;
use CI\CoreBundle\Permissions;

/**
 * Group
 *
 * @ORM\Table(name="user_group")
 * @ORM\Entity(repositoryClass="CI\CoreBundle\Entity\GroupRepository")
 * @UniqueEntity(fields="name", message="This Role name is already in use.")
 */
class Group extends BaseGroup
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank
	 */
	protected $name;

	/**
	 * @Assert\Count(min="1", minMessage="Please select at least one (1) permission.")
	 */
	protected $roles;

	/**
	 * @ORM\ManyToMany(targetEntity="User", mappedBy="groups")
	 */
	private $users;

	public function __construct($name, $roles = array())
	{
		$this->name = $name;
		$this->roles = $roles;
		$this->users = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add user
	 *
	 * @param \CI\CoreBundle\Entity\User $user
	 *
	 * @return Group
	 */
	public function addUser(\CI\CoreBundle\Entity\User $user)
	{
		$this->users[] = $user;

		return $this;
	}

	/**
	 * Remove user
	 *
	 * @param \CI\CoreBundle\Entity\User $user
	 */
	public function removeUser(\CI\CoreBundle\Entity\User $user)
	{
		$this->users->removeElement($user);
	}

	/**
	 * Get users
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getUsers()
	{
		return $this->users;
	}

	public function getTranslatedPermissions()
	{
		return array_map(function($role) {
			return Permissions::getPermissions()[$role];
		}, $this->getRoles());
	}

	public function isDeletable()
	{
		if ($this->getUsers()->count() == 0) {
			return true;
		}

		return false;
	}
}
