<?php
namespace CI\CoreBundle\Helper;

use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Utility functions for file uploading or manipulation
 */
class FileUtils
{
    public static function getRealClass($obj) 
    {
    	$classname = is_object($obj) ? get_class($obj) : $obj;
    	
        $matches = array();
    
        if (preg_match('@\\\\([\w]+)$@', $classname, $matches)) {
            $classname = $matches[1];
        }
    
        return $classname;
    }
    
    public static function getExtension($fileName)
    {
        $ext = explode('.', $fileName);
        $ext = $ext[count($ext)-1];
    
        return $ext;
    }
    
    public static function getFileFormInfo(ContainerInterface $container, $configNode)
    {
    	$config = $container->getParameter('ci_inventory.multi_upload')[$configNode];
    	 
    	$info = array(
    		"class" => $config['entityClass'],
    		'formName' => $config['forms']['formName'],
    		'propertyName' => $config['forms']['propertyName'],
    		'childClass' => $config['forms']['childClass'],
    		'childFormName' => $config['forms']['childFormName']
    	);
    	
    	return $info;
    }
}