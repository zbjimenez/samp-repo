<?php 

namespace CI\CoreBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\CoreBundle\Form\Type\CustomProfileFormType;
use CI\CoreBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;

class UserModel
{
	const REPOSITORY = 'CICoreBundle:User';
	const ACTION_UPDATE = 'update';
	const ACTION_DEACTIVATE = 'deactivate';
	const ACTION_REACTIVATE = 'reactivate';
	const ACTION_DELETE = 'delete';
	const ACTION_GENERATE_QR = 'generateQrCode';
	const ACTION_DISABLE = 'disable';
	const ACTION_ENABLE = 'enable';

	private $container;
	private $formFactory;
	private $em;
	private $securityContext;

	public function __construct(ContainerInterface $container, FormFactoryInterface $formFactory)
	{
		$this->container = $container;
		$this->formFactory = $formFactory;
		$this->em = $container->get('doctrine')->getManager();
		$this->securityContext = $container->get('security.context');
	}

	public function findExistingEntity($id)
	{
		return $this->em->getRepository(self::REPOSITORY)->find($id);
	}

	public function getType(User $entity = null)
	{
		return $this->formFactory->create(new CustomProfileFormType($this->securityContext), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}

	public function getMessages($action)
	{
		switch ($action) {
			case self::ACTION_UPDATE: 
				return 'User has been updated successfully.';
			case self::ACTION_DEACTIVATE:
				return 'User has been deactivated.';
			case self::ACTION_REACTIVATE:
				return 'User has been reactivated.';
			case self::ACTION_DELETE: 
				return 'User has been deleted.';
			case self::ACTION_GENERATE_QR:
				return 'QR Code has been successfully generated.';
			case self::ACTION_DISABLE:
				return 'Two factor authentication has been disabled.';
			case self::ACTION_ENABLE:
				return 'Two factor authentication has been enabled.';
		}
	}

	public function getIndex()
	{
		return $this->em->getRepository(self::REPOSITORY)->findAll();
	}

	public function saveEntity(User $entity, $defaultRoles = array())
	{
		if (!empty($entity->getPlainPassword())) {
			$userManager = $this->container->get('fos_user.user_manager');
			$userManager->updatePassword($entity);
		}

		$roles = $entity->getRoles();
		$entity->setRoles($defaultRoles + $roles);

		$this->em->persist($entity);
		$this->em->flush();
	}

	public function manageAccess($id, $access)
	{
		$entity = $this->findExistingEntity($id);
		$access == User::ACCESS_DEACTIVATE ? $entity->setEnabled(false) : $entity->setEnabled(true);
		$this->saveEntity($entity);
	}

	public function manageAuth($id, $auth)
	{
		$entity = $this->findExistingEntity($id);
		$auth == User::AUTH_DISABLE ? $entity->setEnableTwoFactor(false) : $entity->setEnableTwoFactor(true);
		$this->saveEntity($entity);
	}

	public function getLoginHistory($id)
	{
		return $this->em->getRepository(self::REPOSITORY)->findLoginHistory($id);
	}
	
	public function setSidebarCollapsed($request)
	{
		$sidebarCollapsed = $request->request->get('sidebarCollapsed') == 'expanded' ? false : true;
		$user = $this->securityContext->getToken()->getUser();
		
		$user->setIsSidebarCollapsed($sidebarCollapsed);
		
		$em = $this->em;
		$em->persist($user);
		$em->flush();
		
		return new JsonResponse($sidebarCollapsed, 200);
	}

	public function isDeletable($id)
	{
		$sc = $this->securityContext;
		$user = $sc->getToken()->getUser();
		$entity = $this->findExistingEntity($id);

		if ($entity->getId() == $user->getId()) {
			throw new \Exception('You cannot delete your own account.');
		}

		if (false === $entity->isDeletable()) {
			throw new \Exception('This user cannot be deleted because it is connected to a transaction.');
		}
	}

	public function deleteEntity($id)
	{
		$em = $this->em;
		$entity = $this->findExistingEntity($id);
		foreach ($entity->getGroups() as $group) {
			$group->removeUser($entity);
			$entity->removeGroup($group);
			$em->persist($group);
		}
		$entity->setRoles(array());
		$entity->setExpired(true);
		$em->flush();
	}

	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'user_delete',
			'return_path' => 'user_show',
			'name' => '[User] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
		);
	}

	public function generateQrCode($id)
	{
		$user = $this->securityContext->getToken()->getUser();
		
		$em = $this->em;
		$entity = $this->findExistingEntity($id);
		
		$secret = $this->container->get('scheb_two_factor.security.google_authenticator')->generateSecret();
		$entity->setGoogleAuthenticatorSecret($secret);
		
		$em->persist($entity);
		$em->flush();
	}

	public function getInternalRoles(User $entity)
	{
		$roles = array();

		if ($entity->hasRole(User::ROLE_SUPER_ADMIN)) {
			$roles[] = User::ROLE_SUPER_ADMIN;
		}
		
		return $roles;
	}
}