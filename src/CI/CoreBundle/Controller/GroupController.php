<?php

namespace CI\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

use CI\CoreBundle\Entity\Group;
use CI\CoreBundle\Form\Type\GroupFilterType;
use CI\CoreBundle\Form\Type\GroupType;
use CI\InventoryBundle\Controller\BaseController;

/**
 * Group controller.
 */
class GroupController extends BaseController
{
	/**
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function listAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$form = $this->createForm(new GroupFilterType());
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $em->getRepository('CICoreBundle:Group')->findAll($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}

		if (!isset($qb)) {
			$qb = $em->getRepository('CICoreBundle:Group')->findAll();
		}

		$paginator = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);
		
		return $this->render('CICoreBundle:Group:index.html.twig', array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		));
	}

	/**
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function createAction(Request $request)
	{
		$entity = new Group('', array());
		$form = $this->createForm(new GroupType, $entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($entity);
				$em->flush();
				
				$this->get('session')->getFlashBag()->add('success', 'Role has been successfully created.');
				
				return $this->redirect($this->generateUrl('fos_user_group_show', array('id' => $entity->getId())));
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return $this->render('CICoreBundle:Group:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),
			'angular' => true,
		));
	}
	
	/**
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function newAction()
	{
		$entity = new Group('', array());
		$form = $this->createForm(new GroupType, $entity);
		
		return $this->render('CICoreBundle:Group:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),
			'angular' => true,
		));
	}

	/**
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function showAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('CICoreBundle:Group')->find($id);
		
		return $this->render('CICoreBundle:Group:show.html.twig', array(
			'entity' => $entity,
		));
	}

	/**
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function editAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('CICoreBundle:Group')->find($id);
		$editForm = $this->createForm(new GroupType, $entity, array('method' => 'PUT'));
		
		return $this->render('CICoreBundle:Group:edit.html.twig', array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
			'angular' => true,
		));
	}

	/**
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('CICoreBundle:Group')->find($id);
		$editForm = $this->createForm(new GroupType, $entity, array('method' => 'PUT'));
		
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				$em->persist($entity);
				$em->flush();
				
				$this->get('session')->getFlashBag()->add('success', 'Role has successfully been updated.');
				
				return $this->redirect($this->generateUrl('fos_user_group_show', array('id' => $id)));
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return $this->render('CICoreBundle:Group:edit.html.twig', array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
			'angular' => true,
		));
	}
	
	/**
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function confirmDeleteAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('CICoreBundle:Group')->find($id);
		$deleteForm = $this->createDeleteForm($id);

		if ($entity->isDeletable() === false) {
			$this->get('session')->getFlashBag()->add('danger', 'This role has already been added to a user, therefore can no longer be deleted.');
			return $this->redirect($this->generateUrl('fos_user_group_show', array('id' => $id)));
		}

		$params = array(
			'path' => 'fos_user_group_delete',
			'return_path' => 'fos_user_group_show',
			'name' => '[Role] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')',
		);

		return $this->render('CIInventoryBundle:Misc:delete.html.twig', array(
			'params' => $params,
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
		));
	}
	
	/**
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function deleteAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('CICoreBundle:Group')->find($id);
		$deleteForm = $this->createDeleteForm($id);
		$deleteForm->handleRequest($request);
	
		if ($entity->isDeletable() === false) {
			$this->get('session')->getFlashBag()->add('danger', 'This role has already been added to a user, therefore can no longer be deleted.');
			return $this->redirect($this->generateUrl('fos_user_group_show', array('id' => $id)));
		}
	
		if ($deleteForm->isValid()) {
			try {
				$em->remove($entity);
				$em->flush();
	
				$this->get('session')->getFlashBag()->add('success', 'Role has been deleted.');
				return $this->redirect($this->generateUrl('fos_user_group_list'));
			} catch (\Exception $e) {
				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			}
		} else {
			$this->get('session')->getFlashBag()->add('danger', 'Oops! Unknown error occured. Please try again.');
		}
	
		$params = array(
			'path' => 'fos_user_group_delete',
			'return_path' => 'fos_user_group_show',
			'name' => '[Role] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')',
		);
	
		return $this->render('CIInventoryBundle:Misc:delete.html.twig', array(
			'params' => $params,
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
		));
	}
}
