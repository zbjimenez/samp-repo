<?php

namespace CI\CoreBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use CI\CoreBundle\Entity\User;
use CI\CoreBundle\Model\UserModel as Model;

class ProfileController extends BaseController
{
	/**
	 * Show the user
	 */
	public function showAction()
	{
		$user = $this->getUser();
		if (!is_object($user) || !$user instanceof UserInterface) {
			throw new AccessDeniedException('This user does not have access to this section.');
		}
		
		$url = $this->container->get("scheb_two_factor.security.google_authenticator")->getUrl($user);
	
		return $this->render('FOSUserBundle:Profile:show.html.twig', array(
			'user' => $user,
			'url' => $url,
		));
	}

	/**
	 * Edit the user
	 */
	public function editAction(Request $request)
	{
		$user = $this->getUser();
		if (!is_object($user) || !$user instanceof UserInterface) {
			throw new AccessDeniedException('This user does not have access to this section.');
		}

		/** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
		$dispatcher = $this->get('event_dispatcher');

		$event = new GetResponseUserEvent($user, $request);
		$dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

		if (null !== $event->getResponse()) {
			return $event->getResponse();
		}

		/** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
		$formFactory = $this->get('fos_user.profile.form.factory');

		$form = $formFactory->createForm();
		$form->setData($user);
		$isSuperAdmin = $user->hasRole(User::ROLE_SUPER_ADMIN);
		$currentRoles = $user->getRoles();

		$form->handleRequest($request);

		if ($form->isValid()) {
			/** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
			$userManager = $this->get('fos_user.user_manager');

			$event = new FormEvent($form, $request);
			$dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);
			$userManager->updateUser($user, false);

			if ($isSuperAdmin) {
				$currentRoles[] = 'ROLE_SUPER_ADMIN';
				$user->setRoles($currentRoles);
			}

			$this->getDoctrine()->getManager()->flush();

			if (null === $response = $event->getResponse()) {
				$url = $this->generateUrl('fos_user_profile_show');
				$response = new RedirectResponse($url);
			}

			$dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

			return $response;
		}

		return $this->render('FOSUserBundle:Profile:edit.html.twig', array(
			'form' => $form->createView()
		));
	}

	/**
	 * Generates a QR code for the current user.
	 * 
	 * @Route("/generate-qr-code", name="profile_generate_qr_code")
	 */
	public function generateQrCodeAction()
	{
		$id = $this->getUser()->getId();
		$model = $this->get('ci.user.model');
		
		try {
			$model->generateQrCode($id);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_GENERATE_QR));
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}
		
		return $this->redirect($this->generateUrl('fos_user_profile_show'));
	}
}