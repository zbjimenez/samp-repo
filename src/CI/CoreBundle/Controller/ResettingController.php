<?php

namespace CI\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Controller\ResettingController as FOSResettingController;

class ResettingController extends FOSResettingController
{
	/**
	 * Request reset user password: submit form and send email
	 */
	public function sendEmailAction(Request $request)
	{
		$username = $request->request->get('username');
		
		/** @var $user UserInterface */
		$user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);
		
		if (null === $user) {
			return $this->render('FOSUserBundle:Resetting:request.html.twig', array(
				'error' => true,
				'invalid_username' => $username
			));
		}
		
		if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
			return $this->render('FOSUserBundle:Resetting:request.html.twig', array(
				'error' => true,
				'already_requested' => true
			));
		}
		
		if (null === $user->getConfirmationToken()) {
			/** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
			$tokenGenerator = $this->get('fos_user.util.token_generator');
			$user->setConfirmationToken($tokenGenerator->generateToken());
		}
		
		$this->get('fos_user.mailer')->sendResettingEmailMessage($user);
		$user->setPasswordRequestedAt(new \DateTime());
		$this->get('fos_user.user_manager')->updateUser($user);
		
		return new RedirectResponse($this->generateUrl('fos_user_resetting_check_email',
			array('email' => $this->getObfuscatedEmail($user))
		));
	}
	
	/**
	 * Tell the user to check his email provider
	 */
	public function checkEmailAction(Request $request)
	{
		$email = $request->query->get('email');
		
		if (empty($email)) {
			// the user does not come from the sendEmail action
			return new RedirectResponse($this->generateUrl('fos_user_resetting_request'));
		}
		
		return $this->render('FOSUserBundle:Resetting:request.html.twig', array(
			'check_email' => true,
			'email' => $email
		));
	}
	
	/**
	 * Return the first character and the part following the @ in the email address
	 *
	 * @param \FOS\UserBundle\Model\UserInterface $user
	 *
	 * @return string
	 */
	protected function getObfuscatedEmail(UserInterface $user)
	{
		$email = $user->getEmail();
		if (false !== $pos = strpos($email, '@')) {
			$email = substr($email, 0, 1) . '...' . substr($email, $pos);
		}
	
		return $email;
	}
}