<?php

namespace CI\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use CI\CoreBundle\Model\UserModel as Model;
use CI\InventoryBundle\Controller\BaseController;

/**
 * User controller.
 *
 * @Route("/user")
 */
class UserController extends BaseController
{
	public function getModel()
	{
		return $this->get('ci.user.model');
	}
	
	/**
	 * Lists all User entities.
	 *
	 * @Route("/", name="user")
	 * @Template()
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function indexAction()
	{
		$model = $this->getModel();
		$qb = $model->getIndex();
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
				$qb,
				$this->get('request')->query->get('page', 1),
				$this->container->getParameter('pagination_limit_per_page'),
				array('distinct' => true)
		);
		
		return array(
				'pagination' => isset($pagination) ? $pagination : null,
		);
	}
	
	/**
     * Finds and displays a User entity.
     *
     * @Route("/{id}/show", name="user_show")
     * @Template()
     * @PreAuthorize("hasRole('ROLE_ADMIN')")
     */
    public function showAction($id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	
    	$url = $this->container->get("scheb_two_factor.security.google_authenticator")->getUrl($entity);

    	return array(
    			'user' => $entity,
    			'url' => $url
    	);
    }
	
	/**
	 * Displays a form to edit an existing User entity.
	 *
	 * @Route("/{id}/edit", name="user_edit")
	 * @Template()
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function editAction(Request $request, $id)
	{		
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getType($entity);
		$roles = $model->getInternalRoles($entity);

		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($entity, $roles);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
				'entity'    => $entity,
				'edit_form' => $editForm->createView(),
		);
	}

	/**
	 * Deletes a User entity.
	 *
	 * @Route("/{id}/confirm-delete", name="user_confirm_delete")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function confirmDeleteAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
		}
			
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}

	/**
	 * @Route("/{id}", name="user_delete")
	 * @Method("DELETE")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$form = $this->createDeleteForm($id);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->isDeletable($id);
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again later.');
			}
		}
		
		return $this->redirect($this->generateUrl('user'));
	}
	
	/**
	 * Deactivates an existing user entity.
	 *
	 * @Route("/{id}/access/{access}", name="user_manage_access")
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function manageAccessAction($id, $access)
	{
		$model = $this->getModel();
		$model->manageAccess($id, $access);
		
		$this->get('session')->getFlashBag()->add('success', $model->getMessages($access));
		return $this->redirect($this->generateUrl('user'));
	}
	
	/**
	 * @Route("/{id}/login-history", name="user_login_history")
	 * @Template("CICoreBundle:User:loginHistory.html.twig")
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function getLoginHistoryAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$log = $model->getLoginHistory($id);
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$log,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);
		
		return array(
			'user' => $entity,
			'pagination' => $pagination
		);
	}
	
	/**
	 * @Route("/sidebar-collapsed", name="user_sidebar_collapsed")
	 * @Method("POST")
	 * @PreAuthorize("isFullyAuthenticated()")
	 */
	public function setSidebarCollapsedAction(Request $request)
	{
		$model = $this->getModel();
		return $model->setSidebarCollapsed($request);
	}

	/**
	 * Generates a QR code for the user.
	 * 
	 * @Route("/{id}/generate-qr-code", name="user_generate_qr_code", requirements={"id"="\d+"})
	 */
	public function generateQrCodeAction($id)
	{
		$model = $this->getModel();
		
		try {
			$model->generateQrCode($id);
			$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_GENERATE_QR));
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
		}
		
		return $this->redirect($this->generateUrl('user_show', array('id' => $id)));
	}

	/**
	 * Disable an authentication for user entity.
	 *
	 * @Route("/{id}/auth/{auth}", name="user_manage_auth")
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function manageAuthAction($id, $auth)
	{
		$model = $this->getModel();
		$model->manageAuth($id, $auth);
		
		$this->get('session')->getFlashBag()->add('success', $model->getMessages($auth));
		return $this->redirect($this->generateUrl('user'));
	}
}