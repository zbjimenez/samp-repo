<?php
namespace CI\CoreBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;

use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use CI\CoreBundle\Entity\User;

use Symfony\Component\Security\Core\SecurityContext;

class AddRoleFieldSubscriber implements EventSubscriberInterface
{
	private $factory;
	private $securityContext;

	public function __construct(FormFactoryInterface $factory, SecurityContext $securityContext)
	{
		$this->factory = $factory;
		$this->securityContext = $securityContext;
	}

	public static function getSubscribedEvents()
	{
		// Tells the dispatcher that we want to listen on the form.pre_set_data
		// event and that the preSetData method should be called.
		return array(FormEvents::PRE_SET_DATA => 'preSetData');
	}

	public function preSetData(FormEvent $event)
	{
		$data = $event->getData();
		$form = $event->getForm();

		if (null === $data) {
			return;
		}
		
		if (true === $this->securityContext->isGranted('ROLE_ADMIN')) {
			$form->add($this->factory->createNamed('roles', 'choice', null, 
				array(
				'auto_initialize' => false,
				'multiple' => true,
				'expanded' => true,
				'choices' => array(
					User::ROLE_ADMIN => 'Admin',
				)
			)));
		}
	}
}