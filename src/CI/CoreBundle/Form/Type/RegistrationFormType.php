<?php

namespace CI\CoreBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Doctrine\ORM\EntityRepository;
use CI\CoreBundle\Entity\User;

class RegistrationFormType extends BaseType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);
		
		$builder
			->add('firstName')
			->add('lastName')
			->add('username')
			->add('email', 'email')
			->add('plainPassword', 'repeated', array(
				'required' => true,
				'first_options'  => array('label' => 'Password'),
				'second_options' => array('label' => 'Repeat Password'),
				'type' => 'password'
			))
			->add('gender', 'choice', array(
				'expanded' => true,
				'multiple' => false,
				'required' => true,
				'attr' => array('inline' => true),
				'choices' => array(
					'M' => 'Male',
					'F' => 'Female'
				)
			))
			->add('birthDate', 'date', array(
				'label'    => 'Birthday',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'required' => false,
				'attr'	   => array(
					'datepicker' => true,
					'input_group' => array('append' => 'calendar'),
				)
			))
			->add('roles', 'choice', array(
				'required' => true,
				'multiple' => true,
				'expanded' => true,
				'choices' => array(
					User::ROLE_ADMIN => 'Admin',
				)
			))
			->add('groups', 'entity', array(
				'class' => 'CICoreBundle:Group',
				'label' => 'Groups',
				'required' => true,
				'property' => 'name',
				'attr' => array(
					'class' => 'group select2',
				),
				'query_builder' => function($er) {
					return $er->findAllQb();
				},
				'multiple' => true,
			))
			->add('contactNumber', 'text', array(
				'required' => false,
			))
		;
	}
	
	public function getName()
	{
		return 'ci_core_user_registration';
	}
}