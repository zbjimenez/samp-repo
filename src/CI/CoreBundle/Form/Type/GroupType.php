<?php

namespace CI\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use CI\CoreBundle\Permissions;

class GroupType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name')
			->add('roles', 'choice', array(
				'label' => 'Permissions',
				'multiple' => true,
				'expanded' => true,
				'choices' => Permissions::getPermissions(),
			))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\CoreBundle\Entity\Group'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_corebundle_group';
	}
}
