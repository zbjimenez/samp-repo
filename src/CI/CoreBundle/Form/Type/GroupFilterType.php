<?php

namespace CI\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class GroupFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->setMethod('GET')
		->add('name', 'text', array(
			'label' => 'Name',  
			'required' => false,
			'attr' => array(
				'placeholder' => 'Search by name',
			),
		));
	}
	
	public function getName()
	{
		return 'ci_corebundle_groupfilter';
	}
}
