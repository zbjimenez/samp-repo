<?php

namespace CI\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;
use CI\CoreBundle\Form\EventListener\AddRoleFieldSubscriber;
use Symfony\Component\Security\Core\SecurityContext;

class ProfileFormType extends BaseType
{
	private $class;
	private $securityContext;
	
	/**
	 * @param string $class The User class name
	 */
	public function __construct($class, SecurityContext $context)
	{
		$this->class = $class;
		$this->securityContext = $context;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$constraint = new UserPassword();
		
		$this->buildUserForm($builder, $options);
		
		$builder->add('current_password', 'password', array(
			'label' => 'form.current_password',
			'translation_domain' => 'FOSUserBundle',
			'mapped' => false,
			'constraints' => $constraint
		));
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => $this->class,
			'intention'  => 'profile'
		));
	}
	
	public function getName()
	{
		return 'ci_core_user_profile';
	}
	
	/**
	 * Builds the embedded form representing the user.
	 *
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	protected function buildUserForm(FormBuilderInterface $builder, array $options)
	{
		$subscriber = new AddRoleFieldSubscriber($builder->getFormFactory(), $this->securityContext);
		$builder->addEventSubscriber($subscriber);
		$builder
			->add('firstName')
			->add('lastName')
			->add('username')
			->add('email', 'email')
			->add('gender', 'choice', array(
				'expanded' => true,
				'choices' => array(
					'M' => 'Male',
					'F' => 'Female'
				)
			))
			->add('birthDate', 'date', array(
				'label'    => 'Birthdate',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'required' => false,
				'attr'	   => array(
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
			))
			->add('contactNumber', 'text')
			->add('enableTwoFactor', 'checkbox', array(
				'label' => 'Enable Two Factor Authentication',
				'required' => false,
				'attr' => array(
					'align_with_widget' => true,
				)
			))
		;
	}
}