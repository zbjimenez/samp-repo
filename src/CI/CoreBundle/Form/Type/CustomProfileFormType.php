<?php

namespace CI\CoreBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\SecurityContext;
use CI\CoreBundle\Form\EventListener\AddRoleFieldSubscriber;

class CustomProfileFormType extends AbstractType
{
	protected $securityContext;
	
	public function __construct(SecurityContext $securityContext)
	{
		$this->securityContext = $securityContext;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$subscriber = new AddRoleFieldSubscriber($builder->getFormFactory(), $this->securityContext);
		$builder->addEventSubscriber($subscriber);
		$builder
			->add('firstName')
			->add('lastName')
			->add('username')
			->add('email', 'email')
			->add('gender', 'choice', array(
				'expanded' => true,
				'choices' => array(
					'M' => 'Male', 
					'F' => 'Female'
				)
			))
			->add('birthDate', 'date', array(
				'label'    => 'Birthday',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'required' => false,
				'attr'	   => array(
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
			))
			->add('contactNumber', 'text')
			->add('groups', 'entity', array(
				'class' => 'CICoreBundle:Group',
				'label' => 'Groups',
				'required' => true,
				'property' => 'name',
				'attr' => array(
					'class' => 'group select2',
				),
				'query_builder' => function($er) {
					return $er->findAllQb();
				},
				'multiple' => true,
			))
			->add('enableTwoFactor', 'checkbox', array(
				'label' => 'Enable Two Factor Authentication',
				'required' => false,
				'attr' => array(
					'align_with_widget' => true,
				)
			))
			;
			
		if ($this->securityContext->isGranted('ROLE_ADMIN')) {
			$builder->add('plainPassword', 'password', array(
				'required' => false,
			));
		}
	}
	
	public function getDefaultOptions(array $options)
	{
		return array(
			'data_class' => 'CI\CoreBundle\Entity\User'
		);
	}
	
	public function getName()
	{
		return 'ci_core_custom_user_edit_profileform';
	}
}