<?php
namespace CI\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use CI\CoreBundle\Entity\User;
use CI\CoreBundle\Entity\Group;

class LoadUserData implements FixtureInterface, OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		// username, email, password, enable?, role, firstname, lastname, contactnumber, gender, birthday
		
		$users = array(
			array('admin', 'admin@creatinginfo.com', 'admin', true, array('ROLE_SUPER_ADMIN'),'admin', 'admin', '0917-999-9999', 'M', new \DateTime('now')),
			array('agent1', 'agent1@gmail.com', 'agent1', true, array(), 'Agent1', 'Agent', '0917-999-9999', 'F', new \DateTime('now')),
			array('manager1', 'manager1@gmail.com', 'manager1', true, array(), 'Manager1', 'Manager', '0917-999-9999', 'F', new \DateTime('now')),
		);
		
		$salesAgent = $manager->getRepository('CICoreBundle:Group')->findOneByName('Sales Agent');
		$salesManager = $manager->getRepository('CICoreBundle:Group')->findOneByName('Sales Manager');

		for ($row = 0; $row < 3; $row++) {
			$user = new User();
			$user->setUsername($users[$row][0]);
			$user->setEmail($users[$row][1]);
			$user->setPlainPassword($users[$row][2]);
			$user->setEnabled($users[$row][3]);
			$user->setRoles($users[$row][4]);
			$user->setFirstName($users[$row][5]);
			$user->setLastName($users[$row][6]);
			$user->setContactNumber($users[$row][7]);
			$user->setGender($users[$row][8]);
			$user->setBirthDate($users[$row][9]);
			$user->setCreatedAt(new \DateTime('now'));
			$user->setCreatedBy("admin");

			if ($row === 1) {
				$user->addGroup($salesAgent);
			}
			if ($row === 2) {
				$user->addGroup($salesManager);
			}

			$manager->persist($user);
			$manager->flush();
		}		
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 3;
	}
}
